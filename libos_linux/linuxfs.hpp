#pragma once
#include <libos/os.hpp>

namespace linuxfs
{

void registerLinuxFS(os::OS* os);
void unregisterLinuxFS();

}
