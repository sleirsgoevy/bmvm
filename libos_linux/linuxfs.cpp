#include <libos/fncall.hpp>
#include <libos/hub.hpp>
#include <libboot/memrw.hpp>
#include <cstring>
#include <mutex>
extern "C"
{
    #include <fs.h>
}
#include "linuxfs.hpp"

namespace linuxfs
{

constexpr size_t BUFSIZE = 1024;

struct linux_FILE : public FILE
{
    uint64_t epoch;
    uintptr_t handle;
    std::mutex buffer_lock;
    off_t cached_off;
    size_t cached_sz;
    char buf[BUFSIZE];
};

static os::OS* the_os;
static uintptr_t kmalloc;
static uintptr_t kfree;
static uintptr_t filp_open;
static uintptr_t filp_close;
static uintptr_t kernel_read;
static uint64_t epoch;

constexpr uintptr_t error_start = 0xfffffffffffff000ull;

extern struct fs_driver linux_driver;

static FILE* linux_open(const char* path, int mode)
{
    if(strncmp(path, "linux:", 6))
        return nullptr;
    path += 6;
    os::KernelMode km(*the_os);
    size_t l = strlen(path) + 1;
    uintptr_t buf = fncall::call<fncall::CC_SYSV>(kmalloc, l, 0xd0);
    if(buf >= error_start)
        return nullptr;
    if(!utils::copy_to_guest_vm(buf, path, l))
    {
        fncall::call<fncall::CC_SYSV>(kfree, buf);
        return nullptr;
    }
    uintptr_t handle = fncall::call<fncall::CC_SYSV>(filp_open, buf, 0, 0);
    fncall::call<fncall::CC_SYSV>(kfree, buf);
    if(handle >= error_start)
        return nullptr;
    linux_FILE* ans = new linux_FILE();
    ans->drv = &linux_driver;
    ans->epoch = epoch;
    ans->handle = handle;
    ans->cached_off = ans->cached_sz = 0;
    return (FILE*)ans;
}

static ssize_t linux_pread(FILE* f0, void* p, size_t sz, off_t off)
{
    linux_FILE* f = (linux_FILE*)f0;
    if(f->epoch != epoch)
        return -9;
    size_t frombuf = 0;
    f->buffer_lock.lock();
    if(f->cached_off <= off && f->cached_off + f->cached_sz >= off)
    {
        size_t chunk_sz = f->cached_sz - (off - f->cached_off);
        if(sz < chunk_sz)
            chunk_sz = sz;
        memcpy(p, f->buf + (off - f->cached_off), chunk_sz);
        p = (char*)p + chunk_sz;
        sz -= chunk_sz;
        off += chunk_sz;
        frombuf = chunk_sz;
    }
    f->buffer_lock.unlock();
    if(!sz)
        return frombuf;
    size_t sz0 = sz;
    sz += BUFSIZE;
    os::KernelMode km(*the_os);
    uintptr_t buf = fncall::call<fncall::CC_SYSV>(kmalloc, sizeof(off) + sz, 0xd0);
    if(buf >= error_start)
        return frombuf ? frombuf : (ssize_t)buf;
    utils::copy_to_guest_vm(buf, &off, sizeof(off));
    ssize_t out_sz = fncall::call<fncall::CC_SYSV>(kernel_read, f->handle, buf + sizeof(off), sz, buf);
    if(out_sz >= sz0)
    {
        utils::copy_from_guest_vm(p, buf + sizeof(off), sz0);
        f->buffer_lock.lock();
        f->cached_off = off + sz0;
        f->cached_sz = out_sz - sz0;
        utils::copy_from_guest_vm(f->buf, buf + sizeof(off) + sz0, out_sz - sz0);
        f->buffer_lock.unlock();
        out_sz = sz0;
    }
    else if(out_sz > 0)
        utils::copy_from_guest_vm(p, buf + sizeof(off), out_sz);
    fncall::call<fncall::CC_SYSV>(kfree, buf);
    if(out_sz < 0 && frombuf)
        return frombuf;
    return frombuf + out_sz;
}

static void linux_close(FILE* f0)
{
    linux_FILE* f = (linux_FILE*)f0;
    if(f->epoch == epoch)
    {
        os::KernelMode km(*the_os);
        fncall::call<fncall::CC_SYSV>(filp_close, f->handle, 0);
    }
    delete f;
}

struct fs_driver linux_driver = {
    .name = "linux",
    .open = linux_open,
    .pread = linux_pread,
    .close = linux_close,
};

struct RegisterOnFirstUse
{
    RegisterOnFirstUse()
    {
        register_fs_driver(&linux_driver);
    }
};

void registerLinuxFS(os::OS* os)
{
    static RegisterOnFirstUse rofi;
    epoch++;
    the_os = os;
    kmalloc = the_os->kdlsym("__kmalloc");
    kfree = the_os->kdlsym("kfree");
    filp_open = the_os->kdlsym("filp_open");
    filp_close = the_os->kdlsym("filp_close");
    kernel_read = the_os->kdlsym("kernel_read");
}

void unregisterLinuxFS()
{
    epoch++;
    the_os = nullptr;
}

}
