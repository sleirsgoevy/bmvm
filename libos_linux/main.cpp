#include <pair>
#include <vector>
#include <cstring>
#include <libboot/memrw.hpp>
#include <libboot/vm.hpp>
#include <libvm/vm.hpp>
#include <libos/hub.hpp>
#include <libos/fncall.hpp>
#include <sysregs.h>
#include "linuxfs.hpp"

static uintptr_t step_back(uintptr_t back, uint64_t cr3)
{
    uintptr_t ans = back;
    for(int shift = 12; shift <= 30; shift += 9)
    {
        uintptr_t q = back - (1ull << shift);
        uint64_t phys;
        uint64_t limit;
        bool u, w, nx;
        if(!utils::guest_vm_translate(q, phys, limit, u, w, nx, cr3))
            break;
        if(limit - phys + 1 < (1ull << shift))
            break;
        ans = q;
    }
    return ans;
}

static uintptr_t step_forward(uintptr_t top, uint64_t cr3)
{
    uint64_t phys;
    uint64_t limit;
    bool u, w, nx;
    if(!utils::guest_vm_translate(top, phys, limit, u, w, nx, cr3))
        return top;
    return top + limit - phys + 1;
}

static std::pair<uintptr_t, uintptr_t> get_segment_of(uintptr_t q, uint64_t cr3)
{
    uintptr_t low = q;
    for(uintptr_t i; (i = step_back(low, cr3)) < low; low = i);
    uintptr_t high = q;
    for(uintptr_t i; (i = step_forward(high, cr3)) > high; high = i);
    return std::make_pair(low, high);
}

static std::vector<std::pair<uintptr_t, uintptr_t> > get_linux_kernel_segments(uint64_t& cr3)
{
    vm::VM* vcpu = boot::curVCPU();
    uintptr_t syscall_entry = vcpu->rdmsr(IA32_LSTAR);
    syscall_entry &= ~4095ull;
    cr3 = vcpu->get_reg(vm::Register::CR3);
    auto text_segment = get_segment_of(syscall_entry, cr3);
    std::vector<std::pair<uintptr_t, uintptr_t> > ans;
    if(text_segment.second - text_segment.first < (1ull << 22)) //meltdown mitigation
    {
        cr3 -= 4096;
        text_segment = get_segment_of(syscall_entry, cr3);
        if(text_segment.second - text_segment.first < (1ull << 22)) //wtf??
            return ans;
    }
    ans.push_back(text_segment);
    while(true)
    {
        uintptr_t start_of_next = (ans.back().second + 0x1fffffull) & ~0x1fffffull;
        auto next_segment = get_segment_of(start_of_next, cr3);
        if(next_segment.second == next_segment.first || next_segment.first != start_of_next)
            break;
        ans.push_back(next_segment);
    }
    return ans;
}

static std::vector<uintptr_t> find_shorts(uint64_t& cr3, uintptr_t& kernel_base)
{
    kernel_base = -1;
    auto segments = get_linux_kernel_segments(cr3);
    if(segments.empty())
        return std::vector<uintptr_t>();
    kernel_base = segments[0].first;
    uint16_t page[2048];
    size_t cnt = 0;
    uint32_t prev = 65536;
    std::vector<uintptr_t> shorts;
    for(const auto& i : segments)
        for(uintptr_t addr = i.first; addr < i.second; addr += 4096)
        {
            if(!utils::copy_from_guest_vm(page, addr, 4096, cr3))
                continue;
            for(size_t i = 0; i < 2048; i++)
            {
                uint32_t q = page[i];
                if(q - prev == 2)
                    cnt++;
                else
                {
                    if(cnt == 26)
                        shorts.push_back(addr + 2*i);
                    cnt = 0;
                }
                prev = q;
            }
        }
    return shorts;
}

static bool find_token_table(uint64_t& cr3, uintptr_t& token_table, std::vector<char>& token_table_s, uint16_t token_indices[256], uintptr_t& kernel_base)
{
    std::vector<uintptr_t> shorts = find_shorts(cr3, kernel_base);
    if(shorts.size() != 2 || shorts[1] - shorts[0] != 64)
        return false;
    uintptr_t token_index_addr = shorts[0] - 0xb8;
    token_table = token_index_addr;
    char c;
    while(utils::copy_from_guest_vm(&c, token_table - 1, 1, cr3) && !c)
        token_table--;
    uintptr_t token_table_end = token_table + 1;
    while(utils::copy_from_guest_vm(&c, token_table - 1, 1, cr3) && c)
        token_table--;
    uint16_t last_idx;
    if(!utils::copy_from_guest_vm(&last_idx, token_index_addr + 0x1fe, 2, cr3))
        return false;
    token_table -= last_idx;
    if(token_table % 8)
        return false;
    token_table_s.resize(token_table_end - token_table);
    if(!utils::copy_from_guest_vm(&token_table_s[0], token_table, token_table_s.size(), cr3))
        return false;
    return utils::copy_from_guest_vm(&token_indices[0], token_index_addr, 512, cr3);
}

static uintptr_t find_kallsyms_header(uintptr_t addr, uintptr_t kernel_base, uint64_t cr3)
{
    uint64_t buf[512] = {0};
    uintptr_t page = addr - addr % 4096;
    if(!utils::copy_from_guest_vm(buf, page, addr % 4096, cr3))
        return -1;
    for(size_t i = (addr % 4096) / 8 + 1; i > 0; i--)
        if(buf[i-1] == kernel_base)
            return page + i * 8 - 8;
    auto segment = get_segment_of(page, cr3);
    while(page > segment.first)
    {
        page -= 4096;
        if(!utils::copy_from_guest_vm(buf, page, 4096, cr3))
            return -1;
        for(size_t i = 512; i > 0; i--)
            if(buf[i-1] == kernel_base)
                return page + i * 8 - 8;
    }
    return -1;
}

struct LinuxKallsyms
{
    uintptr_t kernel_base;
    std::vector<int32_t> offsets;
    std::vector<char> names;
    std::vector<char> token_table;
    uint16_t token_indices[256];
    int strcmp_packed(size_t& idx, const char* name, const char* name_end, size_t skip = 0)
    {
        if(idx == names.size())
            return 1;
        size_t len = (unsigned char)names[idx++];
        if(idx + len > names.size())
            return 1;
        for(size_t i = 0; i < len; i++)
        {
            const char* name1 = &token_table[token_indices[(unsigned char)names[idx+i]]];
            size_t sl = strlen(name1);
            size_t skip_sz = std::min(skip, sl);
            name1 += skip_sz;
            skip -= skip_sz;
            sl -= skip_sz;
            if(name_end - name < sl || memcmp(name1, name, sl))
            {
                idx += len;
                return 1;
            }
            name += sl;
        }
        idx += len;
        return name != name_end || skip;
    }
    bool find_symbol(const char* name, uintptr_t& ans, size_t skip = 0)
    {
        const char* name_end = name + strlen(name);
        size_t idx = 0;
        for(size_t i = 0; i < offsets.size(); i++)
            if(!strcmp_packed(idx, name, name_end, skip))
            {
                int32_t o = offsets[i];
                if(o < 0)
                    ans = kernel_base + ~(uint32_t)o;
                else
                    ans = o;
                return true;
            }
        return false;
    }
};

static bool find_kallsyms(LinuxKallsyms& ans)
{
    uint64_t cr3;
    uintptr_t token_table_addr;
    if(!find_token_table(cr3, token_table_addr, ans.token_table, ans.token_indices, ans.kernel_base))
        return false;
    uintptr_t kallsyms_header = find_kallsyms_header(token_table_addr, ans.kernel_base, cr3);
    uintptr_t kallsyms_names = kallsyms_header + 16;
    ans.names.resize(token_table_addr - kallsyms_names);
    if(!utils::copy_from_guest_vm(&ans.names[0], kallsyms_names, token_table_addr - kallsyms_names, cr3))
        return false;
    uint32_t num_syms;
    if(!utils::copy_from_guest_vm(&num_syms, kallsyms_header + 8, sizeof(num_syms), cr3))
        return false;
    uintptr_t kallsyms_offsets = kallsyms_header - (num_syms + num_syms % 2) * sizeof(int32_t);
    ans.offsets.resize(num_syms);
    return utils::copy_from_guest_vm(&ans.offsets[0], kallsyms_offsets, num_syms * sizeof(int32_t), cr3);
}

class LinuxOS : public os::OS
{
    LinuxKallsyms ks;
    uintptr_t find_symbol_addr = 0;
    uintptr_t kmalloc_addr = 0;
    uintptr_t kfree_addr = 0;
    uintptr_t task_seq_get_next_addr = 0;
    uintptr_t wait_for_owner_exiting_addr = 0;
    uintptr_t init_pid_ns_addr = 0;
    uintptr_t find_task_by_pid_ns_addr = 0;
    uintptr_t kallsyms_dlsym(const char* name);
    uintptr_t module_dlsym(const char* name, bool gplok);
    class LinuxProcess : public os::OS::ProcessImpl
    {
        LinuxOS* os;
        uintptr_t task_struct;
    public:
        LinuxProcess(LinuxOS* os, uintptr_t task_struct) : os(os), task_struct(task_struct){}
        std::string name();
        uint64_t cr3();
        std::vector<Library> libraries();
        ~LinuxProcess();
    };
    friend class LinuxProcess;
public:
    LinuxOS(const LinuxKallsyms& ks) : ks(ks)
    {
        linuxfs::registerLinuxFS(this);
    }
    ~LinuxOS()
    {
        linuxfs::unregisterLinuxFS();
    }
    uintptr_t kdlsym(const char* name);
    void enter_kernel_mode(os::KernelMode& km);
    void exit_kernel_mode(const os::KernelMode& km);
    std::vector<uintmax_t> pids();
    os::OS::Process find_by_pid(uintmax_t pid);
};

uintptr_t LinuxOS::kallsyms_dlsym(const char* name)
{
    uintptr_t ans;
    if(ks.find_symbol(name, ans, 1))
        return ans;
    return 0;
}

constexpr uintptr_t error_start = 0xfffffffffffff000ull;

uintptr_t LinuxOS::module_dlsym(const char* name, bool gplok)
{
    if(!find_symbol_addr)
        find_symbol_addr = kdlsym("find_symbol");
    if(!kmalloc_addr)
        kmalloc_addr = kdlsym("__kmalloc");
    if(!kfree_addr)
        kfree_addr = kdlsym("kfree");
    if(!find_symbol_addr || !kmalloc_addr || !kfree_addr)
        return 0;
    os::OS::KernelMode km(*this);
    size_t l = strlen(name);
    uintptr_t buf = fncall::call<fncall::CC_SYSV>(kmalloc_addr, l+9, 0xd0);
    if(buf >= error_start)
        return 0;
    if(!utils::copy_to_guest_vm(buf+8, name, l+1))
    {
        fncall::call<fncall::CC_SYSV>(kfree_addr, buf);
        return 0;
    }
    //the function checks for null in args 2-4, but its static, thus a compiler can optimize the checks out, so provide a writable pointer anyway
    uintptr_t ksym = fncall::call<fncall::CC_SYSV>(find_symbol_addr, buf+8, buf, buf, buf, gplok ? 1 : 0, 0);
    fncall::call<fncall::CC_SYSV>(kfree_addr, buf);
    if(!ksym)
        return 0;
    int32_t shift;
    if(!utils::copy_from_guest_vm(&shift, ksym, sizeof(shift)))
        return 0;
    return ksym + shift;
}

uintptr_t LinuxOS::kdlsym(const char* name)
{
    if(!strncmp(name, "exports::", 9))
        return module_dlsym(name+9, false);
    else if(!strncmp(name, "exports_gpl::", 13))
        return module_dlsym(name+13, true);
    else
        return kallsyms_dlsym(name);
}

void LinuxOS::enter_kernel_mode(os::KernelMode& km)
{
    vm::VM* vcpu = boot::curVCPU();
    uintptr_t rsp = vcpu->get_reg(vm::Register::RSP);
    km.os_specific[0] = vcpu->get_reg(vm::Register::CR3);
    os::OS::enter_kernel_mode(km);
    if((km.cs & 3)) //was in usermode
    {
        //OS::enter_kernel_mode does swapgs for us
        //revert it and run actual kernel code instead
        uintptr_t kgs = vcpu->rdmsr(IA32_GS_BASE);
        uintptr_t gs = vcpu->rdmsr(IA32_KERNEL_GS_BASE);
        vcpu->wrmsr(IA32_KERNEL_GS_BASE, kgs);
        vcpu->wrmsr(IA32_GS_BASE, gs);
        vcpu->set_reg(vm::Register::RFLAGS, 2); //no flags
        vcpu->set_reg(vm::Register::RSP, rsp);
        vcpu->set_reg(vm::Register::RIP, vcpu->rdmsr(IA32_LSTAR));
        for(int i = 0; i < 32; i++)
            vcpu->intercept_exception(i, true);
        while(vcpu->get_reg(vm::Register::RSP) < 0xffff800000000000ul)
            vcpu->execute(true);
        for(int i = 0; i < 32; i++)
            vcpu->intercept_exception(i, false);
        vcpu->set_reg(vm::Register::RFLAGS, 514); //IF
    }
}

void LinuxOS::exit_kernel_mode(const os::KernelMode& km)
{
    vm::VM* vcpu = boot::curVCPU();
    os::OS::exit_kernel_mode(km);
    vcpu->set_reg(vm::Register::CR3, km.os_specific[0]);
}

std::vector<uintmax_t> LinuxOS::pids()
{
    if(!kmalloc_addr)
        kmalloc_addr = kallsyms_dlsym("__kmalloc");
    if(!kfree_addr)
        kfree_addr = kallsyms_dlsym("kfree");
    if(!task_seq_get_next_addr)
        task_seq_get_next_addr = kallsyms_dlsym("task_seq_get_next_addr");
    if(!wait_for_owner_exiting_addr)
        wait_for_owner_exiting_addr = kallsyms_dlsym("wait_for_owner_exiting");
    if(!init_pid_ns_addr)
        init_pid_ns_addr = module_dlsym("init_pid_ns", true);
    if(!kmalloc_addr
    || !kfree_addr
    || !task_seq_get_next_addr
    || !wait_for_owner_exiting_addr
    || !init_pid_ns_addr)
        return std::vector<uintmax_t>();
    std::vector<uintmax_t> ans;
    os::OS::KernelMode km(*this);
    uintptr_t buf = fncall::call<fncall::CC_SYSV>(kmalloc_addr, sizeof(int32_t));
    if(buf >= error_start)
        return 0;
    int32_t pid = 0;
    for(;;)
    {
        pid++;
        utils::copy_to_guest_vm(buf, &pid, sizeof(pid));
        uintptr_t task = fncall::call<fncall::CC_SYSV>(task_seq_get_next_addr, init_pid_ns_addr, buf, 0);
        if(!task || task >= error_start)
            break;
        utils::copy_from_guest_vm(&pid, buf, sizeof(pid));
        ans.push_back(pid);
        //wait_for_owner_exiting does the following: mutex_lock, mutex_unlock, put_task_struct
        //the mutex is futex-related, so unless we trapped from there we're fine
        fncall::call<fncall::CC_SYSV>(wait_for_owner_exiting_addr, -16, task);
    }
    fncall::call<fncall::CC_SYSV>(kfree_addr, buf);
    return ans;
}

os::OS::Process LinuxOS::find_by_pid(uintmax_t pid)
{
    if(!find_task_by_pid_ns_addr)
        find_task_by_pid_ns_addr = kallsyms_dlsym("find_task_by_pid_ns");
    if(!wait_for_owner_exiting_addr)
        wait_for_owner_exiting_addr = kallsyms_dlsym("wait_for_owner_exiting");
    if(!init_pid_ns_addr)
        init_pid_ns_addr = module_dlsym("init_pid_ns", true);
    if(!find_task_by_pid_ns_addr
    || !wait_for_owner_exiting_addr
    || !init_pid_ns_addr)
        return os::OS::Process();
    os::OS::KernelMode km(*this);
    uintptr_t task_struct = fncall::call<fncall::CC_SYSV>(find_task_by_pid_ns_addr, pid, init_pid_ns_addr);
    if(!task_struct || task_struct >= error_start)
        return os::OS::Process();
    return os::OS::Process(*new LinuxProcess(this, task_struct));
}

std::string LinuxOS::LinuxProcess::name()
{
    return "";
}

uint64_t LinuxOS::LinuxProcess::cr3()
{
    return -1;
}

std::vector<os::OS::Library> LinuxOS::LinuxProcess::libraries()
{
    return std::vector<os::OS::Library>();
}

LinuxOS::LinuxProcess::~LinuxProcess()
{
    os::OS::KernelMode km(*this->os);
    fncall::call<fncall::CC_SYSV>(this->os->wait_for_owner_exiting_addr, -16, task_struct);
}

static os::OS* detectLinux(void*)
{
    LinuxKallsyms ks;
    if(!find_kallsyms(ks))
        return nullptr;
    return new LinuxOS(ks);
}

int main(const char* cmdline)
{
    if(!strcmp(cmdline, "osdetect"))
        os::addOSDetector(detectLinux);
}
