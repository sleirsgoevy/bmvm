#include <libconfig/conffile.hpp>
#include <liblog/logging.hpp>
#include <sstream>
extern "C"
{
    #include <elf.h>
}

using logging::log;

int main(const char* cmdline)
{
    log << "Kernel command line: " << cmdline << std::endl;
    std::string cmdl = cmdline;
    for(char& c : cmdl)
        if(c == '\t' || c == '\n')
            c = ' ';
    config::ConfigFile parsed_cmdline(std::istringstream(cmdl), ' ');
    std::string s_config_name = parsed_cmdline.getString("configfile", "config.txt");
    log << "Using configuration file " << s_config_name << std::endl;
    config::ConfigFile conffile(s_config_name);
    for(const std::pair<const std::string, std::string>& i : conffile)
    {
        log << "Loading module " << i.first << "...";
        std::string* p_module_name = new std::string("lib");
        std::string& module_name = *p_module_name;
        module_name += i.first;
        module_name += ".so";
        auto mod = load_elf(nullptr, module_name.c_str());
        if(!mod)
        {
            log << "failed\n";
            continue;
        }
        log << "ok, (struct module*)" << std::hex << (uintptr_t)mod << std::dec << "\n";
        auto mod_main = (int(*)(const char*))elf_dlsym_module(mod, "main");
        if(!mod_main)
        {
            log << "Warning: " << i.first << " has no main!\n";
            continue;
        }
        std::string args = i.second;
        int rv = mod_main(args.c_str());
        if(rv)
        {
            log << i.first << " main returned with non-zero exit code " << rv << std::endl;
            return rv;
        }
    }
    return 0;
}
