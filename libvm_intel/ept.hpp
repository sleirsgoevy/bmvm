#pragma once
#include <libvm/ept.hpp>

namespace vmx
{

using vm::ept::EPTPML;

enum Protection { EPT_R = 1, EPT_W = 2, EPT_X = 1028, EPT_HUGE = 128 };
enum MemoryType { MT_UC = 0, MT_WC = 1, MT_WT = 4, MT_WP = 5, MT_WB = 6 };

class EPT : public vm::ept::EPT<vmx::EPT>
{
    friend class vm::ept::EPT<vmx::EPT>;
    static constexpr int HUGE = EPT_HUGE;
    static constexpr int PROT = EPT_R | EPT_W | EPT_X;
};

}
