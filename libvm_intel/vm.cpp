#include <libvm/vm.hpp>
#include <libvm/intel.hpp>
#include <liblog/logging.hpp>
#include <libsmp/ipi.hpp>
#include <libsmp/nmi_window.hpp>
#include <cstring>
#include <vector>
#include <assert.h>
#include <sysregs.h>
#include "ept.hpp"

namespace vmx
{

static inline uint64_t vmread(uint32_t index)
{
    uint64_t ans;
    asm volatile("vmread %1, %0":"=r"(ans):"r"((uint64_t)index));
    return ans;
}

static inline void vmwrite(uint32_t index, uint64_t value)
{
    asm volatile("vmwrite %1, %0"::"r"((uint64_t)index),"r"(value));
}

#define VM_MSR_BITMAPS      0x2004
#define VM_EPTPTR           0x201a
#define VM_XSS_BITMAP       0x202c
#define VM_GUEST_ADDRESS    0x2400
#define VM_LINK_POINTER     0x2800
#define VM_GUEST_IA32_PAT   0x2804
#define VM_GUEST_IA32_EFER  0x2806
#define VM_PINBASED_CTLS    0x4000
#define VM_PROCBASED_CTLS   0x4002
#define VM_EXCEPTION_BITMAP 0x4004
#define VM_EXIT_CTLS        0x400c
#define VM_ENTRY_CTLS       0x4012
#define VM_ENTRY_INT_INFO   0x4016
#define VM_ENTRY_ERROR_CODE 0x4018
#define VM_PROCBASED_CTLS2  0x401e
#define VM_INSTR_ERROR      0x4400
#define VM_EXIT_REASON      0x4402
#define VM_EXIT_INT_INFO    0x4404
#define VM_EXIT_ERROR_CODE  0x4406
#define VM_EXIT_INSTR_LEN   0x440c
#define VM_GUEST_IDT_LIMIT  0x4812
#define VM_GUEST_INT_STATE  0x4824
#define VM_GUEST_ACTIVITY   0x4826
#define VM_PREEMPTION_TIMER 0x482e
#define VM_CR0_MASK         0x6000
#define VM_CR4_MASK         0x6002
#define VM_CR0_READ_SHADOW  0x6004
#define VM_CR4_READ_SHADOW  0x6006
#define VM_EXIT_QUAL        0x6400
#define VM_GUEST_CR0        0x6800
#define VM_GUEST_CR3        0x6802
#define VM_GUEST_CR4        0x6804
#define VM_GUEST_IDT_BASE   0x6818
#define VM_GUEST_RSP        0x681c
#define VM_GUEST_RIP        0x681e
#define VM_GUEST_RFLAGS     0x6820
#define VM_HOST_CR0         0x6c00
#define VM_HOST_CR3         0x6c02
#define VM_HOST_CR4         0x6c04
#define VM_HOST_FS_BASE     0x6c06
#define VM_HOST_GDTR        0x6c0c
#define VM_HOST_IDTR        0x6c0e
#define VM_HOST_RSP         0x6c14
#define VM_HOST_RIP         0x6c16

#define VM_GUEST_SEG_SELECTOR(id) (0x800+2*(id))
#define VM_GUEST_SEG_LIMIT(id) (0x4800+2*(id))
#define VM_GUEST_SEG_RIGHTS(id) (0x4814+2*(id))
#define VM_GUEST_SEG_BASE(id) (0x6806+2*(id))

#define EXIT_REASON_EXCEPTION        0
#define EXIT_REASON_INTERRUPT        1
#define EXIT_REASON_TRIPLE_FAULT     2
#define EXIT_REASON_INIT             3
#define EXIT_REASON_SIPI             4
#define EXIT_REASON_INTERRUPT_WINDOW 7
#define EXIT_REASON_VMCALL           18
#define EXIT_REASON_CR_ACCESS        28
#define EXIT_REASON_RDMSR            31
#define EXIT_REASON_WRMSR            32
#define EXIT_REASON_MONITOR_TRAP     37
#define EXIT_REASON_EPT_VIOLATION    48
#define EXIT_REASON_PREEMPTION_TIMER 52
#define EXIT_REASON_XSETBV           55

class IntelVM;

extern "C"
{
    int vmx_launch(IntelVM* vm, uint64_t* regs, int is_resume);
    void vmx_save_host_state(void);
    extern smp::NmiWindow vmx_nmi_critical;
}

static inline void write_guest_segment(int which, uint16_t sel, uint64_t base, uint32_t limit, uint32_t rights)
{
    vmwrite(VM_GUEST_SEG_SELECTOR(which), sel);
    vmwrite(VM_GUEST_SEG_BASE(which), base);
    vmwrite(VM_GUEST_SEG_LIMIT(which), limit);
    vmwrite(VM_GUEST_SEG_RIGHTS(which), rights);
}

static inline bool set_control_reg(const char* reg_name, uint32_t reg, uint32_t msr, uint32_t value)
{
    uint64_t msr_value = rdmsr(msr);
    uint32_t mustbe1 = (uint32_t)msr_value;
    uint32_t canbe1 = (uint32_t)(msr_value >> 32);
    if((~value & mustbe1))
    {
        logging::log << reg_name << ": these bits must be 1: " << (~value & mustbe1) << std::endl;
        return false;
    }
    if((value & ~canbe1))
    {
        logging::log << reg_name << ": these bits must be 0: " << (value & ~canbe1) << std::endl;
        return false;
    }
    vmwrite(reg, value);
    return true;
}

static inline void cpu_reset()
{
    for(int seg = 0; seg < 6; seg++)
        write_guest_segment(seg, 0, 0, 0xffff, (seg == 1) ? 0x9b : 0x93);
    write_guest_segment(6, 0, 0, 0, 0x82); //LDTR
    write_guest_segment(7, 0, 0, 0, 0x8b); //TR
    vmwrite(VM_GUEST_IDT_LIMIT, 1023);
    vmwrite(VM_GUEST_IDT_BASE, 0);
    vmwrite(VM_GUEST_CR0, 0x24);
    vmwrite(VM_GUEST_CR4, 0x2000);
    vmwrite(VM_CR0_MASK, 32);
    vmwrite(VM_CR0_READ_SHADOW, 0);
    vmwrite(VM_CR4_MASK, 8192);
    vmwrite(VM_CR4_READ_SHADOW, 0);
    vmwrite(VM_GUEST_RSP, 0);
    vmwrite(VM_GUEST_RIP, 0);
    vmwrite(VM_GUEST_RFLAGS, 2);
}

class IntelVM : public vm::VM
{
    uint64_t regs[16] = {0};
    void* vmxon = nullptr;
    void* vmcs = nullptr;
    EPT& ept;
    char tss[0x68];
    bool has_run = false;
    uint64_t pending_nmis = 0;
    uint8_t* msr_bitmaps = nullptr;
    uint64_t* remap_pages;
    IntelVM(EPT* ept_p) : ept(ept_p ? ept_p->incref() : *new EPT())
    {
        if(!ept)
            return;
        uint32_t cpuq[4];
        cpuid(0, cpuq);
        if(memcmp(cpuq+1, "GenuineIntel", 12) || cpuq[0] < 1)
            return;
        cpuid(1, cpuq);
        if(!(cpuq[3] & 32)) //vmx support
            return;
        uint64_t ia32fc = ::rdmsr(IA32_FEATURE_CONTROL);
        if((ia32fc & 1) && !(ia32fc & 4))
        {
            logging::log << "VMX is disabled in the BIOS!\n";
            return;
        }
        if(!(ia32fc & 1))
            ::wrmsr(IA32_FEATURE_CONTROL, ia32fc | 5);
        set_cr4(get_cr4() | (1 << 13) | (1 << 18));
        vmxon = mmap(0, 4096, PROT_READ|PROT_WRITE|PROT_NOEXEC, MAP_ANON);
        if(vmxon == MAP_FAILED)
            return;
        vmcs = mmap(0, 4096, PROT_READ|PROT_WRITE|PROT_NOEXEC, MAP_ANON);
        if(vmcs == MAP_FAILED)
        {
            munmap(vmxon, 4096);
            return;
        }
        msr_bitmaps = (uint8_t*)mmap(0, 4096, PROT_READ|PROT_WRITE|PROT_NOEXEC, MAP_ANON);
        if(msr_bitmaps == MAP_FAILED)
        {
            munmap(vmcs, 4096);
            munmap(vmxon, 4096);
            return;
        }
        remap_pages = (uint64_t*)mmap(0, 7<<12, PROT_READ|PROT_WRITE|PROT_NOEXEC, MAP_ANON);
        if(remap_pages == MAP_FAILED)
        {
            munmap(msr_bitmaps, 4096);
            munmap(vmcs, 4096);
            munmap(vmxon, 4096);
            return;
        }
        memset(vmxon, 0, 4096);
        memset(vmcs, 0, 4096);
        memset(msr_bitmaps, -1, 4096);
        *reinterpret_cast<uint32_t*>(vmxon) = ::rdmsr(IA32_VMX_BASIC) & 0x7fffffff;
        uint64_t vmxon_phys = phys_addr_4kb(vmxon);
        uint64_t vmcs_phys = phys_addr_4kb(vmcs);
        uint16_t ax;
        asm volatile("vmxon %1\nlahf":"=a"(ax):"m"(vmxon_phys):"memory");
        assert(!(ax & 66));
        *reinterpret_cast<uint32_t*>(vmcs) = ::rdmsr(IA32_VMX_BASIC) & 0x7fffffff;
        asm volatile("vmclear %1\nlahf":"=a"(ax):"m"(vmcs_phys):"memory");
        assert(!(ax & 66));
        asm volatile("vmptrld %1\nlahf":"=a"(ax):"m"(vmcs_phys):"memory");
        assert(!(ax & 66));
        if(!set_control_reg("VM_PINBASED_CTLS", VM_PINBASED_CTLS, IA32_VMX_TRUE_PINBASED_CTLS, 0x3e)
        || !set_control_reg("VM_PROCBASED_CTLS", VM_PROCBASED_CTLS, IA32_VMX_TRUE_PROCBASED_CTLS, 0x94006172)
        || !set_control_reg("VM_PROCBASED_CTLS2", VM_PROCBASED_CTLS2, IA32_VMX_PROCBASED_CTLS2, 0x10108a)
        || !set_control_reg("VM_EXIT_CTLS", VM_EXIT_CTLS, IA32_VMX_TRUE_EXIT_CTLS, 0x003f6fff)
        || !set_control_reg("VM_ENTRY_CTLS", VM_ENTRY_CTLS, IA32_VMX_TRUE_ENTRY_CTLS, 0x0000d1ff))
        {
            logging::log << "Required VMX features are missing!\n";
            asm volatile("vmclear %0\nvmxoff"::"m"(vmcs_phys):"memory");
            munmap(vmcs, 4096);
            munmap(vmxon, 4096);
            return;
        }
        vmwrite(VM_XSS_BITMAP, 0);
        vmwrite(VM_LINK_POINTER, (uint64_t)-1);
        vmwrite(VM_EPTPTR, ept.phys() | 0x1e);
        vmwrite(VM_MSR_BITMAPS, phys_addr_4kb(msr_bitmaps));
        vmx_save_host_state();
        cpu_reset();
        if(!ept_p)
            smp::smp_nmi_window = vmx_nmi_critical;
        else
            vmwrite(VM_GUEST_ACTIVITY, 1);
        ok = true;
    }
public:
    IntelVM() : IntelVM(nullptr){}
    ~IntelVM();
    vm::VM* createVCPU();
    bool map_pages(uint64_t start, uint64_t end, uint64_t phys, bool r, bool w, bool x, int mem_type);
    void unmap_pages(uint64_t start, uint64_t end);
    uint64_t translate(uint64_t virt, bool& r, bool& w, bool& x, int& mem_type);
    bool fast_remap(uint64_t virt, uint64_t phys, bool r, bool w, bool x, int mem_type);
    bool temp_remap_lock(uint64_t addr1, void*& map1, uint64_t addr2, void*& map2, uint64_t phys1 = MAP_ANON, uint64_t phys2 = MAP_ANON, bool add_r = false, bool add_w = false, bool add_x = false);
    void temp_remap_unlock();
    uint64_t get_reg(vm::Register reg) const;
    void set_reg(vm::Register reg, uint64_t value);
    uint16_t get_segment(vm::Register reg, vm::GDTEntry& out);
    void set_segment(vm::Register reg, uint16_t sel, const vm::GDTEntry& data);
    vm::ExitReason execute(bool singlestep = false);
    uint64_t get_faulting_address() const;
    vm::CrashType get_fault_reason() const;
    void get_vmcall(char[3]) const;
    uint64_t rdmsr(uint32_t msr) const;
    void wrmsr(uint32_t msr, uint64_t value);
    void msr_protect(uint32_t msr, bool r, bool w);
    void intercept_exception(int vector, bool do_intercept);
    int get_exception(bool& has_error_code, uint32_t& error_code);
    void inject_exception(int vector);
    void inject_exception(int vector, uint32_t error_code);
};

IntelVM::~IntelVM()
{
    if(ok)
    {
        uint64_t vmcs_phys = phys_addr_4kb(vmcs);
        asm volatile("vmclear %0\nvmxoff"::"m"(vmcs_phys):"memory");
        munmap(remap_pages, 7*4096);
        munmap(msr_bitmaps, 4096);
        munmap(vmcs, 4096);
        munmap(vmxon, 4096);
    }
    ept.decref();
}

vm::VM* IntelVM::createVCPU()
{
    IntelVM* ans = new IntelVM(&ept);
    if(!*ans)
    {
        delete ans;
        return nullptr;
    }
    return ans;
}

bool IntelVM::map_pages(uint64_t start, uint64_t end, uint64_t phys, bool r, bool w, bool x, int mem_type)
{
    int prot = 0;
    if(r)
        prot |= EPT_R;
    if(w)
        prot |= EPT_W;
    if(x)
        prot |= EPT_X;
    return ept.map(start, end, prot, mem_type, phys);
}

void IntelVM::unmap_pages(uint64_t start, uint64_t end)
{
    return ept.unmap(start, end);
}

uint64_t IntelVM::translate(uint64_t virt, bool& r, bool& w, bool& x, int& mt)
{
    int prot = 0;
    uint64_t ans = ept.translate(virt, prot, mt);
    r = prot & EPT_R;
    w = prot & EPT_W;
    x = prot & EPT_X;
    if(!r && !w && !x)
        ans = vm::NO_MAPPING;
    return ans;
}

bool IntelVM::fast_remap(uint64_t virt, uint64_t phys, bool r, bool w, bool x, int mt)
{
    int prot = 0;
    if(r)
        prot |= EPT_R;
    if(w)
        prot |= EPT_W;
    if(x)
        prot |= EPT_X;
    if(ept.fast_remap(virt, phys, prot, mt))
        return true;
    unmap_pages(virt, virt+4096);
    return map_pages(virt, virt+4096, phys, r, w, x, mt);
}

uint64_t IntelVM::get_reg(vm::Register reg) const
{
    switch(reg)
    {
    case vm::Register::RSP:
        return vmread(VM_GUEST_RSP);
    case vm::Register::RIP:
        return vmread(VM_GUEST_RIP);
    case vm::Register::RFLAGS:
        return vmread(VM_GUEST_RFLAGS);
    case vm::Register::CR0:
        return vmread(VM_GUEST_CR0); //XXX: account for read shadow
    case vm::Register::CR3:
        return vmread(VM_GUEST_CR3);
    case vm::Register::CR4:
        return vmread(VM_GUEST_CR4); //XXX: account for read shadow
    default:
        if((int)reg >= (int)vm::Register::ES && (int)reg <= (int)vm::Register::TR)
            return vmread(VM_GUEST_SEG_SELECTOR((int)reg - (int)vm::Register::ES));
        return regs[(int)reg];
    }
}

void IntelVM::set_reg(vm::Register reg, uint64_t value)
{
    switch(reg)
    {
    case vm::Register::RSP:
        vmwrite(VM_GUEST_RSP, value);
        break;
    case vm::Register::RIP:
        vmwrite(VM_GUEST_RIP, value);
        break;
    case vm::Register::RFLAGS:
        vmwrite(VM_GUEST_RFLAGS, value);
        break;
    case vm::Register::CR0:
        vmwrite(VM_GUEST_CR0, value);
        break;
    case vm::Register::CR3:
        vmwrite(VM_GUEST_CR3, value);
        break;
    case vm::Register::CR4:
        vmwrite(VM_GUEST_CR4, value);
        break;
    default:
        if((int)reg >= (int)vm::Register::ES && (int)reg <= (int)vm::Register::GS)
        {
            uint64_t cr0 = vmread(VM_GUEST_CR0);
            if(!(cr0 & 1))
            {
                write_guest_segment((int)reg - (int)vm::Register::ES, (uint16_t)value, (uint32_t)value << 4, 0xffff, (reg == vm::Register::CS) ? 0x9b : 0x93);
                break;
            }
        }
        regs[(int)reg] = value;
    }
}

uint16_t IntelVM::get_segment(vm::Register which, vm::GDTEntry& out)
{
    int idx = (int)which - (int)vm::Register::ES;
    uint16_t sel;
    if(idx < (int)vm::Register::GDT - (int)vm::Register::ES)
        sel = vmread(VM_GUEST_SEG_SELECTOR(idx));
    else
        sel = 0;
    uint64_t base = vmread(VM_GUEST_SEG_BASE(idx));
    uint32_t limit = vmread(VM_GUEST_SEG_LIMIT(idx));
    uint32_t rights = vmread(VM_GUEST_SEG_RIGHTS(idx));
    out.data[0] = limit;
    out.data[1] = limit >> 8;
    out.data[2] = base;
    out.data[3] = base >> 8;
    out.data[4] = base >> 16;
    out.data[5] = rights;
    out.data[6] = ((limit >> 16) & 0xf) | ((rights >> 8) & 0xf0);
    for(int i = 3; i < 8; i++)
        out.data[i+4] = (base >> (i << 3));
    out.data[12] = out.data[13] = out.data[14] = out.data[15] = 0;
    return sel;
}

void IntelVM::set_segment(vm::Register which, uint16_t sel, const vm::GDTEntry& data)
{
    int idx = (int)which - (int)vm::Register::ES;
    if(idx < (int)vm::Register::GDT - (int)vm::Register::ES)
        vmwrite(VM_GUEST_SEG_SELECTOR(idx), sel);
    uint32_t limit = (uint32_t)data.data[0] | (uint32_t)data.data[1] << 8 | ((uint32_t)data.data[6] & 0xf) << 16;
    uint64_t base = (uint32_t)data.data[2] | (uint32_t)data.data[3] << 8 | (uint32_t)data.data[4] << 16;
    for(int i = 3; i < 8; i++)
        base |= (uint64_t)data.data[i+4] << (i << 3);
    uint32_t rights = (uint32_t)data.data[5] | ((uint32_t)data.data[6] & 0xf0) << 8;
    if((rights & 0x8000))
        limit = limit << 12 | 0xfff;
    vmwrite(VM_GUEST_SEG_LIMIT(idx), limit);
    vmwrite(VM_GUEST_SEG_BASE(idx), base);
    if(idx < (int)vm::Register::GDT - (int)vm::Register::ES)
        vmwrite(VM_GUEST_SEG_RIGHTS(idx), rights);
}

__attribute__((optimize(3)))
vm::ExitReason IntelVM::execute(bool singlestep)
{
    while(true)
    {
        pending_nmis += smp::pull_nmis();
        while(pending_nmis > 0 && smp::ipi_isr())
            pending_nmis--;
        if(singlestep)
            vmwrite(VM_PROCBASED_CTLS, vmread(VM_PROCBASED_CTLS) | (1ull << 27));
        //TODO: handle NMIs
        //XXX: should interrupt handling be in assembly?
        int status = vmx_launch(this, regs, has_run);
        if(status == 4) //NMI arrived in the middle
            continue;
        if(singlestep)
            vmwrite(VM_PROCBASED_CTLS, vmread(VM_PROCBASED_CTLS) & ~(1ull << 27));
        if(status == 0)
            has_run = true;
        else
            return vm::ExitReason::UNKNOWN_ERROR;
        uint64_t reason = vmread(VM_EXIT_REASON);
        switch(reason)
        {
        case EXIT_REASON_EXCEPTION:
        {
            uint64_t int_info = vmread(VM_EXIT_INT_INFO);
            if((int_info & 0x80000700u) != 0x80000200u) //NMI
                return vm::ExitReason::EXCEPTION;
            pending_nmis++;
            break;
        }
        case EXIT_REASON_CR_ACCESS:
        {
            uint64_t exit_qual = vmread(VM_EXIT_QUAL);
            if((exit_qual & 0x3f)) //not: mov cr0, ...
                return (vm::ExitReason)(0x10000 + reason);
            uint64_t source = get_reg((vm::Register)((exit_qual & 0xf00) >> 8));
            vmwrite(VM_CR0_READ_SHADOW, source & 0x20);
            break;
        }
        //XXX: should this really be here and not in consumer?
        case EXIT_REASON_INIT:
            vmwrite(VM_GUEST_ACTIVITY, 3);
            break;
        case EXIT_REASON_SIPI:
        {
            uint8_t vector = (uint8_t)vmread(VM_EXIT_QUAL); //XXX: is it documented anywhere?
            cpu_reset();
            write_guest_segment(1, vector << 8, vector << 12, 0xffff, 0x9b);
            vmwrite(VM_GUEST_RIP, 0);
            vmwrite(VM_GUEST_ACTIVITY, 0); 
            break;
        }
        case EXIT_REASON_XSETBV:
        {
            uint32_t reg = get_reg(vm::Register::RCX);
            uint32_t eax = get_reg(vm::Register::RAX);
            uint32_t edx = get_reg(vm::Register::RDX);
            if(reg != 0)
            {
                inject_exception(13, 0);
                break;
            }
            if(!(eax & 1)
            || ((eax & 4) && !(eax & 2))
            || ((eax & 24) && (eax & 24) != 24)
            || ((eax & 224) && (eax & 230) != 230))
            {
                inject_exception(13, 0);
                break;
            }
            uint32_t supported_eax, supported_edx;
            uint64_t clobber_rcx;
            asm volatile("cpuid":"=a"(supported_eax),"=c"(clobber_rcx),"=d"(supported_edx):"a"(13),"c"(0):"rbx");
            if((eax & ~supported_eax) || (edx & ~supported_edx))
            {
                inject_exception(13, 0);
                break;
            }
            asm volatile("xsetbv"::"c"(reg),"a"(eax),"d"(edx));
            vmwrite(VM_GUEST_RIP, vmread(VM_GUEST_RIP) + vmread(VM_EXIT_INSTR_LEN));
            break;
        }
        case EXIT_REASON_EPT_VIOLATION:
            return vm::ExitReason::PAGE_FAULT;
        case EXIT_REASON_TRIPLE_FAULT:
            return vm::ExitReason::TRIPLE_FAULT;
        case EXIT_REASON_VMCALL:
            return vm::ExitReason::HYPERCALL;
        case EXIT_REASON_RDMSR:
            return vm::ExitReason::RDMSR;
        case EXIT_REASON_WRMSR:
            return vm::ExitReason::WRMSR;
        case EXIT_REASON_MONITOR_TRAP:
            return vm::ExitReason::SINGLESTEP;
        default:
            return (vm::ExitReason)(0x10000 + reason);
        }
    }
}

uint64_t IntelVM::get_faulting_address() const
{
    return vmread(VM_GUEST_ADDRESS);
}

vm::CrashType IntelVM::get_fault_reason() const
{
    uint64_t qual = vmread(VM_EXIT_QUAL);
    if((qual & 4))
        return vm::CrashType::EXECUTE;
    else if((qual & 2))
        return vm::CrashType::WRITE;
    return vm::CrashType::READ;
}

void IntelVM::get_vmcall(char vmcall[3]) const
{
    vmcall[0] = 0x0f;
    vmcall[1] = 0x01;
    vmcall[2] = 0xc1;
}

void IntelVM::msr_protect(uint32_t msr, bool r, bool w)
{
    size_t bit;
    if(msr < 0x2000)
        bit = msr;
    else if(msr >= 0xc0000000 && msr < 0xc0002000)
        bit = msr - 0xc0000000 + 0x2000;
    else
        return;
    size_t byte = bit >> 3;
    uint8_t mask = (1 << (bit & 7));
    if(!r)
        msr_bitmaps[byte] |= mask;
    else
        msr_bitmaps[byte] &= ~mask;
    if(!w)
        msr_bitmaps[byte+2048] |= mask;
    else
        msr_bitmaps[byte+2048] &= ~mask;
}

void IntelVM::intercept_exception(int vector, bool do_intercept)
{
    uint32_t bitmap = vmread(VM_EXCEPTION_BITMAP);
    if(do_intercept)
        bitmap |= (1u << vector);
    else
        bitmap &= ~(1u << vector);
    vmwrite(VM_EXCEPTION_BITMAP, bitmap);
}

uint64_t IntelVM::rdmsr(uint32_t msr) const
{
    switch(msr)
    {
    case IA32_PAT: return vmread(VM_GUEST_IA32_PAT);
    case IA32_EFER: return vmread(VM_GUEST_IA32_EFER);
    case IA32_FS_BASE: return vmread(VM_GUEST_SEG_BASE(4));
    case IA32_GS_BASE: return vmread(VM_GUEST_SEG_BASE(5));
    default: return ::rdmsr(msr);
    }
}

void IntelVM::wrmsr(uint32_t msr, uint64_t value)
{
    switch(msr)
    {
    case IA32_PAT: vmwrite(VM_GUEST_IA32_PAT, value); break;
    case IA32_EFER: vmwrite(VM_GUEST_IA32_EFER, value); break;
    case IA32_FS_BASE: vmwrite(VM_GUEST_SEG_BASE(4), value); break;
    case IA32_GS_BASE: vmwrite(VM_GUEST_SEG_BASE(5), value); break;
    default: ::wrmsr(msr, value); break;
    }
}

static uint64_t* copy_eptpml(uint64_t* tgt, EPTPML* src)
{
    for(size_t i = 0; i < 512; i++)
        tgt[i] = src->get_phys(i);
    return tgt;
}

bool IntelVM::temp_remap_lock(uint64_t addr1, void*& map1, uint64_t addr2, void*& map2, uint64_t phys_a, uint64_t phys_b, bool r, bool w, bool x)
{
    {
        uint64_t add_flags = 0;
        if(r)
            add_flags |= EPT_R;
        if(w)
            add_flags |= EPT_W;
        if(x)
            add_flags |= EPT_X;
        EPTPML* pml4 = ept.lock_pml4();
        uint64_t* pml4_ = copy_eptpml(remap_pages, pml4);
        if(!pml4_[PML4_IDX(addr1)])
            goto fail;
        EPTPML* pml3a = pml4->get(PML4_IDX(addr1));
        uint64_t* pml3a_ = copy_eptpml(remap_pages+512, pml3a);
        pml4_[PML4_IDX(addr1)] = phys_addr_4kb(pml3a_) | (pml4_[PML4_IDX(addr1)] & ~PG_ADDR_MASK_4KB) | add_flags;
        if(!pml3a_[PML3_IDX(addr1)])
            goto fail;
        EPTPML* pml2a = pml3a->get(PML3_IDX(addr1));
        uint64_t* pml2a_ = copy_eptpml(remap_pages+1024, pml2a);
        pml3a_[PML3_IDX(addr1)] = phys_addr_4kb(pml2a_) | (pml3a_[PML3_IDX(addr1)] & ~PG_ADDR_MASK_4KB) | add_flags;
        EPTPML* pml1a;
        uint64_t* pml1a_;
        if(!pml2a_[PML2_IDX(addr1)])
            goto fail;
        else if((pml2a_[PML2_IDX(addr1)] & EPT_HUGE))
        {
            pml1a = nullptr;
            pml1a_ = remap_pages+1536;
            uint64_t base = pml2a_[PML2_IDX(addr1)] & ~(uint64_t)EPT_HUGE;
            for(size_t i = 0; i < 512; i++)
            {
                pml1a_[i] = base;
                base += 4096;
            }
            pml2a_[PML2_IDX(addr1)] = phys_addr_4kb(pml1a_) | (base & ~PG_ADDR_MASK_4KB) | add_flags;
        }
        else
        {
            pml1a = pml2a->get(PML2_IDX(addr1));
            pml1a_ = copy_eptpml(remap_pages+1536, pml1a);
            pml2a_[PML2_IDX(addr1)] = phys_addr_4kb(pml1a_) | (pml2a_[PML2_IDX(addr1)] & ~PG_ADDR_MASK_4KB) | add_flags;
        }
        if(!(pml1a_[PML1_IDX(addr1)]))
            goto fail;
        if(!(pml4_[PML4_IDX(addr2)]))
            goto fail;
        EPTPML* pml3b = pml4->get(PML4_IDX(addr2));
        uint64_t* pml3b_;
        if(pml3a == pml3b)
            pml3b_ = pml3a_;
        else
        {
            pml3b_ = copy_eptpml(remap_pages+2048, pml3b);
            pml4_[PML4_IDX(addr2)] = phys_addr_4kb(pml3b_) | (pml4_[PML4_IDX(addr2)] & ~PG_ADDR_MASK_4KB) | add_flags;
        }
        if(!(pml3b_[PML3_IDX(addr2)]))
            goto fail;
        EPTPML* pml2b = pml3b->get(PML3_IDX(addr2));
        uint64_t* pml2b_;
        if(pml2a == pml2b)
            pml2b_ = pml2a_;
        else
        {
            pml2b_ = copy_eptpml(remap_pages+2560, pml2b);
            pml3b_[PML3_IDX(addr2)] = phys_addr_4kb(pml2b_) | (pml3b_[PML3_IDX(addr2)] & ~PG_ADDR_MASK_4KB) | add_flags;
        }
        if(!(pml2b_[PML2_IDX(addr2)]))
            goto fail;
        EPTPML* pml1b;
        uint64_t* pml1b_;
        if(pml2a == pml2b && PML2_IDX(addr1) == PML2_IDX(addr2))
        {
            pml1b = pml1a;
            pml1b_ = pml1a_;
        }
        else if((pml2b_[PML2_IDX(addr2)] & EPT_HUGE))
        {
            pml1b = nullptr;
            pml1b_ = remap_pages+3072;
            uint64_t base = pml2b_[PML2_IDX(addr2)] & ~(uint64_t)EPT_HUGE;
            for(size_t i = 0; i < 512; i++)
            {
                pml1b_[i] = base;
                base += 4096;
            }
            pml2b_[PML2_IDX(addr2)] = phys_addr_4kb(pml1b_) | (base & ~PG_ADDR_MASK_4KB) | add_flags;
        }
        else
        {
            pml1b = pml2b->get(PML2_IDX(addr2));
            pml1b_ = copy_eptpml(remap_pages+3072, pml1b);
            pml2b_[PML2_IDX(addr2)] = phys_addr_4kb(pml1b_) | (pml2b_[PML2_IDX(addr2)] & ~PG_ADDR_MASK_4KB) | add_flags;
        }
        if(!pml1b_[PML1_IDX(addr2)])
            goto fail;
        uint64_t phys1 = pml1a_[PML1_IDX(addr1)] & PG_ADDR_MASK_4KB;
        uint64_t phys2 = pml1b_[PML1_IDX(addr2)] & PG_ADDR_MASK_4KB;
        map1 = (phys_a == MAP_ANON) ? mmap(0, 4096, PROT_READ|PROT_WRITE|PROT_NOEXEC, MAP_ANON) : nullptr;
        map2 = (phys1 == phys2) ? map1 : ((phys_b == MAP_ANON) ? mmap(0, 4096, PROT_READ|PROT_WRITE|PROT_NOEXEC, phys2) : nullptr);
        void* map1_real = mmap(0, 4096, PROT_READ|PROT_WRITE|PROT_NOEXEC, phys1);
        if(map1)
        {
            memcpy(map1, map1_real, 4096);
            munmap(map1_real, 4096);
        }
        if(map1 != map2 && map2)
        {
            void* map2_real = mmap(0, 4096, PROT_READ|PROT_WRITE|PROT_NOEXEC, phys2);
            memcpy(map2, map2_real, 4096);
            munmap(map2_real, 4096);
        }
        pml1a_[PML1_IDX(addr1)] = ((phys_a == MAP_ANON) ? phys_addr_4kb(map1) : phys_a) | (pml1a_[PML1_IDX(addr1)] & ~PG_ADDR_MASK_4KB) | add_flags;
        pml1b_[PML1_IDX(addr2)] = ((phys_b == MAP_ANON) ? phys_addr_4kb(map2) : phys_b) | (pml1b_[PML1_IDX(addr2)] & ~PG_ADDR_MASK_4KB) | add_flags;
        vmwrite(VM_EPTPTR, phys_addr_4kb(pml4_) | 0x1e);
        return true;
    }
fail:
    ept.unlock_pml4();
    return false;
}

void IntelVM::temp_remap_unlock()
{
    uint64_t descr[2] = { phys_addr_4kb(remap_pages), 0 };
    asm volatile("invept %1, %0"::"r"((uint64_t)2),"m"(descr):"memory");
    vmwrite(VM_EPTPTR, ept.phys() | 0x1e);
    ept.unlock_pml4();
}

static uint32_t craft_intr_info(int vector)
{
    uint32_t exc_type;
    switch(vector)
    {
        case 3: exc_type = 0x400; break;
        case 4: exc_type = 0x400; break;
        case 1: exc_type = 0x500; break;
        default: exc_type = 0x300; break;
    }
    return (uint8_t)vector | exc_type;
}

int IntelVM::get_exception(bool& has_error_code, uint32_t& error_code)
{
    uint32_t qual = vmread(VM_EXIT_INT_INFO);
    has_error_code = qual & 2048;
    if(has_error_code)
        error_code = vmread(VM_EXIT_ERROR_CODE);
    return (uint8_t)qual;
}

void IntelVM::inject_exception(int vector)
{
    vmwrite(VM_ENTRY_INT_INFO, craft_intr_info(vector) | 0x80000000u);
}

void IntelVM::inject_exception(int vector, uint32_t error_code)
{
    vmwrite(VM_ENTRY_INT_INFO, craft_intr_info(vector) | 0x80000800u);
    vmwrite(VM_ENTRY_ERROR_CODE, error_code);
}

}

namespace vm
{

extern "C" VM* createIntelVM()
{
    auto ans = new vmx::IntelVM();
    if(!*ans)
    {
        delete ans;
        return nullptr;
    }
    return ans;
}

}
