use64

global vmx_save_host_state
global vmx_launch
global vmx_nmi_critical

section .text

; void vmx_save_host_state(void)
vmx_save_host_state:
mov rax, cr0
mov ecx, 0x6c00
vmwrite rcx, rax
mov rax, cr3
mov ecx, 0x6c02
vmwrite rcx, rax
mov rax, cr4
mov ecx, 0x6c04
vmwrite rcx, rax
mov ecx, 0xc0000100 ; IA32_FS_BASE
rdmsr
shl rdx, 32
or rax, rdx
mov ecx, 0x6c06
vmwrite rcx, rax
lea rax, [rel host_gdtr]
sgdt [rax]
mov rax, [rax+2]
mov ecx, 0x6c0c
vmwrite rcx, rax
lea rax, [rel host_idtr]
sidt [rax]
mov rax, [rax+2]
mov ecx, 0x6c0e
vmwrite rcx, rax
mov ecx, 0xc0000080 ; IA32_EFER
rdmsr
shl rdx, 32
or rax, rdx
mov ecx, 0x2c02
vmwrite rcx, rax
mov ecx, 0x277 ; IA32_PAT
rdmsr
shl rdx, 32
or rax, rdx
mov ecx, 0x2c00
vmwrite rcx, rax
mov ax, es
mov ecx, 0xc00
vmwrite rcx, rax
mov ax, cs
mov ecx, 0xc02
vmwrite rcx, rax
mov ax, ss
mov ecx, 0xc04
vmwrite rcx, rax
mov ax, ds
mov ecx, 0xc06
vmwrite rcx, rax
mov ax, fs
mov ecx, 0xc08
vmwrite rcx, rax
mov ax, gs
mov ecx, 0xc0a
vmwrite rcx, rax
mov ax, 0x20 ; TR
mov ecx, 0xc0c
vmwrite rcx, rax
ret

; int vmx_launch(IntelVM* this, uint64_t regs, int is_resume)
vmx_launch:
push r15
push r14
push r13
push r12
push rbp
push rbx
push rdi
push rsi
push rdx
mov eax, 0x6c14 ; VM_HOST_RSP
vmwrite rax, rsp
mov eax, 0x6c16 ; VM_HOST_RIP
lea rcx, [rel .vmexit]
vmwrite rax, rcx
mov rax, [rsi]
mov rcx, [rsi+8]
mov rdx, [rsi+16]
mov rbx, [rsi+24]
mov rbp, [rsi+40]
mov rdi, [rsi+56]
mov r8, [rsi+64]
mov r9, [rsi+72]
mov r10, [rsi+80]
mov r11, [rsi+88]
mov r12, [rsi+96]
mov r13, [rsi+104]
mov r14, [rsi+112]
mov r15, [rsi+120]
mov rsi, [rsi+48]
cmp dword [rsp], 0
jnz .resume
.critical_start:
;XXX: peeking at internals of libsmp
cmp qword [fs:8], 0
jnz .critical_handler
vmlaunch
jc .fail_invalid
jnz .fail_wtf

.resume:
cmp qword [fs:8], 0
jnz .critical_handler
vmresume
.critical_end:
jc .fail_invalid
jz .fail_valid
jmp .fail_wtf

.fail_invalid:
mov eax, 1
jmp .popret

.fail_valid:
mov eax, 2
jmp .popret

.fail_wtf:
mov eax, 3
jmp .popret

.critical_handler:
mov eax, 4
jmp .popret

.vmexit:
push rsi
mov rsi, 0x4402 ; VM_EXIT_REASON
vmread rsi, rsi
cmp rsi, 10 ; cpuid
jz .cpuid
mov rsi, [rsp+16]
mov [rsi], rax
mov [rsi+8], rcx
mov [rsi+16], rdx
mov [rsi+24], rbx
mov [rsi+40], rbp
mov [rsi+56], rdi
mov [rsi+64], r8
mov [rsi+72], r9
mov [rsi+80], r10
mov [rsi+88], r11
mov [rsi+96], r12
mov [rsi+104], r13
mov [rsi+112], r14
mov [rsi+120], r15
pop rax
mov [rsi+48], rax
xor eax, eax

.popret:
add rsp, 24
pop rbx
pop rbp
pop r12
pop r13
pop r14
pop r15
ret

.cpuid:
push rax
mov esi, 0x681e ; VM_GUEST_RIP
vmread rax, rsi
add rax, 2
vmwrite rsi, rax
pop rax
cmp eax, 0x40000000
jz .shut_up_kvm
cmp eax, 1
cpuid
pop rsi
jnz .resume
and ecx, 0xffffffdf
jmp .resume
.shut_up_kvm:
xor eax, eax
cpuid
cpuid
pop rsi
jmp .resume

section .data

host_gdtr:
times 10 db 0

host_idtr:
times 10 db 0

vmx_nmi_critical:
dq vmx_launch.critical_start
dq vmx_launch.critical_end
dq vmx_launch.critical_handler
