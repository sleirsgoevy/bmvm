#include <liblog/logging.hpp>
#include "../ept.hpp"
extern "C"
{
    #include "types.h"
    #include "mm.h"
    #include "assert.h"
}

using logging::log;
using std::dec;
using std::hex;
using std::endl;

static inline uint64_t phys_addr_4kb(void* p)
{
    return ((uint64_t*)PML1_MOD_BASE)[((uint64_t)p) >> 12] & PG_ADDR_MASK_4KB;
}

static inline uint64_t rdmsr(uint32_t msr)
{
    uint32_t low, high;
    asm volatile("rdmsr":"=a"(low),"=d"(high):"c"(msr));
    return (uint64_t)high << 32 | low;
}

static inline void wrmsr(uint32_t msr, uint64_t value)
{
    uint32_t low = value, high = value >> 32;
    asm volatile("wrmsr"::"a"(low),"d"(high),"c"(msr));
}

static inline uint64_t get_cr0()
{
    uint64_t ans;
    asm volatile("mov %%cr0, %0":"=r"(ans));
    return ans;
}

static inline uint64_t get_cr4()
{
    uint64_t ans;
    asm volatile("mov %%cr4, %0":"=r"(ans));
    return ans;
}

static inline void set_cr4(uint64_t cr4)
{
    asm volatile("mov %0, %%cr4"::"r"(cr4));
}

#define IA32_FEATURE_CONTROL         0x03a
#define IA32_VMX_BASIC               0x480
#define IA32_VMX_PINBASED_CTLS       0x481
#define IA32_VMX_PROCBASED_CTLS      0x482
#define IA32_VMX_EXIT_CTLS           0x483
#define IA32_VMX_ENTRY_CTLS          0x484
#define IA32_VMX_CR0_FIXED0          0x486
#define IA32_VMX_CR0_FIXED1          0x487
#define IA32_VMX_CR4_FIXED0          0x488
#define IA32_VMX_CR4_FIXED1          0x489
#define IA32_VMX_PROCBASED_CTLS2     0x48b
#define IA32_VMX_TRUE_PINBASED_CTLS  0x48d
#define IA32_VMX_TRUE_PROCBASED_CTLS 0x48e
#define IA32_VMX_TRUE_EXIT_CTLS      0x48f
#define IA32_VMX_TRUE_ENTRY_CTLS     0x490

static inline uint64_t vmread(uint32_t index)
{
    uint64_t ans;
    asm volatile("vmread %1, %0":"=r"(ans):"r"((uint64_t)index));
    return ans;
}

static inline void vmwrite(uint32_t index, uint64_t value)
{
    asm volatile("vmwrite %1, %0"::"r"((uint64_t)index),"r"(value));
}

#define VM_EPTPTR          0x201a
#define VM_LINK_POINTER    0x2800
#define VM_PINBASED_CTLS   0x4000
#define VM_PROCBASED_CTLS  0x4002
#define VM_EXIT_CTLS       0x400c
#define VM_ENTRY_CTLS      0x4012
#define VM_PROCBASED_CTLS2 0x401e
#define VM_INSTR_ERROR     0x4400
#define VM_EXIT_REASON     0x4402
#define VM_GUEST_CR0       0x6800
#define VM_GUEST_CR4       0x6804
#define VM_GUEST_RSP       0x681c
#define VM_GUEST_RIP       0x681e
#define VM_GUEST_RFLAGS    0x6820
#define VM_HOST_CR0        0x6c00
#define VM_HOST_CR3        0x6c02
#define VM_HOST_CR4        0x6c04
#define VM_HOST_GDTR       0x6c0c
#define VM_HOST_IDTR       0x6c0e
#define VM_HOST_RSP        0x6c14
#define VM_HOST_RIP        0x6c16

#define VM_GUEST_SEG_SELECTOR(id) (0x800+2*(id))
#define VM_GUEST_SEG_LIMIT(id) (0x4800+2*(id))
#define VM_GUEST_SEG_RIGHTS(id) (0x4814+2*(id))
#define VM_GUEST_SEG_BASE(id) (0x6806+2*(id))

static inline uint16_t vmlaunch(void)
{
    uint16_t ax;
    asm volatile(
        "vmwrite %%rsp, %%rax\n"
        "lea 1f(%%rip), %%rax\n"
        "vmwrite %%rax, %%rcx\n"
        "vmlaunch\n"
        "jmp 2f\n"
        "1:\n"
        "xor %%ax, %%ax\n"
        "jmp 3f\n"
        "2:\n"
        "sahf\n"
        "3:\n"
        :"=a"(ax)
        :"a"((uint64_t)VM_HOST_RSP),
         "c"((uint64_t)VM_HOST_RIP)
        :"memory"
    );
    return ax;
}

extern "C" void vmx_save_host_state(void);

static char tss[0x68];

static inline void write_guest_segment(int which, uint16_t sel, uint64_t base, uint32_t limit, uint32_t rights)
{
    vmwrite(VM_GUEST_SEG_SELECTOR(which), sel);
    vmwrite(VM_GUEST_SEG_BASE(which), base);
    vmwrite(VM_GUEST_SEG_LIMIT(which), limit);
    vmwrite(VM_GUEST_SEG_RIGHTS(which), rights);
}

static void create_tss(void)
{
    struct
    {
        uint16_t size;
        void* ptr;
    } __attribute__((packed)) gdtr;
    asm volatile("sgdt %0":"=m"(gdtr)::"memory");
    uint64_t* gdt = reinterpret_cast<uint64_t*>(gdtr.ptr);
    uint64_t tss_ptr = (uint64_t)tss;
    assert((uint64_t)(uint32_t)tss_ptr == tss_ptr);
    gdt[4] = 0x68 | (tss_ptr & 0xffffff) << 16 | (tss_ptr & 0xff000000) << 32 | 0x108b000000ull;
}

static inline bool set_control_reg(const char* reg_name, uint32_t reg, uint32_t msr, uint32_t value)
{
    uint64_t msr_value = rdmsr(msr);
    uint32_t mustbe1 = (uint32_t)msr_value;
    uint32_t canbe1 = (uint32_t)(msr_value >> 32);
    if((~value & mustbe1))
    {
        log << reg_name << ": these bits must be 1: " << (~value & mustbe1) << endl;
        return false;
    }
    if((value & ~canbe1))
    {
        log << reg_name << ": these bits must be 0: " << (value & ~canbe1) << endl;
        return false;
    }
    vmwrite(reg, value);
    return true;
}

#define set_control_reg(reg, msr, value) set_control_reg(#reg, reg, msr, value)

int main()
{
    log << hex;
    log << "IA32_FEATURE_CONTROL=" << rdmsr(IA32_FEATURE_CONTROL) << endl;
    log << "IA32_VMX_BASIC=" << rdmsr(IA32_VMX_BASIC) << endl;
    log << "IA32_VMX_CR0_FIXED0=" << rdmsr(IA32_VMX_CR0_FIXED0) << endl;
    log << "IA32_VMX_CR0_FIXED1=" << rdmsr(IA32_VMX_CR0_FIXED1) << endl;
    log << "IA32_VMX_CR4_FIXED0=" << rdmsr(IA32_VMX_CR4_FIXED0) << endl;
    log << "IA32_VMX_CR4_FIXED1=" << rdmsr(IA32_VMX_CR4_FIXED1) << endl;
    log << "IA32_VMX_PINBASED_CTLS=" << rdmsr(IA32_VMX_PINBASED_CTLS) << endl;
    log << "IA32_VMX_TRUE_PINBASED_CTLS=" << rdmsr(IA32_VMX_TRUE_PINBASED_CTLS) << endl;
    log << "IA32_VMX_PROCBASED_CTLS=" << rdmsr(IA32_VMX_PROCBASED_CTLS) << endl;
    log << "IA32_VMX_TRUE_PROCBASED_CTLS=" << rdmsr(IA32_VMX_TRUE_PROCBASED_CTLS) << endl;
    log << "IA32_VMX_EXIT_CTLS=" << rdmsr(IA32_VMX_EXIT_CTLS) << endl;
    log << "IA32_VMX_TRUE_EXIT_CTLS=" << rdmsr(IA32_VMX_TRUE_EXIT_CTLS) << endl;
    log << "IA32_VMX_ENTRY_CTLS=" << rdmsr(IA32_VMX_ENTRY_CTLS) << endl;
    log << "IA32_VMX_TRUE_ENTRY_CTLS=" << rdmsr(IA32_VMX_TRUE_ENTRY_CTLS) << endl;
    log << "IA32_VMX_PROCBASED_CTLS2=" << rdmsr(IA32_VMX_PROCBASED_CTLS2) << endl;
    log << "cr0=" << get_cr0() << endl;
    log << "cr4=" << get_cr4() << endl;
    log << dec;
    uint64_t ia32fc = rdmsr(IA32_FEATURE_CONTROL);
    if((ia32fc & 1) && !(ia32fc & 4))
    {
        log << "VMX is disabled in the BIOS!" << endl;
        return 1;
    }
    if(!(ia32fc & 1))
        wrmsr(IA32_FEATURE_CONTROL, ia32fc | 5);
    set_cr4(get_cr4() | (1 << 13));
    void* vmxon = mmap(0, 4096, PROT_READ|PROT_WRITE, MAP_ANON);
    memset(vmxon, 0, 4096);
    *reinterpret_cast<uint32_t*>(vmxon) = rdmsr(IA32_VMX_BASIC) & 0x7fffffff;
    uint64_t vmxon_phys = phys_addr_4kb(vmxon);
    log << hex;
    log << "123=" << 123 << endl;
    uint16_t ax;
    asm volatile("vmxon %1\nlahf":"=a"(ax):"m"(vmxon_phys):"memory");
    log << "vmxon: flags=" << (ax >> 8) << endl;
    void* vmcs = mmap(0, 4096, PROT_READ|PROT_WRITE, MAP_ANON);
    memset(vmcs, 0, 4096);
    *reinterpret_cast<uint32_t*>(vmcs) = rdmsr(IA32_VMX_BASIC) & 0x7fffffff;
    uint64_t vmcs_phys = phys_addr_4kb(vmcs);
    asm volatile("vmclear %1\nlahf":"=a"(ax):"m"(vmcs_phys):"memory");
    log << "vmclear: flags=" << (ax >> 8) << endl;
    asm volatile("vmptrld %1\nlahf":"=a"(ax):"m"(vmcs_phys):"memory");
    log << "vmptrld: flags=" << (ax >> 8) << endl;
    if(!set_control_reg(VM_PINBASED_CTLS, IA32_VMX_TRUE_PINBASED_CTLS, 0x16)
    || !set_control_reg(VM_PROCBASED_CTLS, IA32_VMX_TRUE_PROCBASED_CTLS, 0x84006172)
    || !set_control_reg(VM_PROCBASED_CTLS2, IA32_VMX_PROCBASED_CTLS2, 0x82)
    || !set_control_reg(VM_EXIT_CTLS, IA32_VMX_TRUE_EXIT_CTLS, 0x003fefff)
    || !set_control_reg(VM_ENTRY_CTLS, IA32_VMX_TRUE_ENTRY_CTLS, 0x0000d1ff))
    {
        log << "Required VMX features are missing!";
        return 1;
    }
    vmwrite(VM_LINK_POINTER, (uint64_t)-1);
    /*vmwrite(VM_HOST_CR0, get_cr0());
    vmwrite(VM_HOST_CR3, get_cr3());
    vmwrite(VM_HOST_CR4, get_cr4());*/
    create_tss();
    char* page0 = (char*)mmap(0, 16384, PROT_READ|PROT_WRITE, MAP_ANON);
    //vmcall
    page0[0] = 0x0f;
    page0[1] = 0x01;
    page0[2] = 0xc1;
    /*uint64_t* ept = (uint64_t*)mmap(0, 16384, PROT_READ|PROT_WRITE, MAP_ANON);
    memset(ept, 0, 16384);
    ept[0] = phys_addr_4kb(ept+512) | 0x407;
    ept[512] = phys_addr_4kb(ept+1024) | 0x407;
    ept[1024] = phys_addr_4kb(ept+1536) | 0x407;
    ept[1536] = phys_addr_4kb(page0) | 0x437;*/
    vmx::EPT ept;
    ept.map(0, 4096, vmx::EPT_R|vmx::EPT_W|vmx::EPT_X, vmx::MT_WB, phys_addr_4kb(page0));
    vmwrite(VM_EPTPTR, ept.phys() | 0x1e);
    vmx_save_host_state();
    //asm volatile("vmlaunch\nlahf":"=a"(ax)::"memory");
    for(int seg = 0; seg < 6; seg++)
        write_guest_segment(seg, 0, 0, 0xffff, (seg == 1) ? 0x9b : 0x93);
    write_guest_segment(6, 0, 0, 0, 0x82); //LDTR
    write_guest_segment(7, 0, 0, 0, 0x8b); //TR
    vmwrite(VM_GUEST_CR0, 0x24);
    vmwrite(VM_GUEST_CR4, 0x2000);
    vmwrite(VM_GUEST_RSP, 0);
    vmwrite(VM_GUEST_RIP, 0);
    vmwrite(VM_GUEST_RFLAGS, 2);
    ax = vmlaunch();
    log << "vmlaunch: flags=" << (ax >> 8) << endl;
    log << dec;
    log << "vmexit reason: " << vmread(VM_EXIT_REASON) << endl;
    log << "vm-instruction error: " << vmread(VM_INSTR_ERROR) << endl;
    return 0;
}
