#include <libhash/hash.hpp>
#include <liblog/logging.hpp>
#include <sstream>
extern "C"
{
    #include <fs.h>
}

hash::Hash::~Hash(){}

int main(const char* cmdline)
{
    std::istringstream sin(cmdline);
    std::string algo_name;
    if(!(sin >> algo_name))
    {
        logging::log << "hash: must specify the algorithm" << std::endl;
        return 1;
    }
    if(algo_name != "sha256")
    {
        logging::log << "hash: unsupported algorithm" << std::endl;
        return 1;
    }
    hash::Hash* h = hash::sha256();
    char buf[4096];
    std::string filename;
    bool any_files = false;
    bool errors = false;
    while(sin >> filename)
    {
        any_files = true;
        FILE* f = open(filename.c_str(), O_RDONLY);
        if(!f)
        {
            logging::log << "hash: failed to open " << filename << std::endl;
            errors = true;
            continue;
        }
        off_t offset = 0;
        ssize_t chk;
        while((chk = pread(f, buf, sizeof(buf), offset)) > 0)
        {
            offset += chk;
            h->feed(buf, buf+chk);
        }
        close(f);
        if(chk < 0)
        {
            logging::log << "hash: failed to read from " << filename << std::endl;
            errors = true;
            continue;
        }
        std::span<const char> digest = h->close();
        logging::log << std::hex;
        for(size_t i = 0; i < digest.size(); i++)
        {
            uint8_t byte = digest[i];
            logging::log << byte / 16 << byte % 16;
        }
        logging::log << ' ' << filename << std::dec << std::endl;
    }
    if(!any_files)
    {
        logging::log << "hash: no files specified" << std::endl;
        return 1;
    }
    if(errors)
    {
        logging::log << "hash: errors encountered" << std::endl;
        return 1;
    }
    return 0;
}
