#include <libhash/hash.hpp>
#include <cstring>
extern "C"
{
    #include <types.h>
}

namespace
{

constexpr void constexpr_assert(bool x)
{
    (void)(1 / (int)x);
}

struct Uint128
{
    uint32_t chunks[4];
    constexpr Uint128(uint64_t q = 0) : chunks{(uint32_t)q, (uint32_t)(q>>32), 0, 0}{}
    constexpr Uint128 operator*(uint32_t other)
    {
        Uint128 ans;
        uint64_t carry = 0;
        for(size_t i = 0; i < 4; i++)
        {
            carry += chunks[i] * (uint64_t)other;
            ans.chunks[i] = carry;
            carry >>= 32;
        }
        constexpr_assert(!carry);
        return ans;
    }
    constexpr Uint128 operator*(Uint128 other)
    {
        Uint128 ans;
        for(size_t i = 0; i < 4; i++)
        {
            Uint128 part = (*this) * other.chunks[i];
            uint64_t carry = 0;
            for(size_t j = 0; i + j < 4; j++)
            {
                carry += ans.chunks[i+j] + (uint64_t)part.chunks[j];
                ans.chunks[i+j] = carry;
                carry >>= 32;
            }
            constexpr_assert(!carry);
            for(size_t j = 4 - i; j < 4; j++)
                constexpr_assert(!part.chunks[j]);
        }
        return ans;
    }
};

constexpr uint32_t root(int32_t prime, bool cubic = false)
{
#define POW(x) (cubic ? x*x*x : x*x)
    int32_t whole_part = 0;
    {
        int32_t low = 1;
        int32_t high = prime + 1;
        while(high - low > 1)
        {
            int32_t mid = (high + low) / 2;
            if(POW(mid) <= prime)
                low = mid;
            else
                high = mid;
        }
        whole_part = low;
    }
    {
        int64_t low = (int64_t)whole_part << 32;
        int64_t high = (int64_t)(whole_part + 1) << 32;
        while(high - low > 1)
        {
            int64_t mid = (low + high) / 2;
            Uint128 mid_int128 = mid;
            Uint128 p = POW(mid_int128);
            //we're only taking roots of prime numbers, so no equality possible here
            if(p.chunks[cubic ? 3 : 2] < (uint32_t)prime)
                low = mid;
            else
                high = mid;
        }
        return (uint32_t)low;
    }
#undef POW
}

constexpr bool is_prime(int32_t prime)
{
    for(int32_t i = 2; i * i <= prime; i++)
        if(prime % i == 0)
            return false;
    return true;
}

constexpr void next_prime(int32_t& prime)
{
    prime++;
    while(!is_prime(prime))
        prime++;
}

struct Constants
{
    struct H
    {
        uint32_t h[8] = {};
    } h;
    uint32_t k[64] = {};
    constexpr Constants()
    {
        int prime = 2;
        for(size_t i = 0; i < 8; i++)
        {
            h.h[i] = root(prime, false);
            next_prime(prime);
        }
        prime = 2;
        for(size_t i = 0; i < 64; i++)
        {
            k[i] = root(prime, true);
            next_prime(prime);
        }
    }
};

class SHA256 : public hash::Hash
{
    static constexpr Constants constants = Constants();
    template<int bits>
    uint32_t ror(uint32_t x)
    {
        return (x >> bits) | (x << (32 - bits));
    }
    template<int a, int b, int c>
    uint32_t xorrotshr(uint32_t x)
    {
        return ror<a>(x) ^ ror<b>(x) ^ (x >> c);
    }
    template<int a, int b, int c>
    uint32_t xorrotrot(uint32_t x)
    {
        return ror<a>(x) ^ ror<b>(x) ^ ror<c>(x);
    }
    uint32_t vote(uint32_t a, uint32_t b, uint32_t c)
    {
        return (a & b) ^ (a & c) ^ (b & c);
    }
    Constants::H h = constants.h;
    char buf[64];
    size_t pos = 0;
    uint64_t count = 0;
    //source: wikipedia
    void round(const char* buf)
    {
        Constants::H h = this->h;
        uint32_t w[64];
        for(size_t i = 0; i < 16; i++)
            w[i] = (uint8_t)buf[4*i] << 24 | (uint8_t)buf[4*i+1] << 16 | (uint8_t)buf[4*i+2] << 8 | (uint8_t)buf[4*i+3];
        for(size_t i = 16; i < 64; i++)
        {
            uint32_t s0 = xorrotshr<7, 18, 3>(w[i-15]);
            uint32_t s1 = xorrotshr<17, 19, 10>(w[i-2]);
            w[i] = w[i-16] + s0 + w[i-7] + s1;
        }
        for(size_t i = 0; i < 64; i++)
        {
            uint32_t sigma0 = xorrotrot<2, 13, 22>(h.h[0]);
            uint32_t ma = vote(h.h[0], h.h[1], h.h[2]);
            uint32_t t2 = sigma0 + ma;
            uint32_t sigma1 = xorrotrot<6, 11, 25>(h.h[4]);
            uint32_t ch = (h.h[4] & h.h[5]) ^ (~h.h[4] & h.h[6]);
            uint32_t t1 = h.h[7] + sigma1 + ch + constants.k[i] + w[i];
            for(size_t i = 7; i > 0; i--)
                h.h[i] = h.h[i-1];
            h.h[4] += t1;
            h.h[0] = t1 + t2;
        }
        for(size_t i = 0; i < 8; i++)
            this->h.h[i] += h.h[i];
    }
public:
    virtual void feed(const char* start, const char* end)
    {
        count += end - start;
        if(end - start < 64 - pos)
        {
            memcpy(buf+pos, start, end-start);
            pos += end-start;
            return;
        }
        memcpy(buf+pos, start, 64-pos);
        start += 64 - pos;
        round(buf);
        pos = 0;
        while(end - start >= 64)
        {
            round(start);
            start += 64;
        }
        memcpy(buf, start, end-start);
        pos = end-start;
    }
    virtual std::span<const char> close()
    {
        buf[pos] = 0x80;
        for(size_t i = pos + 1; i < 64; i++)
            buf[i] = 0;
        if(pos + 8 >= 64)
        {
            round(buf);
            for(size_t i = 0; i <= pos; i++)
                buf[i] = 0;
        }
        for(size_t i = 0; i < 8; i++)
            buf[56 + i] = (count * 8) >> (56-8*i);
        round(buf);
        for(size_t i = 0; i < 8; i++)
            for(size_t j = 0; j < 4; j++)
                buf[4*i+j] = h.h[i] >> (24-8*j);
        pos = 0;
        count = 0;
        h = constants.h;
        return std::span<const char>(buf, 32);
    }
};

}

namespace hash
{

hash::Hash* sha256()
{
    return new SHA256();
}

}
