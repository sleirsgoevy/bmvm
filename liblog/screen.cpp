#include "backend.hpp"
#include <cstring>
extern "C"
{
    #include <types.h>
    #include <mm.h>
}

namespace logging
{

struct ScreenBackend : LogBackend
{
    int cursorX = -1, cursorY = -1;
    uint16_t* b8000;
    ScreenBackend()
    {
        b8000 = (uint16_t*)mmap(0, 80 * 25 * 2, PROT_READ|PROT_WRITE, 0xb8000);
        getCursor();
    }
    ~ScreenBackend()
    {
        munmap(b8000, 80 * 25 * 2);
    }
    void getCursor()
    {
        uint8_t pos_low, pos_high;
        asm volatile("outb %%al, %%dx"::"a"((uint8_t)0x0f),"d"(0x3d4));
        asm volatile("inb %%dx, %%al":"=a"(pos_low):"d"(0x3d5));
        asm volatile("outb %%al, %%dx"::"a"((uint8_t)0x0e),"d"(0x3d4));
        asm volatile("inb %%dx, %%al":"=a"(pos_high):"d"(0x3d5));
        uint16_t pos = (uint16_t)pos_high << 8 | pos_low;
        cursorY = pos / 80;
        cursorX = pos % 80;
    }
    void setCursor()
    {
        uint16_t pos = cursorY * 80 + cursorX;
        asm volatile("outb %%al, %%dx"::"ba"((uint8_t)0x0f),"d"(0x3d4));
        asm volatile("outb %%al, %%dx"::"ba"((uint8_t)pos),"d"(0x3d5));
        asm volatile("outb %%al, %%dx"::"ba"((uint8_t)0x0e),"d"(0x3d4));
        asm volatile("outb %%al, %%dx"::"ba"((uint8_t)(pos >> 8)),"d"(0x3d5));
        asm volatile("outb %%al, %%dx"::"ba"((uint8_t)0x0d),"d"(0x3d4));
        asm volatile("outb %%al, %%dx"::"ba"((uint8_t)0),"d"(0x3d5));
        asm volatile("outb %%al, %%dx"::"ba"((uint8_t)0x0c),"d"(0x3d4));
        asm volatile("outb %%al, %%dx"::"ba"((uint8_t)0),"d"(0x3d5));
    }
    void scrollUp()
    {
        memcpy(b8000, b8000 + 80, 80 * 24 * 2);
        for(size_t i = 0; i < 80; i++)
            b8000[80*24+i] = 0x720;
        cursorY--;
    }
    void putchar(char c)
    {
        if(c == '\r')
            cursorX = 0;
        else if(c == '\n')
        {
            cursorX = 0;
            cursorY++;
            if(cursorY == 25)
                scrollUp();
        }
        else
        {
            b8000[80 * cursorY + cursorX] = 0x700 | (unsigned char)c;
            cursorX++;
            if(cursorX == 80)
            {
                cursorX = 0;
                cursorY++;
                if(cursorY == 25)
                    scrollUp();
            }
        }
        setCursor();
    }
};

LogBackend* openScreen()
{
    return new ScreenBackend();
}

}
