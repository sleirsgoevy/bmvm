#include <stream>
#include "backend.hpp"
#include <liblog/logging.hpp>
#include <cstring>

namespace logging
{

static LogBackend* bak;

class LogOutputStream : public std::ostream
{
    LogOutputStream& put(char c)
    {
        if(bak)
            bak->putchar(c);
        return *this;
    }
};

LogOutputStream logOutputStream;
std::ostream& log = logOutputStream;

}

int main(const char* cmdline)
{
    if(!strcmp(cmdline, "screen"))
        logging::bak = logging::openScreen();
    else if(!strcmp(cmdline, "e9"))
        logging::bak = logging::openE9();
    return 0;
}
