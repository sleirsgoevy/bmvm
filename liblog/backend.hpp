#pragma once

namespace logging
{

struct LogBackend
{
    virtual void putchar(char c) = 0;
};

LogBackend* openScreen();
LogBackend* openE9();

}
