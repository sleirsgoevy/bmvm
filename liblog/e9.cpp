#include "backend.hpp"

namespace logging
{

struct E9Backend : LogBackend
{
    void putchar(char c)
    {
        asm volatile("outb %0,$0xe9"::"r"(c));
    }
};

LogBackend* openE9()
{
    return new E9Backend();
}

}
