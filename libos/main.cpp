#include <vector>
#include <mutex>
#include <sstream>
#include <libos/hub.hpp>
#include <libboot/vm.hpp>
#include <libboot/memrw.hpp>
#include <libboot/hook.hpp>
#include <liblog/logging.hpp>
#include <libvm/vm.hpp>
#include <libsmp/ipi.hpp>
#include <sysregs.h>
extern "C"
{
    #include <elf.h>
}

struct OSDetector
{
    os::OS*(*fn)(void*);
    void* arg;
};

struct OSHandler
{
    void(*fn)(void*, os::OS*);
    void* arg;
};

static std::vector<OSDetector> detectors;
static std::vector<OSHandler> handlers;
static os::OS* currentOS;
static std::mutex mtx_detect;

void armDetector(void)
{
    static hooks::Hook* vcpuInitHook;
    static hooks::Hook* rdmsr_hook;
    static hooks::Hook* wrmsr_hook;
    static hooks::Hook* ud_hook;
    static std::vector<uint64_t> efer_read_shadow(smp::ncpus());
    constexpr uint64_t efer_mask = 1;
    vcpuInitHook = hooks::createVCPUInitHook([]
    {
        vm::VM* vcpu = boot::curVCPU();
        vcpu->msr_protect(IA32_EFER, false, false);
        vcpu->intercept_exception(6, true);
        return false;
    });
    rdmsr_hook = hooks::createHook(vm::ExitReason::RDMSR, hooks::ANY_PC, []
    {
        vm::VM* vcpu = boot::curVCPU();
        if((uint32_t)vcpu->get_reg(vm::Register::RCX) == (uint32_t)IA32_EFER)
        {
            uint64_t value = vcpu->rdmsr(IA32_EFER) | efer_read_shadow[smp::curcpu()];
            vcpu->set_reg(vm::Register::RAX, (uint32_t)value);
            vcpu->set_reg(vm::Register::RDX, value >> 32);
            vcpu->set_reg(vm::Register::RIP, vcpu->get_reg(vm::Register::RIP) + 2);
            return true;
        }
        return false;
    });
    wrmsr_hook = hooks::createHook(vm::ExitReason::WRMSR, hooks::ANY_PC, []
    {
        vm::VM* vcpu = boot::curVCPU();
        if((uint32_t)vcpu->get_reg(vm::Register::RCX) == (uint32_t)IA32_EFER)
        {
            uint32_t eax = vcpu->get_reg(vm::Register::RAX);
            uint32_t edx = vcpu->get_reg(vm::Register::RDX);
            uint64_t value = (uint64_t)edx << 32 | eax;
            efer_read_shadow[smp::curcpu()] = value & efer_mask;
            vcpu->wrmsr(IA32_EFER, value & ~efer_mask);
            vcpu->set_reg(vm::Register::RIP, vcpu->get_reg(vm::Register::RIP) + 2);
            return true;
        }
        return false;
    });
    ud_hook = hooks::createHook(vm::ExitReason::EXCEPTION, hooks::ANY_PC, []
    {
        //check if syscall or sysret
        vm::VM* vcpu = boot::curVCPU();
        bool has_error_code;
        uint32_t error_code;
        int vector = vcpu->get_exception(has_error_code, error_code);
        if(vector != 6)
            return false;
        char instr[2];
        utils::copy_from_guest_vm(instr, vcpu->get_reg(vm::Register::RIP), 2);
        if(instr[0] != 15 || (instr[1] != 5 /* syscall */ && instr[1] != 7 /* sysret */))
            return false;
        //only run detector on first occurrence
        static uintptr_t guard = 0;
        if(!cmpxchg(&guard, 0, 1))
            return true;
        os::OS* os = os::getOS();
        mtx_detect.lock();
        //copy handlers vector to avoid nasty deadlocks
        std::vector<OSHandler> handlers_copy = handlers;
        mtx_detect.unlock();
        //uninstall the hook
        for(int i = 0; i < smp::ncpus(); i++)
            if(i != smp::curcpu())
                smp::send_ipi(i, []
                {
                    vm::VM* vcpu = boot::curVCPU();
                    vcpu->wrmsr(IA32_EFER, vcpu->rdmsr(IA32_EFER) | efer_read_shadow[smp::curcpu()]);
                    vcpu->msr_protect(IA32_EFER, true, true);
                    vcpu->intercept_exception(6, false);
                });
        vcpu->wrmsr(IA32_EFER, vcpu->rdmsr(IA32_EFER) | efer_read_shadow[smp::curcpu()]);
        vcpu->msr_protect(IA32_EFER, true, true);
        vcpu->intercept_exception(6, false);
        hooks::removeHook(rdmsr_hook);
        hooks::removeHook(wrmsr_hook);
        hooks::removeHook(ud_hook);
        //now run the handlers
        for(const OSHandler& i : handlers_copy)
            i.fn(i.arg, os);
        return true;
    });
}

namespace os
{

void addOSDetector(OS*(*fn)(void*), void* arg)
{
    mtx_detect.lock();
    detectors.push_back(OSDetector{fn, arg});
    mtx_detect.unlock();
}

void addOSHandler(void(*fn)(void*, OS*), void* arg)
{
    mtx_detect.lock();
    if(currentOS)
        fn(arg, currentOS);
    handlers.push_back(OSHandler{fn, arg});
    mtx_detect.unlock();
}

OS* getOS()
{
    mtx_detect.lock();
    if(currentOS)
    {
        OS* ans = currentOS;
        mtx_detect.unlock();
        return ans;
    }
    for(const OSDetector& i : detectors)
    {
        OS* q = i.fn(i.arg);
        if(q)
        {
            currentOS = q;
            break;
        }
    }
    mtx_detect.unlock();
    return currentOS;
}

void unsetOS()
{
    mtx_detect.lock();
    if(currentOS)
    {
        delete currentOS;
        currentOS = nullptr;
        armDetector();
    }
    mtx_detect.unlock();
}

}

int main(const char* cmdline)
{
    std::istringstream s(cmdline);
    std::string lib;
    while(s >> lib)
    {
        std::string* name = new std::string("libos_" + lib + ".so");
        struct module* mod = load_elf(0, name->c_str());
        if(!mod)
        {
            delete name;
            logging::log << "osdetect: failed to load libos_" << lib << std::endl;
            return 1;
        }
        auto mod_main = (int(*)(const char*))elf_dlsym_module(mod, "main");
        if(mod_main)
        {
            int ans = mod_main("osdetect");
            if(ans)
            {
                logging::log << "osdetect: libos_" << lib << " returned " << ans << std::endl;
                return 1;
            }
        }
    }
    armDetector();
    return 0;
}
