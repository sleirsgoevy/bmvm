#include <vector>
#include <libos/coroutine.hpp>
#include <libsmp/ipi.hpp>
extern "C"
{
    #include <mm.h>
    #include <setjmp.h>
}

namespace coroutine
{

class Coroutine
{
public:
    jmp_buf out_state;
    jmp_buf self_state;
    void* stack;
    size_t stack_size;
};

static std::vector<Coroutine*> selves(smp::ncpus());

Coroutine* self(void)
{
    return selves[smp::curcpu()];
}

static void call(void* arg, void(*fn)(void*))
{
    fn(arg);
    terminate();
}

bool start(void(*fn)(void*), void* arg, size_t stack_size)
{
    char* stack = (char*)mmap(0, stack_size, PROT_READ|PROT_WRITE|PROT_NOEXEC, MAP_ANON);
    if(!stack)
        return false;
    int cpu = smp::curcpu();
    Coroutine* parent = selves[cpu];
    Coroutine* self = selves[cpu] = new Coroutine;
    self->stack = stack;
    self->stack_size = stack_size;
    int state = setjmp(self->out_state);
    if(state)
    {
        if(state == 2)
        {
            munmap(stack, stack_size);
            delete self;
        }
        selves[cpu] = parent;
        return true;
    }
    asm volatile("mov %0, %%rsp\ncall *%1"::"r"(stack+stack_size),"r"(call),"D"(arg),"S"(fn));
    //unreachable
    return false;
}

void resume(Coroutine* co)
{
    int cpu = smp::curcpu();
    Coroutine* parent = selves[cpu];
    selves[cpu] = co;
    int state = setjmp(co->out_state);
    if(state)
    {
        if(state == 2)
        {
            munmap(co->stack, co->stack_size);
            delete co;
        }
        selves[cpu] = parent;
        return;
    }
    longjmp(co->self_state, 1);
}

void yield(void)
{
    Coroutine* self = selves[smp::curcpu()];
    if(setjmp(self->self_state))
        return;
    longjmp(self->out_state, 1);
}

void terminate(void)
{
    Coroutine* self = selves[smp::curcpu()];
    longjmp(self->out_state, 2);
}

}
