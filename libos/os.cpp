#include <libos/os.hpp>
#include <libboot/vm.hpp>
#include <libboot/memrw.hpp>
#include <sysregs.h>

namespace os
{

static uint64_t get_seg_base(vm::VM* vcpu, vm::Register seg)
{
    vm::GDTEntry gdt_entry;
    vcpu->get_segment(seg, gdt_entry);
    uint64_t base = (uint32_t)gdt_entry.data[2] | (uint32_t)gdt_entry.data[3] << 8 | (uint32_t)gdt_entry.data[4] << 16;
    for(int i = 3; i < 8; i++)
        base |= (uint64_t)gdt_entry.data[i+4] << (i << 3);
    return base;
}

void OS::enter_kernel_mode(KernelMode& km)
{
    vm::VM* vcpu = boot::curVCPU();
    for(int i = 0; i < 16; i++)
        km.regs[i] = vcpu->get_reg((vm::Register)i);
    km.rip = vcpu->get_reg(vm::Register::RIP);
    km.rflags = vcpu->get_reg(vm::Register::RFLAGS);
    km.cs = vcpu->get_segment(vm::Register::CS, km.cs_segment);
    km.ss = vcpu->get_segment(vm::Register::SS, km.ss_segment);
    for(int i = 0; i < 16; i++)
        vcpu->set_reg((vm::Register)i, 0);
    if((km.cs & 3)) //usermode
    {
        uintptr_t gs_base = vcpu->rdmsr(IA32_GS_BASE);
        uintptr_t kernel_gs_base = vcpu->rdmsr(IA32_KERNEL_GS_BASE);
        vcpu->wrmsr(IA32_GS_BASE, kernel_gs_base);
        vcpu->wrmsr(IA32_KERNEL_GS_BASE, gs_base);
        uint64_t idt_base = get_seg_base(vcpu, vm::Register::IDT);
        uint64_t tss = get_seg_base(vcpu, vm::Register::TR);
        unsigned char idt_entry[16] = {0};
        utils::copy_from_guest_vm(&idt_entry, idt_base+6*16, 16);
        vm::GDTEntry kernel_cs = {{0xff, 0xff, 0, 0, 0, 0x9b, 0xaf, 0}};
        vcpu->set_segment(vm::Register::CS, (uint16_t)idt_entry[3] << 8 | idt_entry[2], kernel_cs);
        vm::GDTEntry kernel_ss = {{0xff, 0xff, 0, 0, 0, 0x93, 0xcf, 0}};
        vcpu->set_segment(vm::Register::SS, 0, kernel_ss);
        uint64_t krsp = 0;
        utils::copy_from_guest_vm(&krsp, tss+4, sizeof(krsp));
        vcpu->set_reg(vm::Register::RSP, krsp - 128);
    }
    vcpu->set_reg(vm::Register::RFLAGS, 514); //IF
}

void OS::exit_kernel_mode(const KernelMode& km)
{
    vm::VM* vcpu = boot::curVCPU();
    for(int i = 0; i < 16; i++)
        vcpu->set_reg((vm::Register)i, km.regs[i]);
    vcpu->set_reg(vm::Register::RIP, km.rip);
    vcpu->set_reg(vm::Register::RFLAGS, km.rflags);
    vcpu->set_segment(vm::Register::CS, km.cs, km.cs_segment);
    vcpu->set_segment(vm::Register::SS, km.ss, km.ss_segment);
    if((km.cs & 3))
    {
        uintptr_t gs_base = vcpu->rdmsr(IA32_KERNEL_GS_BASE);
        uintptr_t kernel_gs_base = vcpu->rdmsr(IA32_GS_BASE);
        vcpu->wrmsr(IA32_GS_BASE, gs_base);
        vcpu->wrmsr(IA32_KERNEL_GS_BASE, kernel_gs_base);
    }

}

}
