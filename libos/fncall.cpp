#include <cstring>
#include <libos/coroutine.hpp>
#include <libos/fncall.hpp>
#include <libboot/memrw.hpp>
#include <libboot/hook.hpp>
#include <libvm/vm.hpp>
#include <sysregs.h>

namespace fncall
{

bool call_raw(uintptr_t fn_addr, uint64_t regs[16])
{
    coroutine::Coroutine* self = coroutine::self();
    if(!self)
        return false;
    vm::VM* vcpu = boot::curVCPU();
    //save gp regs
    uint64_t old_regs[16];
    for(int i = 0; i < 16; i++)
        old_regs[i] = vcpu->get_reg((vm::Register)i);
    uint64_t rflags = vcpu->get_reg(vm::Register::RFLAGS);
    //set up stack
    uintptr_t rsp = old_regs[(int)vm::Register::RSP];
    rsp -= 16;
    rsp -= rsp % 4096;
    rsp += 8;
    uintptr_t fake_ret = 0x1234;
    uint64_t phys, limit;
    bool u, w, nx;
    if(!utils::guest_vm_translate(rsp, phys, limit, u, w, nx) || !w)
        return false; //invalid stack
    uint64_t phys2, limit2;
    bool u2, w2, nx2;
    regs[(int)vm::Register::RSP] = rsp;
    utils::copy_to_guest(phys, &fake_ret, 8);
    phys &= PG_ADDR_MASK_4KB;
    bool vm_r, vm_w, vm_x;
    int mem_type;
    uint64_t host_phys = vcpu->translate(phys, vm_r, vm_w, vm_x, mem_type);
    if(!vm_r && !vm_w && !vm_x)
        return false;
    //save fp regs
    uint32_t cpuid_ans[4];
    cpuid(13, cpuid_ans);
    char xsave_area[cpuid_ans[1]+63];
    memset(xsave_area, 0, sizeof(xsave_area));
    uintptr_t xsave = (uintptr_t)xsave_area;
    xsave = (xsave + 63) & ~63ull;
    uint32_t xsave_eax, xsave_edx;
    asm volatile("xgetbv":"=a"(xsave_eax),"=d"(xsave_edx):"c"(0));
    asm volatile("xsave (%2)"::"a"(xsave_eax),"d"(xsave_edx),"r"(xsave):"memory");
    asm volatile("mov %%cr0, %%rax\nand $0xf3, %%al\nmov %%rax, %%cr0\nfninit\nbts $2, %%rax\nmov %%rax, %%cr0":::"rax");
    //copy arguments
    for(int i = 0; i < 16; i++)
        vcpu->set_reg((vm::Register)i, regs[i]);
    vcpu->set_reg(vm::Register::RFLAGS, 514); // IF, TODO: copy current flags
    uint64_t old_rip = vcpu->get_reg(vm::Register::RIP);
    vcpu->set_reg(vm::Register::RIP, fn_addr);
    //install hook
    vcpu->unmap_pages(phys, phys + 4096);
    vcpu->map_pages(phys, phys + 4096, host_phys, false, false, false, 6);
    hooks::Hook* pf_hook = hooks::createHook(vm::ExitReason::PAGE_FAULT, hooks::ANY_PC, [=]()
    {
        vm::VM* vcpu = boot::curVCPU();
        uint64_t phys2 = vcpu->get_faulting_address();
        vm::CrashType ct = vcpu->get_fault_reason();
        if((phys2 & PG_ADDR_MASK_4KB) != phys)
            return false;
        if(ct == vm::CrashType::READ)
        {
            unsigned char instr;
            if(utils::copy_from_guest_vm(&instr, vcpu->get_reg(vm::Register::RIP), 1) && (instr & 0xf6) == 0xc2 && vcpu->get_reg(vm::Register::RSP) == rsp) //return
            {
                vcpu->unmap_pages(phys, phys + 4096);
                vcpu->map_pages(phys, phys + 4096, host_phys, vm_r, vm_w, vm_x, mem_type);
                coroutine::resume(self);
                return true;
            }
        }
        void* map;
        if(!vcpu->temp_remap_lock(phys, map, phys, map, host_phys, host_phys, vm_r, vm_w, vm_x))
            return false;
        bool ans = vcpu->execute(true) == vm::ExitReason::SINGLESTEP;
        vcpu->temp_remap_unlock();
        return ans;
    });
    //TODO: what if another core returns on that stack before yield?
    coroutine::yield();
    vcpu = boot::curVCPU(); //we could've been rescheduled
    //restore fp regs
    asm volatile("xrstor (%2)"::"a"(xsave_eax),"d"(xsave_edx),"r"(xsave));
    //save results & restore gp regs
    vcpu->set_reg(vm::Register::RIP, old_rip);
    for(int i = 0; i < 16; i++)
    {
        regs[i] = vcpu->get_reg((vm::Register)i);
        vcpu->set_reg((vm::Register)i, old_regs[i]);
    }
    vcpu->set_reg(vm::Register::RFLAGS, rflags);
    return true;
}

}
