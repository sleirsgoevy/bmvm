section .text

global smp_boot_start
global smp_boot_end
global smp_entry

use16
smp_boot_start:
dq 0x06eb
cli
jmp far 0:(entry-smp_boot_start+0x8000)
entry:
xor ax, ax
mov ds, ax
mov es, ax
mov ss, ax
mov fs, ax
mov gs, ax
mov ax, 0x8003
mov [0x8000], ax ; loop to ourselves
lgdt [gdtr + 0x8000 - smp_boot_start]
mov eax, cr0
or al, 1
mov cr0, eax
jmp far 8:(entry32 + 0x8000 - smp_boot_start)

gdtr:
dw 39
dq 0x80d8

times 64 - ($ - smp_boot_start) db 0

dq 0x8003 ; idmap our page

use32
entry32:
mov ax, 16
mov ds, ax
mov es, ax
mov ss, ax
mov fs, ax
mov gs, ax
mov eax, cr4
bts eax, 5
mov cr4, eax
mov ecx, 0xc0000080
rdmsr
bts eax, 8
wrmsr
mov eax, 0x8000
mov cr3, eax
mov eax, cr0
or eax, 0x80010020
mov cr0, eax
jmp far 24:(entry64 + 0 - smp_boot_start) ; we're also mapped at 0, lets abuse it

use64
entry64
lidt [rel smp_boot_start + 0xbe]
mov rax, [rel smp_boot_start + 0xd0]
mov rcx, [rel smp_boot_start + 0xc8]
lgdt [rel smp_boot_start + 0xae]
mov rsp, rcx
mov cr3, rax ; page #0 no longer mapped, page fault into libsmp
ud2

smp_boot_end:

smp_entry:
mov rsp, rcx
mov eax, [rsp]
mov edx, [rsp+4]
mov ecx, 0xc0000100 ; IA32_FS_BASE
wrmsr
mov qword [rsp+64], 1
.wait_for_confirm:
cmp qword [rsp+64], 1
jz .wait_for_confirm
lidt [rsp+8]
add rsp, 24
mov eax, 5
cpuid
mov edi, ebx
iretq
