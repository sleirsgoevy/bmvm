#include <vector>
#include <cstring>
#include <pair>
#include <mutex>
#include <libsmp/ipi.hpp>
#include <libsmp/acpi.hpp>
#include <liblog/logging.hpp>
#include <sysregs.h>
#include "interrupts.hpp"
extern "C"
{
    #include <mm.h>
    #include <assert.h>
}

namespace smp
{

bool disable_smp = false;

static uint64_t apic_address = 0;
static volatile uint32_t* apic;
static std::vector<uint32_t> cpus;
static std::vector<std::vector<std::pair<void(*)(void*), void*> > > async_ipi;
static std::vector<size_t> async_ipi_size;
static std::vector<std::mutex> async_ipi_lock;
static std::vector<uintptr_t> sync_ipi;

//WARNING: offsets into this struct are referenced from inline assembly
struct PerCpu
{
    uint64_t curcpu;
    uint64_t nmi_count;
};
static std::vector<PerCpu> percpu;

static uint64_t stub_idt[512];

extern "C"
{
extern char smp_boot_start[];
extern char smp_boot_end[];
void smp_entry(void);
void smp_nmi_handler(void);
volatile uintptr_t* smp_sync_ipi;
}

int curcpu(void)
{
    uint64_t ans;
    asm volatile("movq %%fs:0, %0":"=r"(ans));
    return ans;
}

uint64_t pull_nmis(void)
{
    uint64_t ans;
    asm volatile("movq %%fs:8, %0\nlock subq %0, %%fs:8":"=r"(ans));
    return ans;
}

static inline void do_send_ipi(uint32_t high, uint32_t low)
{
    if((rdmsr(IA32_APIC_BASE) & 0x400)) // x2APIC
        wrmsr(IA32_X2APIC_ICR, (uint64_t)high << 32 | low);
    else
    {
        while((apic[192] & 4096));
        apic[196] = high << 24;
        apic[192] = low;
        while((apic[192] & 4096));
    }
}

static inline void wait()
{
    uint64_t t0 = rdtsc();
    while(rdtsc() < t0 + 10000000);
}

static bool fetch_data(void)
{
    static bool done = false;
    if(done)
        return true;
    uint64_t rsdp = get_rsdp();
    if(!rsdp)
        return false;
    uint64_t madt = get_madt(rsdp);
    if(!madt)
        return false;
    apic_address = get_apic_addr(madt);
    cpus = get_cpus(madt);
    done = true;
    return true;
}

int ncpus()
{
    if(disable_smp)
        return 1;
    fetch_data();
    return cpus.size();
}

static void do_tlb_shootdown(void* arg)
{
    uint64_t tmp;
    asm volatile("mov %%cr3, %0\nmov %0, %%cr3":"=r"(tmp));
}

static void tlb_shootdown()
{
    for(int i = 0; i < ncpus(); i++)
        if(i != curcpu())
            send_ipi(i, do_tlb_shootdown, nullptr, true);
}

bool bring_up_cpus(void(*callback)(void), void* stacks0, size_t stack_size, int& boot_cpu)
{
    uint64_t p_iret = (uint64_t)&smp_nmi_handler;
    stub_idt[4] = (p_iret & 0xffff) | (p_iret & 0xffff0000) << 32 | 0x8e0000180000;
    stub_idt[5] = p_iret >> 32;
    setup_trap_fatal(stub_idt);
    char stub_lidt[10];
    *reinterpret_cast<uint16_t*>(stub_lidt) = sizeof(stub_idt) - 1;
    *reinterpret_cast<uint64_t*>(stub_lidt+2) = (uintptr_t)stub_idt;
    asm volatile("lidt %0"::"m"(stub_lidt));
    if(disable_smp)
    {
        boot_cpu = 0;
        return true;
    }
    uint32_t regs[4];
    cpuid(0, regs);
    if(regs[0] < 11)
        return false;
    cpuid(11, regs);
    uint32_t local_apic_id = regs[2];
    char* stacks = (char*)stacks0;
    if(!fetch_data() || !apic_address)
        return false;
    apic = (volatile uint32_t*)mmap(0, 4096, PROT_READ|PROT_WRITE|PROT_NOEXEC|PG_NOCACHE, apic_address);
    if(apic == MAP_FAILED)
    {
        apic = 0;
        apic_address = 0;
        return false;
    }
    if(cpus.empty())
    {
        munmap((void*)apic, 4096);
        apic = 0;
        apic_address = 0;
        return false;
    }
    boot_cpu = -1;
    for(size_t i = 0; i < cpus.size(); i++)
        if(cpus[i] == local_apic_id)
        {
            boot_cpu = i;
            break;
        }
    if(boot_cpu < 0)
        return false;
    async_ipi.resize(cpus.size());
    async_ipi_size.resize(cpus.size());
    async_ipi_lock.resize(cpus.size());
    sync_ipi.resize(3*cpus.size());
    smp_sync_ipi = &sync_ipi[0];
    percpu.resize(cpus.size());
    for(int i = 0; i < cpus.size(); i++)
    {
        percpu[i].curcpu = i;
        percpu[i].nmi_count = 0;
    }
    set_shootdown_hook(tlb_shootdown);
    char* page8000 = (char*)mmap(0, 4096, PROT_READ|PROT_WRITE|PROT_NOEXEC, 0x8000);
    memcpy(page8000, smp_boot_start, smp_boot_end-smp_boot_start);
    uint64_t p_smp_entry = (uintptr_t)&smp_entry;
    uint64_t idt[15][2] = {0};
    idt[14][0] = (p_smp_entry & 0xffff) | (p_smp_entry & 0xffff0000) << 32 | 0x8e0000180000;
    idt[14][1] = p_smp_entry >> 32;
    asm volatile("sgdt (%0)"::"r"(page8000+0xae):"memory");
    *reinterpret_cast<uint16_t*>(page8000+0xbe) = sizeof(idt) - 1;
    *reinterpret_cast<uint64_t*>(page8000+0xc0) = (uintptr_t)idt;
    asm volatile("mov %%cr3, %0":"=r"(*reinterpret_cast<uint64_t*>(page8000+0xd0)));
    memcpy(page8000+0xd8, *reinterpret_cast<void**>(page8000+0xb0), 40);
    PerCpu* pcpu = &percpu[0];
    for(uint32_t apic_id : cpus)
    {
        uint64_t fs_base = (uintptr_t)(pcpu++);
        if(apic_id == local_apic_id)
        {
            wrmsr(IA32_FS_BASE, fs_base);
            continue;
        }
        stacks += stack_size;
        *reinterpret_cast<uint64_t*>(stacks-72) = fs_base;
        *reinterpret_cast<uint16_t*>(stacks-64) = sizeof(stub_idt) - 1;
        *reinterpret_cast<uint64_t*>(stacks-62) = (uintptr_t)stub_idt;
        *reinterpret_cast<uint64_t*>(stacks-48) = (uintptr_t)callback;
        *reinterpret_cast<uint64_t*>(stacks-40) = 0x18; //cs
        *reinterpret_cast<uint64_t*>(stacks-32) = 2; //eflags
        *reinterpret_cast<void**>(stacks-24) = stacks-8; //rsp
        *reinterpret_cast<uint64_t*>(stacks-16) = 0x10; //ss
        *reinterpret_cast<void**>(page8000+0xc8) = stacks-72;
        asm volatile("":::"memory");
        do_send_ipi(apic_id, 0x4500);
        wait();
        do_send_ipi(apic_id, 0x4608);
        wait();
        if(!*reinterpret_cast<volatile uint64_t*>(stacks-8))
        {
            do_send_ipi(apic_id, 0x4608);
            wait();
        }
        assert(*reinterpret_cast<volatile uint64_t*>(stacks-8));
        *reinterpret_cast<volatile uint64_t*>(stacks-8) = 2;
    }
    return true;
}

static bool check_for_ipis()
{
    int cpu = curcpu();
    if(smp_sync_ipi[3*cpu+2] == 2)
    {
        uint64_t fn = smp_sync_ipi[3*cpu];
        uint64_t arg = smp_sync_ipi[3*cpu+1];
        if(fn == 1)
            asm volatile("lock incq %%fs:8":::"memory");
        else
            ((void(*)(void*))fn)((void*)arg);
        cmpxchg(&smp_sync_ipi[3*cpu+2], 2, 3);
        return true;
    }
    async_ipi_lock[cpu].lock();
    if(async_ipi_size[cpu])
    {
        auto data = async_ipi[cpu][--async_ipi_size[cpu]];
        async_ipi_lock[cpu].unlock();
        data.first(data.second);
        return true;
    }
    async_ipi_lock[cpu].unlock();
    return false;
}

void send_ipi(int which, void(*callback)(void*), void* arg, bool sync)
{
    if(sync)
    {
        while(!cmpxchg(&smp_sync_ipi[3*which+2], 0, 1));
        cmpxchg(&smp_sync_ipi[3*which+1], 0, (uintptr_t)arg);
        cmpxchg(&smp_sync_ipi[3*which], 0, (uintptr_t)callback);
        cmpxchg(&smp_sync_ipi[3*which+2], 1, 2);
        uint64_t start = rdtsc();
        int done = 0;
        //we sometimes lose nmis (very rare)
        do
        {
            do_send_ipi(cpus[which], 0x4402);
            while(/*rdtsc() - start < 10000000 && */!(done = cmpxchg(&smp_sync_ipi[3*which+2], 3, 4)));
        }
        while(!done);
        cmpxchg(&smp_sync_ipi[3*which+1], (uintptr_t)arg, 0);
        cmpxchg(&smp_sync_ipi[3*which], (uintptr_t)callback, 0);
        cmpxchg(&smp_sync_ipi[3*which+2], 4, 0);
    }
    else
    {
        async_ipi_lock[which].lock();
        std::vector<std::pair<void(*)(void*), void*> >& vec = async_ipi[which];
        size_t& sz = async_ipi_size[which];
        std::pair<void(*)(void*), void*> data = {callback, arg};
        if(sz == vec.size())
        {
            sz++;
            vec.push_back(data);
        }
        else
            vec[sz++] = data;
        async_ipi_lock[which].unlock();
        send_ipi(which, (void(*)(void*))1, nullptr, true);
    }
}

extern "C" int ipi_isr(void)
{
    return check_for_ipis() ? 1 : 0;
}

struct StubFS
{
    PerCpu payload = {0};
    StubFS()
    {
        wrmsr(IA32_FS_BASE, (uintptr_t)&payload);
    }
} stub_fs;

}
