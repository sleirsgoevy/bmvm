#include <libsmp/acpi.hpp>
extern "C"
{
    #include <mm.h>
    #include <bootproto.h>
}

namespace smp
{

static inline unsigned char checksum(void* p, size_t sz)
{
    unsigned char* pp = reinterpret_cast<unsigned char*>(p);
    unsigned char ans = 0;
    for(size_t i = 0; i < sz; i++)
        ans += pp[i];
    return ans;
}

constexpr uint64_t RSD_PTR = 'R' | 'S' << 8 | 'D' << 16 | ' ' << 24 | (uint64_t)('P' | 'T' << 8 | 'R' << 16 | ' ' << 24) << 32;
constexpr uint32_t XSDT = 'X' | 'S' << 8 | 'D' << 16 | 'T' << 24;
constexpr uint32_t RSDT = 'R' | 'S' << 8 | 'D' << 16 | 'T' << 24;
constexpr uint32_t APIC = 'A' | 'P' << 8 | 'I' << 16 | 'C' << 24;

uint64_t get_rsdp(void)
{
    //if the bootloader gave us rsdp, prefer that
    //necessary to support uefi
    uint64_t bp_rsdp = get_bootproto_rsdp();
    if(bp_rsdp)
        return bp_rsdp;
    //map dos-mode memory
    char* mem = (char*)mmap(0, 1048576, PROT_READ|PROT_WRITE|PROT_NOEXEC, 0);
    //search ebda for rsdp
    size_t ebda = *reinterpret_cast<uint16_t*>(mem + 0x40e);
    ebda <<= 4;
    for(size_t i = ebda; i < 1048556 && i - ebda < 1024; i += 8)
        if(*reinterpret_cast<uint64_t*>(mem + i) == RSD_PTR && !checksum(mem + i, 20))
        {
            munmap(mem, 1048576);
            return i;
        }
    //search rom for rsdp
    for(size_t i = 0xe0000; i < 1048556; i += 8)
        if(*reinterpret_cast<uint64_t*>(mem + i) == RSD_PTR && !checksum(mem + i, 20))
        {
            munmap(mem, 1048576);
            return i;
        }
    munmap(mem, 1048576);
    return 0;
}

static inline void* map_struct(uint64_t ptr, size_t sz)
{
    size_t offset = ptr & 0xfff;
    return (char*)mmap(0, sz + offset, PROT_READ|PROT_WRITE|PROT_NOEXEC, ptr - offset) + offset;
}

static inline void unmap_struct(void* ptr, size_t sz)
{
    uintptr_t p = (uintptr_t)ptr;
    size_t offset = p & 0xfff;
    return munmap((void*)(p - offset), sz + offset);
}

struct rsdp_struct
{
    uint64_t sig;
    uint8_t cksum;
    char oemid[6];
    uint8_t rev;
    uint32_t rsdp;
    uint32_t length;
    uint64_t xsdp;
    uint8_t cksum2;
    uint8_t res[3];
};

struct table_header
{
    uint32_t sig;
    uint32_t len;
    uint8_t rev;
    uint8_t cksum;
    char oemid[6];
    char oemtbl[8];
    uint32_t oemrev;
    uint32_t cr_id;
    uint32_t cr_rev;
    uint32_t end;
};

struct madt
{
    uint32_t local_apic;
    uint32_t flags;
};

struct madt_record
{
    uint8_t type;
    uint8_t length;
};

struct madt_lapic
{
    struct madt_record header;
    uint8_t acpi_id;
    uint8_t apic_id;
    uint32_t flags;
};

struct madt_x2apic
{
    struct madt_record header;
    uint16_t res;
    uint32_t apic_id; 
    uint32_t flags;
    uint32_t acpi_id;
};

template<class ptr_type, uint32_t sig>
uint64_t get_madt_from(uint64_t xsdp)
{
    table_header* p2 = (table_header*)map_struct(xsdp, 36);
    if(p2->sig != sig || p2->len < 36)
    {
        unmap_struct(p2, 36);
        return 0;
    }
    uint32_t len = p2->len;
    p2 = (table_header*)map_struct(xsdp, len);
    if(checksum(p2, len))
    {
        unmap_struct(p2, len);
        return 0;
    }
    ptr_type* arr = reinterpret_cast<ptr_type*>(&p2->end);
    size_t cnt = (len - 36) / sizeof(ptr_type);
    for(size_t i = 0; i < cnt; i++)
    {
        table_header* tbl = (table_header*)map_struct(arr[i], 36);
        if(tbl->sig != APIC || tbl->len < 36)
        {
            unmap_struct(tbl, 36);
            continue;
        }
        uint32_t len2 = tbl->len;
        unmap_struct(tbl, 36);
        tbl = (table_header*)map_struct(arr[i], len2);
        if(checksum(tbl, len2))
        {
            unmap_struct(tbl, len2);
            continue;
        }
        unmap_struct(tbl, len2);
        uint64_t ans = arr[i];
        unmap_struct(p2, len);
        return ans;
    }
    unmap_struct(p2, len);
    return 0;
}

uint64_t get_madt(uint64_t rsdp)
{
    rsdp_struct* p = (rsdp_struct*)map_struct(rsdp, 36);
    if(p->rev == 2)
    {
        if(checksum(reinterpret_cast<char*>(p) + 20, 16))
            return 0;
        uint64_t xsdp = p->xsdp;
        unmap_struct(p, 36);
        return get_madt_from<uint64_t, XSDT>(xsdp);
    }
    else
    {
        uint64_t rsdp = p->rsdp;
        unmap_struct(p, 36);
        return get_madt_from<uint32_t, RSDT>(rsdp);
    }
}

uint64_t get_apic_addr(uint64_t madp)
{
    table_header* th = (table_header*)map_struct(madp, 36);
    uint32_t len = th->len;
    unmap_struct(th, 36);
    th = (table_header*)map_struct(madp, len);
    char* p1 = (char*)th + 36;
    char* p2 = (char*)th + len;
    madt* p3 = (madt*)p1;
    uint64_t lapic = p3->local_apic;
    p1 = (char*)(p3 + 1);
    while(p1 < p2)
    {
        madt_record* rec = (madt_record*)p1;
        p1 += rec->length;
        if(rec->type == 5)
            lapic = *reinterpret_cast<uint64_t*>(rec+1);
    }
    unmap_struct(th, len);
    return lapic;
}

std::vector<uint32_t> get_cpus(uint64_t madp)
{
    table_header* th = (table_header*)map_struct(madp, 36);
    uint32_t len = th->len;
    unmap_struct(th, 36);
    th = (table_header*)map_struct(madp, len);
    char* p1 = (char*)th + 36;
    char* p2 = (char*)th + len;
    madt* p3 = (madt*)p1;
    std::vector<uint32_t> ans;
    p1 = (char*)(p3 + 1);
    while(p1 < p2)
    {
        madt_record* rec = (madt_record*)p1;
        p1 += rec->length;
        if(rec->type == 0)
        {
            madt_lapic* x = reinterpret_cast<madt_lapic*>(rec);
            if((x->flags & 3))
                ans.push_back(x->apic_id);
        }
        else if(rec->type == 9)
        {
            madt_x2apic* x = reinterpret_cast<madt_x2apic*>(rec);
            if((x->flags & 3))
                ans.push_back(x->apic_id);
        }
    }
    unmap_struct(th, len);
    return ans;
}

}
