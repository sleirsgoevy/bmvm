#include <types.h>
#include <liblog/logging.hpp>
extern "C"
{
    #include <elf.h>
}
#include "interrupts.hpp"

asm(
    "page_fault_handler:\n"
    //save registers
    "push %rax\n"
    "push %rcx\n"
    "push %rdx\n"
    "push %rbx\n"
    "push %rsp\n"
    "push %rbp\n"
    "push %rsi\n"
    "push %rdi\n"
    "push %r8\n"
    "push %r9\n"
    "push %r10\n"
    "push %r11\n"
    "push %r12\n"
    "push %r13\n"
    "push %r14\n"
    "push %r15\n"
    "mov %rsp, %rdi\n"
    //stack alignment
    "or $15, %rsp\n"
    "xor $15, %rsp\n"
    //call real handler
    "call real_pf_handler\n"
);

namespace smp
{

extern "C" void page_fault_handler(void);

static void print_rip(uintptr_t rip)
{
    logging::log << "0x" << rip;
    const char* name;
    void* base_addr;
    struct module* mod = elf_dladdr((void*)rip, &name, &base_addr);
    if(mod)
        logging::log << "(" << name << "+0x" << (rip - (uintptr_t)base_addr) << ")";
}

extern "C" void real_pf_handler(uint64_t* trap_frame)
{
    uint64_t* saved_regs = trap_frame;
    trap_frame += 16;
    static int in_pf = 0;
    if(in_pf)
    {
        //crashed while crashing, just hang up
        for(;;) asm volatile("");
    }
    in_pf = 1;
    int vector;
    uint64_t error_code = 0;
    uint64_t rip;
    if(trap_frame[1] >= 0xb3000 && trap_frame[1] < 0xb3100) //phony handlers
    {
        vector = trap_frame[1] & 255;
        trap_frame = (uint64_t*)trap_frame[4];
        if(vector == 8 || (vector >= 10 && vector <= 14) || vector == 17 || vector == 30)
            error_code = *trap_frame++;
        rip = trap_frame[0];
        saved_regs[11] = trap_frame[3];
    }
    else
    {
        vector = 14;
        error_code = trap_frame[0];
        rip = trap_frame[1];
        saved_regs[11] = trap_frame[4];
    }
    //hope for the best, state may be too fucked up
    logging::log << "Exception " << vector << ":" << std::hex;
    logging::log << " RIP=";
    print_rip(rip);
    logging::log << " error_code=0x" << error_code << " ";
    if(vector == 14)
    {
        uint64_t cr2;
        asm volatile("mov %%cr2, %0":"=r"(cr2));
        logging::log << "CR2=0x" << cr2 << " ";
    }
    logging::log << std::endl;
    for(int j = 0; j < 16; j += 4)
    {
        for(int i = j; i < j + 4; i++)
            logging::log << "R" << std::dec << i << std::hex << " = 0x" << saved_regs[15-i] << " ";
        logging::log << std::endl;
    }
    struct stack_frame
    {
        volatile struct stack_frame* next;
        uintptr_t rip;
    };
    logging::log << "Backtrace: " << std::endl;
    volatile struct stack_frame* fr = (struct stack_frame*)saved_regs[10];
    do
    {
        print_rip(fr->rip);
        logging::log << " at 0x" << (uintptr_t)fr << ", ";
    }
    while(fr->next > fr && (fr = fr->next));
    for(;;) asm volatile("");
}

void setup_trap_fatal(uint64_t* idt)
{
    for(int i = 0; i < 256; i++)
        if(i != 2 && i != 14)
        {
            idt[2*i] = 0xb8e0000183000+i;
            idt[2*i+1] = 0;
        }
    uint64_t p_page_fault = (uintptr_t)&page_fault_handler;
    idt[28] = (p_page_fault & 0xffff) | (p_page_fault & 0xffff0000) << 32 | 0x8e0000180000;
    idt[29] = p_page_fault >> 32;
}

}
