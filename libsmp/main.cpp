#include <libsmp/ipi.hpp>
#include <liblog/logging.hpp>
extern "C"
{
    #include <mm.h>
}

namespace smp
{

extern bool disable_smp;

}

void wait_for_ipis(void)
{
    for(;;)
        for(int i = smp::pull_nmis(); i; i--)
            smp::ipi_isr();
}

static int boot_cpu;

void do_calc()
{
    int myself0 = smp::curcpu();
    int myself = smp::curcpu();
    if(myself > boot_cpu)
        myself--;
    smp::send_ipi(boot_cpu, [myself, myself0]()
    {
        smp::global_lock.lock();
        logging::log << "cpu #" << myself << " (" << myself0 << ") started up" << std::endl;
        smp::global_lock.unlock();
    });
    int total = smp::ncpus() - 1;
    int low = 1000000000l * myself / total;
    int high = 1000000000l * (myself + 1) / total;
    long long summ = 0;
    for(int i = low; i < high; i++)
        summ += i;
    smp::send_ipi(boot_cpu, [=]()
    {
        smp::global_lock.lock();
        logging::log << "cpu #" << myself << ": sum from " << low << " to " << high << " is " << summ << std::endl;
        smp::global_lock.unlock();
    });
    wait_for_ipis();
}

int main(const char* cmdline)
{
    if(!strcmp(cmdline, "test"))
    {
        int n = smp::ncpus();
        if(n <= 0)
        {
            logging::log << "Could not find any CPUs!" << std::endl;
            return 1;
        }
        void* stacks = mmap(0, n << 22, PROT_READ|PROT_WRITE|PROT_NOEXEC, MAP_ANON);
        if(!smp::bring_up_cpus(do_calc, stacks, 1 << 22, boot_cpu))
        {
            logging::log << "Could not start worker CPUs!" << std::endl;
            return 1;
        }
        wait_for_ipis();
    }
    else if(!strcmp(cmdline, "no"))
        smp::disable_smp = true;
    else
    {
        logging::log << "libsmp: unrecognized option " << cmdline << std::endl;
        return 1;
    }
    return 0;
}
