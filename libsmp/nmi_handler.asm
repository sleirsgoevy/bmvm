use64

global smp_nmi_handler
global smp_nmi_window
extern smp_sync_ipi

smp_nmi_handler:
push rax
push rcx
push rdx
mov rax, [fs:0]
lea rax, [rax+2*rax]
mov rcx, [rel .p_smp_sync_ipi]
mov rcx, [rcx]
mov rdx, [rcx+rax*8+16]
cmp rdx, 3
jz .dry_ret
cmp rdx, 2
jz .sync_ipi
pop rdx
pop rcx
pop rax
lock inc qword [fs:8]
.ret_check_window:
xchg rax, [rsp]
push rcx
mov rcx, [rel .p_smp_nmi_window]
cmp rax, [rcx]
jb .ok
cmp rax, [rcx+8]
jnb .ok
mov rax, [rcx+16]
.ok:
pop rcx
xchg rax, [rsp]
iretq
.sync_ipi:
cmp qword [rcx+rax*8], 1
jz .incr_fs_sync
push rsi
push rdi
push r8
push r9
push r10
push r11
push rbx
push rbp
mov rbp, rsp
or rsp, 0xf
xor rsp, 0xf
mov rsi, [rcx+rax*8]
mov qword [rcx+rax*8], 0
mov rdi, [rcx+rax*8+8]
mov qword [rcx+rax*8+8], 0
lea rbx, [rcx+rax*8+16]
call rsi
mov qword [rbx], 3
leave
pop rbx
pop r11
pop r10
pop r9
pop r8
pop rdi
pop rsi
.dry_ret:
pop rdx
pop rcx
pop rax
iretq
.incr_fs_sync:
lock inc qword [fs:8]
mov qword [rcx+rax*8+16], 3
pop rdx
pop rcx
pop rax
jmp .ret_check_window
.p_smp_nmi_window:
dq smp_nmi_window
.p_smp_sync_ipi:
dq smp_sync_ipi

section .data

smp_nmi_window:
dq 0
dq 0
dq 0
