#include "paging.h"
#include "memmap.h"
#include "assert.h"
#include "mm.h"
#include "string.h"
#include "mmap_lock.h"

static struct memmap_entry* memmap_start;
static struct memmap_entry* memmap_end;
static uint64_t bump_alloc_addr;
static uint64_t freelist_head = 1;
uint64_t exclusive_memory_range_start, exclusive_memory_range_end;

void mm_init_late(void* mm_start, void* mm_end, uint64_t mem_ceiling, uint64_t stack_addr)
{
    memmap_start = mm_start;
    memmap_end = (struct memmap_entry*)mm_end - 1;
    bump_alloc_addr = mem_ceiling;
}

static inline void invlpg(uint64_t addr)
{
    asm volatile("invlpg (%0)"::"r"(addr):"memory");
}

static inline void invlpg_range(uint64_t addr, size_t npages)
{
    for(size_t i = 0; i < npages; i++)
        invlpg(addr + (i << 12));
}

static inline uint64_t* page_arm(uint64_t addr)
{
    ((uint64_t*)PML1_MOD_BASE)[255] = addr | PG_PRESENT | PG_RW;
    invlpg(0xff000);
    return (void*)0xff000;
}

static inline uint64_t bump_alloc(void)
{
    while(memmap_end != memmap_start && memmap_end->start >= bump_alloc_addr)
        memmap_end--;
    if(memmap_end == memmap_start && memmap_end->start >= bump_alloc_addr)
        return (uint64_t)-1;
    if(bump_alloc_addr < memmap_end->end)
        memmap_end->end = bump_alloc_addr;
    return bump_alloc_addr = memmap_end->end -= 4096;
}

uint64_t alloc_page(void)
{
    if(freelist_head == 1)
        return bump_alloc();
    uint64_t ans = freelist_head;
    freelist_head = *page_arm(ans);
    return ans;
}

//TODO: use this instead of free_page when possible
static inline void free_page_ex(uint64_t page, uint64_t* mapping)
{
    *mapping = freelist_head;
    freelist_head = page;
}

void free_page(uint64_t page)
{
    free_page_ex(page, page_arm(page));
}

uint64_t mm_freeze(uint64_t* upper_bound)
{
    if(exclusive_memory_range_start != exclusive_memory_range_end)
    {
        if(upper_bound)
            *upper_bound = exclusive_memory_range_end;
        return exclusive_memory_range_start;
    }
    if(upper_bound)
        *upper_bound = 0;
    static uint64_t mem_break = 0;
    if(bump_alloc_addr > 0)
    {
        mem_break = bump_alloc_addr;
        bump_alloc_addr = 0;
    }
    return mem_break;
}

static void nop(void){}

void(*tlb_shootdown)(void) = nop;

#define PML1_ARR ((uint64_t*)PML1_MOD_BASE)
#define PML2_ARR ((uint64_t*)PML2_MOD_BASE)
#define PML3_ARR ((uint64_t*)PML3_MOD_BASE)
#define PML4_ARR ((uint64_t*)PML4_MOD_BASE)

//assumes no childfree pmls, will crash otherwise
static inline uint64_t get_next_mapping(uint64_t addr)
{
    addr &= PG_ADDR_MASK_4KB;
retry:
    while(addr < -MASK_48BIT && !PML4_ARR[PML4_OFF(addr)])
        addr = (addr & PG_ADDR_MASK_512GB) - (1ull << 39);
    if(addr == -MASK_48BIT)
        return addr;
retry_1gb:
    while(!PML3_ARR[PML3_OFF(addr)])
    {
        addr = (addr & PG_ADDR_MASK_1GB) - (1ull << 30);
        if(!(addr & ~PG_ADDR_MASK_512GB)) 
            goto retry;
    }
retry_2mb:
    while(!PML2_ARR[PML2_OFF(addr)])
    {
        addr = (addr & PG_ADDR_MASK_2MB) - (1ull << 21);
        if(!(addr & ~PG_ADDR_MASK_1GB))
            goto retry_1gb;
    }
    if(PML2_ARR[PML2_OFF(addr)] & PG_HUGE)
        return addr;
    while(!PML1_ARR[PML1_OFF(addr)])
    {
        addr += 4096;
        if(!(addr & ~PG_ADDR_MASK_2MB))
            goto retry_2mb;
    }
    return addr;
}

//search for an unmapped (optionally 2mb-aligned) address
//same warning as above
static inline uint64_t get_next_hole(uint64_t addr, int align_2mb)
{
    addr = (addr + ~PG_ADDR_MASK_2MB) & PG_ADDR_MASK_2MB;
    while(
        addr < -MASK_48BIT
     && PML4_ARR[PML4_OFF(addr)]
     && PML3_ARR[PML3_OFF(addr)]
     && PML2_ARR[PML2_OFF(addr)]
     && PML1_ARR[PML1_OFF(addr)])
    {
        if((PML2_ARR[PML2_OFF(addr)] & PG_HUGE))
        {
            addr += 0x200000;
            continue;
        }
        if(PML1_ARR[PML1_OFF(addr)])
        {
            addr += align_2mb ? 0x200000 : 0x1000;
        }
    }
    return addr;
}

static uint64_t memmap_bighole_start = 0;
static uint64_t memmap_bighole_end = 0;

//TODO: 2mb mappings
void* mmap_unlocked(uintptr_t addr, size_t size, int prot, uint64_t phys_addr)
{
    if(!addr)
    {
        if(memmap_bighole_end - memmap_bighole_start >= size)
        {
            addr = memmap_bighole_start;
            memmap_bighole_start += (size + 4095) & ~4095ull;
            assert(!(memmap_bighole_start > memmap_bighole_end));
        }
        else
        {
            addr = 0x100000;
            while(addr != -MASK_48BIT)
            {
                uint64_t hole = get_next_hole(addr, size >= 0x100000);
                addr = get_next_mapping(hole);
                if(addr - hole >= size)
                {
                    if(hole + size < 2 * hole)
                    {
                        memmap_bighole_start = hole + (size + 4095) & ~4095ull;
                        memmap_bighole_end = addr;
                        if(memmap_bighole_end > 2 * hole)
                            memmap_bighole_end = 2 * hole;
                        if(memmap_bighole_end < memmap_bighole_start)
                            memmap_bighole_end = memmap_bighole_start;
                    }
                    addr = hole;
                    break;
                }
            }
            if(addr == -MASK_48BIT)
                return MAP_FAILED;
        }
    }
    if(!prot)
        return (void*)addr;
    for(uint64_t page = (addr & PG_ADDR_MASK_4KB); page < addr + size; page += 4096)
    {
        if(!PML4_ARR[PML4_OFF(page)])
        {
            uint64_t pg = alloc_page();
            if(pg == (uint64_t)-1)
                return MAP_FAILED;
            PML4_ARR[PML4_OFF(page)] = pg | PG_PRESENT | PG_RW;
            memset(PML3_ARR + (PML4_OFF(page) << 9), 0, 4096);
        }
        if(!PML3_ARR[PML3_OFF(page)])
        {
            uint64_t pg = alloc_page();
            if(pg == (uint64_t)-1)
                return MAP_FAILED;
            PML3_ARR[PML3_OFF(page)] = pg | PG_PRESENT | PG_RW;
            memset(PML2_ARR + (PML3_OFF(page) << 9), 0, 4096);
        }
        if((PML2_ARR[PML2_OFF(page)] & PG_HUGE))
        {
            if(phys_addr != MAP_ANON)
            {
                PML2_ARR[PML2_OFF(page)] = 0;
                invlpg((uint64_t)(PML1_ARR+PML1_OFF(page & PG_ADDR_MASK_2MB)));
                invlpg(page & PG_ADDR_MASK_2MB);
            }
            else
            {
                PML2_ARR[PML2_OFF(page)] = ((PML2_ARR[PML2_OFF(page)] ^ PG_NX) | (prot ^ PG_NX)) ^ PG_NX;
                invlpg((uint64_t)(PML1_ARR+PML1_OFF(page & PG_ADDR_MASK_2MB)));
                invlpg(page & PG_ADDR_MASK_2MB);
                page = (page & PG_ADDR_MASK_2MB) + 0x1ff000;
                continue;
            }
        }
        if(!PML2_ARR[PML2_OFF(page)])
        {
            uint64_t pg = alloc_page();
            if(pg == (uint64_t)-1)
                return MAP_FAILED;
            PML2_ARR[PML2_OFF(page)] = pg | PG_PRESENT | PG_RW;
            memset(PML1_ARR + (PML2_OFF(page) << 9), 0, 4096);
        }
        if(PML1_ARR[PML1_OFF(page)])
        {
            if(phys_addr != MAP_ANON)
            {
                if((PML1_ARR[PML1_OFF(page)] & PG_ANON))
                    free_page(PML1_ARR[PML1_OFF(page)] & PG_ADDR_MASK_4KB);
                PML1_ARR[PML1_OFF(page)] = 0;
                invlpg(page);
            }
            else
            {
                PML1_ARR[PML1_OFF(page)] |= prot;
                invlpg(page);
                continue;
            }
        }
        uint64_t phys;
        if(phys_addr == MAP_ANON)
        {
            phys = alloc_page();
            if(phys == (uint64_t)-1)
                return MAP_FAILED;
            phys |= PG_ANON;
        }
        else
        {
            phys = phys_addr;
            phys_addr += 4096;
        }
        PML1_ARR[PML1_OFF(page)] = phys | prot;
    }
    return (void*)addr;
}

void* mmap(uintptr_t addr, size_t size, int prot, uint64_t phys_addr)
{
    mmap_lock();
    void* ans = mmap_unlocked(addr, size, prot, phys_addr);
    mmap_unlock();
    return ans;
}

void mprotect(void* addr, size_t sz, int prot)
{
    mmap_lock();
    uint64_t u_addr = (uint64_t)addr;
    if(u_addr > -MASK_48BIT)
    {
        mmap_unlock();
        return;
    }
    if(-MASK_48BIT - u_addr < sz)
        sz = -MASK_48BIT - u_addr;
    for(uint64_t page = u_addr & ~PG_ADDR_MASK_4KB; page < u_addr + sz;)
    {
        if(!PML4_ARR[PML4_OFF(page)])
            page = (page & PG_ADDR_MASK_512GB) - (1ull << 39);
        else if(!PML3_ARR[PML3_OFF(page)])
            page = (page & PG_ADDR_MASK_1GB) - (1ull << 30);
        else if(!PML2_ARR[PML2_OFF(page)])
            page = (page & PG_ADDR_MASK_2MB) - (1ull << 21);
        else if(PML2_ARR[PML2_OFF(page)] & PG_HUGE)
        {
            PML2_ARR[PML2_OFF(page)] = (PML2_ARR[PML2_OFF(page)] & PG_ADDR_MASK_2MB) | prot | PG_HUGE;
            invlpg((uint64_t)(PML1_ARR+PML1_OFF(page & PG_ADDR_MASK_2MB)));
            invlpg(page & PG_ADDR_MASK_2MB);
            page = (page & PG_ADDR_MASK_2MB) - (1ull << 21);
        }
        else
        {
            PML1_ARR[PML1_OFF(page)] = (PML1_ARR[PML1_OFF(page)] & PG_ADDR_MASK_4KB) | prot;
            invlpg(page);
            page += 4096;
        }
    }
    mmap_unlock();
    tlb_shootdown();
}

static inline int unmap_pml1(uint64_t* pml, size_t start, size_t end)
{
    int empty = 1;
    for(size_t i = 0; i < start; i++)
        if(pml[i])
            empty = 0;
    for(size_t i = start; i < end; i++)
    {
        if((pml[i] & PG_ANON))
            free_page(pml[i] & PG_ADDR_MASK_4KB);
        pml[i] = 0;
        invlpg(PML_IN((uint64_t)(pml+i)) & ~MASK_48BIT);
    }
    for(size_t i = end; i < 512; i++)
        if(pml[i])
            empty = 0;
    return empty;
}

static inline int unmap_pml2(uint64_t* pml, size_t start, size_t end)
{
    if(start == end)
        return 0;
    size_t big_start = start >> 9;
    size_t big_end = (end - 1) >> 9;
    int empty = 1;
    for(size_t i = 0; i < big_start; i++)
        if(pml[i])
            empty = 0;
    for(size_t i = big_start; i <= big_end; i++)
        if((pml[i] & PG_HUGE))
        {
            //2mb page cannot be anonymous
            pml[i] = 0;
            invlpg(PML_IN((uint64_t)(pml + i)));
            invlpg(PML_IN(PML_IN((uint64_t)(pml + i))) & ~MASK_48BIT);
        }
        else if(pml[i])
        {
            size_t left = (i == big_start) ? (start & 511) : 0;
            size_t right = (i == big_end) ? (end & 511) : 512;
            uint64_t* pml1 = (uint64_t*)PML_IN((uint64_t)(pml + i));
            if(unmap_pml1(pml1, left, right))
            {
                free_page(pml[i] & PG_ADDR_MASK_4KB);
                pml[i] = 0;
                invlpg((uint64_t)pml1);
            }
            else
                empty = 0;
        }
    for(size_t i = big_end + 1; i < 512; i++)
        if(pml[i])
            empty = 0;
    return empty;
}

static inline int unmap_pml3(uint64_t* pml, size_t start, size_t end)
{
    if(start == end)
        return 0;
    size_t big_start = start >> 18;
    size_t big_end = (end - 1) >> 18;
    int empty = 1;
    for(size_t i = 0; i < big_start; i++)
        if(pml[i])
            empty = 0;
    for(size_t i = big_start; i <= big_end; i++)
        if(pml[i])
        {
            size_t left = (i == big_start) ? (start & 0x3ffff) : 0;
            size_t right = (i == big_end) ? (end & 0x3ffff) : 0x40000;
            uint64_t* pml2 = (uint64_t*)PML_IN((uint64_t)(pml + i));
            if(unmap_pml2(pml2, left, right))
            {
                free_page(pml[i] & PG_ADDR_MASK_4KB);
                pml[i] = 0;
                invlpg((uint64_t)pml2);
            }
            else
                empty = 0;
        }
    for(size_t i = big_end + 1; i < 512; i++)
        if(pml[i])
            empty = 0;
    return empty;
}

static inline void unmap_pml4(uint64_t* pml, size_t start, size_t end)
{
    if(start == end)
        return;
    size_t big_start = start >> 27;
    size_t big_end = (end - 1) >> 27;
    for(size_t i = big_start; i <= big_end; i++)
        if(pml[i])
        {
            size_t left = (i == big_start) ? (start & 0x7ffffff) : 0;
            size_t right = (i == big_end) ? (end & 0x7ffffff) : 0x8000000;
            uint64_t* pml3 = (uint64_t*)PML_IN((uint64_t)(pml + i));
            if(unmap_pml3(pml3, left, right))
            {
                free_page(pml[i] & PG_ADDR_MASK_4KB);
                pml[i] = 0;
                invlpg((uint64_t)pml3);
            }
        }
}

void munmap(void* addr, size_t size)
{
    mmap_lock();
    uint64_t start = (uint64_t)addr;
    if(start > (1ull << 47))
    {
        mmap_unlock();
        return;
    }
    if(size > (1ull << 47))
        size = (1ull << 47);
    uint64_t end = start + size;
    if(end > (1ull << 47))
        end = (1ull << 47);
    unmap_pml4(PML4_ARR, start >> 12, (end + 4095ull) >> 12);
    mmap_unlock();
    tlb_shootdown();
}

void set_shootdown_hook(void(*hook)(void))
{
    tlb_shootdown = hook;
}
