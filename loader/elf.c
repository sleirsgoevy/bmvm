#include "types.h"
#include "fs.h"
#include "elf.h"
#include "elf_struct.h"
#include "mm.h"
#include "mmap_lock.h"
#include "string.h"
#include "exports.h"
#include "cmpxchg.h"

struct module
{
    struct module* next_module;
    uint64_t module_base;
    const char* module_name;
    char* strtab;
    struct symbol* symtab;
    struct gnu_hash* gnu_hash;
};

static uint32_t calc_gnu_hash(const char* name)
{
    uint32_t ans = 5381;
    for(size_t i = 0; name[i]; i++)
        ans = 33*ans + name[i];
    return ans;
}

static void* elf_dlsym_internal(struct module* mod, const char* name, uint32_t hash)
{
    uint32_t hash2 = hash >> mod->gnu_hash->gnu_shift;
    uint64_t word = mod->gnu_hash->bitmasks[(hash >> 6) % mod->gnu_hash->nwords];
    if(!(word & (1ull << (hash & 63))))
        return 0;
    if(!(word & (1ull << (hash2 & 63))))
        return 0;
    uint32_t* buckets = (uint32_t*)(mod->gnu_hash->bitmasks + mod->gnu_hash->nwords);
    uint32_t* chains = buckets + mod->gnu_hash->nbuckets;
    size_t bucket = hash % mod->gnu_hash->nbuckets;
    hash >>= 1;
    if(buckets[bucket] < mod->gnu_hash->symbol_base)
        return 0;
    uint32_t* p = chains + (buckets[bucket] - mod->gnu_hash->symbol_base);
    do
    {
        struct symbol* sym = mod->symtab + ((p - chains) + mod->gnu_hash->symbol_base);
        if((p[0] >> 1) == hash && !strcmp(mod->strtab + sym->name, name))
        {
            if(sym->shndx == SHN_ABS)
                return (void*)sym->value;
            else if(sym->shndx == SHN_UNDEF)
                return 0;
            else
                return (void*)(mod->module_base + sym->value);
        }
    }
    while(!((*p++) & 1));
    return 0;
}

static volatile uintptr_t module_list_head;
#define module_list ((struct module*)module_list_head)

void* elf_dlsym(const char* name)
{
    uint32_t hash = calc_gnu_hash(name);
    for(struct module* i = module_list; i; i = i->next_module)
    {
        void* ans = elf_dlsym_internal(i, name, hash);
        if(ans)
            return ans;
    }
    return exports_dlsym(name);
}

void* elf_dlsym_module(struct module* mod, const char* name)
{
    return elf_dlsym_internal(mod, name, calc_gnu_hash(name));
}

static inline void* resolve_symbol(struct module* mod, size_t idx)
{
    struct symbol* sym = mod->symtab + idx;
    if(sym->shndx == SHN_ABS)
        return (void*)sym->value;
    else if(sym->shndx == SHN_UNDEF)
        return elf_dlsym(mod->strtab + sym->name);
    else
        return (void*)(mod->module_base + sym->value);
}

static inline void disable_wp(void)
{
    register uintptr_t rax asm("rax");
    asm volatile("mov %cr0, %rax\nbtc $16, %rax\nmov %rax, %cr0");
}

static inline void enable_wp(void)
{
    register uintptr_t rax asm("rax");
    asm volatile("mov %cr0, %rax\nbts $16, %rax\nmov %rax, %cr0");
}

static inline int apply_relocation(struct module* mod, struct rela* reloc)
{
    uint64_t addr = mod->module_base + reloc->rel.offset;
    switch(reloc->rel.type)
    {
    case R_X86_64_64:
    {
        void* sym = resolve_symbol(mod, reloc->rel.sym);
        if(!sym)
            return -1;
        *(uint64_t*)addr = (uint64_t)sym + reloc->offset;
        break;
    }
    case R_X86_64_GLOB_DAT:
    case R_X86_64_JUMP_SLOT:
    {
        void* sym = resolve_symbol(mod, reloc->rel.sym);
        if(!sym)
            return -1;
        *(uint64_t*)addr = (uint64_t)sym;
        break;
    }
    case R_X86_64_RELATIVE:
    {
        *(uint64_t*)addr = mod->module_base + reloc->offset;
        break;
    }
    case R_X86_64_32:
    {
        void* sym = resolve_symbol(mod, reloc->rel.sym);
        if(!sym)
            return -1;
        uint32_t value = (uint32_t)(uint64_t)sym;
        if((uint64_t)value != (uint64_t)sym)
            return -1;
        *(uint32_t*)addr = value;
        break;
    }
    case R_X86_64_32S:
    {
        void* sym = resolve_symbol(mod, reloc->rel.sym);
        if(!sym)
            return -1;
        int32_t value = (int32_t)(int64_t)sym;
        if((int64_t)value != (int64_t)sym)
            return -1;
        *(int32_t*)addr = value;
        break;
    }
    default:
        return -1;
    }
    return 0;
}

static int apply_rel(struct module* mod, struct section* rel)
{
    struct rel* begin = rel->base;
    struct rel* end = (void*)(rel->size + (uintptr_t)rel->base);
    for(struct rel* i = begin; i < end; i++)
    {
        struct rela rela = { .rel = *i, .offset = 0 };
        if(apply_relocation(mod, &rela)) 
            return -1;
    }
    return 0;
}

static int apply_rela(struct module* mod, struct section* rela)
{
    struct rela* begin = rela->base;
    struct rela* end = (void*)(rela->size + (uintptr_t)rela->base);
    for(struct rela* i = begin; i < end; i++)
        if(apply_relocation(mod, i))
            return -1;
    return 0;
}

static const char E_IDENT[] = "\x7f""ELF" "\x02" "\x01" "\x01";

struct module* load_elf(FILE* f, const char* name)
{
    for(struct module* i = module_list; i; i = i->next_module)
        if(i->module_name && name && !strcmp(i->module_name, name))
            return i;
    if(!f)
        f = open(name, O_RDONLY);
    if(!f)
        return 0;
    struct ehdr ehdr = {0};
    pread(f, &ehdr, sizeof(ehdr), 0);
    if(memcmp(ehdr.e_ident, E_IDENT, sizeof(E_IDENT) - 1))
        return 0;
    if(ehdr.e_type != 3 /* ET_DYN */ || ehdr.e_machine != 62 /* x86_64 */)
        return 0;
    uintptr_t lowest_addr = -1;
    uintptr_t highest_addr = 0;
    int have_dynamic = 0;
    uintptr_t dynamic_addr;
    uintptr_t dynamic_sz;
    int ehdr_is_mapped = 0;
    uintptr_t ehdr_addr;
    for(size_t i = 0; i < ehdr.e_phnum; i++)
    {
        struct phdr phdr = {0};
        pread(f, &phdr, sizeof(phdr), ehdr.e_phoff + ehdr.e_phentsize * i);
        if(phdr.p_type == PT_TLS)
            return 0;
        if(phdr.p_type == PT_DYNAMIC)
        {
            have_dynamic = 1;
            dynamic_addr = phdr.p_vaddr;
            dynamic_sz = phdr.p_memsz;
        }
        if(phdr.p_type == PT_LOAD && phdr.p_offset == 0 && phdr.p_filesz >= sizeof(ehdr))
        {
            ehdr_is_mapped = 1;
            ehdr_addr = phdr.p_vaddr;
        }
        if(phdr.p_vaddr < lowest_addr)
            lowest_addr = phdr.p_vaddr;
        if(phdr.p_vaddr + phdr.p_memsz > highest_addr)
            highest_addr = phdr.p_vaddr + phdr.p_memsz;
    }
    if(!have_dynamic)
        return 0;
    lowest_addr &= PG_ADDR_MASK_4KB;
    highest_addr = (highest_addr + 4095) & PG_ADDR_MASK_4KB;
    mmap_lock();
    uint64_t first_mapping = (uint64_t)mmap_unlocked(0, highest_addr - lowest_addr, PROT_NONE, MAP_ANON);
    uint64_t base = first_mapping - lowest_addr;
    for(size_t i = 0; i < ehdr.e_phnum; i++)
    {
        struct phdr phdr = {0};
        pread(f, &phdr, sizeof(phdr), ehdr.e_phoff + ehdr.e_phentsize * i);
        if(phdr.p_type == PT_LOAD
        || phdr.p_type == PT_DYNAMIC
        || phdr.p_type == PT_INTERP
        || phdr.p_type == PT_PHDR)
        {
            int prot = 0;
            if((phdr.p_flags & (PF_R | PF_X)))
                prot |= PROT_READ;
            if((phdr.p_flags & PF_W))
                prot |= PROT_WRITE;
            if(!(phdr.p_flags & PF_X))
                prot |= PROT_NOEXEC;
            mmap_unlocked(base + phdr.p_vaddr, phdr.p_memsz, prot, MAP_ANON);
            disable_wp();
            memset((void*)(base + phdr.p_vaddr), 0, phdr.p_memsz);
            pread(f, (void*)(base + phdr.p_vaddr), phdr.p_filesz, phdr.p_offset);
            enable_wp();
        }
    }
    struct module* mod;
    if(sizeof(*mod) <= sizeof(ehdr) && ehdr_is_mapped)
        mod = (void*)(base + ehdr_addr);
    else
        mod = mmap_unlocked(0, sizeof(*mod), PROT_READ, MAP_ANON);
    mmap_unlock();
    struct section sections[4] = {0};
    int jmprel_is_rela = 0;
    void (*init_fn)(void) = 0;
    disable_wp();
    mod->module_base = base;
    mod->module_name = name;
    mod->symtab = 0;
    mod->strtab = 0;
    mod->gnu_hash = 0;
    for(struct dynamic* entry = (void*)(base + dynamic_addr); entry < (struct dynamic*)(base + dynamic_addr + dynamic_sz); entry++)
    {
        switch(entry->key)
        {
        case DT_PLTRELSZ: sections[2].size = entry->value; break;
        case DT_STRTAB: mod->strtab = (void*)(base + entry->value); break;
        case DT_SYMTAB: mod->symtab = (void*)(base + entry->value); break;
        case DT_RELA: sections[0].base = (void*)(base + entry->value); break;
        case DT_RELASZ: sections[0].size = entry->value; break;
        case DT_INIT: init_fn = (void(*)(void))(base + entry->value); break;
        case DT_REL: sections[1].base = (void*)(base + entry->value); break;
        case DT_RELSZ: sections[1].size = entry->value; break;
        case DT_PLTREL: jmprel_is_rela = (entry->value == DT_RELA); break;
        case DT_JMPREL: sections[2].base = (void*)(base + entry->value); break;
        case DT_INIT_ARRAY: sections[3].base = (void*)(base + entry->value); break;
        case DT_INIT_ARRAYSZ: sections[3].size = entry->value; break;
        case DT_GNU_HASH: mod->gnu_hash = (void*)(base + entry->value); break;
        }
    }
    enable_wp();
    do
    {
        disable_wp();
        mod->next_module = module_list;
        enable_wp();
    }
    while(!cmpxchg(&module_list_head, (uintptr_t)mod->next_module, (uintptr_t)mod));
    for(struct dynamic* entry = (void*)(base + dynamic_addr); entry < (struct dynamic*)(base + dynamic_addr + dynamic_sz); entry++)
        if(entry->key == DT_NEEDED && !load_elf(0, mod->strtab + entry->value))
            return 0;
    disable_wp();
    int rel_fail = apply_rela(mod, sections) || apply_rel(mod, sections+1);
    if(!rel_fail)
    {
        if(jmprel_is_rela)
            rel_fail = apply_rela(mod, sections+2);
        else
            rel_fail = apply_rel(mod, sections+2);
    }
    enable_wp();
    if(rel_fail)
        return 0;
    if(init_fn)
        init_fn();
    void(**init_array)(void) = (void*)sections[3].base;
    void(**init_array_end)(void) = (void*)(sections[3].size + (uintptr_t)sections[3].base);
    while(init_array < init_array_end)
        (**init_array++)();
    return mod;
}

struct module* elf_dladdr(void* addr, const char** module_name, void** module_base)
{
    struct module* out = 0;
    for(struct module* i = (struct module*)module_list_head; i; i = i->next_module)
        if((!out || i->module_base >= out->module_base) && i->module_base <= (uintptr_t)addr)
            out = i;
    *module_base = out ? (void*)out->module_base : 0;
    *module_name = out ? out->module_name : 0;
    return out;
}
