#include "assert.h"
#include "elf.h"
#include "mm.h"
#include "fs.h"
#include "exports.h"
#include "string.h"
#include "bootproto.h"

struct export_entry
{
    const char* name;
    void* value;
};

static struct export_entry exports[] = {
    /*
        struct module* load_elf(FILE* f, const char* name);

        Loads an ELF file from the specified FILE, or from the filesystem if f is NULL.
        Non-NULL names must be unique.
        Returns a pointer to the module object (NOT the ELF header).
    */
    {"load_elf", load_elf},
    /*
        void* elf_dlsym(const char* name);

        Search for a symbol in all currently loaded modules.
        Returns NULL if no symbol with such name is present.
    */
    {"elf_dlsym", elf_dlsym},
    /*
        void* elf_dlsym_module(struct module* mod, const char* name);

        Search for a symbol ONLY in the specified module.
        Returns NULL if no symbol with such name is present in the module.
    */
    {"elf_dlsym_module", elf_dlsym_module},
    /*
        struct module* elf_dladdr(void* addr, const char** module_name, void** module_base);

        Search for a module this address is from, and return it if found. `*module_name` is
        set to a pointer to the module name, and `*module_base` is set to the base address
        of the module. If the module was not found, NULL is returned, and `*module_name`
        and `*module_base` have undefined value.
    */
    {"elf_dladdr", elf_dladdr},
    /*
        void* mmap(uintptr_t addr, size_t size, int prot, uint64_t phys);

        Maps a range of pages into the virtual address space.
        If `addr` is nonzero, pages are placed at the specified address,
        otherwise an empty address range is searched for.
        If `prot` is zero, only the search is performed, and the page tables are untouched.
        Otherwise `prot` should contain the page table flags for the new mapping,
        the constants PROT_READ, PROT_WRITE, PROT_NOEXEC are provided for convenience.
        If `phys` is nonzero, it should be page-aligned, and pages are mapped starting
        from that physical address. Otherwise anonymous memory is mapped; in this case,
        existing mappings are preserved, and only their permissions are loosed.
    */
    {"mmap", mmap},
    /*
        void mprotect(void* addr, size_t size, int prot);

        Changes the page protections of the page to `prot`. See above.
        Huge page protections are changed for the whole huge pages.
    */
    {"mprotect", mprotect},
    /*
        void munmap(void* addr, size_t size);

        Unmaps all pages in the specified range. Pages belonging to anonymous mappings
        are freed.
        Huge pages are unmapped entirely.
    */
    {"munmap", munmap},
    /*
        FILE* open(const char* path, int mode);

        Opens a file on the filesystem. The mode argument is ignored (should be O_RDONLY).
        Initially this opens files from the tarball embedded as initramfs.
        Other filesystem drivers may be added via `register_fs_driver`.
    */
    {"open", open},
    /*
        ssize_t pread(FILE* f, void* dest, size_t sz, off_t off);

        Reads `sz` bytes from offset `off` in the file `f` into memory pointed by `dest`.
        The returned value is a non-negative number of bytes actually read, or a negative
        error code. The tarball driver never fails.
    */
    {"pread", pread},
    /*
        off_t lseek(FILE* f, off_t off, int whence);

        If whence is SEEK_SET, returns off.
        If whence is SEEK_END, returns off + file size if file size is known, otherwise
        returns -1.
        If whence is any other value, returns -1.
    */
    {"lseek", lseek},
    /*
        void close(FILE* f);

        Closes an open file handle `f`. The tarball driver does nothing.
    */
    {"close", close},
    /*
        void register_fs_driver(struct fs_driver* drv);

        Registers a new filesystem driver.
        `struct fs_driver` is as follows:
        struct fs_driver
        {
            const char* name;
            FILE*(*open)(const char* path, int mode);
            ssize_t(*pread)(FILE* f, void* dest, size_t sz, off_t off);
            void(*close)(FILE* f);
            struct fs_driver* next;
        };
    */
    {"register_fs_driver", register_fs_driver},
    /*
        uint64_t mm_freeze(uint64_t* upper_bound);

        Reserves as many low physical memory as possible, and returns a physical pointer,
        such that no pages below the pointer will be used.

        If upper_bound is non-NULL, a nonzero address may be stored there. This means that
        pages at or above that address are also not used.
    */
    {"mm_freeze", mm_freeze},
    /*
        void set_shootdown_hook(void(*shootdown_hook)(void));

        Sets the shootdown helper function. This function gets called when TLB invalidation
        is necessary, and performs additional invalidation beyond standard invlpg.
    */
    {"set_shootdown_hook", set_shootdown_hook},
    /*
        int get_boot_protocol(void);

        Returns the BOOT_PROTOCOL_* constant corresponding to the boot protocol being used.
    */
    {"get_boot_protocol", get_boot_protocol},
    /*
        int get_linux_loader_info(uint8_t info[3]);

        Returns type_of_loader and ext_loader_{ver,type} from the kernel header.
    */
    {"get_linux_loader_info", get_linux_loader_info},
    /*
        void get_linux32_memmap(void** start, void** end);

        Returns the start and end pointers of the backed-up memory map.
    */
    {"get_linux32_memmap", get_linux32_memmap},
    /*
        void get_bootproto_rsdp(void);

        Returns the ACPI RSDP pointer, as passed in the boot protocol.
        Returns 0 if the boot protocol did not pass the ACPI RSDP pointer.
    */
    {"get_bootproto_rsdp", get_bootproto_rsdp},
};

void* exports_dlsym(const char* name)
{
    for(size_t i = 0; i < sizeof(exports) / sizeof(exports[0]); i++)
        if(!strcmp(exports[i].name, name))
            return exports[i].value;
    return 0;
}
