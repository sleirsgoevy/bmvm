#include "types.h"
#include "memmap.h"
#include "paging.h"
#include "assert.h"
#include "string.h"
#include "bootproto.h"

typedef struct memmap_entry* p_memmap_entry __attribute__((may_alias));

enum { START_BAD, END_BAD, START_GOOD, END_GOOD };

struct memmap_sort_entry
{
    uint64_t pos;
    uint16_t type;
} __attribute__((packed));

typedef struct memmap_sort_entry* p_memmap_sort_entry __attribute__((may_alias));

//header.asm will overwrite this when using 32- and 64-bit linux protocols
struct memmap_header
{
    struct memmap_entry* begin;
    struct memmap_entry* end;
}* memmap_header = (void*)0x7c00;

extern uint64_t exclusive_memory_range_start, exclusive_memory_range_end;
static struct memmap_entry exclusive_memory_range;

static struct memmap_entry* memmap_begin;
static struct memmap_entry* memmap_end;
static uint64_t memmap_begin_phys;
static uint64_t memmap_end_phys;

static_assert(sizeof(struct memmap_entry) == 2 * sizeof(struct memmap_sort_entry));

static inline void invlpg(uint64_t addr)
{
    asm volatile("invlpg (%0)"::"r"(addr):"memory");
}

#ifdef EARLY_LOGS

static uint16_t* early_fb = 0;
static int line = 0;

static void early_puts(const char* s)
{
    if(!early_fb)
    {
        *(volatile uint64_t*)56 = PG_PRESENT|PG_RW|PG_HUGE;
        invlpg(0xe00000);
        early_fb = (void*)0xeb8000;
        for(size_t i = 0; i < 80 * 25; i++)
            early_fb[i] = 0;
    }
    for(size_t i = 0; s[i]; i++)
        early_fb[80*line+i] = 0x700 | (unsigned char)s[i];
    line++;
}

#else
#define early_puts(...)
#endif

static inline void* early_arm1(uint64_t phys_addr)
{
    *(uint64_t*)8 = (phys_addr & PG_ADDR_MASK_2MB) | PG_HUGE | PG_PRESENT | PG_RW;
    *(uint64_t*)16 = (phys_addr & PG_ADDR_MASK_2MB) | 0x200000 | PG_HUGE | PG_PRESENT | PG_RW;
    invlpg(0x200000);
    invlpg(0x400000);
    return (void*)(0x200000 | (phys_addr & ~PG_ADDR_MASK_2MB));
}

static inline uint64_t* early_arm4(int i, uint64_t phys_addr)
{
    uint64_t* out = (uint64_t*)8;
    if(out[i] != (phys_addr & PG_ADDR_MASK_2MB) | PG_PRESENT | PG_RW | PG_HUGE)
    {
        out[i] = (phys_addr & PG_ADDR_MASK_2MB) | PG_PRESENT | PG_RW | PG_HUGE;
        invlpg((i + 1) << 21);
    }
    return (void*)((i + 1) << 21 | (phys_addr & ~PG_ADDR_MASK_2MB));
}

struct memmap_entry memmap_backup[256];
struct memmap_entry* memmap_backup_end = memmap_backup;

void get_linux32_memmap(void** start, void** end)
{
    *start = memmap_backup;
    *end = memmap_backup_end;
}

static void normalize_memmap(uint64_t kernel_base)
{
    struct memmap_header* mh = early_arm1((uint64_t)memmap_header);
    uint64_t ph_begin = (uint64_t)mh->begin;
    uint64_t ph_end = (uint64_t)mh->end;
    assert(ph_end - ph_begin <= 0x200000);
    p_memmap_entry begin = (void*)early_arm1(ph_begin);
    p_memmap_entry end = (void*)((uint64_t)begin + (ph_end - ph_begin));
    if(get_boot_protocol() == BOOT_PROTOCOL_LINUX32 || get_boot_protocol() == BOOT_PROTOCOL_UEFI_RUNTIME)
    {
        //this memmap is our only copy. back it up before trashing
        size_t n_entries = end - begin;
        if(n_entries > 256)
            n_entries = 256;
        for(size_t i = 0; i < n_entries; i++)
            memmap_backup[i] = begin[i];
        memmap_backup_end = memmap_backup + n_entries;
    }
    if(exclusive_memory_range_start != exclusive_memory_range_end)
    {
        //if an exclusive memory range is provided, use only that memory
        exclusive_memory_range.start = exclusive_memory_range_start = (exclusive_memory_range_start + 4095) & -4096;
        exclusive_memory_range.end = exclusive_memory_range_end = exclusive_memory_range_end & -4096;
        assert(exclusive_memory_range.end > exclusive_memory_range.start);
        memmap_begin = &exclusive_memory_range;
        memmap_end = 1 + &exclusive_memory_range;
        memmap_begin_phys = (uint64_t)memmap_begin + kernel_base - 0x100000;
        memmap_end_phys = (uint64_t)memmap_end + kernel_base - 0x100000;
        return;
    }
    p_memmap_sort_entry begin_s = (void*)begin;
    p_memmap_sort_entry end_s = (void*)end;
    p_memmap_sort_entry j = begin_s;
    for(p_memmap_entry i = begin; i < end; i++)
    {
        struct memmap_entry cur = *i;
        j->pos = cur.start;
        j->type = (cur.type == FREE ? START_GOOD : START_BAD);
        j++;
        j->pos = cur.start + cur.length;
        j->type = (cur.type == FREE ? END_GOOD : END_BAD);
        j++;
    }
    assert(j == end_s);
    for(p_memmap_sort_entry i = begin_s; i < end_s; i++)
        for(p_memmap_sort_entry k = i+1; k < end_s; k++)
            if(i->pos > k->pos || (i->pos == k->pos && i->type > k->type))
            {
                struct memmap_sort_entry tmp = *i;
                *i = *k;
                *k = tmp;
            }
    j = begin_s;
    int good_depth = 0, bad_depth = 0;
    for(p_memmap_sort_entry i = begin_s; i < end_s; i++)
    {
        if(i->type == START_GOOD)
        {
            if(++good_depth == 1 && !bad_depth)
                *j++ = *i;
        }
        else if(i->type == END_GOOD)
        {
            if(!--good_depth && !bad_depth)
                *j++ = *i;
        }
        else if(i->type == START_BAD)
        {
            if(++bad_depth == 1 && good_depth)
            {
                j->pos = i->pos;
                j->type = END_GOOD;
                j++;
            }
        }
        else if(i->type == END_BAD)
        {
            if(!--bad_depth && good_depth)
            {
                j->pos = i->pos;
                j->type = START_GOOD;
                j++;
            }
        }
    }
    assert(j <= end_s);
    assert((j - begin_s) % 2 == 0);
    end_s = j;
    p_memmap_entry out = begin;
    for(p_memmap_sort_entry i = begin_s; i < end_s; i += 2)
    {
        assert(i[0].type == START_GOOD && i[1].type == END_GOOD);
        if(out != begin && out[-1].end == i[0].pos)
            out[-1].end = i[1].pos;
        else
        {
            out->start = i[0].pos;
            out->end = i[1].pos;
            out->type = FREE;
            out++;
        }
    }
    end = out;
    out = begin;
    for(p_memmap_entry i = begin; i < end; i++)
    {
        i->start = ((i->start - 1) | 4095ull) + 1;
        i->end &= ~4095ull;
        if(i->end > i->start)
            *out++ = *i;
    }
    end = out;
    memmap_begin = begin;
    memmap_end = end;
    assert(memmap_begin != memmap_end);
    memmap_begin_phys = ph_begin;
    memmap_end_phys = ph_begin + (uint64_t)memmap_end - (uint64_t)memmap_begin;
}

struct early_alloc
{
    struct memmap_entry* cur;
    uint64_t addr;
    uint64_t bottom;
};

extern uint64_t kernel_end;

static void early_alloc_init(struct early_alloc* ea)
{
    ea->cur = memmap_end - 1;
    ea->addr = ea->cur->end;
    ea->bottom = (uint64_t)&kernel_end;
}

static uint64_t early_alloc(struct early_alloc* ea)
{
    do
    {
        while(ea->cur != memmap_begin && ea->addr <= ea->cur->start)
            ea->cur--;
        if(ea->addr > ea->cur->end)
            ea->addr = ea->cur->end;
        assert(ea->addr);
        ea->addr -= 4096;
        assert(ea->addr >= ea->bottom);
    }
    while(!(ea->addr >= memmap_end_phys || ea->addr + 4096 <= memmap_begin_phys));
    return ea->addr;
}

static size_t move_blob(struct early_alloc* ea, uint64_t start, uint64_t end)
{
    start &= ~4095ull;
    end = (end - 1 | 4095ull) + 1;
    for(uint64_t i = end - 4096; i != start - 4096; i -= 4096)
    {
        uint64_t other = early_alloc(ea);
        if(other != i)
            memcpy(early_arm4(2, other), early_arm4(3, i), 4096);
    }
    return (end - start) >> 12;
}

static void map_from_ea(uint64_t pgd, uint64_t addr, size_t npages, struct early_alloc* ea, struct early_alloc* src, int flags)
{
    addr += (npages - 1) << 12;
    uint64_t* pml4 = early_arm4(2, pgd);
#if 0 //will cause troubles in late vm management
#define GET(dst, src, idx) dst = (!src \
    ? memset(early_arm4(idx, (src = early_alloc(ea) | flags) & PG_ADDR_MASK_4KB), 0, 4096) \
    : early_arm4(idx, (src = ((src ^ PG_NX) | (flags ^ PG_NX)) ^ PG_NX) & PG_ADDR_MASK_4KB))
#else
#define GET(dst, src, idx) dst = (!src \
    ? memset(early_arm4(idx, (src = early_alloc(ea) | PG_PRESENT | PG_RW) & PG_ADDR_MASK_4KB), 0, 4096) \
    : early_arm4(idx, src & PG_ADDR_MASK_4KB))
#endif
    GET(uint64_t* pml3, pml4[PML4_IDX(addr)], 3);
    GET(uint64_t* pml2, pml3[PML3_IDX(addr)], 4);
    GET(uint64_t* pml1, pml2[PML2_IDX(addr)], 5);
    for(size_t i = 0; i < npages; i++)
    {
        pml1[PML1_IDX(addr)] = early_alloc(src) | flags;
        addr -= 4096;
        if(!PML1_IDX(addr+4096))
        {
            if(!PML2_IDX(addr+4096))
            {
                if(!PML3_IDX(addr+4096))
                {
                    assert(PML4_IDX(addr+4096));
                    GET(pml3, pml4[PML4_IDX(addr)], 3);
                }
                GET(pml2, pml3[PML3_IDX(addr)], 4);
            }
            GET(pml1, pml2[PML2_IDX(addr)], 5);
        }
    }
}

struct blob_reloc
{
    uint64_t pgd;
    uint64_t memmap_start;
    uint64_t memmap_end;
    uint64_t initrd_start;
    uint64_t initrd_sz;
    uint64_t new_rsp;
    uint64_t mem_ceiling;
};

//XXX: the following code is not entirely correct, and may corrupt data if doing UEFI boot on a really low-memory setup
static uint64_t relocate_blobs(struct blob_reloc* ans, struct early_alloc* ea, uint64_t initrd_loc, uint64_t initrd_sz, uint64_t kernel_base)
{
    struct early_alloc bak = *ea;
    uint64_t addr_memmap = ((uint64_t)&kernel_end + 0x1fffff) & ~4095ull;
    uint64_t addr_initrd;
    size_t memmap_pages;
    size_t initrd_pages;
    if(memmap_begin_phys < initrd_loc)
    {
        initrd_pages = move_blob(ea, initrd_loc, initrd_loc+initrd_sz);
        memmap_pages = move_blob(ea, memmap_begin_phys, memmap_end_phys);
    }
    else
    {
        memmap_pages = move_blob(ea, memmap_begin_phys, memmap_end_phys);
        initrd_pages = move_blob(ea, initrd_loc, initrd_loc+initrd_sz);
    }
    ans->pgd = early_alloc(ea);
    memset(early_arm4(2, ans->pgd), 0, 4096);
    addr_initrd = (addr_memmap + (memmap_pages << 12) + 0x1fffff) & ~4095ull;
    if(memmap_begin_phys < initrd_loc)
    {
        map_from_ea(ans->pgd, addr_initrd, initrd_pages, ea, &bak, PG_PRESENT | PG_RW | PG_NX | PG_ANON);
        map_from_ea(ans->pgd, addr_memmap, memmap_pages, ea, &bak, PG_PRESENT | PG_RW | PG_NX | PG_ANON);
    }
    else
    {
        map_from_ea(ans->pgd, addr_memmap, memmap_pages, ea, &bak, PG_PRESENT | PG_RW | PG_NX | PG_ANON);
        map_from_ea(ans->pgd, addr_initrd, initrd_pages, ea, &bak, PG_PRESENT | PG_RW | PG_NX | PG_ANON);
    }
    bak = *ea;
    size_t kernel_pages = move_blob(ea, kernel_base, (uint64_t)&kernel_end + kernel_base - 0x100000);
    map_from_ea(ans->pgd, 0x100000, kernel_pages, ea, &bak, PG_PRESENT | PG_RW | PG_ANON);
    ans->memmap_start = addr_memmap + (memmap_begin_phys & 4095ull);
    ans->memmap_end = ans->memmap_start + memmap_end_phys - memmap_begin_phys;
    ans->initrd_start = addr_initrd + (initrd_loc & 4095ull);
    ans->initrd_sz = initrd_sz;
    uint64_t addr_stack = (addr_initrd + (initrd_pages << 12) + 0x1fffff) & ~4095ull;
    map_from_ea(ans->pgd, addr_stack, 0x800000 >> 12, ea, ea, PG_PRESENT | PG_RW | PG_ANON);
    ans->new_rsp = addr_stack + 0x800000;
    ans->mem_ceiling = ea->addr;
}

void enter_reloc(struct blob_reloc*, int(*main)(void*, void*, void*, size_t, uint64_t, uint64_t));
extern uint64_t initrd_loc;

static void dbg_addr(uint64_t pgd, uint64_t va)
{
    uint64_t pa;
    uint64_t* pml4 = early_arm4(2, pgd);
    uint64_t* pml3 = early_arm4(3, pml4[PML4_IDX(va)] & PG_ADDR_MASK_4KB);
    uint64_t* pml2 = early_arm4(4, pml3[PML3_IDX(va)] & PG_ADDR_MASK_4KB);
    if(pml2[PML2_IDX(va)] & PG_HUGE)
        pa = (pml2[PML2_IDX(va)] & PG_ADDR_MASK_2MB) | (va & ~PG_ADDR_MASK_2MB);
    else
    {
        uint64_t* pml1 = early_arm4(5, pml2[PML2_IDX(va)] & PG_ADDR_MASK_4KB);
        pa = (pml1[PML1_IDX(va)] & PG_ADDR_MASK_4KB) | (va & ~PG_ADDR_MASK_4KB);
    }
    uint64_t data = *(uint64_t*)early_arm1(pa);
}

uint64_t cmdline = 0;
char late_cmdline[256];

static int cmdline_has_substring(const char* haystack, const char* needle)
{
    size_t l1 = strlen(haystack);
    size_t l2 = strlen(needle);
    if(l1 < l2)
        return 0;
    else if(l1 == l2)
        return !memcmp(haystack, needle, l1+1);
    if(!memcmp(haystack, needle, l2) && haystack[l2] == ' ')
        return 1;
    for(size_t i = 1; i + l2 < l1; i++)
        if(haystack[i-1] == ' ' && !memcmp(haystack+i, needle, l2) && haystack[i+l2] == ' ')
            return 1;
    return haystack[l1-l2-1] == ' ' && !memcmp(haystack+l1-l2, needle, l2+1);
}

void mm_init(int(*main)(void*, void*, void*, size_t, uint64_t, uint64_t), uint64_t kernel_base)
{
    early_puts("in mm_init");
#ifdef EARLY_LOGS
    char buf[17] = "cmdline=";
    for(int i = 7; i >= 0; i--)
    {
        int x = (cmdline >> (i << 2)) & 15u;
        buf[15 - i] = (x < 10) ? (x + '0') : (x - 10 + 'a');
    }
    early_puts(buf);
#endif
    char* cmdline_s = early_arm1((uint64_t)cmdline);
    strncpy(late_cmdline, cmdline_s, 256);
    //XXX: this needs to be adjusted if more boot protocols are added in the future
    if((get_boot_protocol() == BOOT_PROTOCOL_LINUX16 || get_boot_protocol() == BOOT_PROTOCOL_BIOS || get_boot_protocol() == BOOT_PROTOCOL_UEFI_BOOT) && cmdline_has_substring(cmdline_s, "linux32"))
    {
        extern int bootproto;
        bootproto = BOOT_PROTOCOL_LINUX32;
    }
    late_cmdline[255] = 0;
    early_puts("copied command line:");
    early_puts(late_cmdline);
    normalize_memmap(kernel_base);
    early_puts("normalized memmap");
    struct early_alloc ea;
    early_alloc_init(&ea);
    struct
    {
        uint32_t initrd_addr;
        uint32_t initrd_size;
        uint32_t initrd_addr_upper;
    }* initrd_hdr = early_arm1(initrd_loc);
    struct blob_reloc rb;
    relocate_blobs(&rb, &ea, ((uint64_t)initrd_hdr->initrd_addr_upper << 32) | initrd_hdr->initrd_addr, initrd_hdr->initrd_size, kernel_base);
    early_puts("blob relocation done");
    ((uint64_t*)early_arm4(2, rb.pgd))[511] = rb.pgd | PG_PRESENT | PG_RW | PG_NX;
    #ifdef EARLY_LOGS
    struct memmap_entry fake_memmap = {
        .start = 0,
        .end = 0x100000
    };
    struct early_alloc fake_ea = {
        .cur = &fake_memmap,
        .addr = 0xb9000,
        .bottom = 0,
    };
    map_from_ea(rb.pgd, 0xb8000, 1, &ea, &fake_ea, PG_PRESENT | PG_RW | PG_NX);
    #endif
    enter_reloc(&rb, main);
}
