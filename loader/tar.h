#pragma once
#include "types.h"
#include "fs.h"

typedef struct tar_FILE
{
    FILE f;
    char* begin;
    char* end;
} tar_FILE;

struct tar_header
{
    union
    {
        struct
        {
            char name[100];
            char mode[8];
            char uid[8];
            char gid[8];
            char s_size[12];
            char mtime[12];
            char checksum[8];
            char type;
            char link_tgt[100];
            tar_FILE api_object;
        };
        uint64_t header_size[64];
    };
};
