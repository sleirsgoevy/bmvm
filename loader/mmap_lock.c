#include "mmap_lock.h"
#include "cmpxchg.h"

static uintptr_t lock = 0;

void mmap_lock()
{
    while(!cmpxchg(&lock, 0, 1));
}

void mmap_unlock()
{
    cmpxchg(&lock, 1, 0);
}
