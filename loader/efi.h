#pragma once
#include "types.h"

typedef uintptr_t efi_handle;

struct efi_table_header
{
    uint64_t signature;
    uint32_t revision;
    uint32_t header_size;
    uint32_t crc;
    uint32_t reserved;
};

struct efi_system_table
{
    struct efi_table_header hdr;
    uint16_t* firmware_vendor;
    uint32_t firmware_revision;
    efi_handle console_in_handle;
    struct efi_simple_text_input_protocol* con_in;
    efi_handle console_out_handle;
    struct efi_simple_text_output_protocol* con_out;
    efi_handle standard_error_handle;
    struct efi_simple_text_output_protocol* stderr;
    struct efi_runtime_services* runtime_services;
    struct efi_boot_services* boot_services;
    uintptr_t number_of_table_entries;
    struct efi_configuration_table* configuration_table;
};

enum efi_allocate_type
{
    ALLOCATE_ANY_PAGES,
    ALLOCATE_MAX_ADDRESS,
    ALLOCATE_ADDRESS,
    MAX_ALLOCATE_TYPE,
};

enum efi_memory_type
{
    EFI_EESERVED_MEMORY_TYPE,
    EFI_LOADER_CODE,
    EFI_LOADER_DATA,
    EFI_BOOT_SERVICES_CODE,
    EFI_BOOT_SERVICES_DATA,
    EFI_RUNTIME_SERVICES_CODE,
    EFI_RUNTIME_SERVICES_DATA,
    EFI_CONVENTIONAL_MEMORY,
    EFI_UNUSABLE_MEMORY,
    EFI_ACPI_RECLAIM_MEMORY,
    EFI_ACPI_MEMORY_NVS,
    EFI_MEMORY_MAPPED_IO,
    EFI_MEMORY_MAPPED_IO_PORT_SPACE,
    EFI_PAL_CODE,
    EFI_PERSISTENT_MEMORY,
    EFI_UNACCEPTED_MEMORY_TYPE,
    EFI_MAX_MEMORY_TYPE,
};

struct efi_memory_descriptor
{
    uint32_t type;
    uint64_t physical_start;
    uint64_t virtual_start;
    uint64_t number_of_pages;
    uint64_t attribute;
};

struct efi_boot_services
{
    struct efi_table_header hdr;
    __attribute__((ms_abi)) uintptr_t(*raise_tpl)(uintptr_t);
    __attribute__((ms_abi)) void(*restore_tpl)(uintptr_t);
    __attribute__((ms_abi)) int(*allocate_pages)(enum efi_allocate_type, enum efi_memory_type, uintptr_t, void**);
    __attribute__((ms_abi)) int(*free_pages)(void*, uintptr_t);
    __attribute__((ms_abi)) int(*get_memory_map)(uintptr_t*, struct efi_memory_descriptor*, uintptr_t* map_key, uintptr_t* descriptor_size, uint32_t* descriptor_version);
    void* skipped[11];
    __attribute__((ms_abi)) int(*handle_protocol)(efi_handle, const char*, void**);
    void* skipped2[9];
    __attribute__((ms_abi)) int(*exit_boot_services)(efi_handle, uintptr_t);
};

struct efi_simple_text_output_protocol
{
    __attribute__((ms_abi)) int(*reset)(struct efi_simple_text_output_protocol*, bool);
    __attribute__((ms_abi)) int(*output_string)(struct efi_simple_text_output_protocol*, const uint16_t*);
    __attribute__((ms_abi)) int(*test_string)(struct efi_simple_text_output_protocol*, const uint16_t*);
    __attribute__((ms_abi)) int(*query_mode)(struct efi_simple_text_output_protocol*, uintptr_t);
    __attribute__((ms_abi)) int(*set_mode)(struct efi_simple_text_output_protocol*, uintptr_t);
    __attribute__((ms_abi)) int(*set_attribute)(struct efi_simple_text_output_protocol*, uintptr_t);
    __attribute__((ms_abi)) int(*clear_screen)(struct efi_simple_text_output_protocol*);
    __attribute__((ms_abi)) int(*set_cursor_posision)(struct efi_simple_text_output_protocol*, uintptr_t);
    __attribute__((ms_abi)) int(*enable_cursor)(struct efi_simple_text_output_protocol*, bool);
    struct efi_simple_text_output_mode* mode;
};

#define LOADED_IMAGE_PROTOCOL "\xa1\x31\x1b\x5b\x62\x95\xd2\x11\x8e\x3f\x00\xa0\xc9\x69\x72\x3b"

struct efi_loaded_image_protocol
{
    uint32_t revision;
    efi_handle parent_handle;
    struct efi_system_table* system_table;
    efi_handle device_handle;
    struct efi_device_path_protocol* file_path;
    void* reserved;
    uint32_t load_options_size;
    void* load_options;
    void* image_base;
    uint64_t image_size;
    enum efi_memory_type image_code_type;
    enum efi_memory_type image_data_type;
    __attribute__((ms_abi)) int(*unload)(efi_handle);
};

struct efi_file_io_token;

enum
{
    EFI_FILE_MODE_READ = 1,
    EFI_FILE_MODE_WRITE = 2,
    EFI_FILE_MODE_CREATE = 1ull << 63,
};

enum
{
    EFI_FILE_READ_ONLY = 1,
    EFI_FILE_HIDDEN = 2,
    EFI_FILE_SYSTEM = 4,
    EFI_FILE_RESERVED = 8,
    EFI_FILE_DIRECTORY = 16,
    EFI_FILE_ARCHIVE = 32,
    EFI_FILE_VALID_ATTR = 63,
};

struct efi_file_protocol
{
    uint64_t revision;
    __attribute__((ms_abi)) int(*open)(struct efi_file_protocol*, struct efi_file_protocol**, const uint16_t*, uint64_t, uint64_t);
    __attribute__((ms_abi)) int(*close)(struct efi_file_protocol*);
    __attribute__((ms_abi)) int(*delete)(struct efi_file_protocol*);
    __attribute__((ms_abi)) int(*read)(struct efi_file_protocol*, uintptr_t*, void*);
    __attribute__((ms_abi)) int(*write)(struct efi_file_protocol*, uintptr_t*, const void*);
    __attribute__((ms_abi)) int(*get_position)(struct efi_file_protocol*, uint64_t*);
    __attribute__((ms_abi)) int(*set_position)(struct efi_file_protocol*, uint64_t);
    __attribute__((ms_abi)) int(*get_info)(struct efi_file_protocol*, const char*, uintptr_t*, void*);
    __attribute__((ms_abi)) int(*set_info)(struct efi_file_protocol*, const char*, uintptr_t, void*);
    __attribute__((ms_abi)) int(*flush)(struct efi_file_protocol*);
    __attribute__((ms_abi)) int(*open_ex)(struct efi_file_protocol*, struct efi_file_protocol**, const uint16_t*, uint64_t, uint64_t, struct efi_file_io_token*);
    __attribute__((ms_abi)) int(*read_ex)(struct efi_file_protocol*, struct efi_file_io_token*);
    __attribute__((ms_abi)) int(*write_ex)(struct efi_file_protocol*, struct efi_file_io_token*);
    __attribute__((ms_abi)) int(*flush_ex)(struct efi_file_protocol*, struct efi_file_io_token*);
};

struct efi_time
{
    uint16_t year;
    uint8_t month;
    uint8_t day;
    uint8_t hour;
    uint8_t minute;
    uint8_t second;
    uint8_t pad1;
    uint32_t nanosecond;
    int16_t timezone;
    uint8_t daylight;
    uint8_t pad2;
};

#define EFI_FILE_INFO "\x92\x6e\x57\x09\x3f\x6d\xd2\x11\x8e\x39\x00\xa0\xc9\x69\x72\x3b"

struct efi_file_info
{
    uint64_t size;
    uint64_t file_size;
    uint64_t physical_size;
    struct efi_time create_time;
    struct efi_time last_access_time;
    struct efi_time modification_time;
    uint64_t attribute;
    uint16_t filename[];
};

#define SIMPLE_FILE_SYSTEM_PROTOCOL "\x22\x5b\x4e\x96\x59\x64\xd2\x11\x8e\x39\x00\xa0\xc9\x69\x72\x3b"

struct efi_simple_file_system_protocol
{
    uint64_t revision;
    __attribute__((ms_abi)) int(*open_volume)(struct efi_simple_file_system_protocol*, struct efi_file_protocol**);
};

enum
{
    EFI_SUCCESS = 0,
    EFI_BUFFER_TOO_SMALL = 5,
};

struct efi_configuration_table
{
    char vendor_guid[16];
    void* vendor_table;
};

#define EFI_RSDP_GUID_1 "\x30\x2d\x9d\xeb\x88\x2d\xd3\x11\x9a\x16\x00\x90\x27\x3f\xc1\x4d"
#define EFI_RSDP_GUID_2 "\x71\xe8\x68\x88\xf1\xe4\xd3\x11\xbc\x22\x00\x80\xc7\x3c\x88\x81"
