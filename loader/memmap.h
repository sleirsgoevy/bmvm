#pragma once
#include "types.h"
#include "assert.h"

struct memmap_entry
{
    uint64_t start;
    union
    {
        uint64_t end;
        uint64_t length;
    };
    int32_t type;
} __attribute__((packed));

static_assert(sizeof(struct memmap_entry) == 20);

enum { FREE = 1, RESERVED, RECLAIMABLE, NVS, BAD };
