#include "bootproto.h"

int bootproto = BOOT_PROTOCOL_LINUX16;
uint8_t linux_loader_type[3] = {0xff, 0, 0};
uint64_t bootproto_rsdp = 0;

int get_boot_protocol(void)
{
    return bootproto;
}

void get_linux_loader_info(uint8_t data[3])
{
    for(int i = 0; i < 3; i++)
        data[i] = linux_loader_type[i];
}

uint64_t get_bootproto_rsdp(void)
{
    return bootproto_rsdp;
}
