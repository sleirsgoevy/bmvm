use64

global enter_reloc

enter_reloc:
;struct blob_reloc
;{
;    uint64_t pgd;
;    uint64_t memmap_start;
;    uint64_t memmap_end;
;    uint64_t initrd_start;
;    uint64_t initrd_sz;
;    uint64_t new_rsp;
;};
;
;void enter_reloc(struct blob_reloc*, int(*main)(void*, void*, void*, size_t));
mov r10, [rdi]
mov r11, rsi
mov r8, [rdi+48]
mov r9, [rdi+40]
mov rcx, [rdi+32]
mov rdx, [rdi+24]
mov rsi, [rdi+16]
mov rdi, [rdi+8]
mov cr3, r10
mov rsp, r9
mov rax, cr0
bts rax, 16
mov cr0, rax
call r11
cli
hlt
