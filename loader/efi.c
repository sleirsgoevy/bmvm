#include "types.h"
#include "efi.h"
#include "string.h"
#include "memmap.h"
#include "bootproto.h"
#include "tar.h"

struct
{
    uint64_t rcx;
    uint64_t rdx;
    uint64_t rbx;
    uint64_t rsp;
    uint64_t rbp;
    uint64_t rsi;
    uint64_t rdi;
    uint64_t r12;
    uint64_t r13;
    uint64_t r14;
    uint64_t r15;
    uint64_t cr0;
    uint64_t cr3;
    uint64_t cr4;
    uint64_t efer;
    uint64_t eflags;
    char gdtr[10];
    char idtr[10];
    uint16_t es;
    uint16_t cs;
    uint16_t ss;
    uint16_t ds;
    uint16_t fs;
    uint16_t gs;
} efi_stash;

static void puts(struct efi_system_table* system_table, const char* str)
{
    size_t l = 0;
    while(str[l])
        l++;
    uint16_t buf[l+3];
    for(size_t i = 0; i < l; i++)
        buf[i] = (uint8_t)str[i];
    buf[l] = '\r';
    buf[l+1] = '\n';
    buf[l+2] = 0;
    system_table->con_out->output_string(system_table->con_out, buf);
}

static void* alloc(struct efi_system_table* system_table, size_t size, enum efi_memory_type type)
{
    size = (size + 4095) >> 12;
    void* new_memory = 0;
    if(system_table->boot_services->allocate_pages(ALLOCATE_ANY_PAGES, type, size, &new_memory))
        return 0;
    return new_memory;
}

static void free(struct efi_system_table* system_table, void* ptr, size_t size)
{
    size = (size + 4095) >> 12;
    system_table->boot_services->free_pages(ptr, size);
}

static const char* find_option(const char* cmdline, const char* option)
{
    int state = 0;
    while(*cmdline && (state < 0 || option[state]))
    {
        if(*cmdline == ' ' || *cmdline == '\t')
            state = 0;
        else if(state >= 0 && *cmdline == option[state])
            state++;
        else
            state = -1;
        cmdline++;
    }
    if(state > 0 && !option[state])
        return cmdline;
}

static uint64_t atoi(const char* s)
{
    uint64_t ans = 0;
    while(*s >= '0' && *s <= '9')
        ans = 10 * ans + (*s++) - '0';
    return ans;
}

static uint64_t find_and_parse_int(const char* cmdline, const char* option)
{
    cmdline = find_option(cmdline, option);
    if(!cmdline)
        return 0; //default
    return atoi(cmdline);
}

static void parse_initrd_path(const char* cmdline, const char** path_start, const char** path_end)
{
    cmdline = find_option(cmdline, "initrd=");
    if(cmdline)
    {
        *path_start = cmdline;
        while(*cmdline && *cmdline != ' ' && *cmdline != '\t')
            cmdline++;
        *path_end = cmdline;
    }
    else
        *path_start = *path_end = 0;
}

static int load_initrd(struct efi_system_table* system_table, struct efi_loaded_image_protocol* img, const char* path_start, const char* path_end, char** initrd_start, char** initrd_end)
{
    //let's assume that the passed initrd path fits on the stack
    uint16_t initrd_path[path_end-path_start+1];
    for(size_t i = 0; i < path_end-path_start; i++)
        initrd_path[i] = (uint8_t)path_start[i];
    initrd_path[path_end-path_start] = 0;
    struct efi_simple_file_system_protocol* volume = 0;
    if(system_table->boot_services->handle_protocol(img->device_handle, SIMPLE_FILE_SYSTEM_PROTOCOL, (void**)&volume))
    {
        puts(system_table, "Failed to locate the root filesystem");
        return -1;
    }
    struct efi_file_protocol* fs = 0;
    if(volume->open_volume(volume, &fs))
    {
        puts(system_table, "Failed to open the root filesystem");
        return -1;
    }
    struct efi_file_protocol* initrd_file = 0;
    if(fs->open(fs, &initrd_file, initrd_path, EFI_FILE_MODE_READ, 0))
    {
        puts(system_table, "Failed to open initrd");
        fs->close(fs);
        return -1;
    }
    uintptr_t info_size = 0;
    if(initrd_file->get_info(initrd_file, EFI_FILE_INFO, &info_size, 0) != EFI_BUFFER_TOO_SMALL)
    {
        puts(system_table, "Failed to get file info size");
        initrd_file->close(initrd_file);
        fs->close(fs);
        return -1;
    }
    union
    {
        struct efi_file_info info;
        char buf[info_size];
    } info;
    info_size = sizeof(info);
    if(initrd_file->get_info(initrd_file, EFI_FILE_INFO, &info_size, &info))
    {
        puts(system_table, "Failed to get file info");
        initrd_file->close(initrd_file);
        fs->close(fs);
        return -1;
    }
    off_t file_size = info.info.file_size;
    char* buf = alloc(system_table, file_size, EFI_LOADER_DATA);
    if(!buf)
    {
        puts(system_table, "Failed to allocate memory for the initrd");
        return -1;
    }
    off_t offset = 0;
    while(offset < file_size)
    {
        uintptr_t chk = file_size - offset;
        if(initrd_file->read(initrd_file, &chk, buf + offset))
        {
            puts(system_table, "Failed to read from initrd file");
            initrd_file->close(initrd_file);
            fs->close(fs);
            return -1;
        }
        offset += chk;
    }
    *initrd_start = buf;
    *initrd_end = buf + file_size;
    return 0;
}

extern struct memmap_header
{
    struct memmap_entry* begin;
    struct memmap_entry* end;
}* memmap_header;
extern uint64_t exclusive_memory_range_start, exclusive_memory_range_end;

static struct memmap_header* build_memory_map(struct efi_system_table* system_table, uintptr_t* map_key)
{
    struct memmap_header* current_map_header = 0;
    struct memmap_entry* current_map = 0;
    size_t current_size = 0;
    struct efi_memory_descriptor* descr = 0;
    uintptr_t descr_size = 0;
    uintptr_t item_size = 0;
    uint32_t item_version = 0;
    for(;;)
    {
        int status = system_table->boot_services->get_memory_map(&descr_size, descr, map_key, &item_size, &item_version);
        if(status == EFI_BUFFER_TOO_SMALL)
        {
            free(system_table, descr, descr_size);
            descr = alloc(system_table, descr_size, EFI_LOADER_DATA);
            if(!descr)
            {
                puts(system_table, "Failed to allocate memory for the UEFI memory map");
                return 0;
            }
            continue;
        }
        else if(status)
        {
            puts(system_table, "GetMemoryMap failed");
            return 0;
        }
        size_t n_items = descr_size / item_size;
        if(current_size < n_items)
        {
            free(system_table, current_map_header, sizeof(*current_map_header) + current_size * sizeof(*current_map));
            current_size = n_items;
            current_map_header = alloc(system_table, sizeof(*current_map_header) + current_size * sizeof(*current_map), EFI_LOADER_DATA);
            if(!current_map_header)
            {
                puts(system_table, "Failed to allocate memory for the memory map");
                return 0;
            }
            current_map = (void*)(current_map_header + 1);
            continue;
        }
        for(size_t i = 0; i < n_items; i++)
        {
            struct efi_memory_descriptor* src = (void*)((char*)descr + item_size * i);
            struct memmap_entry* dst = current_map + i;
            dst->start = src->physical_start;
            dst->length = src->number_of_pages << 12;
            switch(src->type)
            {
            case EFI_LOADER_CODE:
            case EFI_LOADER_DATA:
            case EFI_BOOT_SERVICES_CODE:
            case EFI_BOOT_SERVICES_DATA:
            case EFI_CONVENTIONAL_MEMORY:
                dst->type = FREE;
                break;
            case EFI_ACPI_RECLAIM_MEMORY:
                dst->type = RECLAIMABLE;
                break;
            case EFI_ACPI_MEMORY_NVS:
                dst->type = NVS;
                break;
            default:
                dst->type = RESERVED;
            }
        }
        current_map_header->begin = current_map;
        current_map_header->end = current_map + current_size;
        return current_map_header;
    }
}

extern uint64_t cmdline;
extern uint64_t initrd_loc;
extern int bootproto;
extern uint64_t bootproto_rsdp;

struct gdtr
{
    uint16_t size;
    uint64_t ptr;
} __attribute__((packed));

extern char kernel_end[];
static uint64_t vmlinux_size = (uint64_t)kernel_end - 0xffa00;
extern uint64_t amalgamation_size;

static int try_parse_amalgam(struct efi_system_table* system_table, char** initrd_start, char** initrd_end, char** cmdline_s)
{
    if(amalgamation_size == vmlinux_size) //not an amalgamation
    {
        *initrd_start = *initrd_end = *cmdline_s = 0;
        return 0;
    }
    *initrd_start = kernel_end;
    struct tar_header* initrd = (void*)kernel_end;
    char* amalgamation_end = kernel_end + (amalgamation_size - vmlinux_size);
    while(amalgamation_end - (char*)initrd >= sizeof(*initrd))
    {
        size_t zeros = 0;
        for(size_t i = 0; i < 64; i++)
            if(!initrd->header_size[i])
                zeros++;
        if(zeros == 64)
            goto found_end;
        size_t sz = 0;
        for(size_t i = 0; i < 12 && initrd->s_size[i]; i++)
            sz = 8 * sz + initrd->s_size[i] - '0';
        sz = (sz + 1023) & -512;
        initrd = (void*)((char*)initrd + sz);
    }
    puts(system_table, "Truncated amalgamation: declared size is smaller than the initrd");
    return -1;
found_end:
    initrd++;
    char* cmdline = *initrd_end = (char*)initrd;
    while(!*cmdline && amalgamation_end - cmdline >= 512)
        cmdline += 512;
    if(cmdline == amalgamation_end || cmdline + 1 == amalgamation_end)
    {
        //no command line is fine
        *cmdline_s = (char*)"";
        return 0;
    }
    if(!*cmdline)
    {
        puts(system_table, "Truncated amalgamation: command line not found, but size is not divisible by 512");
        return -1;
    }
    char* cmdline_end = cmdline;
    while(cmdline_end != amalgamation_end && *cmdline_end)
        cmdline_end++;
    if(cmdline_end == amalgamation_end)
    {
        puts(system_table, "Truncated amalgamation: command line is not null-terminated");
        return -1;
    }
    if(cmdline_end + 1 != amalgamation_end)
    {
        puts(system_table, "Garbage at the end of the amalgamation");
        return -1;
    }
    *cmdline_s = cmdline;
    return 0;
}

int efi_c_entry(efi_handle image_handle, struct efi_system_table* system_table, void* mz_header, uintptr_t kernel_start, uintptr_t kernel_stack, uintptr_t trampoline)
{
    struct efi_loaded_image_protocol* img = 0;
    if(system_table->boot_services->handle_protocol(image_handle, LOADED_IMAGE_PROTOCOL, (void**)&img))
    {
        puts(system_table, "Failed to locate the current efi_loaded_image");
        return -1;
    }
    size_t cmdline_size = img->load_options_size;
    char* initrd_start = 0;
    char* initrd_end = 0;
    char* cmdline_s = 0;
    if(try_parse_amalgam(system_table, &initrd_start, &initrd_end, &cmdline_s))
    {
        puts(system_table, "Failed to parse embedded initrd and command line");
        return -1;
    }
    if(!cmdline_s)
    {
        cmdline_s = alloc(system_table, cmdline_size + 1, EFI_LOADER_DATA);
        if(!cmdline_s)
        {
            puts(system_table, "Failed to allocate memory for the command line");
            return -1;
        }
        for(size_t i = 0; i < cmdline_size; i++)
            cmdline_s[i] = ((uint16_t*)img->load_options)[i];
        cmdline_s[cmdline_size] = 0;
    }
    int uefi_mode = find_and_parse_int(cmdline_s, "uefi=");
    if(uefi_mode < 0 || uefi_mode >= 2)
    {
        puts(system_table, "Unsupported UEFI mode");
        return -1;
    }
    struct memmap_header* header_for_alloc = 0;
    if(uefi_mode == 1)
    {
        uint64_t memory_size = find_and_parse_int(cmdline_s, "uefimem=");
        if(!memory_size)
        {
            puts(system_table, "Must specify a nonzero uefimem= if using uefi=1");
            return -1;
        }
        char* memory = alloc(system_table, memory_size, EFI_RUNTIME_SERVICES_DATA);
        if(!memory)
        {
            puts(system_table, "Failed to allocate runtime memory for uefi=1 mode");
            return -1;
        }
        exclusive_memory_range_start = (uint64_t)memory;
        exclusive_memory_range_end = (uint64_t)(memory + memory_size);
    }
    if(!initrd_start)
    {
        const char* path_start;
        const char* path_end;
        parse_initrd_path(cmdline_s, &path_start, &path_end);
        if(!path_start)
        {
            puts(system_table, "Failed to find initrd. Have you passed the initrd= parameter?");
            return -1;
        }
        if(load_initrd(system_table, img, path_start, path_end, &initrd_start, &initrd_end))
        {
            puts(system_table, "Failed to load initrd");
            return -1;
        }
    }
    struct
    {
        uint32_t initrd_addr;
        uint32_t initrd_size;
        uint32_t initrd_addr_upper;
    }* initrd_ptr = alloc(system_table, sizeof(*initrd_ptr), EFI_LOADER_DATA);
    if(!initrd_ptr)
    {
        puts(system_table, "Failed to allocate memory for the initrd_ptr structure");
        return -1;
    }
    initrd_ptr->initrd_addr = (uint64_t)initrd_start;
    initrd_ptr->initrd_size = initrd_end - initrd_start;
    initrd_ptr->initrd_addr_upper = (uint64_t)initrd_start >> 32;
    uint64_t* page_directories = alloc(system_table, 8192, EFI_LOADER_DATA);
    if(!page_directories)
    {
        puts(system_table, "Failed to allocate memory for the page directories");
        return -1;
    }
    memset(page_directories, 0, 8192);
    uintptr_t map_key;
    struct memmap_header* memmap = build_memory_map(system_table, &map_key);
    if(!memmap)
    {
        puts(system_table, "Failed to build the memory map");
        return -1;
    }
    puts(system_table, "Booting BMVM...");
    uint64_t rsd_ptr = 0;
    uint64_t rsd_ptr_2 = 0;
    for(size_t i = 0; i < system_table->number_of_table_entries; i++)
        if(!memcmp(system_table->configuration_table[i].vendor_guid, EFI_RSDP_GUID_1, 16))
            rsd_ptr = (uintptr_t)system_table->configuration_table[i].vendor_table;
        else if(!memcmp(system_table->configuration_table[i].vendor_guid, EFI_RSDP_GUID_2, 16))
            rsd_ptr_2 = (uintptr_t)system_table->configuration_table[i].vendor_table;
    if(uefi_mode == 0)
        system_table->boot_services->exit_boot_services(image_handle, map_key);
    cmdline = (uintptr_t)cmdline_s;
    initrd_loc = (uintptr_t)initrd_ptr;
    memmap_header = memmap;
    bootproto = uefi_mode ? BOOT_PROTOCOL_UEFI_BOOT : BOOT_PROTOCOL_UEFI_RUNTIME;
    if(rsd_ptr_2)
        bootproto_rsdp = rsd_ptr_2;
    else
        bootproto_rsdp = rsd_ptr;
    uint64_t* final_pgd = page_directories;
    uint64_t* bootstrap_pgd = page_directories + 512;
    char* inner_trampoline = (char*)(bootstrap_pgd+30);
    bootstrap_pgd[511] = (uint64_t)bootstrap_pgd | 3;
    bootstrap_pgd[3] = 0x00af9a000000ffff; //code64 gdt entry
    bootstrap_pgd[28] = ((uint64_t)inner_trampoline & 0xfff) | 0xffff8e000018f000;
    bootstrap_pgd[29] = 0xffffffff;
    //lidt [rel pc+13]
    *inner_trampoline++ = 0x0f;
    *inner_trampoline++ = 0x01;
    *inner_trampoline++ = 0x1d;
    *inner_trampoline++ = 0x0d;
    *inner_trampoline++ = 0x00;
    *inner_trampoline++ = 0x00;
    *inner_trampoline++ = 0x00;
    //lgdt [rel pc+16]
    *inner_trampoline++ = 0x0f;
    *inner_trampoline++ = 0x01;
    *inner_trampoline++ = 0x15;
    *inner_trampoline++ = 0x10;
    *inner_trampoline++ = 0x00;
    *inner_trampoline++ = 0x00;
    *inner_trampoline++ = 0x00;
    //mov rdx, rsp
    *inner_trampoline++ = 0x48;
    *inner_trampoline++ = 0x89;
    *inner_trampoline++ = 0xd4;
    //mov cr3, rcx
    *inner_trampoline++ = 0x0f;
    *inner_trampoline++ = 0x22;
    *inner_trampoline++ = 0xd9;
    *(struct gdtr*)inner_trampoline = (struct gdtr){239, 0x100000};
    *(struct gdtr*)(inner_trampoline+10) = (struct gdtr){31, 0x100000};
    final_pgd[0] = (uint64_t)final_pgd | 3;
    final_pgd[3] = 0x00af9a000000ffff; //code64 gdt entry
    final_pgd[28] = (trampoline & 0xffff) | 0x8e0000180000 | ((trampoline & 0xffff0000) << 32);
    final_pgd[29] = trampoline >> 32;
    final_pgd[256] = (uint64_t)final_pgd | 3;
    for(size_t i = 257; i < 511; i++)
        final_pgd[i] = kernel_start + (i - 256) * 4096 + 3;
    struct gdtr gdt = {31, -4096};
    struct gdtr idt = {239, -4096};
    asm volatile("cli");
    asm volatile("lgdt %0"::"m"(gdt));
    asm volatile("lidt %0"::"m"(idt));
    asm volatile("mov $-8, %%rsp\nmov %%rax, %%cr3"::"a"(bootstrap_pgd),"c"(final_pgd),"d"(kernel_stack),"D"(kernel_start));
    //should not get here
    return 0;
}
