#pragma once
#include "types.h"

extern void* late_initrd_start;
extern size_t late_initrd_size;
extern char late_cmdline[];
