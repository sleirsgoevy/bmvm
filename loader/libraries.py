import gdb, subprocess, sys, os

bmvm_root = os.path.split(os.path.split(__file__)[0])[0]

def load_library(name, base):
    name = '../libs/'+name
    data = subprocess.Popen(('readelf', '-S', name), stdout=subprocess.PIPE, encoding='utf-8').communicate(None)[0]
    data = next(i for i in data.split('\n') if '.text' in i)
    text_offset = int(data.split()[-2], 16)
    gdb.execute('add-symbol-file '+name+' '+hex(base+text_offset))

try: loaded_libs = sys.loaded_libs
except AttributeError: loaded_libs = sys.loaded_libs = set()

def load_library(name, base):
    gdb.execute('add-symbol-file '+os.path.join(bmvm_root, 'build', 'libs', name[:-3], name)+' -o '+hex(base))

def load_libraries():
    x = gdb.parse_and_eval('(struct module*)module_list_head')
    while x != 0:
        y = x.dereference()
        name = y['module_name'].string()
        if name not in loaded_libs:
            loaded_libs.add(name)
            load_library(name, int(y['module_base']))
        x = y['next_module']

load_libraries()
