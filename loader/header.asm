section .text
use16

global initrd_loc
global kernel_end
global amalgamation_size
extern cmdline
extern c_entry
extern memmap_header
extern bootproto
extern linux_loader_type
extern bootproto_rsdp
extern efi_stash
extern efi_c_entry

header:
hdd_heads:
; MZ header
dec bp
pop dx
hdd_cylinders:
; jump to a sane cs, so that the segment registers are known
; unfortunately we've just lost the drive number stored in dl
cli
cld
hdd_sectors:
inc bp ; windows MBR passes the partition table in ds:bp
jmp 0:(0x7c00+(entry_biosboot-header))

entry_biosboot:
dl_stash:
; save the original DS to load partition offset later
mov cx, ds
; fix segment registers
mov ax, cs
mov ss, ax
mov sp, 0x7e00
mov ax, 0x1000
jmp continue

times 0x18-($-header) db 0

freedos_partition_size:
dd 0
freedos_partition_offset:
dd 0

continue:
push cs
pop ds
push ax ; for ss
mov es, ax

; guess the partition offset. [freedos_partition_offset] and [freedos_partition_size] = partition offset guesses on exit from here
cmp dword [0x7c00+(freedos_partition_offset-header)], 0
jnz .guess_done

; if offset was not passed via the freedos bootsector, try to read it from ds:si or ds:bp (windows MBRs like this one)
; assume that the far pointer is valid if it points somewhere in low memory
call try_read_offset
mov [0x7c00+(freedos_partition_offset-header)], eax
mov si, bp
call try_read_offset

; at offset 60 is a pointer to the PE header
jmp .after_pe_offset
times 60-($-header) db 0x90
dd pe_header-header
.after_pe_offset:

mov [0x7c00+(freedos_partition_size-header)], eax
.guess_done:
; now that we no longer have to avoid overwriting MBR, we can move to a bigger stack
pop ss ; pushed above at "continue"
xor sp, sp
sti

; now guess the lost drive number
; GRUB checks drives 0, 1, 0x80-0x8f, but we better check hard drives first to avoid spinning up the floppy motor
; usually BIOSes assign drive number 0x80 to the drive being booted
; the check routine does not return if the check is successful

mov dl, 0x80
hdd_check_loop:
call check_drive
jc check_floppies
inc dl
cmp dl, 0x90
jb hdd_check_loop

check_floppies:
mov dl, 0
call check_drive
jc drive_not_found
mov dl, 1
call check_drive

drive_not_found:
call inline_print_string
db "BMVM boot drive not found. Is your boot partition above 8 gigabytes?", 13, 10, 0
.hang:
jmp .hang

inline_print_string:
mov cx, ds
push cs
pop ds
.out_loop:
pop si
cld
lodsb
push si
test al, al
jz ret_instr
mov ah, 0x0e
int 0x10
jmp .out_loop

check_drive:
; read drive geometry
push dx
mov ah, 8
int 0x13
mov dl, cl
and dl, 63
mov [0x7c00+(hdd_sectors-header)], dl
shr cl, 6
ror cx, 8
inc cx
mov [0x7c00+(hdd_cylinders-header)], cx
shr dx, 8
inc dx
mov [0x7c00+(hdd_heads-header)], dx
pop dx

; check if MBR matches (this means that the whole image is flashed)
xor eax, eax
call try_offset
jc ret_instr
mov eax, [0x7c00+(freedos_partition_offset-header)]
call try_offset
mov eax, [0x7c00+(freedos_partition_size-header)]
call try_offset
clc_ret:
clc
ret_instr:
ret

try_offset:
; check that the provided offset is within the geometry limits
call read_sector ; reads to es:0x200
jc ret_instr
; verify that the sector matches what's currently at 0x7c00
mov si, 0x7c00+(continue-header)
mov di, 0x200+(continue-header)
mov cx, mbr-continue
cld
repe cmpsb
; if they did not match, ZF will be cleared. in this case the trial is *successful* but we haven't found ourselves
jnz clc_ret
; we've got it. read the next sector do a far jump to the continuation code in the second sector
push dx
inc eax
call read_sector ; reads to es:0x200

jmp 0x1000:(found_drive-header)

stc_ret:
stc
ret

read_sector:
; dl = drive number, es:0x200 = destination, eax = which sector
push dx
push eax
xor ecx, ecx
mov bl, dl
mov cl, [0x7c00+(hdd_sectors-header)]
xor edx, edx
div ecx
inc dx
push dx ; which sector
mov cx, [0x7c00+(hdd_heads-header)]
xor edx, edx
div ecx
cmp eax, 0x10000
jae .stc_ret
cmp ax, [0x7c00+(hdd_cylinders-header)]
jae .stc_ret
; ax = which cylinder, dx = which head
mov dh, dl
mov dl, bl
mov cl, ah
mov ch, al
shl cl, 6
pop bx
or cl, bl
mov ax, 0x201 ; read 1 sector
mov bx, 0x200
int 0x13
db 0xb8 ; mov ax, ...
.stc_ret:
stc
pop dx
pop eax
pop dx
ret

try_read_offset:
push cx
movzx edx, cx
movzx esi, si
add edx, edx
lea edx, [8*edx+esi+12]
xor eax, eax
push es
cmp edx, 0xa0000
jae .no_address
sub edx, 4
mov si, dx
shr edx, 16
shl edx, 12
mov es, dx
mov eax, [es:si]
.no_address:
pop es
pop cx
ret

times 0x19c-($-header) db 0
; sha256 hash of vmlinux (or possibly of the whole BIOS boot image)
sha256:
times 32 db 0
; MBR partition table stub
dw 0 ; windows disk ID
mbr:
db 0x80 ; some bioses require at least 1 bootable "partition" in the MBR to boot
times 0x1f1-0x1bf db 0
; linux boot protocol header
db 2 ; setup_sects
dw 0 ; root_flags
dd 0 ; syssize
dw 0 ; ram_size
dw 0 ; vid_mode
dw 0 ; root_dev
db 0x55, 0xaa

cs_base:
jmp entry16
db "HdrS"
dw 0x0202 ; version
dd 0 ; realmode_switch
dw 0 ; start_sys_seg
dw version-cs_base ; kernel_version
type_of_loader:
db 0 ; type_of_loader
db 1 ; loadflags
dw 0 ; setup_move_size
dd 0 ; code32_start

initrd_ptr:
dd 0 ; ramdisk_image
initrd_size:
dd 0 ; ramdisk_size

dd 0 ; bootsect_kludge
dw 0 ; heap_end_ptr
ext_loader_info:
db 0 ; ext_loader_ver
db 0 ; ext_loader_type

p_cmdline:
dd 0 ; cmd_line_ptr

; qemu writes some later version fields in violation of the boot protocol
; this padding here is to prevent it from overwriting executable code
times 0x58-($-cs_base) db 0

entry16:
mov ax, cx
mov ds, ax
mov ss, ax
mov sp, setup_stack_end-cs_base

mov ax, 0x7c0
mov es, ax
mov di, 24
xor ebx, ebx
mov edx, 0x534d4150
.int15_loop:
mov eax, 0xe820
mov ecx, 24
int 0x15
jc .int15_end
movzx cx, cl
mov ax, 20
cmp cx, 20
cmova cx, ax
add di, cx
cmp cx, 20
jae .no_fill_zeroes
sub cx, 20
neg cx
xor al, al
rep stosb
.no_fill_zeroes:
test ebx, ebx
jnz .int15_loop
.int15_end:
movzx edi, di
add edi, 0x7c00
mov dword [es:0], 0x7c18
mov dword [es:4], 0
mov dword [es:8], edi
mov dword [es:12], 0

mov dl, [cs:(type_of_loader-cs_base)]
shl edx, 16
mov dx, [cs:(ext_loader_info-cs_base)]

cli
lgdt [cs:(gdtr16-cs_base)]
mov ax, cs
movzx eax, ax
shl eax, 4
xor cx, cx
mov ds, cx
mov es, cx
mov ss, cx
lea esp, [eax+setup_stack_end-cs_base]
; lgdt [eax+gdtr16-cs_base]
mov ecx, cr0
or cl, 0x25
mov cr0, ecx
mov cx, 16
mov ds, cx
mov es, cx
mov ss, cx
mov fs, cx
mov gs, cx
lea ecx, [eax+initrd_ptr-cs_base]
lea eax, [eax+p_cmdline-cs_base]
jmp far dword 8:start32

gdtr16:
dw end_gdt-1-gdt
dd gdt

version:
db "BMVM loader", 0

found_drive:
lea ebp, [eax-1]
pop dx
; dl = the correct BIOS drive number (found above)
; ebp = partition offset in sectors (or 0 if booting directly from MBR)
; copy the bootsector to its proper location at 0x10000
mov si, 0x7c00
mov di, 0
mov cx, 512
rep movsb

; read the 3rd realmode sector
mov ax, 0x1020
mov es, ax
lea eax, [ebp+2]
call read_sector

; fix the data segment
mov ax, cs
mov ds, ax
mov [dl_stash-header], dl

; unlock the A20 gate
call a20_unlock

; read protected mode code
mov ecx, [kernel_end_p-header]
sub ecx, 0x100000
shr ecx, 9
mov edi, 0x100000
lea eax, [ebp+3]

pm_read_loop:
call pm_read
test ecx, ecx
jnz pm_read_loop

; initrd is loaded right after us
mov [initrd_ptr-header], edi

; now read initrd. we'll have to parse tar to know where it ends
call read_initrd

; save initrd size
mov esi, [initrd_ptr-header]
sub edi, esi
mov [initrd_size-header], edi

; opportunistically try to read the command line
call try_read_cmdline

; set the boot protocol to BIOS
xor ax, ax
mov es, ax
A32 mov byte [es:bootproto], 3 ; BIOS

; set the bootloader identification to "unknown" (0xff)
mov byte [type_of_loader-header], 0xff

; jump to linux16 entry point
jmp 0x1020:0

; int13_read: read at most #cl sectors at #eax (from 0) to si:di
; returns in cl the number of sectors actually read
int13_read:
push cx
movzx ebx, byte [hdd_sectors-header]
xor edx, edx
idiv ebx
mov cx, dx
inc cx
movzx ebx, word [hdd_heads-header]
xor edx, edx
idiv ebx
mov dh, dl
ror ax, 8
shl al, 6
or cx, ax
mov dl, [dl_stash-header]
pop ax
mov ah, byte [hdd_sectors-header]
sub ah, cl
and ah, 63 ; cl contains the high bits of cylinder number, we want only 6-bit arithmetic here
inc ah
cmp al, ah
jb .not_too_much
mov al, ah
.not_too_much:
mov ah, 2
push ax
mov es, si
mov bx, di
sti
int 0x13
cli
call enter_unreal_mode
pop cx
ret_label:
ret

kernel_end_p:
dd kernel_end

; pm_read: read at most #ecx sectors starting at #eax (from 0) to 0:edi, increments eax and edi
; returns in #ecx the remaining number of sectors
; this technically belongs to the bootsector, but it does not fit there
pm_read:
push ds
push ecx
cmp ecx, 256
jb .not_too_much
mov cl, 255
.not_too_much:
push eax
push edi
mov si, 0x1000
mov di, 0x0600
call int13_read
movzx ebx, cl
pop edi
xor ax, ax
mov ds, ax
mov es, ax
mov esi, 0x10600
mov ecx, ebx
shl ecx, 7 ; 512 / 4 = 128
rep A32 movsd
pop eax
pop ecx
sub ecx, ebx
add eax, ebx
pop ds
ret

; same for these
print_loading:
push edi
mov byte [0x0664], 0 ; end of file name, this is a copy so no fear to corrupt
call inline_print_string
db "Loading ", 0
mov si, 0x0600
call print_string
call inline_print_string
db "...", 13, 10, 0
pop edi
ret

print_cmdline:
call inline_print_string
db "Command line: ", 0
mov si, 0x1000
call print_string
call inline_print_string
db 13, 10, "Booting...", 13, 10, 0
ret

print_string:
lodsb
test al, al
jz ret_label
mov ah, 0x0e
int 0x10
jmp print_string

; unlock a20
; copy-pasted from osdev.org
a20_unlock:
cli
 
call a20wait
mov al,0xAD
out 0x64,al
 
call a20wait
mov al,0xD0
out 0x64,al
 
call a20wait2
in al,0x60
push eax
 
call a20wait
mov al,0xD1
out 0x64,al
 
call a20wait
pop eax
or al,2
out 0x60,al
 
call a20wait
mov al,0xAE
out 0x64,al
 
call a20wait
ret
 
a20wait:
in al,0x64
test al,2
jnz a20wait
ret
 
a20wait2:
in al,0x64
test al,1
jz a20wait2
ret

parse_file_size:
mov edx, 0
mov si, 0x067c ; tar_header->s_size
.size_parse_loop:
lodsb
test al, al
jz .ret_label
shl edx, 3
and al, 7
or dl, al
cmp si, 0x0688 ; tar_header->s_size end
jnz .size_parse_loop
.ret_label:
ret

try_read_cmdline:
push eax
mov si, 0x1000
mov es, si
mov di, 0x1000
push di
mov cx, 512/4
xor eax, eax
rep stosd
pop di
pop eax
push eax
; gnu tar adds several empty padding sectors to the end of archive
; detect them by adding a space to the "default" command line
mov byte [es:di], ' '
mov cl, 1
call int13_read ; if it fails, the space is already all zeros
pop eax
inc eax
cmp byte [es:0x1000], 0
jz try_read_cmdline

; save command line pointer
mov dword [p_cmdline-header], 0x11000
mov byte [0x1200], 0 ; end of sector

; print the command line
jmp print_cmdline

; enter unreal mode
; we redo it here each time after disabling interrupts, so that we don't depend on BIOS code not messing with that
enter_unreal_mode:
push ds
push es
lgdt [bootsector_gdtr-header]
mov eax, cr0
or al, 1
mov cr0, eax
mov cx, 8
mov ds, cx
mov es, cx
and al, 0xfe
mov cr0, eax
pop es
pop ds
ret

; read initrd
; eax = start of initrd -> last sector of initrd
read_initrd:
; read first sector
; a copy is stored at ds:0x0600
mov ecx, 1
call pm_read

; parse file size
cmp byte [0x067c], 0 ; tar_header->s_size
jz ret_label ; if the size is 0 digits, we've found the end of the archive

; print the loading banner
push eax
call print_loading

call parse_file_size ; edx = size
pop eax

; if the file is empty, next file follows immediately
test edx, edx
jz read_initrd

; calculate file size in sectors
mov ecx, edx
add ecx, 511
shr ecx, 9

; read these sectors
inner_read_loop:
call pm_read
test ecx, ecx
jnz inner_read_loop
jmp read_initrd

bootsector_gdt:
dq 0
db 0xff, 0xff, 0, 0, 0, 0x92, 0xcf, 0

bootsector_gdtr:
dw 15
dd 0x10000+(bootsector_gdt-header)

times 0x600-($-header) db 0
setup_stack_end:

prot_base: ; 0x100000
entry32:
pml4:
; this contains a jump to comply with the 32-bit boot protocol
; will be patched later into: dq 0x100003 ; loop
dq (proto32-pml4-5) * 256 + 0xe9 ; 32-bit relative jump
dq 0 ; 2mb arb rw 0
dq 0 ; 2mb arb rw 1
dq 0 ; 2mb arb rw 2
dq 0 ; 2mb arb rw 3
dq 0 ; 2mb arb rw 4
dq 0 ; 2mb arb rw 5
dq 0 ; 2mb arm rw for framebuffer

; PE header
pe_header:
db "PE", 0, 0
dw 0x8664
dw 1
dd 0
dd 0
dd 0
dw end_optional_header - optional_header
dw 0x2e

optional_header:
dw 0x20b
db 0
db 0
dd 0
dd 0
dd 0
dd efi_entry - prot_base
dd 0

dq 0
dd 8
dd 8
dw 0
dw 0
dw 0
dw 0
dw 0
dw 0
dd 0
dd kernel_end - header
dd end_sections - header
dd 0
dw 10
dw 0
dq 0
dq 0
dq 0
dq 0
dd 0
dd 16

times 5 dq 0
dd relocations - prot_base
dd 12
times 10 dq 0

end_optional_header:

db ".text", 0, 0, 0
dd kernel_end - end_pml4
dd end_pml4 - prot_base
dd kernel_end - end_pml4
dd end_pml4 - header
dd 0
dd 0
dw 0
dw 0
dd 0xe0000060

end_sections:

times 2048-($-pml4) db 0
x64_stack: ; reuse unused pml4 space as C stack
%rep 256
dq ($ - prot_base) * 512 + 3 ; identity map 0x100000-0x200000
%endrep
end_pml4:

; scripts/watermark.py expects this to be at the start of text section. do not move!
amalgamation_size:
dq kernel_end - header

align 4
relocations:
dd 4096
dd 12
dw 0
dw 0

gdtr:
dw end_gdt-1-gdt
dq gdt ; this is also used from the EFI entrypoint, so has to be 64-bit

use32
proto32:
; kexec jumps here with EFER.LME already set, a 64-bit code selector, and possibly invalid gdt
; we really need to load our own segments
lgdt [gdtr]
mov cx, 16
mov ds, cx
mov es, cx
mov ss, cx
mov fs, cx
mov gs, cx
; before we jump to the common code, set up memory map pointers
movzx eax, byte [esi+0x1e8]
imul eax, eax, 20
lea edi, [esi+0x2d0]
mov [memmap_proto32], edi
add edi, eax
mov [memmap_proto32+8], edi
mov dword [memmap_header], memmap_proto32
; pointers to initrd & cmdline
lea ecx, [esi+initrd_ptr-header]
lea eax, [esi+p_cmdline-header]
; acpi rsdp
mov edx, [esi+0x70]
mov [bootproto_rsdp], edx
mov edx, [esi+0x78]
mov [bootproto_rsdp+4], edx
; loader info
mov dl, [esi+type_of_loader-header]
shl edx, 16
mov dx, [esi+ext_loader_info-header]
; boot protocol marker
mov dword [bootproto], 1 ; BOOT_PROTOCOL_LINUX32
jmp far dword 8:start32

start32:
mov dword [pml4], 0x100003
mov dword [ecx+8], 0
mov [initrd_loc], ecx
mov ecx, [eax]
mov [cmdline], ecx
mov [linux_loader_type+1], dx
shr edx, 16
mov [linux_loader_type], dl
mov eax, pml4
mov cr3, eax
mov eax, cr4
bts eax, 5
mov cr4, eax
mov ecx, 0xc0000080 ; IA32_EFER
rdmsr
bts eax, 8
wrmsr
mov eax, cr0
bts eax, 31
mov cr0, eax
jmp far 24:start64

start64:
use64
mov rsp, x64_stack
mov rdi, 0x100000 ; kernel_base
call_c_entry:
call c_entry
cli
hlt

; __attribute__((ms_abi)) int efi_entry(efi_handle, struct efi_system_table*)
efi_entry:
; save machine context for resuming
mov [rel efi_stash], rcx
mov [rel efi_stash+8], rdx
mov [rel efi_stash+16], rbx
mov [rel efi_stash+24], rsp
mov [rel efi_stash+32], rbp
mov [rel efi_stash+40], rsi
mov [rel efi_stash+48], rdi
mov [rel efi_stash+56], r12
mov [rel efi_stash+64], r13
mov [rel efi_stash+72], r14
mov [rel efi_stash+80], r15
mov rdi, rcx
mov rsi, rdx
mov rax, cr0
mov [rel efi_stash+88], rax
mov rax, cr3
mov [rel efi_stash+96], rax
mov rax, cr4
mov [rel efi_stash+104], rax
mov ecx, 0xc0000080 ; IA32_EFER
rdmsr
mov [rel efi_stash+112], eax
mov [rel efi_stash+116], edx
pushfq
pop qword [rel efi_stash+120]
sgdt [rel efi_stash+128]
sidt [rel efi_stash+138]
mov [rel efi_stash+140], es
mov [rel efi_stash+142], cs
mov [rel efi_stash+144], ss
mov [rel efi_stash+146], ds
mov [rel efi_stash+148], fs
mov [rel efi_stash+150], gs
; perform efi-specific initialization
push rbp
lea rdx, [rel pe_header+(prot_base-header)]
lea rcx, [rel prot_base]
mov r8, x64_stack ; explicitly non-relocated
mov r9, .trampoline ; explicitly non-relocated
; int efi_c_entry(efi_handle, struct efi_system_table*, void* mz_header, uintptr_t kernel_start, uintptr_t kernel_end, uintptr_t trampoline)
call efi_c_entry ; usually does not return
; we've had an early recoverable error. we are still a mostly benign efi app, so can just return
mov rsi, [rel efi_stash+40]
mov rdi, [rel efi_stash+48]
pop rbp
ret
.trampoline:
; we fall here after swapping the pagetables
; rdi = kernel_base
lgdt [rel gdtr]
xor ax, ax
mov ds, ax
mov es, ax
mov ss, ax
mov fs, ax
mov gs, ax
mov rsp, x64_stack
jmp call_c_entry

align 8
gdt:
db 0, 0, 0, 0, 0, 0, 0, 0 ; NULL
db 0xff, 0xff, 0, 0, 0, 0x9a, 0xcf, 0 ; CODE32
db 0xff, 0xff, 0, 0, 0, 0x92, 0xcf, 0 ; DATA32
db 0xff, 0xff, 0, 0, 0, 0x9a, 0xaf, 0 ; CODE64
db 0, 0, 0, 0, 0, 0, 0, 0 ; reserve space for TSS
db 0, 0, 0, 0, 0, 0, 0, 0 ; (a double-size segment)
end_gdt:

initrd_loc:
dq 0

memmap_proto32:
dq 0
dq 0

section .footer
align 512
kernel_end:
