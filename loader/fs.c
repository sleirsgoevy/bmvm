#include "types.h"
#include "fs.h"
#include "initrd.h"
#include "string.h"
#include "tar.h"

static FILE* tar_open(const char* path, int mode);
static ssize_t tar_pread(FILE* f, void* dest, size_t sz, off_t off);
static off_t tar_lseek(FILE* f, off_t off, int whence);
static void tar_close(FILE* f){}

static struct fs_driver fsdriver_tar = {
    .name = "tar",
    .open = tar_open,
    .pread = tar_pread,
    .lseek = tar_lseek,
    .close = tar_close,
    .next = 0,
};

static struct tar_header* tar_begin;
static struct tar_header* tar_end;

void tar_init(void)
{
    struct tar_header* cur = tar_begin = late_initrd_start;
    tar_end = (void*)((char*)late_initrd_start + late_initrd_size);
    while(cur < tar_end)
    {
        size_t zeros = 0;
        for(size_t i = 0; i < 64; i++)
            if(!cur->header_size[i])
                zeros++;
        if(zeros == 64)
        {
            tar_end = cur;
            break;
        }
        off_t size = 0;
        for(int i = 0; i < 12 && cur->s_size[i]; i++)
            size = (size << 3) | (cur->s_size[i] & 7);
        cur->api_object.f.drv = &fsdriver_tar;
        cur->api_object.begin = (char*)(cur + 1);
        cur->api_object.end = cur->api_object.begin + size;
        cur += (size + 1023) >> 9;
    }
}

static FILE* tar_open(const char* path, int mode)
{
    if(strlen(path) > 100)
        return 0;
    struct tar_header* cur = tar_begin;
    while(cur < tar_end && strncmp(cur->name, path, 100))
        cur += ((cur->api_object.end - cur->api_object.begin) + 1023) >> 9;
    if(cur >= tar_end)
        return 0;
    return &cur->api_object.f;
}

static ssize_t tar_pread(FILE* f0, void* p, size_t sz, off_t off)
{
    tar_FILE* f = (void*)f0;
    if((ssize_t)sz < 0)
        sz = ((size_t)-1) / 2;
    if(off >= f->end - f->begin)
        sz = 0;
    else if(off + sz >= f->end - f->begin)
        sz = f->end - f->begin - off;
    memcpy(p, f->begin + off, sz);
    return sz;
}

static off_t tar_lseek(FILE* f0, off_t off, int whence)
{
    tar_FILE* f = (void*)f0;
    if(whence == SEEK_SET)
        return off;
    else if(whence == SEEK_END)
        return off + (f->end - f->begin);
    else
        return -1;
}

static struct fs_driver* driver_list = &fsdriver_tar;

FILE* open(const char* path, int mode)
{
    for(struct fs_driver* i = driver_list; i; i = i->next)
    {
        FILE* ans = i->open(path, mode);
        if(ans)
            return ans;
    }
    return 0;
}

ssize_t pread(FILE* f, void* p, size_t sz, off_t off)
{
    return f->drv->pread(f, p, sz, off);
}

off_t lseek(FILE* f, off_t off, int whence)
{
    if(f->drv->lseek)
        return f->drv->lseek(f, off, whence);
    return -1;
}

void close(FILE* f)
{
    f->drv->close(f);
}

void register_fs_driver(struct fs_driver* drv)
{
    register uintptr_t rax asm("rax");
    asm volatile("mov %cr0, %rax\nbtc $16, %rax\nmov %rax, %cr0");
    drv->next = driver_list;
    driver_list = drv;
    asm volatile("mov %cr0, %rax\nbts $16, %rax\nmov %rax, %cr0");
}
