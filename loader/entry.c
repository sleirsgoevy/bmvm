#include "types.h"
#include "initrd.h"
#include "fs.h"
#include "elf.h"
#include "assert.h"
#include "string.h"

void mm_init(int(*)(void*, void*, void*, size_t, uint64_t, uint64_t), uint64_t);
void mm_init_late(void*, void*, uint64_t, uint64_t);
void tar_init(void);

void* late_initrd_start = 0;
size_t late_initrd_size = 0;

#ifdef EARLY_LOGS

static uint16_t* early_fb = (void*)(uintptr_t)0xb8000;
static int line = 10;

static void early_puts(const char* s)
{
    for(size_t i = 0; s[i]; i++)
        early_fb[80*line+i] = 0x700 | (unsigned char)s[i];
    line++;
}

#else
#define early_puts(...)
#endif

int main(void* memmap_start, void* memmap_end, void* initrd_start, size_t initrd_sz, uint64_t mem_ceiling, uint64_t rsp)
{
    early_puts("in main()");
    mm_init_late(memmap_start, memmap_end, mem_ceiling, rsp);
    early_puts("late mm_init ok");
    late_initrd_start = initrd_start;
    late_initrd_size = initrd_sz;
    tar_init();
    early_puts("tar_init ok, loading libbootstrap");
    void* elf = load_elf(0, "libbootstrap.so");
    assert(elf);
    early_puts("libbootstrap loaded");
#ifdef EARLY_LOGS
    early_puts("early log=screen");
    void* liblog = load_elf(0, "liblog.so");
    if(liblog)
    {
        int(*liblog_main)(const char*) = elf_dlsym_module(liblog, "main");
        if(liblog_main)
            liblog_main("screen");
    }
#endif
    int(*main)(const char*) = elf_dlsym_module(elf, "main");
    assert(main);
    early_puts("calling libbootstrap main");
    return main(late_cmdline);
}

void c_entry(uint64_t kernel_base)
{
    //ensure triple fault on crashes
    char idtr[10] = {0};
    asm volatile("lidt %0"::"m"(idtr));
    mm_init(main, kernel_base);
}
