SECTIONS
{
    . = 0;
    .text (0x100000 - 0x600) : {
        */header.o(.text);
        *(.text);
        *(.text.*);
        *(.rodata);
        *(.rodata.*);
        *(.data);
        *(.data.*);
        *(.got);
        *(.got.plt);
        *(.plt);
        *(.bss);
        *(.footer);
    }
}
