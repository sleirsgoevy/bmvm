#pragma once
#include "types.h"

void* mmap_unlocked(uintptr_t addr, size_t size, int prot, uint64_t phys_addr);
void mmap_lock(void);
void mmap_unlock(void);
