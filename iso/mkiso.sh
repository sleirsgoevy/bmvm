rm output.iso
mkdir root
cp "$SYSLINUX/share/syslinux/isolinux-debug.bin" root/isolinux.bin
chmod +w root/isolinux.bin
cp "$SYSLINUX/share/syslinux/ldlinux.c32" root/
cat > root/isolinux.cfg << EOF
display boot.txt
prompt 1
default 1

label 1
    kernel /vmlinux
    append initrd=/initrd.img
EOF
cp ../loader/vmlinux root/
cp ../libs/initrd.img root/
mkisofs -o output.iso -b isolinux.bin -c boot.cat -no-emul-boot -boot-load-size 4 -boot-info-table root
rm -rf root
