#include <libvm/vm.hpp>
#include <libvm/intel.hpp>
#include <cstring>
extern "C"
{
    #include <elf.h>
}

static vm::VM* dummyCreateVM()
{
    return nullptr;
}

namespace vm
{

VM*(*createVM)(void) = dummyCreateVM;

}

int main(const char* cmdline)
{
    if(!strcmp(cmdline, "intel"))
    {
        struct module* mod = load_elf(nullptr, "libvm_intel.so");
        if(!mod)
            return 1;
        vm::createVM = (vm::VM*(*)(void))elf_dlsym_module(mod, "createIntelVM");
        return 0;
    }
    if(!strcmp(cmdline, "fake"))
    {
        struct module* mod = load_elf(nullptr, "libvm_fake.so");
        if(!mod)
            return 1;
        vm::createVM = (vm::VM*(*)(void))elf_dlsym_module(mod, "createFakeVM");
        return 0;
    }
    //libvm_user explicitly NOT added here, as it's not a real VM
    return 1;
}
