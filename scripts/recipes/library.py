import functools, os.path
from .loader import loader
from ..promise import parallel, coroutine
from ..recipe import recipe

CFLAGS = ['-g', '-O0', '-fno-omit-frame-pointer', '-fno-stack-protector', '-fno-exceptions', '-nostdinc', '-I', 'include', '-mgeneral-regs-only']
CXXFLAGS = ['-fno-rtti', '-fno-use-cxa-atexit']
LDFLAGS = ['-nostdlib', '-shared']

def library_headers(name):
    return library(name).headers

@functools.lru_cache(None)
def library_own_headers(name):
    ans = set()
    if os.path.exists(os.path.join('include', name)):
        for i, j, k in os.walk(os.path.join('include', name)):
            for q in k:
                ans.add(os.path.join(i, q))
    return ans

@functools.lru_cache(None)
def global_headers():
    ans = set()
    for i in os.listdir('include'):
        if not i.startswith('lib'):
            if os.path.isfile(i): ans.add(os.path.join('include', i))
            else: ans |= library_own_headers(i)
    return ans

async def build_c(name, source_name, cflags, headers):
    source_path = os.path.join(name, source_name)
    target_path = os.path.join('build', 'libs', name, source_name+'.o')
    await recipe(target_path, [source_path]+sorted(headers), ['gcc', source_path] + cflags + ['-c', '-o', target_path])
    return target_path

async def build_cpp(name, source_name, cxxflags, headers):
    source_path = os.path.join(name, source_name)
    target_path = os.path.join('build', 'libs', name, source_name+'.o')
    await recipe(target_path, [source_path]+sorted(headers), ['g++', source_path] + cxxflags + ['-c', '-o', target_path])
    return target_path

async def build_asm(name, source_name):
    source_path = os.path.join(name, source_name)
    target_path = os.path.join('build', 'libs', name, source_name+'.o')
    await recipe(target_path, [source_path], ['yasm', '-f', 'elf64', '-g', 'dwarf2', '-o', target_path, source_path])
    return target_path

async def build_library(name, deps=[], *, extra_cflags=[], extra_cxxflags=[], extra_ldflags=[]):
    assert name.startswith('lib')
    assert all(i.startswith('lib') for i in deps), deps
    cflags = CFLAGS + list(extra_cflags)
    cxxflags = CFLAGS + CXXFLAGS + list(extra_cxxflags)
    ldflags = LDFLAGS + list(extra_ldflags)
    headers = library_own_headers(name) | global_headers()
    deps1 = {}
    for i in (await parallel(loader(), *map(library, deps)))[1:]:
        headers |= i.headers
        ldflags += ['-L', i.output_path, '-l'+i.name[3:]]
        deps1.update(i.deps)
    sources = os.listdir(name)
    headers |= {os.path.join(name, i) for i in sources if i.endswith('.h') or i.endswith('.hpp')}
    os.makedirs(os.path.join('build', 'libs', name), exist_ok=True)
    object_files = await parallel(*(
        [build_c(name, i, cflags, headers) for i in sources if i.endswith('.c')]
        + [build_cpp(name, i, cxxflags, headers) for i in sources if i.endswith('.cpp')]
        + [build_asm(name, i) for i in sources if i.endswith('.asm')]))
    target_path = os.path.join('build', 'libs', name, name+'.so')
    await recipe(target_path, object_files, ['gcc'] + object_files + ldflags + ['-o', target_path])
    return os.path.join('build', 'libs', name), headers, deps1

class library_type: pass

@functools.lru_cache(None)
def get_build_config(name):
    config_path = os.path.join(name, 'build.py')
    if os.path.exists(config_path):
        with open(config_path) as file:
            config = file.read()
        out = {}
        exec(compile(config, config_path, 'exec'), dict(globals()), out)
    else:
        out = {'out': build_library(name)}
    return out

async def _library(name):
    self = library_type()
    self.name = name
    self.output_path, self.headers, self.deps = await get_build_config(name)['out']
    self.deps[name] = self
    return self

@functools.lru_cache(None)
def library(name):
    return coroutine(_library(name))
