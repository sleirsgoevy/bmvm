import hashlib

def add_watermark(blob):
    blob = bytearray(blob)
    blob[512-2-64-32:512-2-64] = hashlib.sha256(blob).digest()
    return bytes(blob)

def add_watermark_to_file(file):
    with open(file, 'rb') as f:
        data = f.read()
    data = add_watermark(data)
    with open(file, 'wb') as f:
        f.write(data)

def fix_pe_header(blob):
    blob = bytearray(blob)
    assert blob.startswith(b'MZ')
    pe_offset = int.from_bytes(blob[60:64], 'little')
    assert blob[pe_offset:pe_offset+6] == b'PE\x00\x00\x64\x86'
    size_of_image = int.from_bytes(blob[pe_offset+80:pe_offset+84], 'little')
    delta = len(blob) - size_of_image
    blob[pe_offset+80:pe_offset+84] = len(blob).to_bytes(4, 'little')
    number_of_rva = int.from_bytes(blob[pe_offset+132:pe_offset+136], 'little')
    section_header_offset = int.from_bytes(blob[pe_offset+20:pe_offset+22], 'little') + pe_offset + 24
    assert blob[section_header_offset:section_header_offset+8] == b'.text\x00\x00\x00'
    blob[section_header_offset+8:section_header_offset+12] = (int.from_bytes(blob[section_header_offset+8:section_header_offset+12], 'little') + delta).to_bytes(4, 'little')
    blob[section_header_offset+16:section_header_offset+20] = (int.from_bytes(blob[section_header_offset+16:section_header_offset+20], 'little') + delta).to_bytes(4, 'little')
    section_offset = int.from_bytes(blob[section_header_offset+20:section_header_offset+24], 'little')
    assert int.from_bytes(blob[section_offset:section_offset+4], 'little') == size_of_image
    blob[section_offset:section_offset+4] = len(blob).to_bytes(4, 'little')
    return bytes(blob)
