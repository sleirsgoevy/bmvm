import tarfile, io, os.path
from .loader import loader as build_loader
from .library import library, get_build_config
from .watermark import add_watermark, fix_pe_header
from ..promise import parallel

def get_library_list(config):
    ans = {'libbootstrap'}
    files = set()
    for i in config.split('\n'):
        i = i.split('#', 1)[0].strip()
        if not i: continue
        if '=' not in i: i += '='
        k, v = i.split('=', 1)
        ans.add('lib'+k)
        cfg = get_build_config('lib'+k)
        if 'extra_deps' in cfg:
            extra_libs, extra_files = cfg['extra_deps'](v)
            ans |= set(extra_libs)
            files |= set(extra_files)
    return ans, files

async def get_libraries(config):
    base_deps, files = get_library_list(config)
    libs = {}
    for i in await parallel(*map(library, base_deps)):
        libs.update(i.deps)
    return libs, files

async def get_initrd_bytes(config_path, config_blob=None, blobs={}):
    if config_blob is None:
        with open(config_path, 'rb') as file:
            config_blob = file.read()
    config = config_blob.decode()
    libs, files = await get_libraries(config)
    out = io.BytesIO()
    initrd = tarfile.TarFile(None, 'w', out, format=tarfile.USTAR_FORMAT)
    for k in sorted(libs):
        initrd.add(libs[k].output_path+'/'+k+'.so', k+'.so')
    config_txt = tarfile.TarInfo('config.txt')
    config_txt.size = len(config_blob)
    for i in sorted(files):
        if i in blobs:
            tar_info = tarfile.TarInfo(i)
            tar_info.size = len(blobs[i])
            initrd.addfile(tar_info, io.BytesIO(blobs[i]))
        else:
            initrd.add(os.path.join(os.path.split(config_path)[0] or '.', i), i)
    initrd.addfile(config_txt, io.BytesIO(config_blob))
    initrd.close()
    return out.getvalue()

async def get_biosdisk_bytes(config_path, cmdline='', config_blob=None, blobs={}):
    cmdline = cmdline.encode() + b'\0'
    loader, initrd = await parallel(build_loader(), get_initrd_bytes(config_path, config_blob, blobs))
    with open(loader, 'rb') as file:
        ans = add_watermark(fix_pe_header(file.read() + initrd + cmdline))
    ans += bytes((-len(ans)) % 512)
    ans += bytes(1048576)
    return ans
