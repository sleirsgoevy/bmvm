import os, functools
from ..promise import coroutine, parallel
from ..recipe import recipe
from .watermark import add_watermark_to_file

headers = [os.path.realpath('loader/'+i) for i in os.listdir('loader') if i.endswith('.h')]
asm_sources = ['header.asm', 'mm_asm.asm']
c_sources = ['entry.c', 'mm_init.c', 'string.c', 'mm.c', 'mmap_lock.c', 'fs.c', 'elf.c', 'exports.c', 'bootproto.c']
efi_c_sources = ['efi.c']

CFLAGS = ['-g', '-O0', '-fno-omit-frame-pointer', '-nostdinc', '-std=gnu11', '-ffreestanding', '-no-pie', '-fno-exceptions', '-fno-stack-protector', '-fno-common', '-mno-sse']
LDFLAGS = ['-nostdlib', '-T', 'loader/link.x']

async def build_c(what, extra_cflags=[]):
    target_path = 'build/loader/' + what[:-2] + '.o'
    await recipe(target_path, ['loader/'+what]+headers, ['gcc', 'loader/'+what] + CFLAGS + extra_cflags + ['-c', '-o', target_path])
    return target_path

# efi.c must be fPIE, as it is executed on a wrong offset
def build_efi_c(what):
    return build_c(what, ['-fPIE'])

async def build_asm(what):
    target_path = 'build/loader/' + what[:-4] + '.o'
    await recipe(target_path, ['loader/'+what], ['yasm', '-f', 'elf64', '-g', 'dwarf2', '-o', target_path, 'loader/'+what])
    return target_path

async def build_loader():
    os.makedirs('build/loader', exist_ok=True)
    target_path = 'build/loader/vmlinux'
    object_files = (await parallel(*([build_c(i) for i in c_sources] + [build_efi_c(i) for i in efi_c_sources] + [build_asm(i) for i in asm_sources])))
    await recipe(target_path+'.elf', object_files, ['gcc'] + LDFLAGS + object_files + ['-o', target_path+'.elf'])
    if await recipe(target_path, [target_path+'.elf'], ['objcopy', '--dump-section', '.text='+target_path, target_path+'.elf', '/dev/null']):
        add_watermark_to_file(target_path)
    return target_path

@functools.lru_cache(None)
def loader():
    return coroutine(build_loader())
