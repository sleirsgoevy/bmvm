import os.path, hashlib, sys, shlex, shutil
from .eventloop import popen

def get_file_modification_time(x):
    return os.stat(x).st_mtime

def get_modification_time(x):
    if not os.path.exists(x):
        return -1
    elif os.path.isdir(x):
        ans = 0
        for i, j, k in os.walk(x):
            ans = max(ans, get_file_modification_time(i))
            for q in k:
                ans = max(ans, get_file_modification_time(os.path.join(i, q)))
        return ans
    else:
        return get_file_modification_time(x)

def discard(target):
    if os.path.exists(target):
        try: os.unlink(target)
        except OSError: shutil.rmtree(target)

def cwd(depth=0):
    return os.path.split(sys._getframe(depth+1).f_globals['__file__'])[0]

async def recipe(target, deps, *args, **kwds):
    target_date = get_modification_time(target)
    deps_date = max(map(get_modification_time, deps))
    if not kwds and len(args) == 1:
        cmd = ' '.join(map(shlex.quote, args[0]))
        cmd_file_path = os.path.join(os.path.split(target)[0], '.' + os.path.split(target)[1] + '.cmd')
        try:
            with open(cmd_file_path) as file: old_cmd = file.read()
        except FileNotFoundError: old_cmd = None
        if cmd != old_cmd:
            target_date = -1
            discard(target)
            with open(cmd_file_path, 'w') as file: file.write(cmd)
    if target_date < deps_date:
        discard(target)
        await popen(*args, **kwds, failure_hook=lambda: discard(target))
        return True
    return False
