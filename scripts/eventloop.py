import subprocess, types, shlex, signal, sys, os
from .promise import Promise, coroutine

running_processes = {}
launch_queue = []

class StopMainLoop(BaseException): pass

class BuildFailure(Exception): pass

def popen(*args, failure_hook=None, **kwds):
    p = Promise()
    launch_queue.append((args, kwds, p, failure_hook))
    return p

def mainloop(p):
    if isinstance(p, types.CoroutineType):
        p = coroutine(p)
    results = []
    build_failures = []
    n_cpus = len(os.listdir('/sys/bus/cpu/devices'))
    p.then(results.append)
    while launch_queue or running_processes:
        while launch_queue and len(running_processes) < n_cpus and not build_failures:
            q = launch_queue.pop()
            popen = subprocess.Popen(*q[0], **q[1])
            print('+', *map(shlex.quote, popen.args), file=sys.stderr)
            running_processes[popen.pid] = (popen, q[2], q[3])
        if not running_processes: break
        pid, status = os.waitpid(-1, 0)
        if status:
            if running_processes[pid][2] is not None:
                running_processes[pid][2]()
            command = "`" + ' '.join(map(shlex.quote, running_processes[pid][0].args)) + "'"
            if os.WIFEXITED(status):
                build_failures.append("Process %s exited with code %d"%(command, os.WEXITSTATUS(status)))
            elif os.WIFSIGNALED(status):
                build_failures.append("Process %s died with signal %d (%s)"%(command, os.WTERMSIG(status), signal.strsignal(os.WTERMSIG(status))))
            else:
                build_failures.append("Process %s exited with unrecognized status %d"%(command, status))
        else:
            running_processes[pid][1].resolve(None)
        del running_processes[pid]
    if build_failures:
        raise BuildFailure(build_failures)
    assert len(results) == 1, results
    return results[0]
