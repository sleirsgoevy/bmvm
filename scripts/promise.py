import subprocess, types

class Promise:
    def __init__(self):
        self._then = []
        self._resolved = False
        self._value = None
    def then(self, fn):
        if self._resolved:
            fn(self._value)
        else:
            self._then.append(fn)
    def resolve(self, value):
        self._resolved = True
        self._value = value
        for i in self._then:
            i(value)
        self._then = None
    def __await__(self):
        ans = yield self
        return ans

def coroutine(f):
    f = f.__await__()
    ans = Promise()
    def make_progress(value):
        try: p = f.send(value)
        except StopIteration as err:
            ans.resolve(err.args[0] if err.args else None)
        else:
            p.then(make_progress)
    make_progress(None)
    return ans

def parallel(*promises):
    promises = [coroutine(i) if isinstance(i, types.CoroutineType) else i for i in promises]
    ans = Promise()
    answers = [None]*len(promises)
    unresolved = set(promises)
    for i, j in enumerate(promises):
        j.then(lambda value, i=i, j=j: (unresolved.discard(j), answers.__setitem__(i, value), unresolved or ans.resolve(answers)))
    return ans
