import sys, os.path, shutil

os.chdir(os.path.split(__file__)[0] or '.')

from scripts.recipes.loader import build_loader
from scripts.recipes.library import library
from scripts.recipes.initrd import get_initrd_bytes, get_biosdisk_bytes
from scripts.eventloop import mainloop, BuildFailure

def usage():
    print('''\
usage: build.py <target> [options...]

<target> may be one of:

    clean [other target...]
        Remove the build directory.

    loader
        Build only the loader.

    lib<name>
        Build the specified library.

    initrd <output> <config.txt>
        Build an initrd with the specified config.txt.

    amalgam <output> <config.txt> [cmdline...]
        Build a BIOS-bootable disk image with the specified config.txt and (optional) loader command line, which is also a UEFI executable.
        This build target is also available as "biosdisk".
''')
    exit(1)

def verify(expr):
    if not expr:
        usage()

verify(len(sys.argv) > 1)

if sys.argv[1] == 'clean':
    if os.path.exists('build'):
        shutil.rmtree('build')
    del sys.argv[1]
    if len(sys.argv) == 1: exit(0)

try:
    if sys.argv[1] == 'loader':
        verify(len(sys.argv) == 2)
        mainloop(build_loader())
    elif sys.argv[1].startswith('lib') and '/' not in sys.argv[1] and os.path.isdir(sys.argv[1]):
        verify(len(sys.argv) == 2)
        mainloop(library(sys.argv[1]))
    elif sys.argv[1] == 'initrd':
        verify(len(sys.argv) == 4)
        out = mainloop(get_initrd_bytes(sys.argv[3]))
        with open(sys.argv[2], 'wb') as file:
            file.write(out)
    elif sys.argv[1] in ('amalgam', 'biosdisk'):
        verify(len(sys.argv) >= 4)
        cmdline = ' '.join(sys.argv[4:])
        out = mainloop(get_biosdisk_bytes(sys.argv[3], cmdline))
        with open(sys.argv[2], 'wb') as file:
            file.write(out)
    else:
        print('Unknown target', sys.argv[1], file=sys.stderr)
        exit(1)
except BuildFailure as err:
    print('Build failed:', file=sys.stderr)
    for i in err.args[0]:
        print('*', i, file=sys.stderr)
    exit(1)
