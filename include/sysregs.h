#pragma once
#include <types.h>
#include <paging.h>

static inline uint64_t phys_addr_4kb(void* p)
{
    return ((uint64_t*)PML1_MOD_BASE)[((uint64_t)p) >> 12] & PG_ADDR_MASK_4KB;
}

static inline uint64_t rdmsr(uint32_t msr)
{
    uint32_t low, high;
    asm volatile("rdmsr":"=a"(low),"=d"(high):"c"(msr));
    return (uint64_t)high << 32 | low;
}

static inline void wrmsr(uint32_t msr, uint64_t value)
{
    uint32_t low = value, high = value >> 32;
    asm volatile("wrmsr"::"a"(low),"d"(high),"c"(msr));
}

static inline uint64_t get_cr4()
{
    uint64_t ans;
    asm volatile("mov %%cr4, %0":"=r"(ans));
    return ans;
}

static inline void set_cr4(uint64_t cr4)
{
    asm volatile("mov %0, %%cr4"::"r"(cr4));
}

#define IA32_APIC_BASE               0x01b
#define IA32_FEATURE_CONTROL         0x03a
#define IA32_TSC_ADJUST              0x03b
#define IA32_SPEC_CTRL               0x048
#define IA32_BIOS_SIGN_ID            0x08b
#define MSR_TURBO_RATIO_LIMITS       0x1ad
#define IA32_PLATFORM_INFO           0x0ce
#define IA32_MPERF                   0x0e7
#define IA32_APERF                   0x0e8
#define IA32_MTRRCAP                 0x0fe
#define IA32_ARCH_CAPABILITIES       0x10a
#define MSR_MISC_FEATURES_ENABLES    0x140
#define IA32_SYSENTER_CS             0x174
#define IA32_SYSENTER_ESP            0x175
#define IA32_SYSENTER_EIP            0x176
#define IA32_MCG_CAP                 0x179
#define IA32_MCG_STATUS              0x17a
#define IA32_THERM_INTERRUPT         0x19b
#define IA32_MISC_ENABLE             0x1a0
#define IA32_PACKAGE_THERM_INTERRUPT 0x1b2
#define IA32_PAT                     0x277
#define IA32_MTRR_DEF_TYPE           0x2ff
#define IA32_VMX_BASIC               0x480
#define IA32_VMX_PINBASED_CTLS       0x481
#define IA32_VMX_PROCBASED_CTLS      0x482
#define IA32_VMX_EXIT_CTLS           0x483
#define IA32_VMX_ENTRY_CTLS          0x484
#define IA32_VMX_CR0_FIXED0          0x486
#define IA32_VMX_CR0_FIXED1          0x487
#define IA32_VMX_CR4_FIXED0          0x488
#define IA32_VMX_CR4_FIXED1          0x489
#define IA32_VMX_PROCBASED_CTLS2     0x48b
#define IA32_VMX_TRUE_PINBASED_CTLS  0x48d
#define IA32_VMX_TRUE_PROCBASED_CTLS 0x48e
#define IA32_VMX_TRUE_EXIT_CTLS      0x48f
#define IA32_VMX_TRUE_ENTRY_CTLS     0x490
#define IA32_IACORE_RATIOS           0x66a
#define IA32_TSC_DEADLINE            0x6e0
#define IA32_X2APIC_ICR              0x830
#define IA32_EFER                    0xc0000080u
#define IA32_STAR                    0xc0000081u
#define IA32_LSTAR                   0xc0000082u
#define IA32_CSTAR                   0xc0000083u
#define IA32_FMASK                   0xc0000084u
#define IA32_FS_BASE                 0xc0000100u
#define IA32_GS_BASE                 0xc0000101u
#define IA32_KERNEL_GS_BASE          0xc0000102u
#define IA32_TSC_AUX                 0xc0000103u

#define IA32_MTRR_PHYSBASE(n) (0x200 + 2*(n))
#define IA32_MTRR_PHYSMASK(n) (0x201 + 2*(n))

static inline void cpuid(uint32_t request, void* data)
{
    uint32_t* d = (uint32_t*)data;
    asm volatile("cpuid":"=a"(d[0]),"=b"(d[1]),"=d"(d[2]),"=c"(d[3]):"a"(request));
}

static inline uint64_t rdtsc(void)
{
    uint32_t low;
    uint32_t high;
    asm volatile("rdtsc":"=d"(high),"=a"(low));
    return (uint64_t)high << 32 | low;
}
