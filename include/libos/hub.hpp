#pragma once
#include "os.hpp"

namespace os
{

void addOSDetector(OS*(*fn)(void*), void* arg = nullptr);
void addOSHandler(void(*fn)(void*, OS*), void* arg = nullptr);

namespace
{

template<class T>
static OS* detector_callme(void* lambda)
{
    return (*(T*)lambda)();
}

template<class T>
static void handler_callme(void* lambda, OS* os)
{
    (*(T*)lambda)(os);
}

}

template<class T>
void addOSDetector(const T& lambda)
{
    addOSDetector(&detector_callme<T>, new T(lambda));
}

template<class T>
void addOSHandler(const T& lambda)
{
    addOSHandler(&handler_callme<T>, new T(lambda));
}

OS* getOS();

}
