#pragma once
#include <vector>
#include <string>
#include <libvm/vm.hpp>

namespace os
{

class OS
{
public:
    struct Library
    {
        std::string name;
        uintptr_t base_address;
    };
protected:
    class ProcessImpl
    {
    public:
        virtual std::string name() = 0;
        virtual uint64_t cr3() = 0;
        virtual std::vector<Library> libraries() = 0;
        virtual ~ProcessImpl(){};
    };
public:
    struct KernelMode
    {
        OS& os;
        uint64_t regs[16];
        uint64_t rip;
        uint64_t rflags;
        vm::GDTEntry cs_segment;
        vm::GDTEntry ss_segment;
        uint16_t cs;
        uint16_t ss;
        uint64_t os_specific[8];
        KernelMode(OS& os) : os(os)
        {
            os.enter_kernel_mode(*this);
        }
        ~KernelMode()
        {
            os.exit_kernel_mode(*this);
        }
    };
    class Process
    {
        ProcessImpl* p;
    public:
        Process() : p(nullptr){}
        Process(ProcessImpl& proc) : p(&proc){}
        Process(Process&& other)
        {
            p = other.p;
            other.p = nullptr;
        }
        std::string name() { return p->name(); }
        uint64_t cr3() { return p->cr3(); }
        std::vector<Library> libraries() { return p->libraries(); }
        ~Process()
        {
            if(p)
                delete p;
        }
    };
    virtual ~OS(){}
    virtual uintptr_t kdlsym(const char* name) = 0;
    virtual void enter_kernel_mode(KernelMode& ans);
    virtual void exit_kernel_mode(const KernelMode& kernel);
    virtual std::vector<uintmax_t> pids() = 0;
    virtual Process find_by_pid(uintmax_t pid) = 0;
};

typedef OS::KernelMode KernelMode;

}
