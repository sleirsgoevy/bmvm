#pragma once
#include <libvm/vm.hpp>
#include <libboot/vm.hpp>

namespace fncall
{

template<vm::Register ...regs>
class ArgumentPassingConvention{};

template<>
class ArgumentPassingConvention<>
{
public:
    static inline void set_regs(uint64_t state[16]){}
    template<typename ...regs>
    static inline void set_regs(uint64_t state[16], uint64_t param, regs ...params)
    {
    }
};

template<vm::Register first, vm::Register ...others>
class ArgumentPassingConvention<first, others...>
{
public:
    static inline void set_regs(uint64_t state[16]){}
    template<typename ...regs>
    static inline void set_regs(uint64_t state[16], uint64_t param, regs ...params)
    {
        state[(int)first] = param;
        ArgumentPassingConvention<others...>::set_regs(state, params...);
    }
};

template<vm::Register retval, vm::Register ...params>
class CallingConvention
{
public:
    static inline uint64_t get_ret(uint64_t state[16])
    {
        return state[(int)retval];
    }
    template<typename ...regs>
    static inline void set_regs(uint64_t state[16], regs ...args)
    {
        ArgumentPassingConvention<params...>::set_regs(state, args...);
    }
};

typedef CallingConvention<vm::Register::RAX, vm::Register::RDI, vm::Register::RSI, vm::Register::RDX, vm::Register::RCX, vm::Register::R8, vm::Register::R9> CC_SYSV;

bool call_raw(uintptr_t fn_addr, uint64_t state[16]);

template<typename cconv, typename ...regs>
uint64_t call(uintptr_t fn_addr, regs ...args)
{
    uint64_t state[16] = {0};
    cconv::set_regs(state, args...);
    if(call_raw(fn_addr, state))
        return cconv::get_ret(state);
    else
        return -1;
}

}
