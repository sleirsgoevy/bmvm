#pragma once
#include <types.h>

namespace coroutine
{

class Coroutine;

bool start(void(*fn)(void*), void* arg, size_t stack_size = 65536);
void resume(Coroutine* co);
Coroutine* self();
void yield(void);
void terminate(void);

namespace
{

template<class T>
static void coroutine_entry(void* arg)
{
    T co = *(T*)arg;
    delete *(T*)arg;
    co();
}

}

template<class T>
inline bool start(T coro)
{
    return start(coroutine_entry<T>, new T(coro));
}

}
