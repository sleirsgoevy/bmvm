#pragma once
#include <vector>

namespace smp
{

uint64_t get_rsdp(void);

uint64_t get_madt(uint64_t rsdp);

uint64_t get_apic_addr(uint64_t madt);

std::vector<uint32_t> get_cpus(uint64_t madt);

}
