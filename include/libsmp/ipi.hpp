#pragma once
#include <types.h>
#include "global_lock.hpp"

namespace smp
{

bool bring_up_cpus(void(*callback)(void), void* stacks, size_t stack_size, int& boot_cpu);
int ncpus(void);
int curcpu(void);
uint64_t pull_nmis(void);
void send_ipi(int which, void(*callback)(void*), void* arg, bool sync=false);
extern "C" int ipi_isr(void);

namespace
{

class Callback
{
public:
    virtual void call() = 0;
    static void call(void* p)
    {
        Callback* cb = (Callback*)p;
        cb->call();
        delete cb;
    }
};

}

template<class F>
void send_ipi(int which, F cb)
{
    class FCallback : Callback
    {
        F data;
    public:
        FCallback(F x) : data(x){}
        void call() { data(); }
    };
    auto arg = new FCallback(cb);
    send_ipi(which, Callback::call, arg);
}

}
