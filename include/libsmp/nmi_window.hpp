#pragma once

namespace smp
{

struct NmiWindow
{
    void* window_start;
    void* window_end;
    void* window_handler;
};

extern "C" NmiWindow smp_nmi_window;

}
