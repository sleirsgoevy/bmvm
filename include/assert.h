#pragma once

#ifndef CONCAT
#define CONCAT0(a, b) a ## b
#define CONCAT(a, b) CONCAT0(a, b)
#endif

#ifndef __cplusplus
#define static_assert(expr) extern char CONCAT(static_assert__, __LINE__)[1 / (expr)];
#endif

#ifdef NDEBUG
#define assert(expr) do {} while(0)
#else
#define assert(expr) do { if(!(expr)) asm volatile("cli\nhlt"::"a"(#expr)); } while(0)
#endif
