#pragma once
#include "types.h"

typedef struct FILE FILE;

struct fs_driver
{
    const char* name;
    FILE*(*open)(const char* path, int mode);
    ssize_t(*pread)(FILE* f, void* dest, size_t sz, off_t off);
    off_t(*lseek)(FILE* f, off_t off, int whence);
    void(*close)(FILE* f);
    struct fs_driver* next;
};

struct FILE
{
    struct fs_driver* drv;
};

enum { O_RDONLY }; //writing not supported

FILE* open(const char* path, int mode);
ssize_t pread(FILE* f, void* dest, size_t sz, off_t off);
off_t lseek(FILE* f, off_t off, int whence);
void close(FILE* f);

enum { SEEK_SET, SEEK_END = 2 };

void register_fs_driver(struct fs_driver* drv);
