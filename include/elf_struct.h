#pragma once
#include "elf.h"
#include "types.h"

struct ehdr
{
    char e_ident[16];
    uint16_t e_type;
    uint16_t e_machine;
    uint32_t e_version;
    uint64_t e_entry;
    uint64_t e_phoff;
    uint64_t e_shoff;
    uint32_t e_flags;
    uint16_t e_ehsize;
    uint16_t e_phentsize;
    uint16_t e_phnum;
    uint16_t e_shentsize;
    uint16_t e_shnum;
    uint16_t e_shstrndx;
};

enum { PT_NULL, PT_LOAD, PT_DYNAMIC, PT_INTERP, PT_NOTE, PT_SHLIB, PT_PHDR, PT_TLS };
enum { PF_X = 1, PF_W = 2, PF_R = 4 };
enum
{
    DT_NEEDED = 1,
    DT_PLTRELSZ = 2,
    DT_STRTAB = 5,
    DT_SYMTAB = 6,
    DT_RELA = 7,
    DT_RELASZ = 8,
    DT_INIT = 12,
    DT_REL = 17,
    DT_RELSZ = 18,
    DT_PLTREL = 20,
    DT_JMPREL = 23,
    DT_INIT_ARRAY = 25,
    DT_INIT_ARRAYSZ = 27,
    DT_GNU_HASH = 0x6ffffef5,
};
enum { SHN_UNDEF = 0, SHN_ABS = 65521 };
enum
{
    R_X86_64_64 = 1,
    R_X86_64_GLOB_DAT = 6,
    R_X86_64_JUMP_SLOT = 7,
    R_X86_64_RELATIVE = 8,
    R_X86_64_32 = 10,
    R_X86_64_32S = 11,
};

struct phdr
{
    int32_t p_type;
    uint32_t p_flags;
    uint64_t p_offset;
    uint64_t p_vaddr;
    uint64_t p_paddr;
    uint64_t p_filesz;
    uint64_t p_memsz;
    uint64_t p_align;
};

struct dynamic
{
    uint64_t key;
    uint64_t value;
};

struct symbol
{
    uint32_t name;
    uint8_t info;
    uint8_t other;
    uint16_t shndx;
    uint64_t value;
    uint64_t size;
};

struct section
{
    void* base;
    size_t size;
    size_t elem_size;
};

struct gnu_hash
{
    uint32_t nbuckets;
    uint32_t symbol_base;
    uint32_t nwords;
    uint32_t gnu_shift;
    uint64_t bitmasks[0];
};

struct rel
{
    uint64_t offset;
    uint32_t type;
    uint32_t sym;
};

struct rela
{
    struct rel rel;
    uint64_t offset;
};
