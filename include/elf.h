#pragma once
#include "fs.h"

struct module;

struct module* load_elf(FILE* f, const char* name);
void* elf_dlsym(const char* name);
void* elf_dlsym_module(struct module* mod, const char* name);
struct module* elf_dladdr(void* addr, const char** module_name, void** module_base);
