#pragma once
#include <utility>
#include <mutex>
#include <libsmp/ipi.hpp>
extern "C"
{
    #include <types.h>
    #include <paging.h>
    #include <mm.h>
    #include <assert.h>
}

namespace vm
{

namespace ept
{

class EPTPML
{
    uint64_t m_phys[512] = {0};
    EPTPML* m_virt[512] = {0};
public:
    ~EPTPML()
    {
        for(size_t i = 0; i < 512; i++)
            if(m_virt[i])
                delete m_virt[i];
    }
    void* operator new(size_t sz) throw()
    {
        void* ans = mmap(0, sizeof(EPTPML), PROT_READ|PROT_WRITE, MAP_ANON);
        if(ans == MAP_FAILED)
            return nullptr;
        return ans;
    }
    void operator delete(void* mem)
    {
        munmap(mem, sizeof(EPTPML));
    }
    uint64_t phys()
    {
        return ((uint64_t*)PML1_MOD_BASE)[((uint64_t)this) >> 12] & PG_ADDR_MASK_4KB;
    }
    EPTPML* get(size_t idx)
    {
        return m_virt[idx];
    }
    void get(size_t idx, EPTPML*& tbl, int& flags)
    {
        tbl = m_virt[idx];
        flags = (int)(m_phys[idx] & ~PG_ADDR_MASK_4KB);
    }
    uint64_t get_phys(size_t idx)
    {
        return m_phys[idx];
    }
    void set(size_t idx, EPTPML* tbl, int flags)
    {
        unset(idx);
        m_virt[idx] = tbl;
        m_phys[idx] = tbl->phys() | flags;
    }
    void set(size_t idx, uint64_t phys, int flags)
    {
        unset(idx);
        m_virt[idx] = nullptr;
        m_phys[idx] = phys | flags;
    }
    void add_flags(size_t idx, int flags)
    {
        m_phys[idx] |= flags;
    }
    void unset(size_t idx)
    {
        m_phys[idx] = 0;
        if(m_virt[idx])
        {
            delete m_virt[idx];
            m_virt[idx] = nullptr;
        }
    }
};

template<typename Subclass>
class EPT
{
    EPTPML* pml4 = nullptr;
    uintptr_t refcount = 1;
    std::mutex lock;
    /*void invept0()
    {
        uint64_t descr[2] = { pml4->phys(), 0 };
        asm volatile("invept %1, %0"::"r"((uint64_t)2),"m"(descr):"memory");
    }*/
    void invept()
    {
        ((Subclass*)this)->invalidate_tlb();
        if(refcount > 1)
        {
            for(int i = 0; i < smp::ncpus(); i++)
                if(i != smp::curcpu())
                    smp::send_ipi(i, shootdown, this, true);
        }
    }
    static void shootdown(void* p)
    {
        ((Subclass*)(EPT*)p)->invalidate_tlb();
    }
    template<int size, int inner_size = (size == 12) ? 12 : (size - 9)>
    bool map_pml(EPTPML* pml, uint64_t start, uint64_t stop, uint64_t phys, int prot, int mt)
    {
        if(start == stop)
            return true;
        uint64_t begin = start >> inner_size;
        uint64_t end = (stop - 1) >> inner_size;
        for(uint64_t i = begin; i <= end; i++)
        {
            uint64_t base = i << inner_size;
            if(inner_size == 12)
            {
                if(!pml->get_phys(i))
                    pml->set(i, phys+base, prot | mt);
                else
                    pml->add_flags(i, prot);
                continue;
            }
            EPTPML* pml_next = pml->get(i);
            if(!pml_next)
            {
                if(inner_size == 21 && pml->get_phys(i))
                {
                    pml->add_flags(i, prot);
                    continue;
                }
                if(inner_size == 21 && start <= base && stop >= ((i + 1) << inner_size) && !(phys & ~PG_ADDR_MASK_2MB))
                {
                    pml->set(i, phys + base, prot | mt | Subclass::HUGE);
                    continue;
                }
                pml_next = new EPTPML();
                if(!pml_next)
                    return false;
                pml->set(i, pml_next, prot);
            }
            else
                pml->add_flags(i, prot);
            if(!map_pml<inner_size>(pml_next, std::max(start, base) - base, std::min(stop, (i + 1) << inner_size) - base, phys + base, prot, mt))
                return false;
        }
        return true;
    }
    template<int size, int inner_size = (size == 12) ? 12 : (size - 9)>
    void unmap_pml_idx(EPTPML* pml, uint64_t idx, uint64_t start, uint64_t stop)
    {
        if(inner_size == 21)
        {
            EPTPML* pml1;
            int flags;
            pml->get(idx, pml1, flags);
            if(flags & Subclass::HUGE)
            {
                uint64_t phys = pml->get_phys(idx);
                pml1 = new EPTPML();
                map_pml<21>(pml1, 0, 0x200000, phys & PG_ADDR_MASK_2MB, flags & Subclass::PROT, flags & 56);
                pml->set(idx, pml1, flags & Subclass::PROT);
            }
        }
        EPTPML* pml1 = pml->get(idx);
        if(pml1)
        {
            uint64_t base = idx << inner_size;
            unmap_pml<inner_size>(pml1, std::max(start, base) - base, std::min(stop, (idx + 1) << inner_size) - base);
        }
    }
    template<int size, int inner_size = size - 9>
    void unmap_pml(EPTPML* pml, uint64_t start, uint64_t stop)
    {
        uint64_t begin = start >> inner_size;
        uint64_t end = (stop - 1) >> inner_size;
        if(start == (begin << inner_size) && stop >= ((begin + 1) << inner_size))
            pml->unset(begin);
        else
            unmap_pml_idx<size>(pml, begin, start, stop);
        for(uint64_t i = start + 1; i < end; i++)
            pml->unset(i);
        if(stop == ((end + 1) << inner_size) && start <= (end << inner_size))
            pml->unset(end);
        else
            unmap_pml_idx<size>(pml, end, start, stop);
    }
protected:
    EPT() : pml4(new EPTPML()){}
public:
    ~EPT()
    {
        if(pml4)
            delete pml4;
    }
    Subclass& incref()
    {
        uintptr_t rc;
        do
            rc = refcount;
        while(!cmpxchg(&refcount, rc, rc+1));
        return *(Subclass*)this;
    }
    void invalidate_tlb(){}
    void decref()
    {
        uintptr_t rc;
        do
            rc = refcount;
        while(!cmpxchg(&refcount, rc, rc-1));
        if(rc == 1)
            delete this;
    }
    bool map(uint64_t start, uint64_t stop, int prot, int mt, uint64_t phys)
    {
        lock.lock();
        bool ans = map_pml<48>(pml4, start, stop, phys - start, (int)prot, (int)mt << 3);
        lock.unlock();
        return ans;
    }
    bool map(uint64_t start, uint64_t stop, int prot, int mt)
    {
        return map(start, stop, prot, mt, 0);
    }
    void unmap(uint64_t start, uint64_t stop)
    {
        lock.lock();
        unmap_pml<48>(pml4, start, stop);
        lock.unlock();
        invept();
    }
    uint64_t translate(uint64_t addr, int& prot, int& mt)
    {
        lock.lock();
        prot = mt = 0;
        EPTPML* pml = pml4;
        int flags = 0;
        pml->get(PML4_IDX(addr), pml, flags);
        if(!pml)
        {
            lock.unlock();
            return 0;
        }
        pml->get(PML3_IDX(addr), pml, flags);
        if(!pml)
        {
            lock.unlock();
            return 0;
        }
        EPTPML* pml1;
        pml->get(PML2_IDX(addr), pml1, flags);
        if((flags & Subclass::HUGE))
        {
            prot = flags & Subclass::PROT;
            mt = (flags >> 3) & 7;
            lock.unlock();
            return (pml->get_phys(PML2_IDX(addr)) & PG_ADDR_MASK_2MB) + (addr & ~PG_ADDR_MASK_2MB);
        }
        pml = pml1;
        if(!pml)
        {
            lock.unlock();
            return 0;
        }
        uint64_t ans = pml->get_phys(PML1_IDX(addr));
        if(!ans)
        {
            lock.unlock();
            return 0;
        }
        prot = ans & Subclass::PROT;
        mt = (ans >> 3) & 7;
        lock.unlock();
        return (ans & PG_ADDR_MASK_4KB) + (addr & ~PG_ADDR_MASK_4KB);
    }
    bool fast_remap(uint64_t addr, uint64_t phys, int prot, int mt)
    {
        lock.lock();
        EPTPML* pml = pml4;
        int flags = 0;
        pml->get(PML4_IDX(addr), pml, flags);
        if(!pml)
        {
            lock.unlock();
            return false;
        }
        pml->get(PML3_IDX(addr), pml, flags);
        if(!pml)
        {
            lock.unlock();
            return false;
        }
        pml->get(PML2_IDX(addr), pml, flags);
        if(!pml)
        {
            lock.unlock();
            return false;
        }
        pml->set(PML1_IDX(addr), phys, prot|(mt<<3));
        lock.unlock();
        return true;
    }
    uint64_t phys()
    {
        return pml4->phys();
    }
    operator bool()
    {
        return pml4 != nullptr;
    }
    EPTPML* lock_pml4()
    {
        lock.lock();
        return pml4;
    }
    void unlock_pml4()
    {
        lock.unlock();
    }
};

}

}
