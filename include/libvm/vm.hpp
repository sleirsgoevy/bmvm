#pragma once
#include <types.h>
extern "C"
{
    #include <mm.h>
}

namespace vm
{

enum class Register
{
    RAX, RCX, RDX, RBX, RSP, RBP, RSI, RDI,
    R8, R9, R10, R11, R12, R13, R14, R15,
    RIP, RFLAGS,
    CR0, CR3, CR4,
    ES, CS, SS, DS, FS, GS, LDT, TR, GDT, IDT
};

struct GDTEntry
{
    unsigned char data[16];
};

enum class ExitReason { UNKNOWN_ERROR, PAGE_FAULT, TRIPLE_FAULT, HYPERCALL, RDMSR, WRMSR, SINGLESTEP, EXCEPTION, VMM_SPEC_START };
enum class CrashType { READ, WRITE, EXECUTE };

constexpr uint64_t NO_MAPPING = -1;

class VM
{
protected:
    bool ok = false;
public:
    operator bool() { return ok; }
    virtual ~VM(){}
    virtual VM* createVCPU() = 0;
    virtual bool map_pages(uint64_t start, uint64_t end, uint64_t phys, bool r, bool w, bool x, int mem_type) = 0;
    virtual uint64_t translate(uint64_t virt, bool& r, bool& w, bool& x, int& mem_type) = 0;
    virtual bool fast_remap(uint64_t virt, uint64_t phys, bool r, bool w, bool x, int mem_type) = 0;
    virtual void unmap_pages(uint64_t start, uint64_t end) = 0;
    virtual bool temp_remap_lock(uint64_t addr1, void*& map1, uint64_t addr2, void*& map2, uint64_t phys1 = MAP_ANON, uint64_t phys2 = MAP_ANON, bool add_r = false, bool add_w = false, bool add_x = false) = 0;
    virtual void temp_remap_unlock() = 0;
    virtual uint64_t get_reg(Register reg) const = 0;
    virtual void set_reg(Register reg, uint64_t value) = 0;
    virtual uint16_t get_segment(Register which, GDTEntry& data) = 0;
    virtual void set_segment(Register which, uint16_t selector, const GDTEntry& data) = 0;
    virtual ExitReason execute(bool singlestep = false) = 0;
    virtual uint64_t get_faulting_address() const = 0;
    virtual CrashType get_fault_reason() const = 0;
    virtual void get_vmcall(char vmcall[3]) const = 0;
    virtual uint64_t rdmsr(uint32_t msr) const = 0;
    virtual void wrmsr(uint32_t msr, uint64_t value) = 0;
    virtual void msr_protect(uint32_t msr, bool r, bool w) = 0;
    virtual void intercept_exception(int vector, bool do_intercept) = 0;
    virtual int get_exception(bool& has_error_code, uint32_t& error_code) = 0;
    virtual void inject_exception(int vector) = 0;
    virtual void inject_exception(int vector, uint32_t error_code) = 0;
};

extern VM*(*createVM)(void);

}
