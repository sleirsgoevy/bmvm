#pragma once
#include "types.h"
#include "paging.h"

enum { MAP_ANON = 1 };

enum
{
    PROT_NONE = 0,
    PROT_READ = PG_PRESENT,
    PROT_WRITE = PG_PRESENT | PG_RW,
};
#define PROT_NOEXEC PG_NX

#define MAP_FAILED ((void*)(uintptr_t)-1)

void* mmap(uintptr_t addr, size_t size, int prot, uint64_t phys_addr);
void mprotect(void* addr, size_t size, int prot);
void munmap(void* addr, size_t size);
uint64_t mm_freeze(uint64_t* upper_bound);
void set_shootdown_hook(void(*)(void));
