#pragma once
#include <stream>
#include <fstream>
#include <sstream>
#include <string>
#include <unordered_map>
#include <utility>
extern "C"
{
    #include <fs.h>
}

namespace config
{

class ConfigFile : private std::unordered_map<std::string, std::string>
{
    struct list
    {
        std::pair<const std::string, std::string>& cur;
        list* next = nullptr;
        list(std::pair<const std::string, std::string>& cur) : cur(cur){}
    };
    struct list* first;
    struct list** last;
    class config_iterator
    {
        friend class ConfigFile;
        struct list* cur;
        config_iterator(struct list* i) : cur(i){}
    public:
        config_iterator& operator++()
        {
            cur = cur->next;
            return *this;
        }
        config_iterator operator++(int)
        {
            config_iterator ans = *this;
            ++*this;
            return ans;
        }
        bool operator==(config_iterator other) { return cur == other.cur; }
        bool operator!=(config_iterator other) { return cur != other.cur; }
        const std::pair<const std::string, std::string>& operator*() { return cur->cur; }
        const std::pair<const std::string, std::string>* operator->() { return &**this; }
    };
public:
    ConfigFile(std::istream&& is, char sep='\n');
    ConfigFile(FILE* f, char sep='\n') : ConfigFile(std::ifstream(f), sep){}
    ConfigFile(const char* path, char sep='\n') : ConfigFile(std::ifstream(path), sep){}
    ConfigFile(const std::string& path, char sep='\n') : ConfigFile(std::ifstream(path), sep){}
    ~ConfigFile()
    {
        for(struct list* i = first; i;)
        {
            struct list* tmp = i->next;
            delete i;
            i = tmp;
        }
    }
    const std::string* getString(const std::string& key) const
    {
        auto it = find(key);
        if(it == std::unordered_map<std::string, std::string>::end())
            return nullptr;
        return &it->second;
    }
    std::string getString(const std::string& key, const std::string& fallback) const
    {
        const std::string* ans = getString(key);
        if(ans)
            return *ans;
        return fallback;
    }
    uint64_t getUnsigned(const std::string& key, uint64_t fallback) const
    {
        const std::string* ans = getString(key);
        if(ans)
        {
            std::istringstream sin(*ans);
            sin >> fallback;
        }
        return fallback;
    }
    int64_t getInt(const std::string& key, int64_t fallback) const
    {
        const std::string* ans = getString(key);
        if(ans)
        {
            std::istringstream sin(*ans);
            sin >> fallback;
        }
        return fallback;
    }
    const config_iterator begin() const { return config_iterator(first); }
    const config_iterator end() const { return config_iterator(nullptr); }
};

}
