#pragma once
#include <libvm/vm.hpp>

namespace hooks
{

class Hook;

constexpr uintptr_t ANY_PC = 0xdead000000000000;

Hook* createHook(vm::ExitReason why, uintptr_t pc_filter, bool(*handle)(void*), void(*cleanup)(void*), void* opaque);
Hook* createVCPUInitHook(bool(*handle)(void*), void(*cleanup)(void*), void* opaque);
void removeHook(Hook* h);

namespace
{

class LambdaWrapper
{
public:
    virtual bool operator()() = 0;
    virtual ~LambdaWrapper(){}
    static bool handle(void* o)
    {
        return (*(LambdaWrapper*)o)();
    }
    static void cleanup(void* o)
    {
        delete (LambdaWrapper*)o;
    }
};

template<class F>
class LambdaWrapperFor : LambdaWrapper
{
    F lambda;
public:
    LambdaWrapperFor(const F& lambda) : lambda(lambda){}
    bool operator()()
    {
        return lambda();
    }
};

}

template<class F>
Hook* createHook(vm::ExitReason why, uintptr_t pc_filter, F lambda)
{
    LambdaWrapperFor<F>* wr = new LambdaWrapperFor<F>(lambda);
    return createHook(why, pc_filter, LambdaWrapper::handle, LambdaWrapper::cleanup, wr);
}

template<class F>
Hook* createVCPUInitHook(F lambda)
{
    LambdaWrapperFor<F>* wr = new LambdaWrapperFor<F>(lambda);
    return createVCPUInitHook(LambdaWrapper::handle, LambdaWrapper::cleanup, wr);
}

}
