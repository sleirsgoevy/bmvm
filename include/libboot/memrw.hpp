#pragma once
#include <types.h>

namespace utils
{

bool copy_from_guest(void* buf, uint64_t addr, size_t sz);
bool copy_to_guest(uint64_t addr, const void* buf, size_t sz);
bool guest_vm_translate(uintptr_t virt, uint64_t& phys, uint64_t& limit, bool& u, bool& w, bool& nx);
bool guest_vm_translate(uintptr_t virt, uint64_t& phys, uint64_t& limit, bool& u, bool& w, bool& nx, uint64_t cr3);
bool copy_from_guest_vm(void* buf, uintptr_t addr, size_t sz);
bool copy_from_guest_vm(void* buf, uintptr_t addr, size_t sz, uint64_t cr3);
bool copy_to_guest_vm(uintptr_t addr, const void* buf, size_t sz);
bool copy_to_guest_vm(uintptr_t addr, const void* buf, size_t sz, uint64_t cr3);

}
