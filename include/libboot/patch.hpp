#pragma once
#include <types.h>

namespace hooks
{

class Patch;

Patch* createPatch(uint64_t base, const char* old, const char* patch, size_t size);
void deletePatch(Patch* p);
bool isPatchValid(Patch* p);
bool stepOut(Patch* p, uintptr_t pc_low, uintptr_t pc_high); //used for breakpoints

}
