#pragma once
#include "types.h"

void* malloc(size_t sz);
void* realloc(void* oldp, size_t newsz);
void free(void* p);
