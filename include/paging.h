#pragma once

enum
{
    PG_PRESENT = 1,
    PG_RW = 2,
    PG_USER = 4,
    PG_WRITETHROUGH = 8,
    PG_NOCACHE = 16,
    PG_ACCESSED = 32,
    PG_HUGE = 128,
    PG_ANON = 512,
    PG_USERMASK = 0xe00,
};

#define PG_NX 0
//#define PG_NX 0x8000000000000000ull
#define PG_ADDR_MASK_4KB   0x7ffffffffffff000ull
#define PG_ADDR_MASK_2MB   0x7fffffffffe00000ull
#define PG_ADDR_MASK_1GB   0x7fffffffc0000000ull
#define PG_ADDR_MASK_512GB 0x7fffff8000000000ull

#define KMEM_BASE 0xffff800000000000ull
#define MASK_48BIT 0xffff000000000000ull
#define PML4_IDX(addr) (((addr) & ~MASK_48BIT) >> 39 & 511)
#define PML3_IDX(addr) (((addr) & ~MASK_48BIT) >> 30 & 511)
#define PML2_IDX(addr) (((addr) & ~MASK_48BIT) >> 21 & 511)
#define PML1_IDX(addr) (((addr) & ~MASK_48BIT) >> 12 & 511)
#define PML4_OFF(addr) (((addr) & ~MASK_48BIT) >> 39)
#define PML3_OFF(addr) (((addr) & ~MASK_48BIT) >> 30)
#define PML2_OFF(addr) (((addr) & ~MASK_48BIT) >> 21)
#define PML1_OFF(addr) (((addr) & ~MASK_48BIT) >> 12)

#define PML1_MOD_BASE 0xffffff8000000000ull
#define PML2_MOD_BASE (PML1_MOD_BASE + (PML1_MOD_BASE - MASK_48BIT >> 9))
#define PML3_MOD_BASE (PML1_MOD_BASE + (PML2_MOD_BASE - MASK_48BIT >> 9))
#define PML4_MOD_BASE (PML1_MOD_BASE + (PML3_MOD_BASE - MASK_48BIT >> 9))
#define PML_IN(addr) ((addr) << 9)
