#pragma once

namespace keyboard
{

class Keyboard
{
    unsigned char prev_char;
    bool shift = false;
public:
    Keyboard();
    int getkeycode(void);
    char getchar(void);
};

}
