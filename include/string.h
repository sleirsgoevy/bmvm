#pragma once
#include "types.h"

inline void* memcpy(void* dst, const void* src, size_t sz)
{
    if(!sz)
        return dst;
    void* rdi;
    asm volatile("rep movsb":"=D"(rdi),"=S"(src),"=c"(sz):"D"(dst),"S"(src),"c"(sz):"memory");
    return dst;
}

inline void* memset(void* s, int c, size_t n)
{
    if(!n)
        return s;
    void* rdi;
    asm volatile("rep stosb":"=D"(rdi),"=c"(n):"D"(s),"a"(c),"c"(n):"memory");
    return s;
}

inline int memcmp(const void* a0, const void* b0, size_t n)
{
    if(!n)
        return 0;
    const char* a = a0;
    const char* b = b0;
    const char* a2;
    asm volatile("repe cmpsb":"=D"(a2),"=S"(b),"=c"(n):"D"(a),"S"(b),"c"(n));
    return a2[-1] - b[-1];
}

inline size_t strnlen(const char* a, size_t n)
{
    if(!n)
        return 0;
    const char* b;
    asm volatile("repne scasb":"=D"(b),"=c"(n):"D"(a),"c"(n),"a"(0ull));
    return b - a - (b[-1] ? 0 : 1);
}

inline void* memchr(const void* s0, int c, size_t n)
{
    if(!n)
        return 0;
    const char* s = s0;
    const char* b;
    asm volatile("repne scasb":"=D"(b),"=c"(n):"D"(s),"c"(n),"a"(c));
    b--;
    return (*b == c) ? (void*)b : 0;
}

inline char* strchr(const char* s, int c)
{
    while(*s && *s != (char)c)
        s++;
    return *s ? (char*)s : 0;
}

inline size_t strlen(const char* a)
{
    return strnlen(a, (size_t)-1 / 2);
}

inline int strncmp(const char* a, const char* b, size_t n)
{
    n = strnlen(a, n - 1);
    n = strnlen(b, n);
    return memcmp(a, b, n + 1);
}

inline int strcmp(const char* a, const char* b)
{
    return strncmp(a, b, (size_t)-1 / 2);
}

inline char* strncpy(char* a, const char* b, size_t n)
{
    n = strnlen(b, n-1)+1;
    return memcpy(a, b, n);
}
