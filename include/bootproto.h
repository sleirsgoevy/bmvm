#pragma once
#include "types.h"

enum boot_protocol
{
    BOOT_PROTOCOL_LINUX16 = 0,
    BOOT_PROTOCOL_LINUX32 = 1,
    BOOT_PROTOCOL_NONE = 2,
    BOOT_PROTOCOL_BIOS = 3,
    BOOT_PROTOCOL_UEFI_RUNTIME = 4,
    BOOT_PROTOCOL_UEFI_BOOT = 5,
};

int get_boot_protocol(void);
void get_linux_loader_info(uint8_t data[3]);
void get_linux32_memmap(void** start, void** end);
uint64_t get_bootproto_rsdp(void);
