#pragma once
#include "types.h"

static inline int cmpxchg(volatile uintptr_t* value, uintptr_t from, uintptr_t to)
{
    uintptr_t real;
    asm volatile("lock cmpxchg %3, %2":"=a"(real):"a"(from),"m"(*value),"r"(to));
    return real == from;
}
