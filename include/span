#pragma once
#include <utility>
#include <vector>

namespace std
{

class string_view;

template<class T>
class span
{
    friend class string_view;
    T* start;
    T* stop;
public:
    span(T* ptr, size_t sz) : start(ptr), stop(ptr ? ptr + sz : ptr){}
    span(vector<T>& v) : start(v.begin()), stop(v.end()){}
    span() : start(nullptr), stop(nullptr){}
    T& operator[](size_t i)
    {
        return start[i];
    }
    const T& operator[](size_t i) const
    {
        return start[i];
    }
    T& front() { return *start; }
    T& back() { return stop[-1]; }
    const T& front() const { return *start; }
    const T& back() const { return stop[-1]; }
    size_t size() const { return stop - start; }
    bool empty() const { return stop == start; }
    operator bool() const { return !empty(); }
    T* begin() { return start; }
    const T* begin() const { return start; }
    T* end() { return stop; }
    const T* end() const { return stop; }
};

};
