#pragma once
#include <types.h>

typedef uintptr_t jmp_buf[8];

int setjmp(jmp_buf);
void longjmp(jmp_buf, int) __attribute__((noreturn));
