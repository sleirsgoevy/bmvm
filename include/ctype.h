#pragma once

#ifdef __cplusplus
extern "C"
{
#endif

static inline int isspace(int c)
{
    return c == ' ' || c == '\n' || c == '\t';
}

static inline int isalpha(int c)
{
    return c >= 'A' && c <= 'Z' || c >= 'a' && c <= 'z';
}

static inline int isnumeric(int c)
{
    return c >= '0' && c <= '9';
}

static inline int isalnum(int c)
{
    return isalpha(c) || isnumeric(c);
}

#ifdef __cplusplus
}
#endif
