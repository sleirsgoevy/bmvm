#include <libkbd/keyboard.hpp>
#include <liblog/logging.hpp>

int main()
{
    keyboard::Keyboard kbd;
    while(true)
    {
        logging::log << kbd.getchar() << std::endl;
    }
    return 0;
}
