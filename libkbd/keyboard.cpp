#include <libkbd/keyboard.hpp>

const char symbols[] = "\x00\x1b""1234567890-=\x7f\tqwertyuiop[]\n\x00""asdfghjkl;'`\x00\\zxcvbnm,./\x00*\x00 \x00\0\0\0\0\0\0\0\0\0\0\x00\x00""789-456+1230.";
const char shifted[] = "\x00\x1b""!@#$%^&*()_+\x7f\tQWERTYUIOP{}\n\x00""ASDFGHJKL:\"~\x00|ZXCVBNM<>?\x00*\x00 \x00\0\0\0\0\0\0\0\0\0\0\x00\x00""789-456+1230.";

namespace keyboard
{

Keyboard::Keyboard()
{
    asm volatile("in %%dx, %%al":"=a"(prev_char):"d"(0x60));
}

int Keyboard::getkeycode()
{
    unsigned char cur_char;
    do
        asm volatile("in %%dx, %%al":"=a"(cur_char):"d"(0x60));
    while(cur_char == prev_char);
    prev_char = cur_char;
    return cur_char;
}

char Keyboard::getchar()
{
    int keycode;
    while(((keycode = getkeycode()) & 128) || keycode == 0x2a || keycode > sizeof(symbols) || !symbols[keycode])
    {
        if(keycode == 0x2a)
            shift = true;
        else if(keycode == 0xaa)
            shift = false;
    }
    return shift ? shifted[keycode] : symbols[keycode];
}

}
