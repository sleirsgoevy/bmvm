#include <libconfig/conffile.hpp>
#include <stream>
#include <string>
#include <ctype.h>

namespace config
{

ConfigFile::ConfigFile(std::istream&& stream, char sep)
{
    first = nullptr;
    last = &first;
    std::string s;
    while(std::getline(stream, s, sep))
    {
        size_t sharp = s.find('#');
        if(sharp != s.npos)
            s.resize(sharp);
        size_t eq = s.find('=');
        std::string key;
        std::string value;
        if(eq == s.npos)
        {
            bool all_spaces = true;
            for(char c : s)
                if(!isspace(c))
                {
                    all_spaces = false;
                    break;
                }
             if(all_spaces)
                 continue;
             key.swap(s);
             value = "true";
        }
        else
        {
            key = s.substr(0, eq);
            value = s.substr(eq+1, s.size()-eq-1);
        }
        auto it = find(key);
        if(it != std::unordered_map<std::string, std::string>::end())
            it->second = value;
        else
        {
            (*this)[key] = value;
            it = find(key);
            struct list* i = new list(*it);
            *last = i;
            last = &i->next;
        }
    }
}

}
