#include <libvm/vm.hpp>
#include <cstring>
extern "C"
{
    #include <sysregs.h>
}

extern "C" char trampoline_start[];
extern "C" char trampoline_end[];
extern "C" char trampoline_regs[];

//32-bit trampoline in low memory
extern "C" char trampoline32_start[];
extern "C" char trampoline32_end[];
extern "C" char trampoline32_regs[];

namespace vm_fake
{

class FakeVM : public vm::VM
{
    uint64_t regs[16] = {0};
    uint16_t segregs[6] = {0};
    uint64_t iret_frame[5] = {0};
    uint64_t fs_base = 0;
    uint64_t gs_base = 0;
    uint16_t idt_size = 0;
    uint64_t idt_base = 0;
    uint64_t cr0 = 0;
    uint64_t cr3 = 0;
    uint64_t cr4 = 0;
    uint64_t efer = 0;
    bool is_vcpu;
public:
    FakeVM(bool vcpu);
    ~FakeVM();
    vm::VM* createVCPU();
    bool map_pages(uint64_t start, uint64_t end, uint64_t phys, bool r, bool w, bool x, int mem_type);
    uint64_t translate(uint64_t virt, bool& r, bool& w, bool& x, int& mem_type);
    bool fast_remap(uint64_t virt, uint64_t phys, bool r, bool w, bool x, int mem_type);
    void unmap_pages(uint64_t start, uint64_t end);
    bool temp_remap_lock(uint64_t addr1, void*& map1, uint64_t addr2, void*& map2, uint64_t phys1, uint64_t phys2, bool add_r, bool add_w, bool add_x);
    void temp_remap_unlock();
    uint64_t get_reg(vm::Register reg) const;
    void set_reg(vm::Register reg, uint64_t value);
    uint16_t get_segment(vm::Register which, vm::GDTEntry& data);
    void set_segment(vm::Register which, uint16_t selector, const vm::GDTEntry& data);
    vm::ExitReason execute(bool singlestep);
    uint64_t get_faulting_address() const;
    vm::CrashType get_fault_reason() const;
    void get_vmcall(char vmcall[3]) const;
    uint64_t rdmsr(uint32_t msr) const;
    void wrmsr(uint32_t msr, uint64_t value);
    void msr_protect(uint32_t msr, bool r, bool w);
    void intercept_exception(int vector, bool do_intercept);
    int get_exception(bool& has_error_code, uint32_t& error_code);
    void inject_exception(int vector);
    void inject_exception(int vector, uint32_t error_code);
};

FakeVM::FakeVM(bool vcpu)
{
    ok = true;
    efer = ::rdmsr(IA32_EFER);
    is_vcpu = vcpu;
    iret_frame[2] = 2;
}

FakeVM::~FakeVM(){}

vm::VM* FakeVM::createVCPU()
{
    return new FakeVM(true);
}

bool FakeVM::map_pages(uint64_t start, uint64_t end, uint64_t phys, bool r, bool w, bool x, int mem_type)
{
    //ignore but pretend success
    return true;
}

uint64_t FakeVM::translate(uint64_t virt, bool& r, bool& w, bool& x, int& mem_type)
{
    //all memory is "identity mapped"
    r = w = x = true;
    mem_type = 6;
    return virt;
}

bool FakeVM::fast_remap(uint64_t virt, uint64_t phys, bool r, bool w, bool x, int mem_type)
{
    return true;
}

void FakeVM::unmap_pages(uint64_t start, uint64_t end){}

bool FakeVM::temp_remap_lock(uint64_t addr1, void*& map1, uint64_t addr2, void*& map2, uint64_t phys1, uint64_t phys2, bool add_r, bool add_w, bool add_x)
{
    return false;
}

void FakeVM::temp_remap_unlock(){}

uint64_t FakeVM::get_reg(vm::Register reg) const
{
    if(reg == vm::Register::RSP)
        return iret_frame[3];
    else if(reg <= vm::Register::R15)
        return regs[(int)reg];
    else if(reg == vm::Register::RIP)
        return iret_frame[0];
    else if(reg == vm::Register::RFLAGS)
        return iret_frame[2];
    else if(reg == vm::Register::CS)
        return iret_frame[1];
    else if(reg >= vm::Register::ES && reg <= vm::Register::GS)
        return segregs[(int)reg - (int)vm::Register::ES];
    else if(reg == vm::Register::CR0)
        return cr0;
    else if(reg == vm::Register::CR3)
        return cr3;
    else if(reg == vm::Register::CR4)
        return cr4;
    else
        return 0;
}

void FakeVM::set_reg(vm::Register reg, uint64_t value)
{
    if(reg == vm::Register::RSP)
        iret_frame[3] = value;
    else if(reg <= vm::Register::R15)
        regs[(int)reg] = value;
    else if(reg == vm::Register::RIP)
        iret_frame[0] = value;
    else if(reg == vm::Register::RFLAGS)
        iret_frame[2] = value | 2;
    else if(reg == vm::Register::CR0)
        cr0 = value;
    else if(reg == vm::Register::CR3)
        cr3 = value;
    else if(reg == vm::Register::CR4)
        cr4 = value;
}

uint16_t FakeVM::get_segment(vm::Register reg, vm::GDTEntry& data)
{
    data.data[0] = data.data[1] = 0xff;
    data.data[2] = data.data[3] = data.data[4] = 0;
    data.data[5] = 0x93;
    data.data[6] = 0xcf;
    for(int i = 7; i <= 16; i++)
        data.data[i] = 0;
    if(reg == vm::Register::CS)
    {
        data.data[5] = 0x9b;
        return iret_frame[1];
    }
    else if(reg == vm::Register::LDT || reg == vm::Register::TR || reg == vm::Register::GDT)
        return 0;
    else if(reg != vm::Register::IDT)
        return segregs[(int)reg - (int)vm::Register::ES];
    data.data[0] = idt_size;
    data.data[1] = idt_size >> 8;
    data.data[2] = idt_base;
    data.data[3] = idt_base >> 8;
    data.data[4] = idt_base >> 16;
    data.data[5] = data.data[6] = 0;
    for(int i = 3; i < 8; i++)
        data.data[i+4] = idt_base >> (8*i);
    return 0;
}

void FakeVM::set_segment(vm::Register reg, uint16_t selector, const vm::GDTEntry& data)
{
    if(reg == vm::Register::CS)
        iret_frame[1] = selector;
    else if(reg >= vm::Register::ES && reg <= vm::Register::GS)
        segregs[(int)reg - (int)vm::Register::ES] = selector;
    else if(reg == vm::Register::IDT)
    {
        idt_size = (uint16_t)data.data[1] << 8 | data.data[0];
        idt_base = 0;
        for(int i = 0; i < 3; i++)
            idt_base |= (uint64_t)data.data[i+2] << (i*8);
        for(int i = 3; i < 8; i++)
            idt_base |= (uint64_t)data.data[i+4] << (i*8);
    }
    if(reg == vm::Register::FS || reg == vm::Register::GS)
    {
        uint64_t base = data.data[2] | (uint64_t)data.data[3] << 8 | (uint64_t)data.data[4] << 16;
        for(int i = 3; i < 8; i++)
            base |= (uint64_t)data.data[i+4] << (8*i);
        if(reg == vm::Register::FS)
            fs_base = base;
        else
            gs_base = base;
    }
}

uint64_t FakeVM::get_faulting_address() const
{
    return 0;
}

vm::CrashType FakeVM::get_fault_reason() const
{
    return vm::CrashType::READ;
}

void FakeVM::get_vmcall(char vmcall[3]) const
{
    vmcall[0] = vmcall[1] = vmcall[2] = 0x90;
}

uint64_t FakeVM::rdmsr(uint32_t msr) const
{
    if(msr == IA32_EFER)
        return efer;
    else
        return ::rdmsr(msr);
}

void FakeVM::wrmsr(uint32_t msr, uint64_t value)
{
    if(msr == IA32_EFER)
        efer = value;
    else if(msr != IA32_PAT)
        ::wrmsr(msr, value);
}

void FakeVM::msr_protect(uint32_t msr, bool r, bool w){}

void FakeVM::intercept_exception(int vector, bool do_intercept){}

int FakeVM::get_exception(bool& has_error_code, uint32_t& error_code)
{
    has_error_code = false;
    error_code = 0;
    return 0;
}

void FakeVM::inject_exception(int vector){}

void FakeVM::inject_exception(int vector, uint32_t error_code){}

vm::ExitReason FakeVM::execute(bool singlestep)
{
    asm volatile("cli"); //just for sure
    if(is_vcpu) //just wait for init-sipi from boot cpu
        asm volatile("hlt");
    if((cr0 & 0x80000001ull) != 0x80000001ull || !(efer & 0x100)) //32-bit state
    {
        char* trampoline_page = (char*)mmap(0, 4096, PROT_READ|PROT_WRITE|PROT_NOEXEC, 0x8000);
        if(trampoline_page == MAP_FAILED)
            return vm::ExitReason::UNKNOWN_ERROR;
        memcpy(trampoline_page, trampoline32_start, trampoline32_end-trampoline32_start);
        uint32_t* regs32 = (uint32_t*)(trampoline_page+(trampoline32_regs-trampoline32_start));
        regs32[0] = regs[0];
        regs32[1] = regs[1];
        regs32[2] = regs[2];
        regs32[3] = iret_frame[0];
        regs32[4] = iret_frame[1];
        regs32[5] = iret_frame[2];
        iret_frame[0] = 0x8000;
        iret_frame[1] = 8;
        iret_frame[2] = 2;
        iret_frame[3] = 0;
        iret_frame[4] = 0;
        uint64_t* pmls = (uint64_t*)mmap(0, 4096*3, PROT_READ|PROT_WRITE|PROT_NOEXEC, MAP_ANON);
        if(pmls == MAP_FAILED)
            return vm::ExitReason::UNKNOWN_ERROR;
        memset(pmls, 0, 4096*3);
        pmls[0] = phys_addr_4kb(pmls+512) | PG_RW | PG_PRESENT;
        pmls[512] = phys_addr_4kb(pmls+1024) | PG_RW | PG_PRESENT;
        pmls[1024] = 0 | PG_HUGE | PG_RW | PG_PRESENT;
        cr3 = phys_addr_4kb(pmls);
    }
    uint64_t* pml4 = (uint64_t*)mmap(0, 4096, PROT_READ|PROT_WRITE|PROT_NOEXEC, cr3);
    int pml4_index = 0;
    for(int i = 256; i < 512; i++)
        if(!pml4[i])
            pml4_index = i;
    if(!pml4_index)
        return vm::ExitReason::UNKNOWN_ERROR;
    uint64_t trampoline_virt = 0xffff007ffffff000 | (uint64_t)pml4_index << 39;
    char* trampoline = (char*)mmap(0, 8192, PROT_READ|PROT_WRITE|PROT_NOEXEC, MAP_ANON);
    if(trampoline == MAP_FAILED)
        return vm::ExitReason::UNKNOWN_ERROR;
    memcpy(trampoline, trampoline_start, trampoline_end-trampoline_start);
    char* the_regs = trampoline + (trampoline_regs-trampoline_start);
    memcpy(the_regs, regs, 128);
    memcpy(the_regs+128, iret_frame, 40);
    memcpy(the_regs+168, segregs, 12);
    memcpy(the_regs+180, &idt_size, 2);
    memcpy(the_regs+182, &idt_base, 8);
    memcpy(the_regs+190, &fs_base, 8);
    memcpy(the_regs+198, &gs_base, 8);
    uint64_t* tmp_idt = (uint64_t*)(trampoline + (trampoline_end-trampoline_start));
    memset(tmp_idt, 0, 15*16);
    tmp_idt[14*2] = (uint16_t)trampoline_virt | ((trampoline_virt >> 16) << 48) | 0x8e0000080000;
    tmp_idt[14*2+1] = trampoline_virt >> 32;
    uint8_t tmp_idtr[10];
    tmp_idtr[0] = 15*16-1;
    tmp_idtr[1] = 0;
    uint64_t tmp_idt_virt = trampoline_virt + (trampoline_end-trampoline_start);
    memcpy(tmp_idtr+2, &tmp_idt_virt, 8);
    uint64_t* tmp_gdt = tmp_idt + 15*2;
    tmp_gdt[0] = 0;
    tmp_gdt[1] = 0x00af9a000000ffff; //64-bit code segment
    uint64_t tmp_gdt_virt = tmp_idt_virt + 15*16;
    uint8_t tmp_gdtr[10];
    tmp_gdtr[0] = 15;
    tmp_gdtr[1] = 0;
    memcpy(tmp_gdtr+2, &tmp_gdt_virt, 8);
    uint64_t* tmp_pml4 = (uint64_t*)(trampoline+4096);
    tmp_pml4[-1] = tmp_pml4[pml4_index] = pml4[pml4_index] = phys_addr_4kb(trampoline) | PG_RW | PG_PRESENT;
    uint64_t tmp_cr3 = phys_addr_4kb(tmp_pml4);
    //the trampoline might be the last page, avoid setting rsp to 0
    uint64_t stack = trampoline_virt + 4088;
    asm volatile("lidt %0"::"m"(tmp_idtr));
    asm volatile("lgdt %0"::"m"(tmp_gdtr));
    asm volatile("mov %0, %%cr0"::"r"(cr0 | 0x80000001));
    asm volatile("mov %1, %%rsp\nmov %0, %%cr3"::"r"(tmp_cr3),"r"(stack),"a"(cr3));
    //unreachable
    return vm::ExitReason::TRIPLE_FAULT;
}

}

namespace vm
{

extern "C" VM* createFakeVM()
{
    return new vm_fake::FakeVM(false);
}

}
