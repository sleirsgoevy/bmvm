global trampoline_start
global trampoline_regs
global trampoline_end
use64

trampoline_start:
mov cr3, rax
mov ax, [rel trampoline_regs+168]
call aim
mov es, ax
mov ax, [rel trampoline_regs+172]
call aim
mov ss, ax
mov ax, [rel trampoline_regs+174]
call aim
mov ds, ax
mov ax, [rel trampoline_regs+176]
call aim
mov fs, ax
mov ax, [rel trampoline_regs+178]
call aim
mov gs, ax
; patch data segment into code segment
mov dword [rel data_segment+5], 0xaf9a
mov ax, [rel trampoline_regs+136] ; cs
call aim
; loaded into cs later
mov eax, [rel trampoline_regs+190]
mov edx, [rel trampoline_regs+194]
mov ecx, 0xc0000100
wrmsr
mov eax, [rel trampoline_regs+198]
mov edx, [rel trampoline_regs+202]
mov ecx, 0xc0000101
wrmsr
lidt [rel trampoline_regs+180]
mov rax, [rel trampoline_regs]
mov rcx, [rel trampoline_regs+8]
mov rdx, [rel trampoline_regs+16]
mov rbx, [rel trampoline_regs+24]
mov rbp, [rel trampoline_regs+40]
mov rsi, [rel trampoline_regs+48]
mov rdi, [rel trampoline_regs+56]
mov r8, [rel trampoline_regs+64]
mov r9, [rel trampoline_regs+72]
mov r10, [rel trampoline_regs+80]
mov r11, [rel trampoline_regs+88]
mov r12, [rel trampoline_regs+96]
mov r13, [rel trampoline_regs+104]
mov r14, [rel trampoline_regs+112]
mov r15, [rel trampoline_regs+120]
lea rsp, [rel trampoline_regs+128]
iretq
aim:
movzx edx, ax
and edx, -8
lea rcx, [rel data_segment]
sub rcx, rdx
mov [rel temp_gdt+2], rcx
lgdt [rel temp_gdt]
ret
temp_gdt:
dw 0xffff
dq 0
align 8
data_segment:
db 0xff, 0xff, 0, 0, 0, 0x92, 0xcf, 0
trampoline_regs:
times 16 dq 0 ; normal regs
times 5 dq 0 ; iret frame
times 12 db 0 ; segment regs
times 10 db 0 ; gdt
times 2 dq 0 ; fs & gs base
align 8
trampoline_end:
