global trampoline32_start
global trampoline32_end
global trampoline32_regs

use64

trampoline32_start:
; load stack pointer for iret
lea rsp, [rel trampoline32_regs]

; we can't disable paging while in 64-bit mode, drop to 32-bit mode
lea rax, [rel code_seg-8]
mov [rel gdtr+2], rax
lgdt [rel gdtr]
lea rax, [rel .code32]
mov rcx, rsp
push dword 16
push rcx
push dword 2
push dword 8
push rax
iretq

use32
.code32:

; disable paging
mov eax, cr0
btc eax, 31
mov cr0, eax

; clear IA32_EFER.LME
mov ecx, 0xc0000080
rdmsr
btc eax, 8
wrmsr

; lgdt
lea eax, [esp+(code_seg-trampoline32_regs)]
movzx edx, word [esp+16]
and edx, -8
sub eax, edx
mov [esp+(gdtr+2-trampoline32_regs)], eax
lgdt [esp+(gdtr-trampoline32_regs)]

; pop regs and iret to nowhere
pop eax
pop ecx
pop edx
iretd

align 8
code_seg:
db 0xff, 0xff, 0, 0, 0, 0x9a, 0xcf, 0
stack_seg:
db 0xff, 0xff, 0, 0, 0, 0x92, 0xcf, 0

gdtr:
dw 0xffff
dq 0

times 4 dq 0
align 16
trampoline32_regs:
dd 0 ; eax
dd 0 ; ecx
dd 0 ; edx
dd 0 ; eip
dd 0 ; cs
dd 0 ; eflags

trampoline32_end:
