use64

global setjmp
global longjmp

setjmp:
mov [rdi], rbx
mov [rdi+8], rsp
mov [rdi+16], rbp
mov [rdi+24], r12
mov [rdi+32], r13
mov [rdi+40], r14
mov [rdi+48], r15
mov rax, [rsp]
mov [rdi+56], rax
xor eax, eax
ret

longjmp:
mov rbx, [rdi]
mov rsp, [rdi+8]
mov rbp, [rdi+16]
mov r12, [rdi+24]
mov r13, [rdi+32]
mov r14, [rdi+40]
mov r15, [rdi+48]
mov rax, [rdi+56]
mov [rsp], rax
mov eax, esi
ret
