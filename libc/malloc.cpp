extern "C"
{
    #include <types.h>
    #include <malloc.h>
    #include <mm.h>
}
#include <cstring>
#include <utility>
#include <mutex>

static constexpr size_t PAGE_SIZE = 4096;
static constexpr int DEPTH = 9;

void* operator new(size_t, void* p) { return p; }
void* operator new[](size_t, void* p) { return p; }
void operator delete(void*, void*){}
void operator delete[](void*, void*){}

namespace Allocator
{

struct PageFooter
{
    static constexpr size_t MAX_SIZE()
    {
        return PAGE_SIZE - sizeof(PageFooter);
    }
    struct PageFooter* next;
    struct PageFooter** prev;
    struct PageFooter** head;
    struct freelist
    {
        struct freelist* next;
    }* freelist_head = 0;
    size_t item_size;
    size_t count = 0;
    size_t offset = 0;
    static PageFooter* from_pointer(void* p)
    {
        uintptr_t page = (uintptr_t)p & -PAGE_SIZE;
        return (PageFooter*)(page + PAGE_SIZE - sizeof(PageFooter));
    }
    char* get_page()
    { 
        return (char*)((uintptr_t)this & -PAGE_SIZE);
    }
    void* operator new(size_t sz) throw()
    {
        void* page = mmap(0, PAGE_SIZE, PROT_READ|PROT_WRITE, MAP_ANON);
        if(page == MAP_FAILED)
            return nullptr;
        return from_pointer(page);
    }
    void operator delete(void* p)
    {
        munmap(((PageFooter*)p)->get_page(), PAGE_SIZE);
    }
    void attach()
    {
        next = *head;
        *head = this;
        prev = head;
        if(next)
            next->prev = &next;
    }
    void detach()
    {
        if(next)
            next->prev = prev;
        *prev = next;
    }
    PageFooter(struct PageFooter*& next, size_t item_size) : head(&next), item_size((item_size + 7) & ~7ull)
    {
        attach();
    }
    ~PageFooter()
    {
        detach();
    }
    void* alloc()
    {
        if(freelist_head)
        {
            void* ans = freelist_head;
            freelist_head = freelist_head->next;
            if(!freelist_head && offset + item_size > MAX_SIZE())
                detach();
            count++;
            return ans;
        }
        else if(offset + item_size <= MAX_SIZE())
        {
            void* ans = get_page() + offset;
            offset += item_size;
            if(offset + item_size > MAX_SIZE())
                detach();
            count++;
            return ans;
        }
        return nullptr;
    }
    void free(void* p)
    {
        if(offset + item_size > MAX_SIZE() && !freelist_head)
            attach();
        if(--count == 0)
        {
            delete this;
            return;
        }
        freelist* new_entry = reinterpret_cast<freelist*>(p);
        new_entry->next = freelist_head;
        freelist_head = new_entry;
    }
};

struct SlabAllocator
{
    static constexpr size_t MAX_SIZE = PageFooter::MAX_SIZE();
    size_t sz;
    PageFooter* page_list;
    constexpr SlabAllocator(size_t sz = 0) : sz(sz), page_list(nullptr){}
    void* alloc(void)
    {
        if(!page_list)
            new PageFooter(page_list, sz);
        return page_list->alloc();
    }
    static void free(void* p)
    {
        PageFooter::from_pointer(p)->free(p);
    }
};

void* large_malloc(size_t sz)
{
    void* ans = mmap(0, sz, PROT_READ|PROT_WRITE, MAP_ANON);
    if(ans == MAP_FAILED)
        return nullptr;
    return ans;
}

void large_free(void* p, size_t sz)
{
    munmap(p, sz);
}

struct Malloc
{
    std::mutex lock;
    SlabAllocator allocators[DEPTH];
    constexpr Malloc()
    {
        size_t sz = PAGE_SIZE;
        for(int i = 0; i < DEPTH; i++)
        {
            allocators[i] = SlabAllocator(std::min(SlabAllocator::MAX_SIZE, sz));
            sz /= 2;
        }
    }
    void* malloc(size_t sz)
    {
        int left = 0;
        int right = DEPTH;
        while(right - left > 1)
        {
            int mid = (right + left) / 2;
            if(allocators[mid].sz > sz)
                left = mid;
            else
                right = mid;
        }
        lock.lock();
        void* ans = allocators[left].alloc();
        lock.unlock();
        return ans;
    }
    void free(void* p)
    {
        lock.lock();
        SlabAllocator::free(p);
        lock.unlock();
    }
};

Malloc malloc;

}

extern "C"
{

void* malloc(size_t sz)
{
    if(!sz)
        return nullptr;
    size_t sz1 = sz + sizeof(uintptr_t);
    void* ans;
    if(sz1 > Allocator::SlabAllocator::MAX_SIZE)
    {
        sz1 = (sz1 + PAGE_SIZE - 1) & -PAGE_SIZE;
        ans = Allocator::large_malloc(sz1);
    }
    else
        ans = Allocator::malloc.malloc(sz1);
    uintptr_t* pp = reinterpret_cast<uintptr_t*>(ans);
    *pp = sz1;
    return pp + 1;
}

void free(void* p)
{
    if(!p)
        return;
    uintptr_t* pp = reinterpret_cast<uintptr_t*>(p) - 1;
    if(*pp > Allocator::SlabAllocator::MAX_SIZE)
        Allocator::large_free(pp, *pp);
    else
        Allocator::malloc.free(pp);
}

void* calloc(size_t a, size_t b)
{
    size_t sz = a * b;
    void* ans = malloc(sz);
    if(ans)
        memset(ans, 0, sz);
    return ans;
}

void* realloc(void* oldp, size_t newsz)
{
    if(!oldp)
        return malloc(newsz);
    if(!newsz)
    {
        free(oldp);
        return nullptr;
    }
    newsz += sizeof(uintptr_t);
    size_t oldsz = reinterpret_cast<uintptr_t*>(oldp)[-1];
    if(oldsz >= newsz)
    {
        if(oldsz >= Allocator::SlabAllocator::MAX_SIZE && newsz >= Allocator::SlabAllocator::MAX_SIZE)
        {
            newsz = (newsz + PAGE_SIZE - 1) & -PAGE_SIZE;
            munmap((void*)((uintptr_t)(reinterpret_cast<uintptr_t*>(oldp) - 1) + newsz), oldsz - newsz);
            reinterpret_cast<uintptr_t*>(oldp)[-1] = newsz;
        }
        return oldp;
    }
    newsz -= sizeof(uintptr_t);
    oldsz -= sizeof(uintptr_t);
    void* newp = malloc(newsz);
    if(newp)
    {
        memcpy(newp, oldp, (newsz>oldsz)?oldsz:newsz);
        free(oldp);
    }
    return newp;
}

}

void* operator new(size_t sz) { return malloc(sz); }
void operator delete(void* p) { free(p); }
void* operator new[](size_t sz) { return malloc(sz); }
void operator delete[](void* p) { free(p); }
void operator delete(void* p, size_t sz) { free(p); }
void operator delete[](void* p, size_t sz) { free(p); }
