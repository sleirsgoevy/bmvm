//must provide atexit() for destructors of static variables
//we do not support unloading modules, so its a no-op
int atexit(void(*function)(void))
{
    return 0;
}
