#include <assert.h>

void __cxa_pure_virtual(void)
{
    assert(!"pure virtual method called");
}
