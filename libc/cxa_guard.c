#include <cmpxchg.h>
#include <types.h>

int __cxa_guard_acquire(int64_t* guard)
{
    volatile uintptr_t* p = (void*)guard;
    uintptr_t value;
    while((value = *p) != 1 && !cmpxchg(p, 0, 0x100));
    return value == 0;
}

void __cxa_guard_abort(int64_t* guard)
{
    cmpxchg((volatile uintptr_t*)guard, 0x100, 0);
}

void __cxa_guard_release(int64_t* guard)
{
    cmpxchg((volatile uintptr_t*)guard, 0x100, 1);
}
