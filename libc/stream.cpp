#include <stream>
#include <ctype.h>

static inline bool isnumeric_for(char c, int base)
{
    if(base < 0)
        return false;
    else if(base <= 10)
        return c >= '0' && c < ('0' + base);
    else
    {
        if(base > 36)
            base = 36;
        return (c >= '0' && c <= '9')
            || (c >= 'a' && c < ('a' + base - 10))
            || (c >= 'A' && c < ('A' + base - 10));
    }
}

namespace std
{

istream& istream::operator>>(signed char& c)
{
    int c1;
    while(isspace(c1 = get()));
    if(c1 == -1)
    {
        eof = true;
        return *this;
    }
    c = c1;
    return *this;
}

istream& istream::operator>>(unsigned char& c)
{
    signed char sc = 0;
    *this >> sc;
    c = sc;
    return *this;
}

istream& istream::operator>>(char& c)
{
    unsigned char uc = 0;
    *this >> uc;
    c = uc;
    return *this;
}

istream& istream::operator>>(string& s)
{
    int c;
    while((c = get()) != -1 && isspace(c));
    if(c == -1)
    {
        eof = true;
        return *this;
    }
    s = (char)c;
    while((c = get()) != -1 && !isspace(c))
        s += (char)c;
    if(c != -1)
        unget();
    return *this;
}

istream& istream::operator>>(uint64_t& n)
{
    int c;
    while((c = get()) != -1 && isspace(c));
    if(c == -1)
    {
        eof = true;
        return *this;
    }
    unget();
    n = 0;
    int nchars = 0;
    while((c = get()) != -1 && isnumeric_for(c, base) || (base == 16 && nchars == 1 && (c == 'x' || c == 'X')))
    {
        if(c == 'x' || c == 'X')
            continue;
        n *= base;
        if(c >= '0' && c <= '9')
            n += c - '0';
        else if(c >= 'a' && c <= 'z')
            n += c - 'a' + 10;
        else if(c >= 'A' && c <= 'Z')
            n += c - 'A' + 10;
        nchars++;
    }
    if(c != -1 && !isspace(c))
        eof = true;
    else
        unget();
    return *this;
}

istream& istream::operator>>(int64_t& n)
{
    char c;
    *this >> c;
    if(!*this)
        return *this;
    if(isnumeric(c))
    {
        unget();
        uint64_t m = 0;
        *this >> m;
        n = m;
        return *this;
    }
    else if(c == '-')
    {
        int c2 = get();
        if(!isnumeric(c2))
        {
            eof = true;
            return *this;
        }
        unget();
        uint64_t m = 0;
        *this >> m;
        n = -m;
        return *this;
    }
    else
    {
        eof = true;
        return *this;
    }
}

istream& getline(istream& sin, string& s, char delim)
{
    if(!sin)
        return sin;
    int first = sin.get();
    if(first == -1)
    {
        sin.eof = true;
        return sin;
    }
    s = (char)first;
    int c;
    while((c = sin.get()) != -1 && c != delim)
        s += (char)c;
    return sin;
}

static inline void print_number(ostream& out, uint64_t n, int base)
{
    size_t div = base;
    while(n >= div)
        div *= base;
    while(div >= base)
    {
        n %= div;
        div /= base;
        if(out)
        {
            uint64_t digit = n / div;
            if(digit < 10)
                out.put('0' + digit);
            else
                out.put('a' + (digit - 10));
        }
    }
}

ostream& ostream::operator<<(uint64_t n)
{
    if(n < base * base)
        print_number(*this, n, base);
    else
    {
        print_number(*this, n / base / base, base);
        if(*this)
            print_number(*this, (n / base) % base, base);
        if(*this)
            print_number(*this, n % base, base);
    }
    return *this;
}

}
