#include <fstream>
#include <sstream>
#include <libboot/hook.hpp>
#include <libboot/memrw.hpp>
#include <libboot/vm.hpp>
#include <liblog/logging.hpp>
#include <libsmp/global_lock.hpp>

static void apply_patches(const char* cmdline)
{
    vm::VM* vcpu = boot::curVCPU();
    std::ifstream fin(cmdline);
    std::string line;
    while(std::getline(fin, line))
    {
        bool physical = false;
        std::istringstream sin(line);
        if(line.substr(0, 5) == "phys " || line.substr(0, 2) == "p ") 
        {
            physical = true;
            std::string tmp;
            sin >> tmp;
        }
        sin >> std::hex;
        uint64_t addr;
        sin >> addr;
        std::string old_data, new_data;
        sin >> old_data >> new_data;
        logging::log << "Applying patch ";
        if(physical)
            logging::log << "phys ";
        logging::log << std::hex << "0x" << addr << std::dec;
        logging::log << " " << old_data << " " << new_data << std::endl;
        if(old_data.size() % 2 || old_data.size() != new_data.size())
        {
            logging::log << "=> malformed patch (different or odd size of hex blobs)" << std::endl;
            continue;
        }
        std::vector<char> real_data(old_data.size() / 2);
        std::vector<char> fake_data(old_data.size() / 2);
        bool ok;
        if(physical)
            ok = utils::copy_from_guest(&real_data[0], addr, real_data.size());
        else
            ok = utils::copy_from_guest_vm(&real_data[0], addr, real_data.size());
        if(!ok)
        {
            logging::log << "=> failed to read data from VM" << std::endl;
            continue;
        }
        for(size_t i = 0; i < real_data.size() && ok; i++)
        {
            std::istringstream iss(old_data.substr(2*i, 2));
            std::istringstream iss2(new_data.substr(2*i, 2));
            uint16_t c;
            if(!(iss >> std::hex >> c))
            {
                logging::log << "=> malformed original hex" << std::endl;
                ok = false;
                break;
            }
            uint16_t c2;
            if(!(iss2 >> std::hex >> c2))
            {
                logging::log << "=> malformed modified hex" << std::endl;
                ok = false;
                break;
            }
            if(c != (uint8_t)real_data[i])
            {
                logging::log << "=> data mismatch at offset 0x" << std::hex << i;
                logging::log << " (expected 0x" << c << ", found 0x" << (uint16_t)(uint8_t)real_data[i];
                logging::log << std::dec << ")" << std::endl;
                ok = false;
                break;
            }
            fake_data[i] = c2;
        }
        if(!ok)
            continue;
        if(physical)
            ok = utils::copy_to_guest(addr, &fake_data[0], fake_data.size());
        else
            ok = utils::copy_to_guest_vm(addr, &fake_data[0], fake_data.size());
        if(ok)
            logging::log << "=> OK" << std::endl;
        else
            logging::log << "=> failed to write data to VM" << std::endl;
    }
}

int main(const char* cmdline)
{
    std::string cmd = cmdline;
    hooks::createVCPUInitHook([cmd]() mutable
    {
        smp::global_lock.lock();
        static bool ran = false;
        if(!ran)
        {
            apply_patches(cmd.c_str());
            ran = true;
        }
        smp::global_lock.unlock();
        return true;
    });
    return 0;
}
