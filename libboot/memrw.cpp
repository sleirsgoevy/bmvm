#include <libboot/memrw.hpp>
#include <libvm/vm.hpp>
#include <libboot/vm.hpp>
#include <utility>
#include <cstring>
#include <types.h>
#include <sysregs.h>
extern "C"
{
    #include <mm.h>
}

namespace utils
{

bool copy_from_guest(void* data0, uint64_t addr, size_t sz)
{
    char* data = (char*)data0;
    vm::VM* vm = boot::curVCPU();
    uint64_t end = addr + sz;
    while(addr < end)
    {
        uint64_t limit = std::min(end, (addr | 0xfff) + 1);
        bool r, w, x;
        int mt;
        uint64_t mapping = vm->translate(addr & PG_ADDR_MASK_4KB, r, w, x, mt);
        if(!r && !w && !x)
            return false;
        char* page = (char*)mmap(0, 4096, PROT_READ|PROT_WRITE|PROT_NOEXEC, mapping);
        if(page == MAP_FAILED)
            return false;
        memcpy(data, page + (addr & ~PG_ADDR_MASK_4KB), limit - addr);
        munmap(page, 4096);
        data += limit - addr;
        addr = limit;
    }
    return true;
}

bool copy_to_guest(uint64_t addr, const void* data0, size_t sz)
{
    const char* data = (const char*)data0;
    vm::VM* vm = boot::curVCPU();
    uint64_t end = addr + sz;
    while(addr < end)
    {
        uint64_t limit = std::min(end, (addr | 0xfff) + 1);
        bool r, w, x;
        int mt;
        uint64_t mapping = vm->translate(addr & PG_ADDR_MASK_4KB, r, w, x, mt);
        if(!r && !w && !x)
            return false;
        char* page = (char*)mmap(0, 4096, PROT_READ|PROT_WRITE|PROT_NOEXEC, mapping);
        if(page == MAP_FAILED)
            return false;
        memcpy(page + (addr & ~PG_ADDR_MASK_4KB), data, limit - addr);
        munmap(page, 4096);
        data += limit - addr;
        addr = limit;
    }
    return true;
}

bool guest_vm_translate(uintptr_t virt, uint64_t& phys, uint64_t& limit, bool& u, bool& w, bool& nx, uint64_t cr3)
{
    phys = -1;
    limit = -1;
    u = true;
    w = true;
    nx = false;
    vm::VM* vcpu = boot::curVCPU();
    uint64_t cr0 = vcpu->get_reg(vm::Register::CR0);
    if(!(cr0 & 0x80000000u)) //paging disabled
    {
        phys = virt;
        return true;
    }
    uint64_t cr4 = vcpu->get_reg(vm::Register::CR4);
    uint64_t efer = vcpu->rdmsr(IA32_EFER);
    if(!((cr4 & 32) /* PAE */ && (efer & 1024) /* LMA */ && !(cr4 & 4096) /* PML5 */))
        //currently unsupported
        return false;
    if((int64_t)virt != ((int64_t)(virt << 16) >> 16)) //non-canonical address
        return false;
    uint64_t addr = cr3 & PG_ADDR_MASK_4KB;
    for(int shift = 39; shift >= 12; shift -= 9)
    {
        uint64_t pte;
        int idx = (virt >> shift) & 511;
        if(!copy_from_guest(&pte, addr+idx*sizeof(pte), sizeof(pte)))
            return false;
        addr = pte & PG_ADDR_MASK_4KB;
        if(!(pte & PG_PRESENT))
            return false;
        if(!(pte & PG_USER))
            u = false;
        if(!(pte & PG_RW))
            w = false;
        if((pte & PG_NX))
            nx = true;
        if((pte & PG_HUGE))
        {
            if(shift > 30)
                //lets play it safe
                return false;
            phys = (addr & ~((1ull << shift) - 1)) | (virt & ((1ull << shift) - 1));
            limit = (addr & ~((1ull << shift) - 1)) | ((1ull << shift) - 1);
            return true;
        }
    }
    phys = addr | (virt & 4095);
    limit = addr | 4095;
    return true;
}

bool guest_vm_translate(uintptr_t virt, uint64_t& phys, uint64_t& limit, bool& u, bool& w, bool& nx)
{
    return guest_vm_translate(virt, phys, limit, u, w, nx, boot::curVCPU()->get_reg(vm::Register::CR3));
}

bool copy_from_guest_vm(void* dst, uintptr_t src, size_t sz, uint64_t cr3)
{
    char* dstp = (char*)dst;
    while(sz > 0)
    {
        uint64_t phys, limit;
        bool u, w, nx;
        if(!guest_vm_translate(src, phys, limit, u, w, nx, cr3))
            return false;
        size_t chunk_sz = std::min(sz, (size_t)(limit - phys + 1));
        if(!copy_from_guest(dstp, phys, chunk_sz))
            return false;
        src += chunk_sz;
        dstp += chunk_sz;
        sz -= chunk_sz;
    }
    return true;
}

bool copy_from_guest_vm(void* dst, uintptr_t src, size_t sz)
{
    return copy_from_guest_vm(dst, src, sz, boot::curVCPU()->get_reg(vm::Register::CR3));
}

bool copy_to_guest_vm(uintptr_t dst, const void* src, size_t sz, uint64_t cr3)
{
    const char* srcp = (const char*)src;
    while(sz > 0)
    {
        uint64_t phys, limit;
        bool u, w, nx;
        if(!guest_vm_translate(dst, phys, limit, u, w, nx, cr3))
            return false;
        size_t chunk_sz = std::min(sz, (size_t)(limit - phys + 1));
        if(!copy_to_guest(phys, srcp, chunk_sz))
            return false;
        dst += chunk_sz;
        srcp += chunk_sz;
        sz -= chunk_sz;
    }
    return true;
}

bool copy_to_guest_vm(uintptr_t dst, const void* src, size_t sz)
{
    return copy_to_guest_vm(dst, src, sz, boot::curVCPU()->get_reg(vm::Register::CR3));
}

}
