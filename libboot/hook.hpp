#include <libboot/hook.hpp>

namespace hooks
{

bool handleUnknownExit(vm::VM* vm, vm::ExitReason er);
void handleVCPUInit(vm::VM* vm);

}
