#pragma once
#include <libvm/vm.hpp>
#include "mtrr.hpp"

namespace boot
{

bool prepare_args_freebsd(vm::VM* vcpu, const char* kernel_path, const std::vector<memory_range>& e820_map, const std::vector<memory_range>& e820_map_after_kernel, uint64_t kernend);

}
