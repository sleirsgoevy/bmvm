#include <libvm/vm.hpp>
#include <liblog/logging.hpp>
#include <libkbd/keyboard.hpp>
#include <libsmp/ipi.hpp>
#include <libboot/vm.hpp>
#include <libboot/memrw.hpp>
#include <sstream>
#include <unordered_map>
#include <sysregs.h>
#include "mtrr.hpp"
#include "patch.hpp"
#include "hook.hpp"
#include "linux.hpp"
#include "elf64.hpp"
#include "freebsd.hpp"
extern "C"
{
    #include <mm.h>
    #include <bootproto.h>
}

using vm::VM;

static inline void tone(bool state)
{
    for(int i = 0; i < 1000; i++) 
    {
        char al;
        asm volatile("inb %%dx, %%al":"=a"(al):"d"(0x61));
        al |= 2;
        if(!state)
            al ^= 2;
        asm volatile("outb %%al, %%dx\noutb %%al, %%dx\noutb %%al, %%dx\noutb %%al, %%dx"::"a"(al),"d"(0x61));
        if(state)
            al ^= 2;
        asm volatile("outb %%al, %%dx\noutb %%al, %%dx\noutb %%al, %%dx\noutb %%al, %%dx"::"a"(al),"d"(0x61));
    }
}

static void dump_vm(vm::VM* vm, vm::ExitReason ex)
{
    logging::log << "VM executed: " << (int)ex << std::endl;
    logging::log << std::hex;
    for(int i = 0; i < 16; i++)
        logging::log << "R" << std::dec << i << std::hex << " = " << vm->get_reg((vm::Register)i) << std::endl;
    logging::log << "RIP = " << vm->get_reg(vm::Register::RIP) << std::endl;
    logging::log << "CR0 = " << vm->get_reg(vm::Register::CR0);
    logging::log << " CR3 = " << vm->get_reg(vm::Register::CR3);
    logging::log << " CR4 = " << vm->get_reg(vm::Register::CR4) << std::endl;
    logging::log << "IA32_PAT = " << vm->rdmsr(IA32_PAT) << std::endl;
    logging::log << "IA32_EFER = " << vm->rdmsr(IA32_EFER) << std::endl;
    if(ex == vm::ExitReason::PAGE_FAULT)
    {
        uint64_t fault_addr = vm->get_faulting_address();
        logging::log << "Guilty page: " << fault_addr << std::endl;
        bool r, w, x;
        int mt;
        uint64_t physical_addr = vm->translate(fault_addr & PG_ADDR_MASK_4KB, r, w, x, mt);
        logging::log << "Mapped to: " << physical_addr << " (";
        logging::log << (r ? 'R' : '-') << (w ? 'W' : '-') << (x ? 'X' : '-');
        logging::log << " " << mt << ")" << std::endl;
        logging::log << "Fault reason: " << (int)vm->get_fault_reason() << std::endl;
    }
    else if(ex == vm::ExitReason::EXCEPTION)
    {
        bool has_error_code;
        uint32_t error_code;
        int vector = vm->get_exception(has_error_code, error_code);
        logging::log << "Exception " << std::dec << vector << std::hex << ", ";
        if(has_error_code)
            logging::log << "error code 0x" << error_code;
        else
            logging::log << "no error code";
        logging::log << std::endl;
    }
    unsigned char code1[32];
    unsigned char code2[32];
    for(size_t i = 0; i < 32; i++)
    {
        code1[i] = 0;
        code2[i] = 1;
    }
    uintptr_t addr = vm->get_reg(vm::Register::RIP);
    if(!(vm->get_reg(vm::Register::CR0) & 1))
    {
        logging::log << "Code at CS:EIP";
        addr += vm->get_reg(vm::Register::CS) << 4;
    }
    else
        logging::log << "Code at RIP";
    logging::log << " (0x" << addr << "):";
    utils::copy_from_guest_vm(code1, addr, 32);
    utils::copy_from_guest_vm(code2, addr, 32);
    for(size_t i = 0; i < 32; i++)
    {
        if(code1[i] == code2[i])
            logging::log << " " << code1[i] / 16 << code1[i] % 16;
        else
            logging::log << " ??";
    }
    logging::log << std::endl << std::dec;
    tone(1);
}

static bool debug_mode = false;

static vm::ExitReason vm_main_loop(VM* vm)
{
    vm::ExitReason ex;
    while(true)
    {
        ex = vm->execute(debug_mode);
        switch(ex)
        {
        case vm::ExitReason::WRMSR:
        {
            if(hooks::handleUnknownExit(vm, vm::ExitReason::WRMSR))
                break;
            uint32_t which = vm->get_reg(vm::Register::RCX);
            bool allow = true;
            uint64_t allow_mask;
            uint64_t to = vm->get_reg(vm::Register::RDX) << 32 | (uint32_t)vm->get_reg(vm::Register::RAX);
            if((which >= 0x2000u && which < 0xc0000000u) || which >= 0xc0002000u)
            {
                vm->inject_exception(13, 0);
                break;
            }
            switch(which)
            {
            case IA32_MISC_ENABLE: allow_mask = 0x00851889; break;
            case IA32_EFER: allow_mask = 0xd01; break;
            case IA32_SPEC_CTRL: allow_mask = 0x7; break;
            case IA32_MTRR_DEF_TYPE: //XXX
            case IA32_BIOS_SIGN_ID:
                //ignore the write
                vm->set_reg(vm::Register::RIP, vm->get_reg(vm::Register::RIP) + 2);
                continue;
            case IA32_APIC_BASE:
                allow = (vm->rdmsr(which) ^ to) & 0xfff;
                allow_mask = 0xfff;
                break;
            default:
                return ex;
            }
            uint64_t from = vm->rdmsr(which);
            if(allow && !(~from & to & ~allow_mask))
            {
                vm->wrmsr(which, to);
                vm->set_reg(vm::Register::RIP, vm->get_reg(vm::Register::RIP) + 2);
                break;
            }
            logging::log << std::hex << "blocked wrmsr(" << which << ", " << from << " -> " << to << ")" << std::dec << std::endl;
            return ex;
        }
        case vm::ExitReason::RDMSR:
        {
            if(hooks::handleUnknownExit(vm, vm::ExitReason::RDMSR))
                break;
            uint32_t which = vm->get_reg(vm::Register::RCX);
            uint64_t value;
            if((which > 0x2000 && which < 0xc0000000u) || which > 0xc0002000u)
            {
                vm->inject_exception(13, 0);
                break;
            }
            switch(which)
            {
            case IA32_BIOS_SIGN_ID:
            {
                uint32_t regs[4];
                cpuid(1, regs);
                value = rdmsr(IA32_BIOS_SIGN_ID);
                break;
            }
            case IA32_FEATURE_CONTROL: value = (rdmsr(IA32_FEATURE_CONTROL) & ~6ull) | 1; break;
            default:
                return ex;
            }
            vm->set_reg(vm::Register::RDX, value >> 32);
            vm->set_reg(vm::Register::RAX, (uint32_t)value);
            vm->set_reg(vm::Register::RIP, vm->get_reg(vm::Register::RIP) + 2);
            break;
        }
        case vm::ExitReason::PAGE_FAULT:
        {
            /*smp::global_lock.lock();
            logging::log << "Page fault! addr=" << std::hex << vm->get_faulting_address() << std::dec << std::endl;
            smp::global_lock.unlock();*/
            if(!hooks::handlePatchPageFault(vm->get_faulting_address() & PG_ADDR_MASK_4KB, vm->get_fault_reason())
            && !hooks::handleUnknownExit(vm, ex))
                return ex;
            break;
        }
        case vm::ExitReason::EXCEPTION:
        {
            if(hooks::handleUnknownExit(vm, ex))
                break;
            bool has_error_code;
            uint32_t error_code;
            int vector = vm->get_exception(has_error_code, error_code);
            if(has_error_code)
                vm->inject_exception(vector, error_code);
            else
                vm->inject_exception(vector);
            break;
        }
        case vm::ExitReason::SINGLESTEP:
            if(debug_mode)
            {
                smp::global_lock.lock();
                logging::log << "In singlestep" << std::endl;
                dump_vm(vm, ex);
                keyboard::Keyboard kbd;
                kbd.getchar();
                smp::global_lock.unlock();
                break;
            }
        default:
        {
            if(hooks::handleUnknownExit(vm, ex))
                break;
            return ex;
        }
        }
    }
}

volatile uintptr_t started_cpus = 1;
volatile uintptr_t vcpus_created = 1;
volatile uintptr_t start_secondary = 0;
vm::VM* boot_vcpu = nullptr;

void setup_msrs(vm::VM* vm)
{
    for(size_t i = 0; i < 0x2000; i++)
    {
        vm->msr_protect(i, true, true);
        vm->msr_protect(0xc0000000 + i, true, true);
    }
    mtrr_protect(vm);
    vm->msr_protect(IA32_MISC_ENABLE, true, false);
    vm->msr_protect(IA32_EFER, true, false);
    vm->msr_protect(IA32_BIOS_SIGN_ID, false, false);
    vm->msr_protect(IA32_ARCH_CAPABILITIES, true, false);
    vm->msr_protect(IA32_PLATFORM_INFO, true, false);
    vm->msr_protect(IA32_MCG_CAP, true, false);
    vm->msr_protect(IA32_MCG_STATUS, true, false);
    vm->msr_protect(IA32_IACORE_RATIOS, true, false);
    vm->msr_protect(IA32_SPEC_CTRL, true, false);
    vm->msr_protect(IA32_TSC_DEADLINE, true, true); //XXX: need adjust when we implement tsc spoof?
    //TODO: these two could be used for detection
    vm->msr_protect(IA32_MPERF, true, true);
    vm->msr_protect(IA32_APERF, true, true);
    vm->msr_protect(IA32_TSC_ADJUST, true, true); //is it really ok?
    vm->msr_protect(IA32_APIC_BASE, true, false); //XXX
    vm->msr_protect(MSR_MISC_FEATURES_ENABLES, true, true);
    vm->wrmsr(IA32_PAT, rdmsr(IA32_PAT));
}

static std::vector<vm::VM*> vcpus;

namespace boot
{

vm::VM* curVCPU()
{
    return vcpus[smp::curcpu()];
}

}

void secondary_main(void)
{
    int cur = smp::curcpu();
    uintptr_t sc;
    do
        sc = started_cpus;
    while(!cmpxchg(&started_cpus, sc, sc+1));
    while(start_secondary != 1);
    smp::global_lock.lock();
    vm::VM* vm = boot_vcpu->createVCPU();
    if(!vm)
    {
        logging::log << "On cpu " << cur << ":" << std::endl;
        logging::log << "Could not create VCPU!" << std::endl;
        smp::global_lock.unlock();
        delete vm;
        for(;;) asm volatile("");
    }
    vcpus[smp::curcpu()] = vm;
    smp::global_lock.unlock();
    setup_msrs(vm);
    do
        sc = vcpus_created;
    while(!cmpxchg(&vcpus_created, sc, sc+1));
    while(start_secondary != 2);
    hooks::handleVCPUInit(vm);
    vm::ExitReason ex = vm_main_loop(vm);
    smp::global_lock.lock();
    logging::log << "On cpu " << cur << ":" << std::endl;
    dump_vm(vm, ex);
    smp::global_lock.unlock();
    for(;;) asm volatile("");
}

int main(const char* msg)
{
    std::istringstream param_in(msg);
    std::string which_boot;
    param_in >> which_boot;
    if(which_boot == "debug")
    {
        debug_mode = true;
        param_in >> which_boot;
    }
    //are we sure we are on a bios system?
    //assume bios if booted with linux16 or explicitly requested
    //if booted with linux16, we can only get memmap from bios
    bool is_bios = get_boot_protocol() == BOOT_PROTOCOL_LINUX16
                || get_boot_protocol() == BOOT_PROTOCOL_BIOS
                || which_boot == "int18"
                || which_boot == "int19"
                || which_boot == "int13";
    if(is_bios)
        logging::log << "Note: running on BIOS system" << std::endl;
    else
        logging::log << "Note: running on non-BIOS system" << std::endl;
    void* stacks = mmap(0, 1048576 * smp::ncpus(), PROT_READ|PROT_WRITE|PROT_NOEXEC, MAP_ANON);
    if(!stacks)
    {
        logging::log << "Could not allocate thread stacks!" << std::endl;
        return 1;
    }
    int boot_cpu;
    vcpus.resize(smp::ncpus());
    if(!smp::bring_up_cpus(secondary_main, stacks, 1048576, boot_cpu))
    {
        logging::log << "fatal: could not start CPUs!" << std::endl; 
        for(;;) asm volatile("");
    }
    while(started_cpus != smp::ncpus());
    VM* vm = vm::createVM();
    if(!vm)
    {
        logging::log << "Failed to create VM!" << std::endl;
        return 1;
    }
    setup_msrs(vm);
    char* page0 = (char*)mmap(0, 4096, PROT_READ|PROT_WRITE|PROT_NOEXEC, MAP_ANON);
    memset(page0, 0, 4096);
    vm->map_pages(0, 4096, phys_addr_4kb(page0), true, true, true, 6);
    vm::ExitReason ex;
    if(which_boot == "skip_sanity")
        param_in >> which_boot;
    else
    {
        ex = vm->execute();
        logging::log << "VM executed: " << (int)ex << std::endl;
    }
    vm->unmap_pages(0, 4096);
    std::vector<memory_range> mtrrs = get_mtrr_ranges();
    logging::log << std::hex;
    for(const memory_range& i : mtrrs)
        logging::log << i.start << "-" << i.end << ":" << i.type << std::endl;
    logging::log << std::dec;
    map_from_mtrr(vm, mtrrs, 0x8000000000ull);
    vm->unmap_pages(0x8000, 0x9000);
    vm->map_pages(0x8000, 0x9000, phys_addr_4kb(page0), true, true, true, 6);
    page0[0] = 0xcd;
    page0[1] = 0x15;
    vm->get_vmcall(page0+2);
    std::vector<memory_range> e820_map;
    std::vector<uint32_t> int15_ebx_values(1, 0);
    uint8_t int15_struct_sz = 0;
    bool int15_does_loop = false;
    std::unordered_map<uint32_t, size_t> int15_ebx_to_iter;
    if(is_bios)
    {
        vm->set_reg(vm::Register::RBX, 0);
        for(;;)
        {
            vm->set_reg(vm::Register::RDX, 0x534d4150);
            vm->set_reg(vm::Register::RDI, 0x8018);
            vm->set_reg(vm::Register::RAX, 0xe820);
            vm->set_reg(vm::Register::RCX, 24);
            vm->set_reg(vm::Register::RIP, 0x8000);
            ex = vm->execute();
            int15_ebx_values.push_back((uint32_t)vm->get_reg(vm::Register::RBX));
            int15_struct_sz = vm->get_reg(vm::Register::RCX);
            if(ex != vm::ExitReason::HYPERCALL
            || (uint32_t)vm->get_reg(vm::Register::RAX) != 0x534d4150u
            || !(uint32_t)vm->get_reg(vm::Register::RBX)
            || (vm->get_reg(vm::Register::RFLAGS) & 1) /* CF */)
            {
                if((uint32_t)vm->get_reg(vm::Register::RBX))
                    int15_does_loop = true;
                break;
            }
            for(size_t i = vm->get_reg(vm::Register::RCX) & 255; i < 24; i++)
                page0[0x18 + i] = 0;
            memory_range entry = *reinterpret_cast<memory_range*>(page0+0x18);
            entry.end += entry.start;
            e820_map.push_back(entry);
        }
        for(size_t i = 0; i < e820_map.size(); i++)
            int15_ebx_to_iter[int15_ebx_values[i]] = i;
        logging::log << std::hex << "e820 map:" << std::endl;
        for(const memory_range& i : e820_map)
            logging::log << i.start << "-" << i.end << ":" << i.type << std::endl;
    }
    void* reserve = mmap(0, 16777216, PROT_READ|PROT_WRITE|PROT_NOEXEC, MAP_ANON);
    uint64_t upper_brk = 0;
    uint64_t brk = mm_freeze(&upper_brk);
    logging::log << "brk=" << brk << ", upper_brk=" << upper_brk << std::endl;
    logging::log << std::dec;
    munmap(reserve, 16777216);
    if(!is_bios)
        switch(get_boot_protocol())
        {
        case BOOT_PROTOCOL_LINUX32:
        case BOOT_PROTOCOL_UEFI_RUNTIME:
        case BOOT_PROTOCOL_NONE:
        {
            void* start;
            void* end;
            get_linux32_memmap(&start, &end);
            for(uintptr_t p = (uintptr_t)start; p < (uintptr_t)end; p += 20)
            {
                e820_map.push_back(*reinterpret_cast<memory_range*>(p));
                e820_map.back().end += e820_map.back().start;
            }
            break;
        }
        default:
        {
            logging::log << "Do not know how to get memory map for boot protocol " << get_boot_protocol() << std::endl;
            return 1;
        }
        }
    std::vector<memory_range> sorted_mem = normalize_memmap(e820_map);
    for(const memory_range& i : sorted_mem)
    {
        uint64_t unmap_start = std::max(i.start, brk);
        uint64_t unmap_end = upper_brk ? std::min(i.end, upper_brk) : i.end;
        if(unmap_end > unmap_start)
            vm->unmap_pages(unmap_start, unmap_end);
    }
    std::vector<memory_range> e820_map_for_vm;
    for(memory_range i : e820_map)
    {
        if(i.type == 1)
        {
            if(i.start <= brk && brk <= upper_brk && upper_brk <= i.end)
            {
                memory_range left = i;
                left.end = brk;
                memory_range right = i;
                right.start = upper_brk;
                if(left.start != left.end)
                    e820_map_for_vm.push_back(left);
                if(right.start != right.end)
                    e820_map_for_vm.push_back(right);
                continue;
            }
            if(brk >= i.start)
                i.end = std::min(i.end, brk);
            if(upper_brk && upper_brk <= i.end)
                i.start = std::max(i.start, upper_brk);
            if(i.start < i.end)
                e820_map_for_vm.push_back(i);
        }
        e820_map_for_vm.push_back(i);
    }
    vm->unmap_pages(0x8000, 0x9000);
    vm->map_pages(0x8000, 0x9000, 0x8000, true, true, true, 6);
    munmap(page0, 4096);
    vm->set_reg(vm::Register::RIP, 0x7c00);
    char* page1 = (char*)mmap(0, 4096, PROT_READ|PROT_WRITE|PROT_NOEXEC, 0x7000);
    if(which_boot == "int18")
    {
        page1[0xc00] = 0xcd;
        page1[0xc01] = 0x18;
    }
    else if(which_boot == "int19")
    {
        page1[0xc00] = 0xcd;
        page1[0xc01] = 0x19;
    }
    else if(which_boot == "int13") //manually load bootsector
    {
        int drive_no;
        if(!(param_in >> drive_no))
        {
            logging::log << "int13: expected param" << std::endl;
            delete vm;
            return 1;
        }
        //TODO: hack
        //jmp far [0x13 * 4]
        page1[0xc00] = 0xff;
        page1[0xc01] = 0x2e;
        page1[0xc02] = 0x4c;
        page1[0xc03] = 0x00;
        //eip
        page1[0xffa] = 0;
        page1[0xffb] = 0x7c;
        //cs
        page1[0xffc] = 0;
        page1[0xffd] = 0;
        //eflags
        page1[0xffe] = 2;
        page1[0xfff] = 0;
        //prepare int13
        vm->set_reg(vm::Register::RDX, drive_no);
        vm->set_reg(vm::Register::RCX, 1);
        vm->set_reg(vm::Register::RAX, 0x0201);
        vm->set_reg(vm::Register::RBX, 0x7c00);
        vm->set_reg(vm::Register::RSP, 0x7ffa);
    }
    else if(which_boot == "linux32")
    {
        std::string filename, initrd, cmdline;
        param_in >> filename;
        getline(param_in, cmdline);
        if(!cmdline.empty() && cmdline[0] == ' ')
            cmdline = cmdline.substr(1);
        if(cmdline.substr(0, 7) == "initrd=")
        {
            size_t p = cmdline.find(' ');
            if(p == cmdline.npos)
            {
                initrd = cmdline.substr(7);
                cmdline = "";
            }
            else
            {
                initrd = cmdline.substr(7, p-7);
                cmdline = cmdline.substr(p+1);
            }
        }
        if(!boot::load_linux_kernel(vm, e820_map_for_vm, filename.c_str(), initrd.empty() ? nullptr : initrd.c_str(), cmdline.c_str()))
        {
            logging::log << "Failed to load Linux kernel!" << std::endl;
            return 1;
        }
    }
    else if(which_boot == "elf64")
    {
        std::string filename;
        param_in >> filename;
        uint64_t kernend;
        std::vector<memory_range> e820_map_after_elf;
        if(!boot::load_elf64_kernel(vm, e820_map_for_vm, e820_map_after_elf, filename.c_str(), kernend))
        {
            logging::log << "Failed to load ELF64 kernel!" << std::endl;
            return 1;
        }
    }
    else if(which_boot == "freebsd")
    {
        std::string filename;
        param_in >> filename;
        uint64_t kernend;
        std::vector<memory_range> e820_map_after_elf;
        if(!boot::load_elf64_kernel(vm, e820_map_for_vm, e820_map_after_elf, filename.c_str(), kernend))
        {
            logging::log << "Failed to load ELF64 kernel!" << std::endl;
            return 1;
        }
        if(!boot::prepare_args_freebsd(vm, filename.c_str(), e820_map_for_vm, e820_map_after_elf, kernend))
        {
            logging::log << "Failed to prepare FreeBSD kernel arguments!" << std::endl;
            return 1;
        }
    }
    else if(which_boot == "ebfe")
    {
        page1[0xc00] = 0xeb;
        page1[0xc01] = 0xfe;
    }
    else
    {
        logging::log << "Unknown boot option " << which_boot << std::endl;
        delete vm;
        return 1;
    }
    munmap(page1, 4096);
    uint16_t* ivt = (uint16_t*)mmap(0, 4096, PROT_READ|PROT_WRITE|PROT_NOEXEC, 0);
    uint16_t int15_cs = ivt[0x2b];
    uint16_t int15_ip = ivt[0x2a];
    uint32_t int15_addr = ((uint32_t)int15_cs << 4) + int15_ip;
    logging::log << "int15 is at 0x" << std::hex << int15_addr << std::dec << std::endl;
    munmap(ivt, 4096);
    logging::log << "Starting other CPUs..." << std::endl;
    boot_vcpu = vm;
    vcpus[boot_cpu] = vm;
    smp::global_lock.lock();
    logging::log << smp::ncpus() << " CPUs brought up!" << std::endl;
    smp::global_lock.unlock();
    cmpxchg(&start_secondary, 0, 1);
    while(vcpus_created != smp::ncpus());
    char vmcall[3];
    vm->get_vmcall(vmcall);
    smp::global_lock.lock();
    logging::log << "Booting..." << std::endl;
    smp::global_lock.unlock();
    hooks::Patch* p15 = nullptr;
    if(is_bios || (int15_addr >= 0xf0000 && int15_addr < 0x100000))
    {
        logging::log << "Non-BIOS but int15 pointer seems valid, hooking it to prevent accidential use." << std::endl;
        hooks::createPatch(int15_addr, nullptr, vmcall, 3);
    }
    if(is_bios)
        hooks::createHook(vm::ExitReason::HYPERCALL, int15_ip, [&e820_map_for_vm, &int15_ebx_values, &int15_ebx_to_iter, p15, int15_struct_sz, int15_does_loop, int15_cs, int15_ip]()
        {
            const std::unordered_map<uint32_t, size_t>& ebx_to_iter = int15_ebx_to_iter;
            vm::VM* vm = boot::curVCPU();
            uint64_t cr0 = vm->get_reg(vm::Register::CR0);
            uint32_t eax = vm->get_reg(vm::Register::RAX);
            if((cr0 & 1) || (uint16_t)vm->get_reg(vm::Register::CS) != int15_cs)
                return false; //not int15
            bool carry = false;
            if(eax == 0xe820)
            {
                if((uint32_t)vm->get_reg(vm::Register::RDX) != 0x534d4150)
                {
                    carry = true;
                    goto iret;
                }
                uint32_t key = vm->get_reg(vm::Register::RBX);
                uint8_t size = vm->get_reg(vm::Register::RCX);
                uint32_t addr = ((uint32_t)(uint16_t)vm->get_reg(vm::Register::ES) << 4) + (uint16_t)vm->get_reg(vm::Register::RDI);
                if(size > int15_struct_sz)
                    size = int15_struct_sz;
                auto it = ebx_to_iter.find(key);
                size_t i = (it == ebx_to_iter.end()) ? e820_map_for_vm.size() : it->second;
                if(i >= e820_map_for_vm.size())
                {
                    carry = true;
                    goto iret;
                }
                memory_range for_copyout = e820_map_for_vm[i];
                for_copyout.end -= for_copyout.start;
                utils::copy_to_guest(addr, &for_copyout, size);
                vm->set_reg(vm::Register::RCX, (vm->get_reg(vm::Register::RCX) & ~255ull) | size);
                key = int15_ebx_values[i + 1];
                if(i + 1 >= e820_map_for_vm.size() && int15_does_loop)
                    key = 0;
                vm->set_reg(vm::Register::RBX, key);
                vm->set_reg(vm::Register::RAX, 0x534d4150);
            }
            else
            {
                uint16_t ss = vm->get_reg(vm::Register::SS);
                uint16_t sp = vm->get_reg(vm::Register::RSP);
                uint16_t iret_frame[3];
                utils::copy_from_guest(iret_frame, ((uint32_t)ss << 4) + sp, sizeof(iret_frame));
                /*smp::global_lock.lock();
                logging::log << std::hex << "Unsupported int15 call: CR0=" << cr0 << ", EAX=" << eax << ", from " << iret_frame[1] << ":" << iret_frame[0] << std::dec << std::endl;
                smp::global_lock.unlock();*/
                if(hooks::stepOut(p15, int15_ip, int15_ip+3))
                {
                    /*smp::global_lock.lock();
                    logging::log << "Calling original int15..." << std::endl;
                    smp::global_lock.unlock();*/
                }
                return true;
            }
        iret:
            uint16_t ss = vm->get_reg(vm::Register::SS);
            uint16_t sp = vm->get_reg(vm::Register::RSP);
            uint16_t iret_frame[3];
            utils::copy_from_guest(iret_frame, ((uint32_t)ss << 4) + sp, sizeof(iret_frame));
            iret_frame[2] &= ~1u;
            if(carry)
                iret_frame[2] |= 1;
            vm->set_reg(vm::Register::RIP, iret_frame[0]);
            vm->set_reg(vm::Register::CS, iret_frame[1]);
            vm->set_reg(vm::Register::RFLAGS, iret_frame[2] | 2);
            sp += 6;
            vm->set_reg(vm::Register::RSP, (vm->get_reg(vm::Register::RSP) & ~0xffffull) | sp);
            return true;
        });
    hooks::handleVCPUInit(vm);
    cmpxchg(&start_secondary, 1, 2);
    //vm->intercept_exception(14, true);
    //vm->intercept_exception(8, true);
    ex = vm_main_loop(vm);
    smp::global_lock.lock();
    logging::log << "On boot cpu:" << std::endl;
    dump_vm(vm, ex);
    smp::global_lock.unlock();
    return 0;
}
