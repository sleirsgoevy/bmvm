#pragma once
#include <vector>
#include <libvm/vm.hpp>

struct memory_range
{
    uint64_t start;
    uint64_t end;
    int type;
};

std::vector<memory_range> get_mtrr_ranges();
std::vector<memory_range> normalize_memmap(const std::vector<memory_range>& data);
bool map_from_mtrr(vm::VM* vm, std::vector<memory_range>& mtrr, uint64_t ceiling);
void mtrr_protect(vm::VM* vm);
