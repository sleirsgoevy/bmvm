#pragma once
#include <libvm/vm.hpp>
#include "mtrr.hpp"

namespace boot
{

bool load_linux_kernel(vm::VM* vcpu, const std::vector<memory_range>& memmap, const char* kernel, const char* initrd, const char* cmdline);

}
