#pragma once
#include <libboot/patch.hpp>
#include <libvm/vm.hpp>

namespace hooks
{

bool handlePatchPageFault(uint64_t page, vm::CrashType why);

}

