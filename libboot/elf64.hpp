#pragma once
#include <libvm/vm.hpp>
#include "mtrr.hpp"

namespace boot
{

bool load_elf64_kernel(vm::VM* vcpu, const std::vector<memory_range>& memmap, std::vector<memory_range>& new_memmap, const char* kernel, uint64_t& kernend);

}
