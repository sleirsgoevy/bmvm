#include <vector>
#include <utility>
#include <libvm/vm.hpp>
#include <sysregs.h>

template<class T, class F>
void merge_sort(T* arr, T* tmp, size_t sz, F& is_less)
{
    if(sz < 2)
        return;
    size_t mid = sz / 2;
    merge_sort(arr, tmp, mid, is_less);
    merge_sort(arr+mid, tmp+mid, sz-mid, is_less);
    size_t i1 = 0;
    size_t i2 = mid;
    T* out = tmp;
    while(i1 != mid && i2 != sz)
        if(is_less(arr[i1], arr[i2]))
            *out++ = arr[i1++];
        else
            *out++ = arr[i2++];
    while(i1 != mid)
        *out++ = arr[i1++];
    while(i2 != sz)
        *out++ = arr[i2++];
    for(size_t i = 0; i < sz; i++)
        arr[i] = tmp[i];
}

template<class T, class F>
void merge_sort(std::vector<T>& arr, F is_less)
{
    std::vector<T> tmp(arr.size());
    merge_sort(&arr[0], &tmp[0], arr.size(), is_less);
}

struct memory_range
{
    uint64_t start;
    uint64_t end;
    int type;
};

struct memory_point
{
    uint64_t point;
    int type;
    bool is_end;
};

std::vector<memory_range> normalize_memmap(const std::vector<memory_range>& raw_data)
{
    std::vector<memory_point> points(raw_data.size()*2);
    for(size_t i = 0; i < raw_data.size(); i++)
    {
        points[2*i] = memory_point{raw_data[i].start, raw_data[i].type, false};
        points[2*i+1] = memory_point{raw_data[i].end, raw_data[i].type, true};
    }
    merge_sort(points, [](const memory_point& a, const memory_point& b)
    {
        return a.point < b.point || (a.point == b.point && a.type < b.type);
    });
    int good_depth = 0;
    int bad_depth = 0;
    std::vector<uint64_t> points2;
    for(memory_point& i : points)
        if((i.type == 1 && !i.is_end && ++good_depth == 1 && bad_depth == 0)
        || (i.type == 1 && i.is_end && --good_depth == 0 && bad_depth == 0)
        || (i.type != 1 && !i.is_end && ++bad_depth == 1 && good_depth != 0)
        || (i.type != 1 && i.is_end && --bad_depth == 0 && good_depth != 0))
            points2.push_back(i.point);
    std::vector<memory_range> usable_memory;
    for(size_t i = 0; i + 1 < points2.size(); i += 2)
    {
        uint64_t left = points2[i];
        uint64_t right = points2[i+1];
        left = (left + 4095) & ~4095ull;
        right &= ~4095ull;
        if(right > left)
            usable_memory.push_back(memory_range{left, right, 1});
    }
    return usable_memory;
}

struct
{
    uint64_t base;
    uint64_t chunk_size;
    uint32_t msr;
} fixed_mtrrs[11] = {
    {0x00000, 0x10000, 0x250},
    {0x80000, 0x4000, 0x258},
    {0xa0000, 0x4000, 0x259},
    {0xc0000, 0x1000, 0x268},
    {0xc8000, 0x1000, 0x269},
    {0xd0000, 0x1000, 0x26a},
    {0xd8000, 0x1000, 0x26b},
    {0xe0000, 0x1000, 0x26c},
    {0xe8000, 0x1000, 0x26d},
    {0xf0000, 0x1000, 0x26e},
    {0xf8000, 0x1000, 0x26f},
};

std::vector<memory_range> get_mtrr_ranges()
{
    std::vector<memory_range> ans;
    uint64_t def_type = rdmsr(IA32_MTRR_DEF_TYPE);
    if((def_type & 1024)) //fixed mtrrs
    {
        for(int i = 0; i < 11; i++)
        {
            uint64_t base = fixed_mtrrs[i].base;
            uint64_t size = fixed_mtrrs[i].chunk_size;
            uint64_t msr = rdmsr(fixed_mtrrs[i].msr);
            for(int j = 0; j < 8;)
            {
                int k = j + 1;
                int t = msr & 255;
                while(k < 8 && ((msr >>= 8) & 255) == t)
                    k++;
                ans.push_back(memory_range{base+j*size, base+k*size, t});
                j = k;
            }
        }
    }
    if((def_type & 2048)) //custom mtrrs
    {
        for(int i = 0; i < (rdmsr(IA32_MTRRCAP) & 255); i++)
        {
            uint64_t base = rdmsr(IA32_MTRR_PHYSBASE(i));
            uint64_t mask = rdmsr(IA32_MTRR_PHYSMASK(i));
            if(!(mask & 2048))
                continue;
            mask &= ~2048ull;
            mask |= mask << 1;
            mask |= mask << 2;
            mask |= mask << 4;
            mask |= mask << 8;
            mask |= mask << 16;
            mask |= mask << 32;
            uint64_t start_addr = base & 0xffffffffff000ull;
            uint64_t size = -mask;
            int type = base & 255;
            ans.push_back(memory_range{start_addr, start_addr+size, type});
        }
    }
    return ans;
}

bool map_from_mtrr(vm::VM* vm, std::vector<memory_range>& mtrr, uint64_t ceiling)
{
    int default_type = rdmsr(IA32_MTRR_DEF_TYPE) & 255;
    std::vector<memory_point> p(mtrr.size()*2);
    for(size_t i = 0; i < mtrr.size(); i++)
    {
        p[2*i] = memory_point{mtrr[i].start, mtrr[i].type, false};
        p[2*i+1] = memory_point{mtrr[i].end, mtrr[i].type, true};
    }
    merge_sort(p, [](memory_point& a, memory_point& b) { return a.point < b.point; });
    int memory_types[265];
    int n_types = 0;
    uint64_t floor = 0;
    for(memory_point& i : p)
    {
        int minnn = 256;
        for(int i = 0; i < n_types; i++)
            minnn = std::min(minnn, memory_types[i]);
        if(minnn == 256)
            minnn = default_type;
        if(!vm->map_pages(floor, std::min(ceiling, i.point), floor, true, true, true, minnn))
            return false;
        if(i.point >= ceiling)
            return true;
        if(i.is_end)
        {
            int idx = 0;
            while(idx < n_types && memory_types[idx] != i.type)
                idx++;
            if(idx != n_types)
            {
                for(int j = idx + 1; j < n_types; j++)
                    memory_types[j-1] = memory_types[j];
                n_types--;
            }
        }
        else if(n_types < 265)
            memory_types[n_types++] = i.type;
        floor = i.point;
    }
    return floor >= ceiling || vm->map_pages(floor, ceiling, floor, true, true, true, default_type);
}

void mtrr_protect(vm::VM* vm)
{
    vm->msr_protect(IA32_MTRRCAP, true, false);
    vm->msr_protect(IA32_MTRR_DEF_TYPE, true, false);
    for(int i = 0; i < 11; i++)
        vm->msr_protect(fixed_mtrrs[i].msr, true, false);
    for(int i = 0; i < (rdmsr(IA32_MTRRCAP) & 255); i++)
    {
        vm->msr_protect(IA32_MTRR_PHYSBASE(i), true, false);
        vm->msr_protect(IA32_MTRR_PHYSMASK(i), true, false);
    }
}
