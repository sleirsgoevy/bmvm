#include "linux.hpp"
#include <liblog/logging.hpp>
#include <libsmp/acpi.hpp>
extern "C"
{
    #include <fs.h>
    #include <assert.h>
    #include <bootproto.h>
}

namespace boot
{

bool load_linux_kernel(vm::VM* vcpu, const std::vector<memory_range>& memmap, const char* kernel, const char* initrd, const char* cmdline)
{
    logging::log << "Loading Linux kernel from " << kernel << std::endl;
    if(initrd)
        logging::log << "Initrd: " << initrd << std::endl;
    logging::log << "Kernel command line: " << cmdline << std::endl;
    FILE* f = open(kernel, O_RDONLY);
    if(!f)
    {
        logging::log << kernel << ": not found" << std::endl;
        return false;
    }
    uint8_t bootsect[512] = {0};
    pread(f, bootsect, 512, 0);
    if(bootsect[510] != 0x55 || bootsect[511] != 0xaa)
    {
        logging::log << kernel << ": 55AA not found" << std::endl;
        close(f);
        return false;
    }
    int sects = bootsect[0x1f1];
    if(!sects)
        sects = 4;
    sects++;
    uint8_t zero_page[0x1000];
    size_t zero_page_size = std::min((size_t)0x1000, (size_t)(512*sects));
    if(pread(f, zero_page, zero_page_size, 0) != zero_page_size)
    {
        logging::log << kernel << ": failed to read zero page" << std::endl;
        close(f);
        return false;
    }
    if(memcmp(zero_page+0x202, "HdrS", 4))
    {
        logging::log << kernel << ": HdrS not found" << std::endl;
        close(f);
        return false;
    }
    uint16_t version = zero_page[0x207] << 8 | zero_page[0x206];
    if(version < 0x200+13)
    {
        logging::log << kernel << ": kernel too old (version=0x" << std::hex << version << std::dec << ")" << std::endl;
        close(f);
        return false;
    }
    off_t ksz = lseek(f, 0, SEEK_END);
    if(ksz < 0 || ksz != (size_t)ksz)
    {
        logging::log << kernel << ": failed to determine file size" << std::endl;
        close(f);
        return false;
    }
    if(ksz < zero_page_size)
    {
        logging::log << kernel << ": kernel too short" << std::endl;
        close(f);
        return false;
    }
    uint64_t kernel_start = 0x100000;
    uint64_t kernel_end = kernel_start + ksz - 512*sects;
    bool have_space = false;
    for(const memory_range& i : memmap)
        if(kernel_start >= i.start && kernel_end <= i.end && i.type == 1)
        {
            have_space = true;
            break;
        }
    if(!have_space)
    {
        logging::log << kernel << ": not enough space in the memory map" << std::endl;
        close(f);
        return false;
    }
    {
        void* map = mmap(0, kernel_end-kernel_start, PROT_READ|PROT_WRITE|PROT_NOEXEC, kernel_start);
        if(map == MAP_FAILED)
        {
            logging::log << kernel << ": failed to map kernel memory" << std::endl;
            close(f);
            return false;
        }
        char* p = (char*)map;
        off_t off = 512*sects;
        size_t sz = kernel_end - kernel_start;
        while(sz > 0)
        {
            ssize_t chk = pread(f, p, sz, off);
            if(chk < 0)
            {
                logging::log << kernel << ": failed to read kernel" << std::endl;
                munmap(map, kernel_end-kernel_start);
                close(f);
                return false;
            }
            p += chk;
            off += chk;
            sz -= chk;
        }
        munmap(map, kernel_end-kernel_start);
    }
    close(f);
    uint64_t initrd_addr = 0;
    size_t initrd_size = 0;
    if(initrd)
    {
        uint64_t initrd_max_addr = *reinterpret_cast<uint32_t*>(zero_page+0x22c);
        FILE* f = open(initrd, O_RDONLY);
        if(!f)
        {
            logging::log << initrd << ": file not found" << std::endl;
            return false;
        }
        off_t sz = lseek(f, 0, SEEK_END);
        if(sz < 0 || sz != (size_t)sz)
        {
            logging::log << initrd << ": could not determine file size" << std::endl;
            close(f);
            return false;
        }
        initrd_size = sz;
        for(const memory_range& i : memmap)
        {
            uint64_t start = std::max(kernel_end, i.start);
            uint64_t end = std::min(initrd_max_addr+1, i.end);
            if(end < start)
                continue;
            if(end - start >= initrd_size)
                initrd_addr = std::max(initrd_addr, i.end - initrd_size);
        }
        if(!initrd_addr)
        {
            logging::log << initrd << ": not enough space in the memory map" << std::endl;
            close(f);
            return false;
        }
        uint64_t initrd_map_addr = initrd_addr & -4096;
        size_t initrd_map_size = initrd_addr+initrd_size-initrd_map_addr;
        char* initrd_map = (char*)mmap(0, initrd_map_size, PROT_READ|PROT_WRITE|PROT_NOEXEC, initrd_map_addr);
        if(initrd_map == MAP_FAILED)
        {
            logging::log << initrd << ": failed to map physical memory" << std::endl;
            close(f);
            return false;
        }
        char* dst = initrd_map + (initrd_addr & 4095);
        off_t off = 0;
        size_t sz_left = initrd_size;
        while(sz_left > 0)
        {
            ssize_t chk = pread(f, dst, sz_left, off);
            if(chk <= 0)
            {
                logging::log << initrd << ": failed to read file" << std::endl;
                munmap(initrd_map, initrd_map_size);
                close(f);
                return false;
            }
            dst += chk;
            off += chk;
            sz_left -= chk;
        }
        munmap(initrd_map, initrd_map_size);
        close(f);
    }
    uint8_t bootinfo[3] = {0x70, 0, 0}; //default to grub
    if(get_boot_protocol() == BOOT_PROTOCOL_LINUX16
    || get_boot_protocol() == BOOT_PROTOCOL_LINUX32)
        get_linux_loader_info(bootinfo);
    *reinterpret_cast<uint16_t*>(zero_page+0x1fa) = 0xff; //vid_mode
    zero_page[0x210] = bootinfo[0]; //type_of_loader
    zero_page[0x211] |= 0xa1; //loadflags
    zero_page[0x211] ^= 0x80;
    *reinterpret_cast<uint32_t*>(zero_page+0x218) = initrd_addr;
    *reinterpret_cast<uint32_t*>(zero_page+0x21c) = initrd_size;
    *reinterpret_cast<uint16_t*>(zero_page+0x224) = 0;
    zero_page[0x226] = bootinfo[1];
    zero_page[0x227] = bootinfo[2];
    for(int i = 0; i < 0x1f1; i++)
        zero_page[i] = 0;
    *reinterpret_cast<uint64_t*>(zero_page+0x70) = smp::get_rsdp();
    size_t n_e820 = std::min((size_t)128, memmap.size());
    zero_page[0x1e8] = n_e820;
    for(size_t i = 0x202 + zero_page[0x201]; i < zero_page_size; i++)
        zero_page[i] = 0;
    size_t e820_p = 0x2d0;
    for(size_t i = 0; i < n_e820; i++)
    {
        *reinterpret_cast<uint64_t*>(zero_page+e820_p) = memmap[i].start;
        *reinterpret_cast<uint64_t*>(zero_page+e820_p+8) = memmap[i].end-memmap[i].start;
        *reinterpret_cast<uint32_t*>(zero_page+e820_p+16) = memmap[i].type;
        e820_p += 20;
    }
    uint8_t gdt[32] = {
        0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0,
        0xff, 0xff, 0, 0, 0, 0x9a, 0xcf, 0,
        0xff, 0xff, 0, 0, 0, 0x92, 0xcf, 0,
    };
    size_t payload_size = 0x1000 + sizeof(gdt) + strlen(cmdline) + 1;
    uint64_t payload_addr = 0x100000;
    for(const memory_range& i : memmap)
        if(i.end <= 0x100000 && i.end - i.start >= payload_size && i.type == 1)
        {
            uint64_t try_addr = i.end;
            if(try_addr >= 0x9a000)
                try_addr = 0x9a000;
            try_addr -= payload_size;
            try_addr &= -4096ull;
            if(try_addr < i.start)
                continue;
            payload_addr = try_addr;
            break;
        }
    if(payload_addr == 0x100000)
    {
        logging::log << kernel << ": not enough low memory for the zero page" << std::endl;
        return false;
    }
    logging::log << "Loading zero page at 0x" << std::hex << payload_addr << std::dec << std::endl;
    *reinterpret_cast<uint64_t*>(zero_page+0x228) = payload_addr + 0x1000 + sizeof(gdt);
    {
        char* dos_mem = (char*)mmap(0, 1048576, PROT_READ|PROT_WRITE|PROT_NOEXEC, 0);
        if(dos_mem == MAP_FAILED)
        {
            logging::log << "Failed to map DOS memory!" << std::endl;
            return false;
        }
        memcpy(dos_mem+payload_addr, zero_page, 0x1000);
        memcpy(dos_mem+payload_addr+0x1000, gdt, sizeof(gdt));
        memcpy(dos_mem+payload_addr+0x1000+sizeof(gdt), cmdline, strlen(cmdline)+1);
        munmap(dos_mem, 1048576);
    }
    //enter protected mode
    vcpu->set_reg(vm::Register::CR0, vcpu->get_reg(vm::Register::CR0)|1);
    //load gdt
    vm::GDTEntry gdtr = {0x1f, 0, 0, 0, 0, 0, 0, 0};
    vcpu->set_segment(vm::Register::GDT, 0, gdtr);
    //load idt
    vm::GDTEntry idtr = {0, 0, 0, 0, 0, 0, 0, 0};
    vcpu->set_segment(vm::Register::IDT, 0, idtr);
    //load cs
    vm::GDTEntry cs = {0};
    memcpy(cs.data, gdt+0x10, 8);
    cs.data[5] |= 1;
    vcpu->set_segment(vm::Register::CS, 0x10, cs);
    //load es, ss, ds, fs, gs
    vm::GDTEntry ds = {0};
    memcpy(ds.data, gdt+0x18, 8);
    ds.data[5] |= 1;
    vcpu->set_segment(vm::Register::ES, 0x18, ds);
    vcpu->set_segment(vm::Register::SS, 0x18, ds);
    vcpu->set_segment(vm::Register::DS, 0x18, ds);
    vcpu->set_segment(vm::Register::FS, 0x18, ds);
    vcpu->set_segment(vm::Register::GS, 0x18, ds);
    //cli
    vcpu->set_reg(vm::Register::RFLAGS, vcpu->get_reg(vm::Register::RFLAGS) & 0xfffffdff);
    //set arguments and rip
    vcpu->set_reg(vm::Register::RSI, payload_addr);
    vcpu->set_reg(vm::Register::RBP, 0);
    vcpu->set_reg(vm::Register::RDI, 0);
    vcpu->set_reg(vm::Register::RBX, 0);
    vcpu->set_reg(vm::Register::RIP, 0x100000);
    return true;
}

}
