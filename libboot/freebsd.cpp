#include <vector>
#include <stream>
#include <utility>
#include <liblog/logging.hpp>
#include "freebsd.hpp"
extern "C"
{
    #include <types.h>
}

enum
{
    MODINFO_END = 0,
    MODINFO_NAME = 1,
    MODINFO_TYPE = 2,
    MODINFO_METADATA = 0x8000,
};

enum
{
    MODINFOMD_SMAP = 0x1001,
};

namespace boot
{

static void append1(std::vector<uint32_t>& vec, uint32_t type, const void* data, uint32_t len)
{
    vec.push_back(type);
    vec.push_back(len);
    uint32_t n_elems = (len + 7) / 8 * 2;
    size_t sz = vec.size();
    for(int i = 0; i < n_elems; i++)
        vec.push_back(0);
    memcpy(&vec[sz], data, len);
}

static std::vector<uint32_t> make_modulep(const char* kernel_path, const std::vector<memory_range>& e820_map)
{
    std::vector<uint32_t> ans;
    append1(ans, MODINFO_NAME, kernel_path, strlen(kernel_path)+1);
    append1(ans, MODINFO_TYPE, "elf kernel", 11);
    ans.push_back(MODINFO_METADATA|MODINFOMD_SMAP);
    ans.push_back(20 * e820_map.size());
    for(const memory_range& i : e820_map)
    {
        uint64_t start = i.start;
        uint64_t size = i.end - i.start;
        ans.push_back(start);
        ans.push_back(start >> 32);
        ans.push_back(size);
        ans.push_back(size >> 32);
        ans.push_back(i.type);
    }
    if(e820_map.size() % 2)
        ans.push_back(0);
    append1(ans, 0, nullptr, 0);
    return ans;
}

bool prepare_args_freebsd(vm::VM* vcpu, const char* kernel_path, const std::vector<memory_range>& e820_map, const std::vector<memory_range>& e820_map_after_kernel, uint64_t kernend)
{
    std::vector<uint32_t> modulep_vec = make_modulep(kernel_path, e820_map);
    size_t size = modulep_vec.size() * 4 + 20;
    //abi pointers are 32-bit, need to allocate memory in lower 4gb
    uint64_t addr = -1;
    for(const memory_range& i : e820_map_after_kernel)
    {
        if(i.type != 1)
            continue;
        uint64_t start = ((i.start - 1) | 3) + 1;
        uint64_t end = std::min((uint64_t)0x100000000, i.end);
        if(end >= start && end - start >= size)
        {
            addr = start;
            if(addr >= 0x100000)
                break;
        }
    }
    if(addr == (uint64_t)-1)
    {
        logging::log << "Failed to allocate memory for modulep" << std::endl;
        return false;
    }
    kernend = std::max(kernend, addr + size);
    kernend = ((kernend - 1) | 4095) + 1;
    uint64_t map_addr = addr & -4096;
    char* mapping = (char*)mmap(0, (addr+size)-map_addr, PROT_READ|PROT_WRITE|PROT_NOEXEC, map_addr);
    uint32_t stack[3] = {0, (uint32_t)(addr+20), (uint32_t)kernend};
    memcpy(mapping+(addr-map_addr)+8, stack, sizeof(stack));
    memcpy(mapping+(addr-map_addr)+8+sizeof(stack), &modulep_vec[0], size-8-sizeof(stack));
    vcpu->set_reg(vm::Register::RSP, addr+8);
    return true;
}

}
