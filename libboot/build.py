out = build_library('libboot', ['libvm', 'liblog', 'libkbd', 'libsmp'])

def extra_deps(cmdline):
    cmdline = cmdline.split()
    if cmdline and cmdline[0] == 'debug':
        del cmdline[0]
    if cmdline and cmdline[0] == 'skip_sanity':
        del cmdline[0]
    if not cmdline:
        return [], []
    elif len(cmdline) >= 2 and cmdline[0] == 'linux32':
        if len(cmdline) >= 3 and cmdline[2].startswith('initrd='):
            return [], [cmdline[1], cmdline[2][7:]]
        else:
            return [], [cmdline[1]]
    elif len(cmdline) >= 2 and cmdline[0] in ('elf64', 'freebsd'):
        return [], [cmdline[1]]
    else:
        return [], []
