#include "hook.hpp"
#include <mutex>
#include <list>
#include <unordered_map>
#include <pair>

template<>
struct std::hash<std::pair<vm::ExitReason, uintptr_t> >
{
    uint64_t operator()(const std::pair<vm::ExitReason, uintptr_t>& p)
    {
        return std::hash<int>()((int)p.first) * 1000000007 + std::hash<uintptr_t>()(p.second);
    }
};

namespace hooks
{

static std::mutex hook_lock;

static std::list<Hook> dummy;

class Hook
{
public:
    std::list<Hook>* container = nullptr;
    std::list<Hook>::iterator it = dummy.end();
    bool(*handle)(void*);
    void(*cleanup)(void*);
    void* opaque;
    uint64_t refcount = 1;
};

static std::unordered_map<std::pair<vm::ExitReason, uintptr_t>, std::list<Hook> > all_hooks;

constexpr uintptr_t VCPU_INIT_HOOK = 0xdead000000000001ull;

Hook* createHook(vm::ExitReason why, uintptr_t pc_filter, bool(*handle)(void*), void(*cleanup)(void*), void* opaque)
{
    hook_lock.lock();
    Hook h;
    h.handle = handle;
    h.cleanup = cleanup;
    h.opaque = opaque;
    std::list<Hook>& ls = all_hooks[std::make_pair(why, pc_filter)];
    ls.push_back(h);
    auto it = ls.end();
    --it;
    it->container = &ls;
    it->it = it;
    Hook* ans = &*it;
    hook_lock.unlock();
    return ans;
}

Hook* createVCPUInitHook(bool(*handle)(void*), void(*cleanup)(void*), void* opaque)
{
    return createHook(vm::ExitReason::HYPERCALL, VCPU_INIT_HOOK, handle, cleanup, opaque);
}

static void doRemoveHook(Hook* h)
{
    h->cleanup(h->opaque); //XXX: cleanup should run unlocked
    h->container->erase(h->it);
}

void removeHook(Hook* h)
{
    hook_lock.lock();
    if(!--h->refcount)
        doRemoveHook(h);
    hook_lock.unlock();
}

static bool tryRip(vm::VM* vm, vm::ExitReason er, uintptr_t rip)
{
    auto it = all_hooks.find(std::make_pair(er, rip));
    if(it == all_hooks.end())
        return false;
    std::list<Hook>& ls = it->second;
    for(auto it = ls.begin(); it != ls.end();)
    {
        Hook& h = *it;
        ++h.refcount;
        hook_lock.unlock();
        if(h.handle(h.opaque))
            return true;
        hook_lock.lock();
        if(!--h.refcount)
            doRemoveHook(&*it++);
        else
            ++it;
    }
    return false;
}

bool handleUnknownExit(vm::VM* vm, vm::ExitReason er)
{
    hook_lock.lock();
    uintptr_t rip = vm->get_reg(vm::Register::RIP);
    bool ans = tryRip(vm, er, rip) || tryRip(vm, er, ANY_PC);
    hook_lock.unlock();
    return ans;
}

void handleVCPUInit(vm::VM* vm)
{
    hook_lock.lock();
    tryRip(vm, vm::ExitReason::HYPERCALL, VCPU_INIT_HOOK);
    hook_lock.unlock();
}

}
