#include "patch.hpp"
#include <utility>
#include <cstring>
#include <list>
#include <vector>
#include <unordered_map>
#include <mutex>
#include <libvm/vm.hpp>
#include <libboot/vm.hpp>
#include <libboot/memrw.hpp>
extern "C"
{
    #include <mm.h>
    #include <sysregs.h>
}

namespace hooks
{

static std::mutex patch_lock;

struct PatchPart;

static std::list<PatchPart*> dummy;

struct PatchPage;

struct PatchPart
{
    Patch* base;
    PatchPage* page;
    std::list<PatchPart*>::iterator iter = dummy.end();
    size_t offset;
    size_t size;
    const char* old;
    const char* patched;
    bool apply(const char* src, char* tgt)
    {
        if(memcmp(src+offset, old, size))
            return false;
        memcpy(tgt+offset, patched, size);
        return true;
    }
};

struct PatchPage
{
    uint64_t addr = 0;
    uint64_t mapping = 0;
    const char* page_r = nullptr;
    char* page_x = nullptr;
    std::list<PatchPart*> patches;
    bool dirty = true;
    int mt;
    PatchPage(){}
    bool init(uint64_t addr0)
    {
        addr = addr0;
        vm::VM* vm = boot::curVCPU();
        if(!page_r)
        {
            bool r, w, x;
            mapping = vm->translate(addr, r, w, x, mt);
            if(!r || !w || !x)
                return false;
            page_r = (const char*)mmap(0, 4096, PROT_READ|PROT_NOEXEC, mapping);
            if(page_r == MAP_FAILED)
            {
                page_r = nullptr;
                return false;
            }
            page_x = (char*)mmap(0, 4096, PROT_READ|PROT_WRITE|PROT_NOEXEC, MAP_ANON);
            if(page_x == MAP_FAILED)
            {
                munmap((void*)page_r, 4096);
                page_r = nullptr;
                return false;
            }
            vm->unmap_pages(addr, addr+4096);
            if(!vm->map_pages(addr, addr+4096, mapping, true, true, false, mt))
            {
                vm->unmap_pages(addr, addr+4096);
                vm->map_pages(addr, addr+4096, mapping, true, true, true, mt); //best-effort
                munmap(page_x, 4096);
                munmap((void*)page_r, 4096);
                page_r = nullptr;
                return false;
            }
        }
        return true;
    }
    void add_patch(PatchPart* pp)
    {
        patches.push_back(pp);
        auto it = patches.end();
        --it;
        pp->iter = it;
        oncrash(vm::CrashType::WRITE);
    }
    void remove_patch(PatchPart* pp)
    {
        patches.erase(pp->iter);
        oncrash(vm::CrashType::WRITE);
    }
    void oncrash(vm::CrashType which)
    {
        vm::VM* vm = boot::curVCPU();
        if(which == vm::CrashType::READ)
        {
            //thrash points, credit: MoRE
            //TODO: partial implementation for real mode only
            uint64_t cr0 = vm->get_reg(vm::Register::CR0);
            if(!(cr0 & 1))
            {
                //XXX: fantasy mode??
                uint16_t cs = vm->get_reg(vm::Register::CS);
                uint16_t ip = vm->get_reg(vm::Register::RIP);
                uint32_t code_addr = ((uint32_t)cs << 4) + ip;
                if((code_addr & PG_ADDR_MASK_4KB) == addr)
                {
                    //need local remap to avoid affecting other cpus
                    //execute directly from data page, code within patches would never do this
                    void* map1;
                    void* map2;
                    vm->temp_remap_lock(addr, map1, addr, map2, mapping, mapping, true, false, true);
                    vm->execute(true);
                    vm->temp_remap_unlock();
                    return;
                }
            }
            vm->fast_remap(addr, mapping, true, false, false, mt);
        }
        else if(which == vm::CrashType::WRITE)
        {
            vm->unmap_pages(addr, addr+4096);
            vm->map_pages(addr, addr+4096, mapping, true, true, false, mt);
            dirty = true;
        }
        else if(which == vm::CrashType::EXECUTE)
        {
            if(dirty)
            {
                memcpy(page_x, page_r, 4096);
                for(auto it = patches.begin(); it != patches.end();)
                {
                    if(!(*it)->apply(page_r, page_x))
                        remove_patch(*it++);
                    else
                        ++it;
                }
                dirty = false;
            }
            vm->fast_remap(addr, phys_addr_4kb(page_x), false, false, true, mt);
            asm volatile("wbinvd":::"memory");
        }
    }
    ~PatchPage()
    {
        if(page_r)
        {
            vm::VM* vm = boot::curVCPU();
            vm->unmap_pages(addr, addr+4096);
            vm->map_pages(addr, addr+4096, mapping, true, true, true, mt);
            munmap(page_x, 4096);
            munmap((void*)page_r, 4096);
        }
    }
};

static std::unordered_map<uint64_t, PatchPage> patched_pages;

class Patch
{
    std::vector<PatchPart> parts;
    bool valid = true;
    char* needs_free = nullptr;
public:
    bool init(uint64_t addr, const char* old, const char* patch, size_t sz)
    {
        if(!old)
        {
            old = needs_free = new char[sz];
            if(!needs_free || !utils::copy_from_guest(needs_free, addr, sz))
                return false;
        }
        size_t npages = 0;
        uint64_t end = addr + sz;
        uint64_t cur = addr;
        while(cur < end)
        {
            npages++;
            cur = (cur | 0xfff) + 1;
        }
        cur = addr;
        parts.resize(npages);
        for(size_t i = 0; cur < end; i++)
        {
            uint64_t limit = std::min(end, (cur | 0xfff) + 1);
            parts[i].base = this;
            parts[i].page = &patched_pages[cur & PG_ADDR_MASK_4KB];
            if(!parts[i].page->init(cur & PG_ADDR_MASK_4KB))
            {
                for(size_t j = 0; j <= i; j++)
                    if(parts[j].page->patches.empty())
                        patched_pages.erase(parts[j].page->addr);
                parts.resize(0);
                return false;
            }
            parts[i].offset = cur & ~PG_ADDR_MASK_4KB;
            parts[i].size = limit - cur;
            parts[i].old = old + (cur - addr);
            parts[i].patched = patch + (cur - addr);
            cur = limit;
        }
        for(size_t i = 0; i < npages; i++)
            parts[i].page->add_patch(&parts[i]);
        return true;
    }
    void invalidate()
    {
        valid = false;
        for(PatchPart& i : parts)
        {
            i.page->remove_patch(&i);
            if(i.page->patches.empty())
                patched_pages.erase(i.page->addr);
        }
        parts.resize(0);
        if(needs_free)
        {
            delete needs_free;
            needs_free = nullptr;
        }
    }
    bool isValid() { return valid; }
    const std::vector<PatchPart>& getParts() { return parts; }
};

Patch* createPatch(uint64_t base, const char* old, const char* patch, size_t size)
{
    patch_lock.lock();
    Patch* ans = new Patch;
    if(!ans->init(base, old, patch, size))
    {
        delete ans;
        ans = nullptr;
    }
    patch_lock.unlock();
    return ans;
}

void deletePatch(Patch* p)
{
    patch_lock.lock();
    p->invalidate();
    delete p;
    patch_lock.unlock();
}

bool isPatchValid(Patch* p)
{
    patch_lock.lock();
    bool ans = p->isValid();
    patch_lock.unlock();
    return ans;
}

bool stepOut(Patch* p, uintptr_t pc_low, uintptr_t pc_high)
{
    patch_lock.lock();
    const std::vector<PatchPart>& parts = p->getParts();
    if(parts.size() > 2)
    {
        patch_lock.unlock();
        return false;
    }
    uint64_t phys1 = parts.front().page->addr;
    uint64_t phys2 = parts.back().page->addr;
    void* map1;
    void* map2;
    vm::VM* vm = boot::curVCPU();
    vm->temp_remap_lock(phys1, map1, phys2, map2);
    char* map1c = (char*)map1;
    char* map2c = (char*)map2;
    if(memcmp(map1c+parts.front().offset, parts.front().patched, parts.front().size))
    {
        munmap(map1, 4096);
        if(map1 != map2)
            munmap(map2, 4096);
        vm->temp_remap_unlock();
        patch_lock.unlock();
        return false;
    }
    if(parts.size() == 2 && memcmp(map2c+parts.back().offset, parts.back().patched, parts.back().size))
    {
        munmap(map1, 4096);
        if(map1 != map2)
            munmap(map2, 4096);
        vm->temp_remap_unlock();
        patch_lock.unlock();
        return false;
    }
    memcpy(map1c+parts.front().offset, parts.front().old, parts.front().size);
    if(parts.size() == 2)
        memcpy(map2c+parts.back().offset, parts.back().old, parts.back().size);
    asm volatile("wbinvd":::"memory");
    while(true)
    {
        uintptr_t pc = vm->get_reg(vm::Register::RIP);
        if(pc < pc_low || pc >= pc_high)
            break;
        vm::ExitReason ex = vm->execute(true);
        if(ex != vm::ExitReason::SINGLESTEP)
        {
            munmap(map1, 4096);
            if(map1 != map2)
                munmap(map2, 4096);
            vm->temp_remap_unlock();
            patch_lock.unlock();
            return false;
        }
    }
    munmap(map1, 4096);
    if(map1 != map2)
        munmap(map2, 4096);
    vm->temp_remap_unlock();
    patch_lock.unlock();
    return true;
}

bool handlePatchPageFault(uint64_t page, vm::CrashType why)
{
    patch_lock.lock();
    auto it = patched_pages.find(page);
    if(it != patched_pages.end())
    {
        it->second.oncrash(why);
        patch_lock.unlock();
        return true;
    }
    patch_lock.unlock();
    return false;
}

}
