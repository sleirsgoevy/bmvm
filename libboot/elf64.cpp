#include <utility>
#include <cstring>
#include <vector>
#include <libvm/vm.hpp>
#include <liblog/logging.hpp>
#include "elf64.hpp"
extern "C"
{
    #include <elf_struct.h>
    #include <paging.h>
    #include <sysregs.h>
}

namespace boot
{

static void remove_region(std::vector<memory_range>& dst, const std::vector<memory_range>& src, uint64_t start, uint64_t end)
{
    for(const memory_range& i : src)
    {
        if(start >= i.start && start < i.end)
        {
            memory_range j;
            j.start = i.start;
            j.end = start;
            j.type = i.type;
            if(start != i.start)
                dst.push_back(j);
            if(end < i.end)
            {
                j.start = end;
                j.end = i.end;
                dst.push_back(j);
            }
        }
        else if(end > i.start && end < i.end)
        {
            memory_range j;
            j.start = end;
            j.end = i.end;
            j.type = i.type;
            dst.push_back(j);
        }
        else if(start >= i.end || end <= i.start)
            dst.push_back(i);
    }
}

static bool load_segment(const std::vector<memory_range>& memmap, std::vector<memory_range>& new_memmap, FILE* f, uint64_t v_start, uint64_t v_end, uint64_t offset, uint64_t filesz, uint64_t& kernend)
{
    if(v_end - v_start >= (1 << 30))
    {
        uint64_t shift = v_end - v_start - (1 << 30);
        v_start += shift;
        offset += shift;
        if(filesz >= shift)
            filesz -= shift;
        else
            filesz = 0;
    }
    if(v_start == v_end)
    {
        new_memmap = memmap;
        return true;
    }
    if((v_end >> 30) != (v_start >> 30) && (v_end & ((1 << 30) - 1)))
    {
        uint64_t split = (v_start | ((1 << 30) - 1)) + 1;
        std::vector<memory_range> tmp;
        return load_segment(memmap, tmp, f, v_start, split, offset, std::min(filesz, split-v_start), kernend)
            && load_segment(tmp, new_memmap, f, split, v_end, offset+split-v_start, (split-v_start >= filesz) ? 0 : (filesz - (split-v_start)), kernend);
    }
    v_start &= (1 << 30) - 1;
    v_end &= (1 << 30) - 1;
    bool ok = false;
    for(const memory_range& i : memmap)
        if(i.start <= v_start && i.end >= v_end && i.type == 1)
            ok = true;
    if(!ok)
    {
        logging::log << std::hex;
        logging::log << "Error: failed to load segment 0x" << v_start << "-0x" << v_end;
        logging::log << ": memory range not covered by RAM" << std::endl;
        logging::log << "Memory map:" << std::endl;
        for(const memory_range& i : memmap)
            logging::log << "* 0x" << i.start << "-0x" << i.end << ", type = 0x" << i.type << std::endl;
        logging::log << std::dec;
        return false;
    }
    uint64_t map_base = v_start - v_start % 4096;
    char* mapping = (char*)mmap(0, v_end-map_base, PROT_READ|PROT_WRITE|PROT_NOEXEC, map_base);
    if(!mapping)
    {
        logging::log << "Error: can not map memory for ELF64 segment" << std::endl;
        return false;
    }
    memset(mapping+(v_start-map_base), 0, v_end-v_start);
    if(filesz)
        pread(f, mapping+(v_start-map_base), filesz, offset);
    munmap(mapping, v_end-map_base);
    remove_region(new_memmap, memmap, v_start, v_end);
    kernend = std::max(kernend, v_end);
    return true;
}

bool load_elf64_kernel(vm::VM* vcpu, const std::vector<memory_range>& memmap, std::vector<memory_range>& actual_memmap, const char* kernel, uint64_t& kernend)
{
    logging::log << "Loading ELF64 kernel from " << kernel << std::endl;
    FILE* f = open(kernel, O_RDONLY);
    if(!f)
    {
        logging::log << kernel << ": not found" << std::endl;
        return false;
    }
    struct ehdr eh = {0};
    pread(f, &eh, sizeof(eh), 0);
    if(eh.e_ident[0] != 0x7f
    || eh.e_ident[1] != 'E'
    || eh.e_ident[2] != 'L'
    || eh.e_ident[3] != 'F'
    || eh.e_ident[4] != 2 //64-bit
    || eh.e_ident[5] != 1) //little-endian
    {
        logging::log << "Error: not a 64-bit little-endian ELF file" << std::endl;
        close(f);
        return false;
    }
    //prevent BDA from being overwritten
    remove_region(actual_memmap, memmap, 0, 0x1000);
    for(uint16_t idx = 0; idx < eh.e_phnum; idx++)
    {
        struct phdr ph = {0};
        pread(f, &ph, sizeof(ph), eh.e_phoff+idx*sizeof(ph));
        if(ph.p_type != PT_LOAD)
            continue;
        if(ph.p_filesz > ph.p_memsz)
        {
            logging::log << "Error: ELF segment's filesz is greater than memsz" << std::endl;
            close(f);
            return false;
        }
        uint64_t v_start = ph.p_vaddr;
        uint64_t v_end = ph.p_vaddr + ph.p_memsz;
        logging::log << "Note: PT_LOAD segment at 0x" << std::hex << v_start << "-0x" << v_end << std::dec << std::endl;
        if(v_end < v_start)
        {
            logging::log << "Error: ELF segment causes integer overflow" << std::endl;
            close(f);
            return false;
        }
        std::vector<memory_range> new_memmap;
        if(!load_segment(actual_memmap, new_memmap, f, v_start, v_end, ph.p_offset, ph.p_filesz, kernend))
        {
            close(f);
            return false;
        }
        std::swap(actual_memmap, new_memmap);
    }
    uint64_t pml_addr = -1;
    for(const memory_range& i : actual_memmap)
    {
        if(i.type != 1)
            continue;
        uint64_t start = ((i.start - 1) | 4095) + 1;
        uint64_t end = i.end & -4096;
        if(end - start >= 4096*3)
        {
            pml_addr = start;
            if(pml_addr >= 0x100000)
                break;
        }
    }
    if(pml_addr == (uint64_t)-1)
    {
        logging::log << "Error: failed to find space for pagetables" << std::endl;
        close(f);
        return false;
    }
    {
        std::vector<memory_range> new_memmap;
        remove_region(new_memmap, actual_memmap, pml_addr, pml_addr+4096*3);
        std::swap(actual_memmap, new_memmap);
        if(pml_addr + 4096 * 3 < 0x100000000)
            kernend = std::max(kernend, pml_addr+4096*3);
    }
    uint64_t* pmls = (uint64_t*)mmap(0, 4096*3, PROT_READ|PROT_WRITE|PROT_NOEXEC, pml_addr);
    //PML2
    for(int i = 0; i < 512; i++)
        pmls[i] = (i << 21) | PG_HUGE | PG_RW | PG_PRESENT;
    //PML3
    for(int i = 0; i < 512; i++)
        pmls[i+512] = pml_addr | PG_RW | PG_PRESENT;
    //PML4
    for(int i = 0; i < 512; i++)
        if(i != 257)
            pmls[i+1024] = (pml_addr + 4096) | PG_RW | PG_PRESENT;
    pmls[1281] = 0;
    munmap(pmls, 4096*3);
    //enter protected mode
    vcpu->set_reg(vm::Register::CR0, vcpu->get_reg(vm::Register::CR0) | 0x80000001);
    //enable PAE
    vcpu->set_reg(vm::Register::CR4, vcpu->get_reg(vm::Register::CR4) | 32);
    //set CR3
    vcpu->set_reg(vm::Register::CR3, pml_addr + 8192);
    //enable long mode
    vcpu->wrmsr(IA32_EFER, vcpu->rdmsr(IA32_EFER) | 0x500);
    //set segments
    vm::GDTEntry cs64, ds32;
    memset(cs64.data, 0, 16);
    memset(ds32.data, 0, 16);
    memcpy(cs64.data, "\xff\xff\x00\x00\x00\x9b\xaf\x00", 8);
    memcpy(ds32.data, "\xff\xff\x00\x00\x00\x93\xcf\x00", 8);
    vcpu->set_segment(vm::Register::CS, 8, cs64);
    vcpu->set_segment(vm::Register::DS, 16, ds32);
    vcpu->set_segment(vm::Register::ES, 16, ds32);
    vcpu->set_segment(vm::Register::SS, 16, ds32);
    vcpu->set_segment(vm::Register::FS, 16, ds32);
    vcpu->set_segment(vm::Register::GS, 16, ds32);
    //set RIP & RFLAGS
    vcpu->set_reg(vm::Register::RIP, eh.e_entry);
    vcpu->set_reg(vm::Register::RFLAGS, 2);
    //we're done
    close(f);
    return true;
}

}
