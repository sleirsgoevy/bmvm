#include <libvm/vm.hpp>
#include <libvm/ept.hpp>
#include <vector>
#include <cstring>
extern "C"
{
    #include <sysregs.h>
}

namespace vm_user
{

struct Registers
{
    uint64_t gprs[16];
    uint64_t error_code;
    uint64_t iret_frame[5];
    uint16_t segregs[4]; //es, ds, fs, gs
    uint64_t fs_base;
    uint64_t gs_base;
    uint64_t cr3_trampoline;
    uint64_t cr3_user;
    uint64_t cr3_host;
};
static_assert(sizeof(Registers) == 224);

extern "C" void trampoline_page(Registers* regs);
extern "C" void trampoline_pf_handler(void);

class PageTable : public vm::ept::EPT<PageTable>
{
    friend class vm::ept::EPT<PageTable>;
    static constexpr int HUGE = PG_HUGE;
    static constexpr int PROT = PG_USER | PG_PRESENT | PG_RW | PG_NX;
};

static inline uint64_t virt2phys(void* ptr)
{
    return ((uint64_t*)PML1_MOD_BASE)[((uint64_t)ptr) >> 12] & PG_ADDR_MASK_4KB;
}

struct EnsureTSS
{
    char tss[0x68];
    EnsureTSS()
    {
        struct
        {
            uint16_t size;
            void* ptr;
        } __attribute__((packed)) gdtr;
        asm volatile("sgdt %0":"=m"(gdtr)::"memory");
        uint64_t* gdt = reinterpret_cast<uint64_t*>(gdtr.ptr);
        uint64_t tss_ptr = (uint64_t)tss;
        gdt[4] = 0x68 | (tss_ptr & 0xffffff) << 16 | (tss_ptr & 0xff000000) << 32 | 0x0000890000000000ull;
        gdt[5] = tss_ptr >> 32;
        asm volatile("ltr %0"::"r"(32));
    }
    void ensure(){}
};

struct XSave
{
    std::vector<char> xsave_area;
    uintptr_t xsave_area_ptr;
    uint32_t xsave_eax, xsave_edx;
    bool saved = false;
    XSave()
    {
        uint32_t cpuid_ans[4];
        cpuid(13, cpuid_ans);
        xsave_area.resize(cpuid_ans[1]+63);
        xsave_area_ptr = ((uintptr_t)&xsave_area[0] + 63) & -64;
    }
    void save()
    {
        asm volatile("xgetbv":"=a"(xsave_eax),"=d"(xsave_edx):"c"(0));
        asm volatile("xsave (%2)"::"a"(xsave_eax),"d"(xsave_edx),"r"(xsave_area_ptr):"memory");
        saved = true;
    }
    void restore()
    {
        if(saved)
            asm volatile("xrstor (%2)"::"a"(xsave_eax),"d"(xsave_edx),"r"(xsave_area_ptr):"memory");
        else
        {
            asm volatile("mov %%cr0, %%rax\n\
                and $0xf3, %%al\n\
                mov %%rax, %%cr0\n\
                fninit\n\
                vxorps %%xmm0, %%xmm0\n\
                vxorps %%xmm1, %%xmm1\n\
                vxorps %%xmm2, %%xmm2\n\
                vxorps %%xmm3, %%xmm3\n\
                vxorps %%xmm4, %%xmm4\n\
                vxorps %%xmm5, %%xmm5\n\
                vxorps %%xmm6, %%xmm6\n\
                vxorps %%xmm7, %%xmm7\n\
                vxorps %%xmm8, %%xmm8\n\
                vxorps %%xmm9, %%xmm9\n\
                vxorps %%xmm10, %%xmm10\n\
                vxorps %%xmm11, %%xmm11\n\
                vxorps %%xmm12, %%xmm12\n\
                vxorps %%xmm13, %%xmm13\n\
                vxorps %%xmm14, %%xmm14\n\
                vxorps %%xmm15, %%xmm15\n\
                bts $2, %%rax\n\
                mov %%rax, %%cr0":::"rax");
        }
    }
};

class UserVM : public vm::VM
{
    Registers* regs;
    XSave outer_fpu, inner_fpu;
    PageTable* pagetable;
    PageTable trampoline_pagetable;
    uint64_t* gdt = (uint64_t*)MAP_FAILED;
    uint64_t* idt = (uint64_t*)MAP_FAILED;
    uint64_t* stack = (uint64_t*)MAP_FAILED;
public:
    UserVM(PageTable* pgt) : pagetable(pgt)
    {
        if(!pagetable)
            pagetable = new PageTable();
        ok = !!pagetable
          && !!*pagetable
          && !!trampoline_pagetable
          && (gdt = (uint64_t*)mmap(0, 65536, PROT_READ|PROT_WRITE, MAP_ANON)) != MAP_FAILED
          && (idt = (uint64_t*)mmap(0, 4096, PROT_READ|PROT_WRITE, MAP_ANON)) != MAP_FAILED
          && (stack = (uint64_t*)mmap(0, 4096, PROT_READ|PROT_WRITE, MAP_ANON)) != MAP_FAILED
          && trampoline_pagetable.map((uintptr_t)trampoline_page, (uintptr_t)trampoline_page + 4096, PG_PRESENT, 0, virt2phys((void*)trampoline_page))
          && trampoline_pagetable.map((1ull<<48)-(1ull<<12), (1ull<<48), PG_PRESENT, 0, virt2phys((void*)trampoline_page));
        if(ok && !pgt)
        {
            ok = pagetable->map((1ull<<48)-(1ull<<12), (1ull<<48), PG_PRESENT, 0, virt2phys((void*)trampoline_page))
              && pagetable->map((1ull<<48)-(2ull<<12), (1ull<<48)-(1ull<<12), PG_PRESENT | PG_RW, 0, virt2phys(stack))
              && pagetable->map((1ull<<48)-(3ull<<12), (1ull<<48)-(2ull<<12), PG_PRESENT, 0, virt2phys(idt));
            for(size_t i = 0; i < 16 && ok; i++)
                ok = pagetable->map((1ull<<48)+((i-19)<<12), (1ull<<48)+((i-18)<<12), PG_PRESENT | PG_RW, 0, virt2phys(gdt + (i<<9)));
        }
        if(ok)
        {
            memset(gdt, 0, 65536);
            memset(idt, 0, 4096);
            memset(stack, 0, 4096);
            for(size_t i = 0; i < 256; i++)
            {
                idt[2*i] = 0xfffe8e02fff8c000 + i;
                idt[2*i+1] = 0xffffffff;
            }
            idt[28] = 0xffff8e01fff8f000 + (uintptr_t)trampoline_pf_handler - (uintptr_t)trampoline_page;
            regs = (Registers*)stack;
            char* tss = (char*)(regs + 1);
            uint64_t ist1 = -8192 | ((uint64_t)(regs->iret_frame+5) & 4095);
            memcpy(tss+36, &ist1, 8);
            uint64_t ist2 = -4096;
            memcpy(tss+44, &ist2, 8);
            gdt[8189] = 0xff0089ffe0000068 | ((uint64_t)tss & 4095) << 16;
            gdt[8190] = 0xffffffff;
            gdt[8191] = 0x00af9a000000ffff;
        }
    }
    UserVM* createVCPU()
    {
        UserVM* ans = new UserVM(&pagetable->incref());
        if(!*ans)
        {
            delete ans;
            return nullptr;
        }
        return ans;
    }
    ~UserVM()
    {
        if(stack != MAP_FAILED)
            munmap(stack, 4096);
        if(idt != MAP_FAILED)
            munmap(idt, 4096);
        if(gdt != MAP_FAILED)
            munmap(gdt, 65536);
        pagetable->decref();
    }
    bool map_pages(uint64_t start, uint64_t end, uint64_t phys, bool r, bool w, bool x, int mem_type);
    uint64_t translate(uint64_t virt, bool& r, bool& w, bool& x, int& mem_type);
    bool fast_remap(uint64_t virt, uint64_t phys, bool r, bool w, bool x, int mem_type);
    void unmap_pages(uint64_t start, uint64_t end);
    bool temp_remap_lock(uint64_t addr1, void*& map1, uint64_t addr2, void*& map2, uint64_t phys1 = MAP_ANON, uint64_t phys2 = MAP_ANON, bool add_r = false, bool add_w = false, bool add_x = false);
    void temp_remap_unlock();
    uint64_t get_reg(vm::Register reg) const;
    void set_reg(vm::Register reg, uint64_t value);
    uint16_t get_segment(vm::Register which, vm::GDTEntry& data);
    void set_segment(vm::Register which, uint16_t selector, const vm::GDTEntry& data);
    vm::ExitReason execute(bool singlestep = false);
    uint64_t get_faulting_address() const;
    vm::CrashType get_fault_reason() const;
    void get_vmcall(char vmcall[3]) const;
    uint64_t rdmsr(uint32_t msr) const;
    void wrmsr(uint32_t msr, uint64_t value);
    void msr_protect(uint32_t msr, bool r, bool w);
    void intercept_exception(int vector, bool do_intercept);
    int get_exception(bool& has_error_code, uint32_t& error_code);
    void inject_exception(int vector);
    void inject_exception(int vector, uint32_t error_code);
};

vm::VM* createUserVM()
{
    static EnsureTSS tss;
    tss.ensure();
    vm::VM* ans = new UserVM(nullptr);
    if(ans && !*ans)
    {
        delete ans;
        return nullptr;
    }
    return ans;
}

bool UserVM::map_pages(uint64_t start, uint64_t end, uint64_t phys, bool r, bool w, bool x, int mem_type)
{
    int prot = 0;
    if(r)
        prot |= PG_PRESENT | PG_USER;
    if(w)
        prot |= PG_RW;
    if(!x)
        prot |= PG_NX;
    return pagetable->map(start, end, prot, 0, phys);
}

void UserVM::unmap_pages(uint64_t start, uint64_t end)
{
    return pagetable->unmap(start, end);
}

uint64_t UserVM::translate(uint64_t virt, bool& r, bool& w, bool& x, int& mem_type)
{
    int prot = 0;
    uint64_t ans = pagetable->translate(virt, prot, mem_type);
    r = prot & PG_PRESENT;
    w = prot & PG_RW;
    x = !(prot & PG_NX);
    return ans;
}

bool UserVM::fast_remap(uint64_t virt, uint64_t phys, bool r, bool w, bool x, int mem_type)
{
    int prot = 0;
    if(r)
        prot |= PG_PRESENT | PG_USER;
    if(w)
        prot |= PG_RW;
    if(!x)
        prot |= PG_NX;
    if(pagetable->fast_remap(virt, phys, prot, 0))
        return true;
    unmap_pages(virt, virt+4096);
    return map_pages(virt, virt+4096, phys, r, w, x, 0);
}

bool UserVM::temp_remap_lock(uint64_t addr1, void*& map1, uint64_t addr2, void*& map2, uint64_t phys1, uint64_t phys2, bool add_r, bool add_w, bool add_x)
{
    return false;
}

void UserVM::temp_remap_unlock()
{
    __builtin_trap();
}

uint64_t UserVM::get_reg(vm::Register reg) const
{
    switch(reg)
    {
    case vm::Register::RIP:
        return regs->iret_frame[0];
    case vm::Register::CS:
        return (uint16_t)regs->iret_frame[1];
    case vm::Register::RFLAGS:
        return regs->iret_frame[2];
    case vm::Register::RSP:
        return regs->iret_frame[3];
    case vm::Register::SS:
        return (uint16_t)regs->iret_frame[4];
    case vm::Register::ES:
        return regs->segregs[0];
    case vm::Register::DS:
        return regs->segregs[1];
    case vm::Register::FS:
        return regs->segregs[2];
    case vm::Register::GS:
        return regs->segregs[3];
    default:
        if((int)reg >= (int)vm::Register::RAX && (int)reg <= (int)vm::Register::R15)
            return regs->gprs[(int)reg];
        __builtin_trap();
    }
}

void UserVM::set_reg(vm::Register reg, uint64_t value)
{
    if((int)reg >= (int)vm::Register::ES && (int)reg <= (int)vm::Register::GS)
    {
        if((uint16_t)value >= 0xffe8)
            __builtin_trap();
        value &= -8;
        if((uint16_t)value)
            value |= 3;
    }
    switch(reg)
    {
    case vm::Register::RIP:
        regs->iret_frame[0] = value;
        break;
    case vm::Register::CS:
        regs->iret_frame[1] = (uint16_t)value;
        break;
    case vm::Register::RFLAGS:
        regs->iret_frame[2] = value;
        break;
    case vm::Register::RSP:
        regs->iret_frame[3] = value;
        break;
    case vm::Register::SS:
        regs->iret_frame[4] = (uint16_t)value;
        break;
    case vm::Register::ES:
        regs->segregs[0] = value;
        break;
    case vm::Register::DS:
        regs->segregs[1] = value;
        break;
    case vm::Register::FS:
        regs->segregs[2] = value;
        break;
    case vm::Register::GS:
        regs->segregs[3] = value;
        break;
    default:
        if((int)reg >= (int)vm::Register::RAX && (int)reg <= (int)vm::Register::R15)
            regs->gprs[(int)reg] = value;
        else
            __builtin_trap();
    }
}

uint16_t UserVM::get_segment(vm::Register which, vm::GDTEntry& data)
{
    memset(data.data, 0, 16);
    data.data[0] = data.data[1] = 0xff;
    data.data[5] = 0x93;
    data.data[6] = 0xcf;
    if(which == vm::Register::CS)
    {
        data.data[5] = 0x9b;
        data.data[6] = 0xaf;
    }
    else if(which == vm::Register::FS || which == vm::Register::GS)
    {
        char* base = (char*)&(which == vm::Register::FS ? regs->fs_base : regs->gs_base);
        memcpy(data.data+2, base, 3);
        memcpy(data.data+7, base+3, 5);
    }
    return get_reg(which);
}

void UserVM::set_segment(vm::Register which, uint16_t selector, const vm::GDTEntry& data)
{
    set_reg(which, selector);
}

vm::ExitReason UserVM::execute(bool singlestep)
{
    uint16_t cs = regs->iret_frame[1];
    uint16_t ss = regs->iret_frame[4];
    if(!cs || !ss || cs == ss)
        return vm::ExitReason::EXCEPTION;
    uint16_t data_segs[5];
    data_segs[0] = ss >> 3;
    for(size_t i = 0; i < 4; i++)
        data_segs[i+1] = regs->segregs[i] >> 3;
    for(size_t i = 0; i < 5; i++)
        if(data_segs[i])
            gdt[data_segs[i]] = 0x00cff2000000ffff;
    gdt[cs >> 3] = 0x00affa000000ffff;
    asm volatile("mov %%cr3, %0":"=r"(regs->cr3_host));
    regs->cr3_trampoline = trampoline_pagetable.phys();
    regs->cr3_user = pagetable->phys();
    trampoline_page(regs);
    for(size_t i = 0; i < 5; i++)
        if(data_segs[i])
            gdt[data_segs[i]] = 0;
    gdt[cs >> 3] = 0;
    cs = regs->iret_frame[1];
    if(!(cs & 3))
    {
        uint64_t rsp = ((uint64_t)stack & -4096) | (regs->iret_frame[3] & 4080);
        uint64_t inner_iret_frame[5];
        memcpy(inner_iret_frame, (void*)rsp, 40);
        if((inner_iret_frame[1] & 3))
            memcpy(regs->iret_frame, inner_iret_frame, 40);
        else
            __builtin_trap();
    }
    return vm::ExitReason::EXCEPTION;
}

uint64_t UserVM::get_faulting_address() const
{
    __builtin_trap();
}

vm::CrashType UserVM::get_fault_reason() const
{
    __builtin_trap();
}

void UserVM::get_vmcall(char vmcall[3]) const
{
    vmcall[0] = 0x0f;
    vmcall[1] = 0x05;
    vmcall[2] = 0x90;
}

uint64_t UserVM::rdmsr(uint32_t msr) const
{
    __builtin_trap();
}

void UserVM::wrmsr(uint32_t msr, uint64_t value)
{
    __builtin_trap();
}

void UserVM::msr_protect(uint32_t msr, bool r, bool w)
{
    __builtin_trap();
}

void UserVM::intercept_exception(int vector, bool do_intercept)
{
    __builtin_trap();
}

int UserVM::get_exception(bool& has_error_code, uint32_t& error_code)
{
    __builtin_trap();
}

void UserVM::inject_exception(int vector)
{
    __builtin_trap();
}

void UserVM::inject_exception(int vector, uint32_t error_code)
{
    __builtin_trap();
}

}
