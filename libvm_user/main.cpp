#include <libvm_user/vm.hpp>
#include <liblog/logging.hpp>
extern "C"
{
    #include <mm.h>
}

static inline uint64_t virt2phys(void* ptr)
{
    return ((uint64_t*)PML1_MOD_BASE)[((uint64_t)ptr) >> 12] & PG_ADDR_MASK_4KB;
}

int main(const char* cmdline)
{
    vm::VM* vm = vm_user::createUserVM();
    if(!vm)
    {
        logging::log << "Failed to create user VM!" << std::endl;
        return 1;
    }
    char* codepage = (char*)mmap(0, 4096, PROT_READ|PROT_WRITE, MAP_ANON);
    if(!codepage)
    {
        logging::log << "Failed to map a code page!" << std::endl;
        delete vm;
        return 1;
    }
    memcpy(codepage, "\x01\xc8\x0f\x05", 4); //add eax, ecx; syscall
    if(!vm->map_pages(4096, 8192, virt2phys(codepage), true, false, true, 0))
    {
        logging::log << "Failed to map memory into the VM!" << std::endl;
        delete vm;
        munmap(codepage, 4096);
        return 1;
    }
    vm->set_reg(vm::Register::RIP, 4096);
    vm->set_reg(vm::Register::CS, 8);
    vm->set_reg(vm::Register::RFLAGS, 2);
    vm->set_reg(vm::Register::RSP, 0xdeadbeefdeadbeef);
    vm->set_reg(vm::Register::SS, 16);
    vm->set_reg(vm::Register::RAX, 1234);
    vm->set_reg(vm::Register::RCX, 4321);
    logging::log << vm->get_reg(vm::Register::RAX) << " + " << vm->get_reg(vm::Register::RCX) << " = ";
    vm->execute(false);
    logging::log << vm->get_reg(vm::Register::RAX) << std::endl;
    delete vm;
    munmap(codepage, 4096);
    return 0;
}
