section .text
use64

global trampoline_page
global trampoline_pf_handler
global end_trampoline_page

align 4096
; void trampoline_page(vm_user::Registers*);
trampoline_page:
push rbp
mov rbp, rsp
push rbx
push r12
push r13
push r14
push r15
push rdi
cli ; just in case
mov [rdi+32], rsp
mov rax, [rdi+200]
mov rcx, [rdi+208]
mov cr3, rax
mov rax, .continue - end_trampoline_page
jmp rax

.continue:
mov cr3, rcx
mov rsp, -8192
sgdt [rsp+1024]
sidt [rsp+1034]
str ax
mov [rsp+1044], ax
mov [rsp+1046], cs
lgdt [rel gdtr]
lidt [rel idtr]
mov ax, 0xffe8
ltr ax
mov ecx, 0xc0000100 ; IA32_FS_BASE
rdmsr
mov [rsp+1048], eax
mov [rsp+1052], edx
inc ecx ; IA32_GS_BASE
rdmsr
mov [rsp+1056], eax
mov [rsp+1060], edx
mov eax, [rsp+192]
mov edx, [rsp+196]
wrmsr
dec ecx ; IA32_FS_BASE
mov eax, [rsp+184]
mov edx, [rsp+188]
wrmsr
pop rax
pop rcx
pop rdx
pop rbx
pop rbp
pop rbp
pop rsi
pop rdi
pop r8
pop r9
pop r10
pop r11
pop r12
pop r13
pop r14
pop r15
add rsp, 8
iretq

trampoline_pf_handler:
push r15
push r14
push r13
push r12
push r11
push r10
push r9
push r8
push rdi
push rsi
push rbp
sub rsp, 8
push rbx
push rdx
push rcx
push rax
mov rax, [rsp+200]
mov rcx, [rsp+216]
mov rsp, [rsp+32]
mov cr3, rax
jmp [rel .continue_ptr]

.continue:
mov cr3, rcx
xor eax, eax
mov ds, ax
mov es, ax
mov ss, ax
mov fs, ax
mov gs, ax
pop rdi
lgdt [rdi+1024]
lidt [rdi+1034]
mov rcx, [rdi+1026]
movzx eax, word [rdi+1044]
and byte [rcx+rax+5], 0xfd
ltr ax
mov ecx, 0xc0000100 ; IA32_FS_BASE
mov eax, [rdi+1048]
mov edx, [rdi+1052]
wrmsr
inc ecx ; IA32_GS_BASE
mov eax, [rdi+1056]
mov edx, [rdi+1060]
wrmsr
pop r15
pop r14
pop r13
pop r12
pop rbx
pop rbp
pop rcx
movzx eax, word [rdi+1046]
push rax
push rcx
retf

.continue_ptr:
dq .continue

gdtr:
dw 65535
dq -19*4096

idtr:
dw 4095
dq -3*4096

times (trampoline_page+4096)-$ db 0
end_trampoline_page:
